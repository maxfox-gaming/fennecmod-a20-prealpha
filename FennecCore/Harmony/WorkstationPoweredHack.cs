﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that extends functionality of the workstation.
     * Powered workstations halt crafting when unpowered.
     * Multiblock workstations halt crafting when not formed.
     * Multiblock powered workstations halt crafting when not formed or not receiving power.
     */

    [HarmonyPatch(typeof(TileEntityWorkstation))]
    [HarmonyPatch("UpdateTick")]
    public class WorkstationPoweredHack
    {
        public static bool Prefix(TileEntityWorkstation __instance, World world)
        {
            bool isRequiringPower = ((__instance as ITileEntityReceivePower) != null);
            bool isMultiblockMaster = ((__instance as ITileEntityMultiblockMaster) != null);

            if (!isRequiringPower && !isMultiblockMaster)
            {
                return true;
            }

            if (isMultiblockMaster && !(__instance as ITileEntityMultiblockMaster).MultiblockFormed())
            {
                Log.Warning("Multiblock Form Check");
                WorkstationPoweredHack.UpdateMaster((ITileEntityMultiblockMaster)__instance, world);
                Log.Warning("Multiblock Form Check End");
                return false;
            }

            if (isRequiringPower)
            {
                if (!(__instance as ITileEntityReceivePower).IsPowered(world))
                {
                    return false;
                }
                return true;
            }

            return false;
        }



        private static void UpdateMaster(ITileEntityMultiblockMaster master, World world)
        {
            TileEntityType type = ((TileEntity)master).GetTileEntityType();

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstationPowered] && master is TileEntityMultiblockMasterWorkstationPowered)
            {
                ((TileEntityMultiblockMasterWorkstationPowered)master).UpdateMaster(world);
                return;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstation] && master is TileEntityMultiblockMasterWorkstation)
            {
                ((TileEntityMultiblockMasterWorkstation)master).UpdateMaster(world);
                return;
            }
        }
    }
}