﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
	/**
	 * Harmony patch that allows the recipe info to show in the crafting info window.
	 */

	[HarmonyPatch(typeof(XUiC_CraftingInfoWindow))]
	[HarmonyPatch("GetBindingValue")]
	class XUiC_CraftingInfoWindowRecipeHack
	{
		public static void Postfix(Recipe ___recipe, ref string value, string bindingName)
		{
			if (bindingName == "itemname")
			{
				if (___recipe == null)
				{
					return;
				}

				// If the recipe is performed in a transformer block.
				if ((___recipe is RecipeTransformer))
				{
					value = "[99ffff]" + value + "[ffffff]";
				}

				// If the recipe contains byproduct info.
				if (___recipe is IRecipeByproduct)
				{
					value = "[ffff99]" + value + "[ffffff]";
				}

				// If the recipe only has a probability of returning the item/
				if (___recipe is IRecipeProbability)
				{
					double probValue = (___recipe as IRecipeProbability).ConvertToProbPerc();
					if (probValue < 100)
					{
						value += " [ff99ff](" + probValue.ToString() + "%)[ffffff]";
					}
				}

				// If the recipe has more than one item.
				if (___recipe.count > 1)
				{
					value += " [ffff00]x " + ___recipe.count.ToString() + "[ffffff]";
				}
			}
		}
	}
}