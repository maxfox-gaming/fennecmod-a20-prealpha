﻿using HarmonyLib;
using FennecCore.Scripts;
using System;
using System.Collections;
using System.Reflection;

namespace FennecCore.Harmony
{
	/**
	 * Harmony patch that adds recipes from transformer blocks to transformation list.
	 */

	[HarmonyPatch(typeof(WorldStaticData), "LoadRecipes", new Type[] {typeof(XmlFile)})]
	public class LoadRecipes
	{
		private static bool Prefix(XmlFile _xmlFile)
		{
			return false;
		}

		private static IEnumerator Postfix(IEnumerator __result, XmlFile _xmlFile)
		{
			yield return RecipesFromXmlFM.LoadRecipies(_xmlFile);
			MicroStopwatch msw = new MicroStopwatch(true);
			foreach (ItemClass itemClass in ItemClass.list)
			{
				bool flag = itemClass != null;
				if (flag)
				{
					itemClass.AutoCalcWeight();
					itemClass.AutoCalcEcoVal();
					bool flag2 = msw.ElapsedMilliseconds > 50L;
					if (flag2)
					{
						yield return null;
						msw.ResetAndRestart();
					}
				}
			}
			ItemClass[] array = null;
			yield break;
		}
	}


}