﻿using System.Collections.Generic;
using System;
using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * If there are managers around the tile entity, sets them to update so the new container will be registered in the manager.
     */

    [HarmonyPatch(typeof(TileEntitySecureLootContainerSigned))]
    [HarmonyPatch(MethodType.Constructor)]
    [HarmonyPatch(new Type[] { typeof(Chunk) })]
    public class TileEntitySecureLootContainerSignedConstructor
    {
        public static void Postfix(TileEntitySecureLootContainerSigned __instance)
        {
            List<TileEntityInventoryManager> managers = TileEntityHelper.GetNearbyInventoryManagersAroundSecureSignedContainer(__instance);
            foreach (TileEntityInventoryManager manager in managers)
            {
                manager.SetNeedsUpdate();
            }

        }
    }
}