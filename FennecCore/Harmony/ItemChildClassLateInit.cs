﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that allows the overriding of new ItemClass LateInit() method since it is not marked
     * as virtual, we have to use 'new' and then use this workaround, which is not good practice AT ALL - 
     * however we cannot modify the base assembly so this is the only way to extend vanilla classes to do
     * what we want.
     */

    [HarmonyPatch(typeof(ItemClass))]
    [HarmonyPatch("LateInit")]
    public class ItemChildClassLateInit
    {
        public static void Postfix(ItemClass __instance)
        {
            if (__instance is ItemClassFuelByproduct)
            {
                (__instance as ItemClassFuelByproduct).LateInit();
            }

            if (__instance is ItemClassInputByproduct)
            {
                (__instance as ItemClassInputByproduct).LateInit();
            }
        }
    }
}