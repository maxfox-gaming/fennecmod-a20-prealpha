﻿using System.Collections.Generic;
using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * If a secure container is removed from the world, then we need to set the manager nearby to update so that it forgets about it.
     */

    [HarmonyPatch(typeof(TileEntity))]
    [HarmonyPatch("OnDestroy")]
    public class TileEntitySecureLootContainerSignedOnDestroy
    {
        public static void Postfix(TileEntity __instance)
        {
            if (__instance is TileEntitySecureLootContainerSigned)
            {
                List<TileEntityInventoryManager> managers = TileEntityHelper.GetNearbyInventoryManagersAroundSecureSignedContainer(__instance as TileEntitySecureLootContainerSigned);
                foreach (TileEntityInventoryManager manager in managers)
                {
                    manager.UpdateManager(__instance.ToWorldPos(), TileEntityInventoryManager.UpdateState.Remove);
                }
            }
        }
    }
}