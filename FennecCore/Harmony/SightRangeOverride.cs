﻿using HarmonyLib;
using System.Collections.Generic;
using UnityEngine;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch to dynamically change sight range of zombies based on the
     * world's players' gamestages.
     * Default: 50% at stage 1, 150% at stage 100
     */

    [HarmonyPatch(typeof(EntityAlive))]
    [HarmonyPatch("CopyPropertiesFromEntityClass")]
    public class SightRangeOverride
    {
        // Sets the min and max levels for sight range adjustment.
        // Any level below the minStage will have the minLevel applied.
        // Any level above the maxStage will have the maxLevel applied.
        private static int minStage = 1;
        private static int maxStage = 100;
        private static float minLevel = 0.5f;
        private static float maxLevel = 1.5f;

        private static Dictionary<int, EntitySightRangeData> sightData = new Dictionary<int, EntitySightRangeData>();


        public static void Postfix(EntityAlive __instance)
        {
            if (EntityClass.list.Count == 0)
            {
                return;
            }

            CreateSightData(EntityClass.list);
            if (sightData.Count == 0)
            {
                return;
            }

            EntityClass entityClass = EntityClass.list[__instance.entityClass];
            EntitySightRangeData data = sightData[__instance.entityClass];

            if (entityClass == null)
            {
                return;
            }

            if (!entityClass.entityClassName.ToLower().Contains("zombie"))
            {
                return;
            }

            float threshold = (float)CalculateGameStage() / maxStage;
            float multiplier = Mathf.Min(minLevel + threshold, maxLevel);
            entityClass.SightRange = data.sightRange * multiplier;
        }

        /**
         * Returns the 
         */

        private static int CalculateGameStage()
        {
            World world = GameManager.Instance.World;
            if (world == null)
            {
                return minStage;
            }

            List<EntityPlayer> players = world.GetPlayers();
            if (players.Count == 0)
            {
                return minStage;
            }

            List<int> playerStages = new List<int>();
            foreach (EntityPlayer player in players)
            {
                playerStages.Add(player.gameStage);
            }

            return Mathf.Clamp(GameStageDefinition.CalcPartyLevel(playerStages), minStage, maxStage);
        }



        private static void CreateSightData(DictionarySave<int, EntityClass> list)
        {
            if (sightData.Count > 0)
            {
                return;
            }

            foreach (KeyValuePair<int, EntityClass> entry in list.Dict)
            {
                sightData.Add(entry.Key, new EntitySightRangeData(entry.Key, entry.Value.SightRange));
            }
        }
    }
}