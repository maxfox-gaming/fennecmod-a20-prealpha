﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that sets the controller script on a wind turbine item so it rotates when turned on.
     */

    [HarmonyPatch(typeof(TileEntityPowerSource))]
    [HarmonyPatch("UpdateTick")]
    public class TileEntityPowerSourceExtender
    {
        public static void Postfix(TileEntityPowerSource __instance)
        {
            if (GameManager.Instance.World == null)
            {
                return;
            }

            if (__instance.PowerItemType != PowerItemMapping.Types[PowerItemMapping.WindTurbine])
            {
                return;
            }
                    
            BlockWindTurbine block = GameManager.Instance.World.GetBlock(__instance.ToWorldPos()).Block as BlockWindTurbine;
            if (block == null)
            {
                return;
            }

            Chunk chunk = GameManager.Instance.World.GetChunkFromWorldPos(__instance.ToWorldPos()) as Chunk;
            if (chunk == null)
            {
                return;
            }

            BlockEntityData ebcd = chunk.GetBlockEntity(__instance.ToWorldPos());
            if (ebcd == null)
            {
                return;
            }

            WindTurbineController controller = block.AddOrGetController(ebcd);
            if (controller != null)
            {
                if (__instance.IsOn)
                {
                    controller.SetOn();
                }
                else
                {
                    controller.SetOff();
                }
            }
        }
    }
}