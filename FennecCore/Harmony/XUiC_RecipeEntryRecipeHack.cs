﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
	/**
	 * Harmony patch that allows the recipe info to show in the crafting info window, differing by colour.
	 */

	[HarmonyPatch(typeof(XUiC_RecipeEntry))]
	[HarmonyPatch("GetBindingValue")]
	class XUiC_RecipeEntryRecipeHack
	{
		public static void Postfix(Recipe ___recipe, ref string value, string bindingName)
		{
			if (bindingName == "recipename")
			{
				if (___recipe == null)
				{
					return;
				}

				// If a recipe is used in a transformer or multiblock transformer.
				if ((___recipe is RecipeTransformer))
				{
					value = "[99ffff]" + value + "[ffffff]";
				}

				// If the recipe has a byproduct, or produces something in the output slot.
				if (___recipe is IRecipeByproduct)
				{
					value = "[ffff99]" + value + "[ffffff]";
				}

				// If the recipe has only a certain probability of producing an item.
				if (___recipe is IRecipeProbability)
				{
					double probValue = (___recipe as IRecipeProbability).ConvertToProbPerc();
					if (probValue < 100)
					{
						value += " [ff99ff](" + probValue.ToString() + "%)[ffffff]";
					}
				}

				// If the recipe gives more than 1 crafted item.
				if (___recipe.count > 1)
				{
					value += " [ffff00]x " + ___recipe.count.ToString() + "[ffffff]";
				}
			}
		}
	}
}