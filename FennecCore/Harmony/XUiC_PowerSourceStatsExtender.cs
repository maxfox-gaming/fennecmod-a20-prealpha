﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{

    /**
     * Harmony patch that allows the XUiC to read icons for the new power sources.
     */

    [HarmonyPatch(typeof(XUiC_PowerSourceStats))]
    [HarmonyPatch("GetBindingValue")]
    public class XUiC_PowerSourceStatsExtender
    {
        public static void Postfix(ref bool __result, TileEntityPowerSource ___tileEntity, PowerSource ___powerSource, CachedStringFormatter<ushort> ___maxoutputFormatter, ref string value, string bindingName)
        {
            // Checks to see if the tile entity power item type is a wind or water tuebine, and  gives it the appropriate icon.
            if (bindingName == "powersourceicon")
            {
                if (___tileEntity != null)
                {
                    if (___tileEntity.PowerItemType == PowerItemMapping.Types[PowerItemMapping.WindTurbine])
                    {
                        value = "ui_game_symbol_windturbine";
                        return;
                    }

                    if (___tileEntity.PowerItemType == PowerItemMapping.Types[PowerItemMapping.WaterTurbine])
                    {
                        value = "ui_game_symbol_waterturbine";
                        return;
                    }
                }
            }

            // This will allow the wind turbine to read out the current max power it is allowed to have instead of the global maximum.
            if (bindingName == "maxoutput")
            {
                if (___tileEntity != null)
                {
                    if (___tileEntity.PowerItemType == PowerItemMapping.Types[PowerItemMapping.WindTurbine] && ___powerSource is PowerWindTurbine)
                    {
                        PowerWindTurbine wind = (___powerSource as PowerWindTurbine);
                        value = ___maxoutputFormatter.Format(wind.CalculateOverrideMax());
                    }
                }
            }
        }
    }
}