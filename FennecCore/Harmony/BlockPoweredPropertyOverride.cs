﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
	[HarmonyPatch(typeof(BlockPowered))]
	[HarmonyPatch("Init")]
	class BlockPoweredPropertyOverride
	{
		/**
		 * Harmony patch that allows the block property MultiblockType to be usable with basic power.
		 */

		public static void Postfix(BlockPowered __instance)
		{
			if (__instance.Properties.Values.ContainsKey("MultiblockType") && __instance.Properties.Values["MultiblockType"] != "")
			{
				MultiblockManager.AddBlockType(__instance.Properties.Values["MultiblockType"]);
			}
		}
	}
}
