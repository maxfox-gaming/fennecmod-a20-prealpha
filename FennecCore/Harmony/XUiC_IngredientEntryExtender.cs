﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Allows ingredients to show in different colours based on whether they are a fuel, or need to be placed in a block.
     */

    [HarmonyPatch(typeof(XUiC_IngredientEntry))]
    [HarmonyPatch("GetBindingValue")]
    public class XUiC_IngredientEntryExtender
    {
        public static void Postfix(XUiC_IngredientEntry __instance, ref string value, string bindingName)
        {
            if (bindingName == "itemname")
            {
                // If the ingredient needs to be placed in the fuel slot.
                if (__instance.Ingredient is ItemStackFuel)
                {
                    value = "[ff9999]" + value + " (" + Localization.Get("lblInFuelSlot") + ")[ffffff]";
                    return;
                }

                // If the ingredient needs to be placed in the output slot.
                if (__instance.Ingredient is ItemStackRequirement)
                {
                    value = "[ffff00]" + value + " (" + Localization.Get("lblInOutputSlot") + ")[ffffff]";
                    return;
                }

                // If the ingredient needs to be put in the input slot for a forge.
                if (__instance.Ingredient is ItemStackInput)
                {
                    value = "[44ffff]" + value + " (" + Localization.Get("lblInInputSlot") + ")[ffffff]";
                    return;
                }
            }
        }
    }
}