﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that extends functionality of the workstation XUIC.
     * - Powered workstations will halt their crafting when not powered.
     * - Multiblock workstations will halt their crafting when the structure is not formed properly.
     * - Multiblock powered workstations will halt their crafting when the structure is not formed or it is not powered.
     */

    [HarmonyPatch(typeof(XUiC_WorkstationWindowGroup))]
    [HarmonyPatch("Update")]
    public class XUiC_WorkstationWindowGroupExtension
    {
        public static void Postfix(XUiC_WorkstationWindowGroup __instance, ref XUiC_CraftingQueue ___craftingQueue)
        {
            TileEntityWorkstation workstation = __instance.WorkstationData.TileEntity;
            bool flag1, flag2, flag3;

            World world = GameManager.Instance.World;
            if (world == null)
            {
                return;
            }

            // If the tile entity is just a powered workstation, check that it is receiving power.
            if ((workstation.GetTileEntityType() == TileEntityMapping.Types[TileEntityMapping.WorkstationPowered]) && ___craftingQueue != null && __instance.WorkstationData != null)
            {
                flag1 = (workstation as ITileEntityReceivePower).IsPowered(world);
                flag2 = ___craftingQueue.IsCrafting();

                if (!flag1 && flag2)
                {
                    ___craftingQueue.HaltCrafting();
                }
                else if (flag1 && !flag2)
                {
                    ___craftingQueue.ResumeCrafting();
                }
            }

            // If the tile entity is a multiblock workstation, check that the multiblock is formed.
            else if ((workstation.GetTileEntityType() == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstation]) && ___craftingQueue != null && __instance.WorkstationData != null)
            {
                flag1 = (workstation as TileEntityMultiblockMasterWorkstation).MultiblockFormed();
                flag2 = ___craftingQueue.IsCrafting();

                if (!flag1 && flag2)
                {
                    ___craftingQueue.HaltCrafting();
                }
                else if (flag1 && !flag2)
                {
                    ___craftingQueue.ResumeCrafting();
                }
            }

            // If the tile entity is a multiblock powered workstation, check that the multiblock is formed and getting power.
            else if ((workstation.GetTileEntityType() == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstationPowered]) && ___craftingQueue != null && __instance.WorkstationData != null)
            {
                flag1 = (workstation as TileEntityMultiblockMasterWorkstationPowered).MultiblockFormed();
                flag2 = (workstation as TileEntityMultiblockMasterWorkstationPowered).IsPowered(world);
                flag3 = ___craftingQueue.IsCrafting();

                if ((!flag1 || !flag2) && flag3)
                {
                    ___craftingQueue.HaltCrafting();
                }
                else if (flag1 && flag2 && !flag3)
                {
                    ___craftingQueue.ResumeCrafting();
                }
            }
        }
    }
}