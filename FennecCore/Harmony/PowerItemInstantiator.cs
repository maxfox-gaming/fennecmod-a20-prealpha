using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that allows the reading of more than the default power ite, types. 
     * To add more power item types, register extra in PowerItemMapping
     */

    [HarmonyPatch(typeof(PowerItem))]
    [HarmonyPatch("CreateItem")]
    public class PowerItemInstantiator
    {
        public static bool Prefix(PowerItem.PowerItemTypes itemType, ref PowerItem __result)
        {
            if (itemType == PowerItemMapping.Types[PowerItemMapping.WindTurbine])
            {
                __result = new PowerWindTurbine();
                return false;
            }

            if (itemType == PowerItemMapping.Types[PowerItemMapping.WaterTurbine])
            {
                __result = new PowerWaterTurbine();
                return false;
            }

            return true;
        }

    }
}