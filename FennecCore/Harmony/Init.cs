﻿using System.Reflection;

namespace FennecCore.Harmony
{
    /**
     * FennecCore Initializer for the mod scripts and HarmonyX integration.
     */

    public class Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            var harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}