﻿using HarmonyLib;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that allows the reading of more than the default tile entity types. 
     * If more non-vanilla ones are needed, add new entries and strings to TileEntityMapping.
     */

    [HarmonyPatch(typeof(TileEntity))]
    [HarmonyPatch("Instantiate")]
    public class TileEntityInstantiator
    {
        public static bool Prefix(TileEntityType type, Chunk _chunk, ref TileEntity __result)
        {
            if (type == TileEntityMapping.Types[TileEntityMapping.BlockTransformer])
            {
                __result = new TileEntityTransformer(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.WorkstationImproved])
            {
                __result = new TileEntityWorkstationImproved(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.WorkstationPowered])
            {
                __result = new TileEntityWorkstationPowered(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.InventoryManager]) // NOT YET IMPLEMENTED
            {
                __result = new TileEntityInventoryManager(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBSlave])
            {
                __result = new TileEntityMultiblockSlave(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBSlaveLoot])
            {
                __result = new TileEntityMultiblockSlaveLootContainer(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBSlavePowered])
            {
                __result = new TileEntityMultiblockSlavePowered(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstr])
            {
                __result = new TileEntityMultiblockMaster(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstrTransformer])
            {
                __result = new TileEntityMultiblockMasterTransformer(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstation])
            {
                __result = new TileEntityMultiblockMasterWorkstation(_chunk);
                return false;
            }

            if (type == TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstationPowered])
            {
                __result = new TileEntityMultiblockMasterWorkstationPowered(_chunk);
                return false;
            }

            return true;
        }
    }
}