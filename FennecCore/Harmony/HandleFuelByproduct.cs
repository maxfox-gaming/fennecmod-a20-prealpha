﻿using HarmonyLib;
using FennecCore.Scripts;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Reflection;
//using System.Reflection.Emit;
//using UnityEngine;

namespace FennecCore.Harmony
{

	/**
	 * Inserts call to output for handling fuel byproducts. This will add a call to our static class FuelByproductManager and
	 * invoke the method HandleByproductFor(ItemValue fuelItem, ItemStack[] stackArray). We use the state to check whether the
	 * fuel has changed during method runtime to determine whether to call the method which is far easier than injecting a Harmony
	 * Transpiler method.
	 */

	[HarmonyPatch(typeof(TileEntityWorkstation))]
	[HarmonyPatch("HandleFuel")]
	public class HandleFuelByproduct
	{
		public static void Prefix(TileEntityWorkstation __instance, out ItemStack __state)
		{
			__state = __instance.Fuel[0].Clone();
		}

		public static void Postfix(TileEntityWorkstation __instance, ItemStack __state)
		{
			if (__instance.IsBurning && __instance.Fuel[0].count == __state.count - 1)
			{
				__instance.Output = ByproductManager.HandleFuelByproductFor(__state.itemValue, __instance.Output);
			}
		}
	}
}