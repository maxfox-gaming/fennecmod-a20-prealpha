﻿using HarmonyLib;
using System;
using FennecCore.Scripts;

namespace FennecCore.Harmony
{
    /**
     * Harmony patch that returns the clone of an extended itemstack (since the 'virtual' keyword
     * is not used for ItemStack.Clone() so we have to use a 'new' and then do an override in this
     * way, which is not good practice AT ALL).
     */

    [HarmonyPatch(typeof(ItemStack))]
    [HarmonyPatch("Clone")]
    [HarmonyPatch(new Type[] { })]
    public class ItemStackClonePatch
    {
        public static bool Prefix(ItemStack __instance, ref ItemStack __result)
        {
            if (__instance is ItemStackFuel)
            {
                __result = (__instance as ItemStackFuel).Clone();
                return false;
            }

            if (__instance is ItemStackRequirement)
            {
                __result = (__instance as ItemStackRequirement).Clone();
                return false;
            }

            if (__instance is ItemStackInput)
            {
                __result = (__instance as ItemStackInput).Clone();
                return false;
            }

            return true;
        }
    }
}