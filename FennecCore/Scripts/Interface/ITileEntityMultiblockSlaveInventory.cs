﻿namespace FennecCore.Scripts
{
    public interface ITileEntityMultiblockSlaveInventory : ITileEntityMultiblockSlave
    {
        /**
         * Item stack for the loot container
         */

        ItemStack[] items
        {
            get;
            set;
        }


        /**
         * Sets the inventory size.
         */

        void SetContainerSize(Vector2i _containerSize, bool clearItems = true);


        /**
         * Empties the container.
         */

        void SetEmpty();
    }
}