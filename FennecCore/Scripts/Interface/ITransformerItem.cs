﻿namespace FennecCore.Scripts
{
    public interface ITransformerItem
    {
        string Write();
    }
}