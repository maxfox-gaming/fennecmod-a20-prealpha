﻿namespace FennecCore.Scripts
{
    public interface ITileEntityReceivePower
    {
        /**
         * Checks if the tile entity is receiving power.
         */

        bool IsPowered(World world);
    }
}