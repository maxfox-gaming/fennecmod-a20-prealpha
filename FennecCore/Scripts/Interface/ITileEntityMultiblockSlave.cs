﻿namespace FennecCore.Scripts
{
    public interface ITileEntityMultiblockSlave
    {
        /**
         * Detects whether the slave block has a master block or not.
         */

        bool HasMaster
        {
            get;
        }


        /**
         * Sets the type of the multiblock slave. Useful for referring to slaves by type rather than block names so substitutes can be made.
         */

        void SetSlaveType(string type);


        /**
         * Returns the type of the multiblock slave. 
         */

        string GetSlaveType();


        /**
         * Sets the master block.
         */

        void SetMaster(ITileEntityMultiblockMaster master);


        /**
         * Removes the master block binding.
         */

        void UnbindMaster();


        /**
         * Gets the master block.
         */

        ITileEntityMultiblockMaster GetMaster();


        /**
         * Gets the master position
         */

        Vector3i GetMasterPos();


        /**
         * Returns the world position in worldspace coordinates.
         */

        Vector3i ToWorldPos();


        /**
         * Returns if the main multiblock is formed.
         */

        bool Formed();
    }
}