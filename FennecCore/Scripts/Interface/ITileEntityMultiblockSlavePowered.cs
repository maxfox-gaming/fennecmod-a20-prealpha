﻿using UnityEngine;
using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public interface ITileEntityMultiblockSlavePowered : ITileEntityMultiblockSlave
    {
        bool IsPowered { get; }


        /**
         * Returns the world position of the parent block that is sending power to this one.
         */
        Vector3i GetParent();


        /**
         * Returns whether this block has any other block powering it.
         */

        bool HasParent();


        /**
         * Returns the power item associated with this block.
         */

        PowerItem GetPowerItem();


        /**
         * Checks for new wires to draw.
         */

        void CheckForNewWires();


        /**
         * Draws wires from this source to its parent.
         */

        void DrawWires();


        /**
         * Adds wire data to connect to its parent.
         */

        void AddWireData(Vector3i child);


        /**
         * Sends wire data to the server.
         */

        void SendWireData();


        /**
         * Creates wire data from the associated power item.
         */

        void CreateWireDataFromPowerItem();


        /**
         * Deletes wires.
         */

        void RemoveWires();


        /**
         * Marks wires as dirty, i.e. needing a change.
         */

        void MarkWireDirty();


        /**
         * Marks tile entity to update.
         */

        void MarkChanged();


        /**
         * Initialises power data when world loads or when tile entity is created.
         */

        void InitializePowerData();


        /**
         * Creates a power item for the tile entity
         */

        PowerItem CreatePowerItemForTileEntity(ushort blockID);


        /**
         * Gets the wire offset to draw correct positioning.
         */

        Vector3 GetWireOffset();


        /**
         * Gets the required power for the wires.
         */

        int GetRequiredPower();


        /**
         * When wire is connected, sets the parent block.
         */

        void SetParentWithWireTool(IPowered newParentTE, int wiringEntityID);


        /**
         * Removes the parent when wire tool is used.
         */

        void RemoveParentWithWiringTool(int wiringEntityID);


        /**
         * Adjusts wire data.
         */

        void SetWireData(List<Vector3i> wireChildren);
    }
}