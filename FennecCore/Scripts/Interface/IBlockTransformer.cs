﻿namespace FennecCore.Scripts
{
    public interface IBlockTransformer
    {
        string GetBlockName();
    }
}