﻿using System.Collections.Generic;
using System.Xml;

namespace FennecCore.Scripts
{
	/**
	 * Checks whether a nearby container has an item.
	 * <requirement name="NearbyContainerHas, Mods" item_name="item" count="1" range="1,3,1" />
	 */

	public class NearbyContainerHas : TargetedCompareRequirementBase
	{
		/**
		 * Checks whether the weather is raining at night.
		 */
		public override bool IsValid(MinEventParams _params)
		{
			if (!this.ParamsValid(_params))
			{
				return false;
			}
			if (this.target == null)
			{
				return false;
			}
			World world;
			if ((world = GameManager.Instance.World) == null)
			{
				return false;
			}

			ItemValue item;
			if ((item = ItemClass.GetItem(this.itemName)) == null)
			{
				return false;
			}

			Vector3i targetPos = target.GetBlockPosition() + new Vector3i(0, this.yOffset, 0);
			List<Vector3i> coordinates = CoordinateHelper.GetCoordinatesAround(targetPos, this.range);
			Dictionary<Vector3i, TileEntityLootContainer> positions = CoordinateHelper.GetTileEntitiesInCoordinatesWithType<TileEntityLootContainer>(world, coordinates);
			foreach (KeyValuePair<Vector3i, TileEntityLootContainer> entry in positions)
			{
				ItemStack[] inventory = entry.Value.items;
				for (int i = 0; i < inventory.Length; i += 1)
				{
					if (inventory[i].IsEmpty())
					{
						continue;
					}

					if (inventory[i].itemValue.ItemClass.Equals(item.ItemClass) && inventory[i].count >= this.count)
					{
						return true;
					}
				}
			}
			return false;
		}


		/**
		 * Reads the XML properties.
		 */

		public override bool ParseXmlAttribute(XmlAttribute _attribute)
		{
			bool flag = base.ParseXmlAttribute(_attribute);
			if (!flag)
			{
				string name = _attribute.Name;

				if (name == "item_name")
				{
					this.itemName = _attribute.Value;
					return true;
				}

				if (name == "count")
				{
					if (!int.TryParse(_attribute.Value, out this.count))
					{
						this.count = 1;
					}
					return true;
				}

				if (name == "range")
				{
					this.range = StringHelpers.WriteStringToNonNegativeVector3Range(_attribute.Value);
					return true;
				}

				if (name == "y_offset")
				{
					if (!int.TryParse(_attribute.Value, out yOffset))
					{
						this.yOffset = 0;
					}
					return true;
				}

			}
			return flag;
		}

		private string itemName;
		private int count = 1;
		private Vector3i range = Vector3i.one;
		private int yOffset = 0;
	}
}