﻿using System.Collections.Generic;
using System.Xml;
using UnityEngine;

namespace FennecCore.Scripts
{
	/**
	 * Checks whether a nearby container has an item.
	 * <requirement name="ThisEntityIs, Mods" entity="entity1,entity2,..." />
	 */

	public class ThisEntityIs : TargetedCompareRequirementBase
	{
		/**
		 * Checks whether there are entities nearby the target.
		 */

		public override bool IsValid(MinEventParams _params)
		{
			if (!this.ParamsValid(_params))
			{
				return false;
			}
			if (this.target == null)
			{
				return false;
			}

			return this.entityNames.Contains(target.EntityClass.entityClassName);
		}


		/**
		 * Reads the XML properties.
		 */

		public override bool ParseXmlAttribute(XmlAttribute _attribute)
		{
			bool flag = base.ParseXmlAttribute(_attribute);
			if (!flag)
			{
				string name = _attribute.Name;

				if (name == "entity")
				{
					this.entityNames = StringHelpers.WriteStringToList(_attribute.Value);
					return true;
				}
			}
			return flag;
		}

		private List<string> entityNames;
	}
}