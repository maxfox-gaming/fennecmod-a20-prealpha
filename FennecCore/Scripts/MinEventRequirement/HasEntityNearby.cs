﻿using System.Collections.Generic;
using System.Xml;

namespace FennecCore.Scripts
{
	/**
	 * Checks whether a nearby container has an item.
	 * <requirement name="HasNearbyEntity, Mods" entity="entity1,entity2,..." count="1" range="1,3,1" />
	 */

	public class HasNearbyEntity : TargetedCompareRequirementBase
	{
		/**
		 * Checks whether there are entities nearby the target.
		 */

		public override bool IsValid(MinEventParams _params)
		{
			if (!this.ParamsValid(_params))
			{
				return false;
			}
			if (this.target == null)
			{
				return false;
			}
			World world;
			if ((world = GameManager.Instance.World) == null)
			{
				return false;
			}

			Vector3i pos = this.target.GetBlockPosition();

			List<Entity> entitiesNearby = CoordinateHelper.GetEntitiesInBounds(world, pos, range, entityNames);
			return (entitiesNearby.Count > count);
		}


		/**
		 * Reads the XML properties.
		 */

		public override bool ParseXmlAttribute(XmlAttribute _attribute)
		{
			bool flag = base.ParseXmlAttribute(_attribute);
			if (!flag)
			{
				string name = _attribute.Name;

				if (name == "entity")
				{
					this.entityNames = StringHelpers.WriteStringToList(_attribute.Value);
					return true;
				}

				if (name == "count")
				{
					if (!int.TryParse(_attribute.Value, out this.count))
					{
						this.count = 1;
					}
					return true;
				}

				if (name == "range")
				{
					this.range = StringHelpers.WriteStringToNonNegativeVector3Range(_attribute.Value);
					return true;
				}

			}
			return flag;
		}

		private List<string> entityNames;
		private int count = 1;
		private Vector3i range = Vector3i.one;
	}
}