﻿namespace FennecCore.Scripts
{
    public class IsClear : TargetedCompareRequirementBase
    {
        /**
         * Checks whether the weather is clear.
         */
        public override bool IsValid(MinEventParams _params)
        {
            if (!this.ParamsValid(_params))
            {
                return false;
            }
            if (this.target == null)
            {
                return false;
            }
            if (WeatherManager.Instance == null)
            {
                return false;
            }
            return WeatherManager.Instance.GetCurrentCloudThicknessPercent() < 0.4f;
        }
    }
}