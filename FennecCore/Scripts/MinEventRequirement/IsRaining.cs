﻿namespace FennecCore.Scripts
{
    public class IsRaining : TargetedCompareRequirementBase
    {
        /**
         * Checks whether the weather is raining at night.
         */
        public override bool IsValid(MinEventParams _params)
        {
            if (!this.ParamsValid(_params))
            {
                return false;
            }
            if (this.target == null)
            {
                return false;
            }
            if (WeatherManager.Instance == null)
            {
                return false;
            }
            return WeatherManager.Instance.GetCurrentRainfallValue() > 0.2f;
        }
    }
}