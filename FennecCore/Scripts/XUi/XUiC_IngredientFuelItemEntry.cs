﻿using UnityEngine;

namespace FennecCore.Scripts
{
	public class XUiC_IngredientFuelItemEntry : XUiC_IngredientEntry
	{
		public override bool GetBindingValue(ref string value, string bindingName)
		{
			bool flag = this.ingredient != null;
			if (bindingName == "itemname")
			{
				string text = "";
				if (flag && this.materialBased)
				{
					if (Localization.Exists("lbl" + this.material))
					{
						text = Localization.Get("lbl" + this.material);
					}
					else
					{
						text = XUi.UppercaseFirst(this.material);
					}
				}
				value = "[ff9999]" + (flag ? (this.materialBased ? text : Localization.Get(this.ingredient.itemValue.ItemClass.GetItemName())) : "") + "(" + Localization.Get("lblInFuelSlot") + ")[ffffff]";
				return true;
			}
			if (bindingName == "itemicon")
			{
				value = (flag ? this.ingredient.itemValue.ItemClass.GetIconName() : "");
				return true;
			}
			if (bindingName == "itemicontint")
			{
				Color32 v = Color.white;
				if (flag)
				{
					ItemClass itemClass = this.ingredient.itemValue.ItemClass;
					if (itemClass != null)
					{
						v = itemClass.GetIconTint(this.ingredient.itemValue);
					}
				}
				value = this.itemicontintcolorFormatter.Format(v);
				return true;
			}
			if (bindingName == "havecount")
			{
				XUiC_WorkstationMaterialInputGrid childByType = this.windowGroup.Controller.GetChildByType<XUiC_WorkstationMaterialInputGrid>();
				if (childByType != null)
				{
					if (this.materialBased)
					{
						value = (flag ? this.havecountFormatter.Format(childByType.GetWeight(this.material)) : "");
					}
					else
					{
						value = (flag ? this.havecountFormatter.Format(base.xui.PlayerInventory.GetItemCount(this.ingredient.itemValue)) : "");
					}
				}
				else
				{
					XUiC_WorkstationInputGrid childByType2 = this.windowGroup.Controller.GetChildByType<XUiC_WorkstationInputGrid>();
					if (childByType2 != null)
					{
						value = (flag ? this.havecountFormatter.Format(childByType2.GetItemCount(this.ingredient.itemValue)) : "");
					}
					else
					{
						value = (flag ? this.havecountFormatter.Format(base.xui.PlayerInventory.GetItemCount(this.ingredient.itemValue)) : "");
					}
				}
				return true;
			}
			if (bindingName == "needcount")
			{
				value = (flag ? this.needcountFormatter.Format(this.ingredient.count * this.craftCountControl.Count) : "");
				return true;
			}
			if (!(bindingName == "haveneedcount"))
			{
				return false;
			}
			string str = flag ? this.needcountFormatter.Format(this.ingredient.count * this.craftCountControl.Count) : "";
			XUiC_WorkstationMaterialInputGrid childByType3 = this.windowGroup.Controller.GetChildByType<XUiC_WorkstationMaterialInputGrid>();
			if (childByType3 != null)
			{
				if (this.materialBased)
				{
					value = (flag ? (this.havecountFormatter.Format(childByType3.GetWeight(this.material)) + "/" + str) : "");
				}
				else
				{
					value = (flag ? (this.havecountFormatter.Format(base.xui.PlayerInventory.GetItemCount(this.ingredient.itemValue)) + "/" + str) : "");
				}
			}
			else
			{
				XUiC_WorkstationInputGrid childByType4 = this.windowGroup.Controller.GetChildByType<XUiC_WorkstationInputGrid>();
				if (childByType4 != null)
				{
					value = (flag ? (this.havecountFormatter.Format(childByType4.GetItemCount(this.ingredient.itemValue)) + "/" + str) : "");
				}
				else
				{
					value = (flag ? (this.havecountFormatter.Format(base.xui.PlayerInventory.GetItemCount(this.ingredient.itemValue)) + "/" + str) : "");
				}
			}
			return true;
		}


		// Token: 0x04003029 RID: 12329
		private ItemStack ingredient;

		// Token: 0x0400302A RID: 12330
		private bool isDirty;

		// Token: 0x0400302B RID: 12331
		private bool materialBased;

		// Token: 0x0400302C RID: 12332
		private string material = "";

		// Token: 0x0400302D RID: 12333
		private XUiV_Sprite icoItem;

		// Token: 0x0400302E RID: 12334
		private XUiV_Label lblName;

		// Token: 0x0400302F RID: 12335
		private XUiV_Label lblHaveCount;

		// Token: 0x04003030 RID: 12336
		private XUiV_Label lblNeedCount;

		// Token: 0x04003031 RID: 12337
		private XUiC_RecipeCraftCount craftCountControl;

		// Token: 0x04003033 RID: 12339
		private readonly CachedStringFormatterXuiRgbaColor itemicontintcolorFormatter = new CachedStringFormatterXuiRgbaColor();

		// Token: 0x04003034 RID: 12340
		private readonly CachedStringFormatter<int> havecountFormatter = new CachedStringFormatter<int>((int _i) => _i.ToString());

		// Token: 0x04003035 RID: 12341
		private readonly CachedStringFormatter<int> needcountFormatter = new CachedStringFormatter<int>((int _i) => _i.ToString());
	}
}