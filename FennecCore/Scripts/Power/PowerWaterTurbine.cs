using System;
using System.IO;
using Audio;
using UnityEngine;

namespace FennecCore.Scripts
{
	public class PowerWaterTurbine : PowerSource
	{
		/**
		 * Returns the power item types, as a solar panel.
		 */

		public override PowerItem.PowerItemTypes PowerItemType
		{
			get
			{
				return PowerItemMapping.Types[PowerItemMapping.WaterTurbine];
			}
		}


		/**
		 * Plays this sound when turned on.
		 */

		public override string OnSound
		{
			get
			{
				return "solarpanel_on";
			}
		}


		/**
		 * Plays this sound when it turns off.
		 */

		public override string OffSound
		{
			get
			{
				return "solarpanel_off";
			}
		}


		/**
		 * Gets the current power output and applies wind speed calculations to it.
		 */

		protected override void TickPowerGeneration()
		{
			try
			{
				this.CurrentPower = this.MaxOutput;
			}
			catch (FormatException)
			{
				this.CurrentPower = 0;
			}
			catch (OverflowException)
			{
				this.CurrentPower = 0;
			}
		}


		/**
		 * Handles sending power to outside sources.
		 */

		public override void HandleSendPower()
		{
			if (base.IsOn)
			{
				if (this.CurrentPower < this.MaxPower)
				{
					this.TickPowerGeneration();
				}
				else if (this.CurrentPower > this.MaxPower)
				{
					this.CurrentPower = this.MaxPower;
				}

				if (this.hasChangesLocal)
				{
					this.LastPowerUsed = 0;
					ushort num = (ushort)Mathf.Min((int)this.MaxOutput, (int)this.CurrentPower);
					ushort num2 = num;
					World world = GameManager.Instance.World;
					for (int i = 0; i < this.Children.Count; i++)
					{
						num = num2;
						this.Children[i].HandlePowerReceived(ref num2);
						this.LastPowerUsed += (ushort)(num - num2);
					}
				}

				if (this.LastPowerUsed >= this.CurrentPower)
				{
					base.SendHasLocalChangesToRoot();
					this.CurrentPower = 0;
					return;
				}
				this.CurrentPower -= this.LastPowerUsed;
			}
		}


		protected new void RefreshPowerStats()
		{
			this.SlotCount = 0;
			this.MaxOutput = 0;
			for (int i = 0; i < this.Stacks.Length; i++)
			{
				if (!this.Stacks[i].IsEmpty())
				{
					this.MaxOutput += (ushort)((float)this.OutputPerStack * Mathf.Lerp(0.5f, 1f, (float)this.Stacks[i].itemValue.Quality / 6f));
					this.SlotCount += 1;
				}
			}
			// Take wind account
			if (this.MaxOutput > 0)
			{
				float windSpeed = WeatherManager.GetWindSpeed();
				float heightFactor = Mathf.Lerp(0.5f, 1.5f, Mathf.Min(Mathf.Max((this.TileEntity.ToWorldPos().y - 60), 0), 120) / 120);
				float maxOutput = this.MaxOutput * windSpeed * heightFactor;
				this.MaxOutput = Convert.ToUInt16(maxOutput);
			}

			if (this.BlockID == 0 && this.TileEntity != null)
			{
				this.BlockID = (ushort)GameManager.Instance.World.GetBlock(this.TileEntity.ToWorldPos()).type;
				this.SetValuesFromBlock();
			}
			if (this.MaxPower == 0)
			{
				this.MaxPower = this.MaxOutput;
			}
			if (this.RequiredPower == 0)
			{
				this.RequiredPower = this.MaxOutput;
			}
		}


		/**
		 * Whether the tile entity should clear out the power or not.
		 */

		protected bool ShouldClearPower()
		{
			return false;
		}


		/**
		 * Handles the sounds of turning on and off.
		 */

		protected override void HandleOnOffSound()
		{
			Vector3 position = this.Position.ToVector3();
			Manager.BroadcastPlay(position, this.isOn ? this.OnSound : this.OffSound, 0f);
			if (this.isOn)
			{
				Manager.BroadcastPlay(position, this.runningSound, 0f);
				return;
			}
			Manager.BroadcastStop(position, this.runningSound);
		}


		/**
		 * Reads data on loading.
		 */

		public override void read(BinaryReader _br, byte _version)
		{
			base.read(_br, _version);
		}


		/**
		 * Writes data on saving.
		 */

		public override void write(BinaryWriter _bw)
		{
			base.write(_bw);
		}


		private string runningSound = "solarpanel_idle";
	}
}