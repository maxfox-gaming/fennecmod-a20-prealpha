﻿public class NetPackageRemoveEntityReturnItem : NetPackage
{
	public NetPackageRemoveEntityReturnItem Setup(int _entityToDespawn, int _entityThatRetrieved, ItemStack _stackToReceive)
	{
		Log.Out("[FENNEC] Recived modded netpackage: RemoveEntityReturnItem");
		this.entityToDespawn = _entityToDespawn;
		this.entityThatRetrieved = _entityThatRetrieved;
		this.stackToReceive = _stackToReceive;
		return this;
	}


	/**
	 * Reads data in from a net package.
	 */

	public override void read(PooledBinaryReader _reader)
	{
		this.entityToDespawn = _reader.ReadInt32();
		this.entityThatRetrieved = _reader.ReadInt32();
		this.stackToReceive = new ItemStack();
		this.stackToReceive.Read(_reader);
	}


	/**
	 * Writes data to a net package.
	 */

	public override void write(PooledBinaryWriter _writer)
	{
		base.write(_writer);
		_writer.Write(this.entityToDespawn);
		_writer.Write(this.entityThatRetrieved);
		this.stackToReceive.Write(_writer);
	}


	/**
	 * Process the package data.
	 */

	public override void ProcessPackage(World _world, GameManager _callbacks)
	{
		if (_world == null)
		{
			return;
		}

		EntityPlayer entity = GameManager.Instance.World.GetEntity(this.entityThatRetrieved) as EntityPlayer;
		if (entity == null)
		{
			Log.Out("Entity is not a player.");
			return;
		}

		//entity.inventory.AddItem(this.stackToReceive);
		_world.RemoveEntity(entityToDespawn, EnumRemoveEntityReason.Killed);
	}


	/**
	 * Gets package length.
	 */

	public override int GetLength()
	{
		return 20;
	}


	public int entityToDespawn;
	public int entityThatRetrieved;
	public ItemStack stackToReceive;
}