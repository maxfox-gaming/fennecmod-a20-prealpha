﻿using System.Collections.Generic;
using UnityEngine;
using FennecCore.Scripts;

public class NetPackageEntityPlacedSpawn : NetPackage
{
	public NetPackageEntityPlacedSpawn Setup(int _entityType, Vector3 _pos, Vector3 _rot, ItemValue _itemValue, int _entityThatPlaced = -1, List<string> _buffs = null)
	{
		Log.Out("[FENNEC] Received modded NetPackage: EntityPlacedSpawn");
		this.entityType = _entityType;
		this.pos = _pos;
		this.rot = _rot;
		this.itemValue = _itemValue;
		this.entityThatPlaced = _entityThatPlaced;
		this.buffs = (_buffs != null ? _buffs : new List<string>());
		return this;
	}


	/**
	 * Reads data in from a net package.
	 */

	public override void read(PooledBinaryReader _reader)
	{
		this.entityType = _reader.ReadInt32();
		this.pos = StreamUtils.ReadVector3(_reader);
		this.rot = StreamUtils.ReadVector3(_reader);
		this.itemValue = new ItemValue();
		this.itemValue.Read(_reader);
		this.entityThatPlaced = _reader.ReadInt32();
		this.buffs = StringHelpers.WriteStringToList(_reader.ReadString());
	}


	/**
	 * Writes data to a net package.
	 */

	public override void write(PooledBinaryWriter _writer)
	{
		base.write(_writer);
		_writer.Write(this.entityType);
		StreamUtils.Write(_writer, this.pos);
		StreamUtils.Write(_writer, this.rot);
		this.itemValue.Write(_writer);
		_writer.Write(this.entityThatPlaced);
		_writer.Write(StringHelpers.WriteListToString(this.buffs));
	}


	/**
	 * Process the package data.
	 */

	public override void ProcessPackage(World _world, GameManager _callbacks)
	{
		if (_world == null)
		{
			return;
		}
		EntityAlive entity = (EntityAlive)EntityFactory.CreateEntity(this.entityType, this.pos, this.rot);
		entity.SetSpawnerSource(EnumSpawnerSource.StaticSpawner);
		if (GameManager.Instance.World.GetEntity(this.entityThatPlaced) as EntityPlayer != null)
		{
			entity.Spawned = true;
		}
		if (this.buffs.Count > 0)
		{
			foreach (string buff in this.buffs)
			{
				entity.Buffs.AddBuff(buff);
			}
		}
		_world.SpawnEntityInWorld(entity);
	}


	/**
	 * Gets package length.
	 */

	public override int GetLength()
	{
		return 20;
	}


	public int entityType;
	public Vector3 pos;
	public Vector3 rot;
	public ItemValue itemValue;
	public int entityThatPlaced;
	public List<string> buffs;
}