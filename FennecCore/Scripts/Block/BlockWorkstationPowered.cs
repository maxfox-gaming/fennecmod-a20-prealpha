﻿namespace FennecCore.Scripts
{
    public class BlockWorkstationPowered : BlockWorkstation
    {
        /**
         *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
         */

        public BlockWorkstationPowered()
        {
            this.HasTileEntity = true;
        }

        /**
        * After initial block values loaded, this will load the extra properties for us.
        */

        public override void LateInit()
        {
            base.LateInit();
            this.workstationPoweredPropertyParser = new WorkstationPoweredPropertyParser(this);
            this.workstationPoweredPropertyParser.ParseDynamicProperties();
        }


        /**
         *  Called when the block is added into the world from the generator. 
         */

        public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (_blockValue.ischild)
            {
                return;
            }
            this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
            if (this.isMultiBlock)
            {
                this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            this.checkParticles(world, _chunk.ClrIdx, _blockPos, _blockValue);
            if (_blockValue.ischild)
            {
                return;
            }
            this.addTileEntity(world, _chunk, _blockPos, _blockValue);
        }


        /**
         * Clean up tile entity and particles when the block is removed from the world.
         */

        public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (!_blockValue.ischild)
            {
                this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
                if (this.isMultiBlock)
                {
                    this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                    return;
                }
            }
            else if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            this.removeParticles(_world, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);
            _chunk.RemoveTileEntityAt<TileEntityWorkstationPowered>((World)_world, World.toBlock(_blockPos));
        }


        /**
         * Called when a player or entity places the block down.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            Block block = Block.list[_result.blockValue.type];
            if (block.shape.IsTerrain())
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
            }
            else if (!block.IsTerrainDecoration)
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
            }
            else
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
            }

            TileEntityWorkstationPowered tileEntityWorkstationPowered = (TileEntityWorkstationPowered)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
            if (tileEntityWorkstationPowered != null)
            {
                tileEntityWorkstationPowered.IsPlayerPlaced = true;
            }
        }


        /**
         * Called when someone accessed the block.
         */

        public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            TileEntityWorkstationPowered tileEntityWorkstationPowered = (TileEntityWorkstationPowered)_world.GetTileEntity(_cIdx, _blockPos);
            if (tileEntityWorkstationPowered == null)
            {
                return false;
            }
            _player.AimingGun = false;
            Vector3i blockPos = tileEntityWorkstationPowered.ToWorldPos();
            _world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityWorkstationPowered.entityId, _player.entityId, null);
            return true;
        }


        /**
         * Called when the E key is held down.
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            if (_indexInBlockActivationCommands == 0)
            {
                return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
            }
            if (_indexInBlockActivationCommands != 1)
            {
                return false;
            }
            this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
            return true;
        }


        /**
         * Returns the activation text for the player looking at the block.
         */

        public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            TileEntityWorkstationPowered tileEntityWorkstationPowered = (TileEntityWorkstationPowered)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityWorkstationPowered == null)
            {
                return "";
            }

            if (!tileEntityWorkstationPowered.HasRequiredPower)
            {
                return Localization.Get("useWorkstationNoPower");
            }

            return Localization.Get("useWorkstation");
        }


        /**
         * Returns a complete list of block activation commands.
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
            TileEntityWorkstationPowered tileEntityWorkstationPowered = (TileEntityWorkstationPowered)_world.GetTileEntity(_clrIdx, _blockPos);
            bool flag2 = false;
            if (tileEntityWorkstationPowered != null)
            {
                flag2 = tileEntityWorkstationPowered.IsPlayerPlaced;
            }
            this.cmds[1].enabled = (flag && flag2 && this.TakeDelay > 0f);
            return this.cmds;
        }


        /**
         * Event handler
         */

        private void EventData_Event(TimerEventData timerData)
        {
            World world = GameManager.Instance.World;
            object[] array = (object[])timerData.Data;
            int clrIdx = (int)array[0];
            BlockValue blockValue = (BlockValue)array[1];
            Vector3i vector3i = (Vector3i)array[2];
            BlockValue block = world.GetBlock(vector3i);
            EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
            if (block.damage > 0)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
                return;
            }
            if (block.type != blockValue.type)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), string.Empty, "ui_denied", null);
                return;
            }
            TileEntityWorkstationPowered tileEntityWorkstationPowered = world.GetTileEntity(clrIdx, vector3i) as TileEntityWorkstationPowered;
            if (tileEntityWorkstationPowered.IsUserAccessing())
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), string.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
            this.HandleTakeInternalItems(tileEntityWorkstationPowered, uiforPlayer);
            ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
            if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack))
            {
                uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
            }
            world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
        }

        // Token: 0x0600107D RID: 4221 RVA: 0x00063F7C File Offset: 0x0006217C
        protected virtual void HandleTakeInternalItems(TileEntityWorkstationPowered te, LocalPlayerUI playerUI)
        {
            ItemStack[] array = te.Output;
            for (int i = 0; i < array.Length; i++)
            {
                if (!array[i].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[i]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[i]);
                }
            }
            array = te.Tools;
            for (int j = 0; j < array.Length; j++)
            {
                if (!array[j].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[j]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[j]);
                }
            }
            array = te.Fuel;
            for (int k = 0; k < array.Length; k++)
            {
                if (!array[k].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[k]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[k]);
                }
            }
        }

        // Token: 0x04000D27 RID: 3367
        private float TakeDelay = 2f;

        // Token: 0x04000D29 RID: 3369
        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("open", "campfire", true),
        new BlockActivationCommand("take", "hand", false)
        };

        protected void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            TileEntityWorkstationPowered tileEntityWorkstationPowered = new TileEntityWorkstationPowered(_chunk);
            tileEntityWorkstationPowered.localChunkPos = World.toBlock(_blockPos);
            tileEntityWorkstationPowered.SetRequirePower(
                this.workstationPoweredPropertyParser.requirePower,
                this.workstationPoweredPropertyParser.powerSources,
                this.workstationPoweredPropertyParser.sidedPower
            );

            tileEntityWorkstationPowered.CalculateLookupCoordinates();
            _chunk.AddTileEntity(tileEntityWorkstationPowered);
        }


        // Transformation properties to pass on to the tile entity.
        private WorkstationPoweredPropertyParser workstationPoweredPropertyParser;
    }
}