﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FennecCore.Scripts
{
	public class BlockUpgradeRatedTrunkTip : BlockDamage
	{
		/**
		 * After all XML loaded, post initialise block.
		 */

		public override void LateInit()
		{
			base.LateInit();
			if (this.Properties.Values.ContainsKey(BlockUpgradeRatedTrunkTip.PropUpgradeToBlock))
			{
				ItemValue item = ItemClass.GetItem(this.Properties.Values[BlockUpgradeRatedTrunkTip.PropUpgradeToBlock], false);
				this.upgradeBlock = item.ToBlockValue();
			}
			if (this.Properties.Values.ContainsKey(BlockUpgradeRatedTrunkTip.PropUpgradeBlockCombined))
			{
				this.upgradeBlockCombined = ItemClass.GetItem(this.Properties.Values[BlockUpgradeRatedTrunkTip.PropUpgradeBlockCombined], false).ToBlockValue();
				if (BlockUpgradeRatedTrunkTip.PropUpgradeBlockCombined.Equals(BlockValue.Air))
				{
					throw new Exception("Block with name '" + this.Properties.Values[BlockUpgradeRatedTrunkTip.PropUpgradeBlockCombined] + "' not found!");
				}
			}
			if (this.Properties.Values.ContainsKey(BlockUpgradeRatedTrunkTip.PropUpgradeRate))
			{
				this.upgradeRate = int.Parse(this.Properties.Values[BlockUpgradeRatedTrunkTip.PropUpgradeRate]);
			}
			this.IsRandomlyTick = true;
		}


		/**
		 * If a block nearby changes, execute.
		 */

		public override void OnNeighborBlockChange(WorldBase world, int _clrIdx, Vector3i _myBlockPos, BlockValue _myBlockValue, Vector3i _blockPosThatChanged, BlockValue _newNeighborBlockValue, BlockValue _oldNeighborBlockValue)
		{
			if (this.upgradeBlockCombined.type == 0)
			{
				return;
			}
			Block block = Block.list[_newNeighborBlockValue.type];
			if (block is BlockUpgradeRatedTrunkTip && ((BlockUpgradeRatedTrunkTip)block).upgradeBlockCombined.type == this.upgradeBlockCombined.type)
			{
				world.SetBlockRPC(_clrIdx, _blockPosThatChanged, this.upgradeBlockCombined);
				world.SetBlockRPC(_clrIdx, _myBlockPos, this.upgradeBlockCombined);
			}
		}


		/**
		 * Each tick, do this action.
		 */

		public override bool UpdateTick(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bRandomTick, ulong _ticksIfLoaded, GameRandom _rnd)
		{
			base.UpdateTick(_world, _clrIdx, _blockPos, _blockValue, _bRandomTick, _ticksIfLoaded, _rnd);
			BlockValue blockValue = BlockPlaceholderMap.Instance.Replace(this.upgradeBlock, _world.GetGameRandom(), _blockPos.x, _blockPos.z, false, QuestTags.none);
			blockValue.rotation = _blockValue.rotation;
			blockValue.meta = 0;
			_blockValue = blockValue;
			Block block = _blockValue.Block;
			if (_blockValue.damage >= block.blockMaterial.MaxDamage)
			{
				_blockValue.damage = block.blockMaterial.MaxDamage - 1;
			}
			if (block.shape.IsTerrain())
			{
				_world.SetBlockRPC(_clrIdx, _blockPos, _blockValue, block.Density);
			}
			else
			{
				_world.SetBlockRPC(_clrIdx, _blockPos, _blockValue);
			}
			return true;
		}


		/**
		 * Checks whether update is needed.
		 */

		public override void CheckUpdate(BlockValue _oldBV, BlockValue _newBV, out bool bUpdateMesh, out bool bUpdateNotify, out bool bUpdateLight)
		{
			if (_oldBV.type == _newBV.type && _oldBV.damage == _newBV.damage)
			{
				bUpdateMesh = (bUpdateNotify = (bUpdateLight = false));
				return;
			}
			bUpdateMesh = (bUpdateNotify = (bUpdateLight = true));
		}


		/**
		 * Get collision vectors.
		 */

		public override void GetCollisionAABB(BlockValue _blockValue, int _x, int _y, int _z, float _distortedY, List<Bounds> _result)
		{
			base.GetCollisionAABB(_blockValue, _x, _y, _z, _distortedY, _result);
			Vector3 b = new Vector3(-0.3f, -0.2f, -0.3f);
			for (int i = 0; i < _result.Count; i++)
			{
				Bounds value = _result[i];
				value.SetMinMax(value.min - b, value.max + b);
				_result[i] = value;
			}
		}


		/**
		 * Rotates on collision check.
		 */

		public override bool RotateVerticesOnCollisionCheck(BlockValue _blockValue)
		{
			return false;
		}


		protected static string PropUpgradeToBlock = "UpgradeRated.ToBlock";
		protected static string PropUpgradeBlockCombined = "UpgradeRated.BlockCombined";
		protected static string PropUpgradeRate = "UpgradeRated.Rate";
		protected BlockValue upgradeBlock;
		protected BlockValue upgradeBlockCombined;
		protected int upgradeRate;
	}

}