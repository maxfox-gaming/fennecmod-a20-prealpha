﻿using System;

namespace FennecCore.Scripts
{
    public class BlockMultiblockMasterWorkstation : BlockWorkstation, IBlockMultiblockComponent
    {
        /**
         *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
         */

        public BlockMultiblockMasterWorkstation()
        {
            this.HasTileEntity = true;
        }


        /**
         * When workstation is initialized
         */

        public override void Init()
        {
            base.Init();
            if (!this.Properties.Values.ContainsKey("Structure"))
            {
                throw new Exception("Multiblock " + this.GetBlockName() + " does not contain a Structure property.");
            }
            this.Structure = this.Properties.Values["Structure"];

            MultiblockData multiblockData = new MultiblockData(this.Structure, this.GetBlockName());
            MultiblockManager.AddMultiblock(multiblockData);
        }


        /**
        * After initial block values loaded, this will load the extra properties for us.
        */

        public override void LateInit()
        {
            base.LateInit();
            MultiblockManager.ValidateMultiblock(this.GetBlockName());
        }


        /**
         *  Called when the block is added into the world from the generator. 
         */

        public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (_blockValue.ischild)
            {
                return;
            }
            this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
            if (this.isMultiBlock)
            {
                this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            this.checkParticles(world, _chunk.ClrIdx, _blockPos, _blockValue);
            if (_blockValue.ischild)
            {
                return;
            }
            this.addTileEntity(world, _chunk, _blockPos, _blockValue);
        }


        /**
         * Clean up tile entity and particles when the block is removed from the world.
         */

        public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (!_blockValue.ischild)
            {
                this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
                if (this.isMultiBlock)
                {
                    this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                    return;
                }
            }
            else if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            this.removeParticles(_world, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);

            _chunk.RemoveTileEntityAt<TileEntityMultiblockMasterWorkstation>((World)_world, World.toBlock(_blockPos));
        }


        /**
         * Called when a player or entity places the block down.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            Block block = Block.list[_result.blockValue.type];
            if (block.shape.IsTerrain())
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
            }
            else if (!block.IsTerrainDecoration)
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
            }
            else
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
            }

            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = (TileEntityMultiblockMasterWorkstation)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
            if (tileEntityMultiblockMasterWorkstation != null)
            {
                tileEntityMultiblockMasterWorkstation.IsPlayerPlaced = true;
            }
        }


        /**
         * Called when someone accessed the block with E.
         */

        public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = (TileEntityMultiblockMasterWorkstation)_world.GetTileEntity(_cIdx, _blockPos);
            if (tileEntityMultiblockMasterWorkstation == null)
            {
                return false;
            }

            if (!tileEntityMultiblockMasterWorkstation.MultiblockFormed())
            {
                EntityPlayerLocal _playerLocal = _player as EntityPlayerLocal;
                if (_playerLocal != null)
                {
                    GameManager.ShowTooltip(_playerLocal as EntityPlayerLocal, Localization.Get("ttMultiblockNotFormed"), String.Empty, "ui_denied", null);
                }
                Log.Warning("OnBlockActivated End");
                return false;
            }

            _player.AimingGun = false;
            Vector3i blockPos = tileEntityMultiblockMasterWorkstation.ToWorldPos();
            _world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityMultiblockMasterWorkstation.entityId, _player.entityId, null);
            return true;
        }



        /**
         * Called when someone accessed the block using the hold E menu
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            switch (_indexInBlockActivationCommands)
            {
                case 0:
                    return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
                case 1:
                    this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
                    return true;
                default:
                    return false;
            }
        }


        /**
         * Returns the activation text for the player looking at the block.
         */

        public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = (TileEntityMultiblockMasterWorkstation)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockMasterWorkstation == null)
            {
                return "";
            }

            if (!tileEntityMultiblockMasterWorkstation.MultiblockFormed())
            {
                return Localization.Get("multiblockMasterNotFormed");
            }

            return Localization.Get("useWorkstation");
        }


        /**
         * Returns a list of block activation commands
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = (TileEntityMultiblockMasterWorkstation)_world.GetTileEntity(_clrIdx, _blockPos);
            bool flag2 = false;
            if (tileEntityMultiblockMasterWorkstation != null)
            {
                flag2 = tileEntityMultiblockMasterWorkstation.IsPlayerPlaced;
            }
            this.cmds[1].enabled = (flag && flag2 && this.TakeDelay > 0f);
            return this.cmds;
        }


        /**
         * Event handler
         */

        private void EventData_Event(TimerEventData timerData)
        {
            World world = GameManager.Instance.World;
            object[] array = (object[])timerData.Data;
            int clrIdx = (int)array[0];
            BlockValue blockValue = (BlockValue)array[1];
            Vector3i vector3i = (Vector3i)array[2];
            BlockValue block = world.GetBlock(vector3i);
            EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
            if (block.damage > 0)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), String.Empty, "ui_denied", null);
                return;
            }
            if (block.type != blockValue.type)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), String.Empty, "ui_denied", null);
                return;
            }
            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = world.GetTileEntity(clrIdx, vector3i) as TileEntityMultiblockMasterWorkstation;
            if (tileEntityMultiblockMasterWorkstation.IsUserAccessing())
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), String.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
            this.HandleTakeInternalItems(tileEntityMultiblockMasterWorkstation, uiforPlayer);
            ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
            if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack))
            {
                uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
            }
            world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
        }


        /**
         * Handles taking items and returning them to the player when workstation is picked up.
         */

        protected virtual void HandleTakeInternalItems(TileEntityMultiblockMasterWorkstation te, LocalPlayerUI playerUI)
        {
            ItemStack[] array = te.Output;
            for (int i = 0; i < array.Length; i++)
            {
                if (!array[i].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[i]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[i]);
                }
            }
            array = te.Tools;
            for (int j = 0; j < array.Length; j++)
            {
                if (!array[j].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[j]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[j]);
                }
            }
            array = te.Fuel;
            for (int k = 0; k < array.Length; k++)
            {
                if (!array[k].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[k]))
                {
                    playerUI.xui.PlayerInventory.DropItem(array[k]);
                }
            }
        }


        /**
         * Returns the list of block activation commands.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("open", "campfire", true),
        new BlockActivationCommand("takeMaster", "hand", false)
        };


        /**
         * Adds the tile entity to the loaded chunk.
         */

        protected void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            TileEntityMultiblockMasterWorkstation tileEntityMultiblockMasterWorkstation = new TileEntityMultiblockMasterWorkstation(_chunk);
            tileEntityMultiblockMasterWorkstation.localChunkPos = World.toBlock(_blockPos);
            tileEntityMultiblockMasterWorkstation.SetMasterBlockName(this.GetBlockName());
            _chunk.AddTileEntity(tileEntityMultiblockMasterWorkstation);
        }



        private string Structure;
        private float TakeDelay = 2f;
    }
}