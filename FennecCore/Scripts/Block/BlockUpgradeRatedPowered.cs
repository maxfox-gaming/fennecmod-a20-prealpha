﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
	public class BlockUpgradeRatedPowered : BlockUpgradeRated
	{
		/**
		 * Initialise block data after blocks XML is loaded.
		 */

		public override void LateInit()
		{
			base.LateInit();
			string sources;
			if (PropertyHelpers.PropExists(this.Properties, BlockUpgradeRatedPowered.PropPowerSources, out sources))
			{
				List<string> blockNames = StringHelpers.WriteStringToList(sources);
				this.powerSources = BlockHelpers.CheckBlocksDefined(blockNames);
			}
			else
			{
				this.powerSources = new List<string>() { "electricwirerelay" };
			}

			string sided;
			if (PropertyHelpers.PropExists(this.Properties, BlockUpgradeRatedPowered.PropSidedPower, out sided))
			{
				this.sidedPower = SidedPowerMapping.Map(sided);
			}
		}


		/**
		 * When the block is added to the world, add new entry to the manager so that we can keep its data bundled.
		 */

		public override void OnBlockAdded(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			base.OnBlockAdded(_world, _chunk, _blockPos, _blockValue);
			List<Vector3i> multiblockChildren = BlockHelpers.GetMultiblockCoordsFor(_world, _blockPos);
			List<Vector3i> multiblockPositions = multiblockChildren;
			multiblockPositions.Add(_blockPos);
			List<Vector3i> lookupCoords;

			switch (sidedPower)
			{
				case SidedPower.TOP:
					lookupCoords = CoordinateHelper.GetCoordinatesAbove(multiblockPositions);
					break;
				case SidedPower.BOTTOM:
					lookupCoords = CoordinateHelper.GetCoordinatesBelow(multiblockPositions);
					break;
				case SidedPower.SIDES:
					lookupCoords = CoordinateHelper.GetCoordinatesAround(multiblockPositions, true, 1, 0, 1);
					break;
				default:
					lookupCoords = CoordinateHelper.GetCoordinatesAround(multiblockPositions, true);
					break;
			}

			ReceivePowerManager.AddLink(new ReceivePowerLink(_blockPos, multiblockChildren, lookupCoords));
		}


		/**
		 * Removes entry from manager when block is removed.
		 */

		public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			base.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
			ReceivePowerManager.RemoveLink(_blockPos);
		}


		/**
		 * Handles block updates. If block is not powered don't do anything.
		 */

		public override bool UpdateTick(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bRandomTick, ulong _ticksIfLoaded, GameRandom _rnd)
		{
			if (!this.IsPowered(_world, _blockPos))
			{
				return false;
			}

			return base.UpdateTick(_world, _clrIdx, _blockPos, _blockValue, _bRandomTick, _ticksIfLoaded, _rnd);
		}


		/**
		 * Checks if the block is being powered.
		 */

		protected bool IsPowered(WorldBase _world, Vector3i _blockPos)
		{
			ReceivePowerLink link = ReceivePowerManager.GetLinkFor(_blockPos);
			if (link == null)
			{
				return false;
			}

			Dictionary<Vector3i, TileEntity> tileEntitiesNearby = CoordinateHelper.GetTileEntitiesInCoordinatesWithType((World)_world, link.GetLookupCoords(), TileEntityType.Powered);

			if (tileEntitiesNearby.Count == 0)
			{
				link.SetUnpowered();
				return link.HasPower();
			}

			foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesNearby)
			{
				TileEntityPowered otherTileEntity = entry.Value as TileEntityPowered;
				Vector3i otherTileEntityPos = entry.Key;

				foreach (string powerSource in this.powerSources)
				{
					string name = CoordinateHelper.GetBlockNameAtCoordinate((World)_world, otherTileEntityPos);
					if (!CoordinateHelper.BlockAtCoordinateIs((World)_world, otherTileEntityPos, powerSource))
					{
						continue;
					}

					if (otherTileEntity.IsPowered)
					{
						link.SetPowered();
						return link.HasPower();
					}
				}
			}
			link.SetUnpowered();
			return link.HasPower();
		}


		/**
		 * Gets block activation text.
		 */

		public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
		{
			if (!this.IsPowered(_world, _blockPos))
			{
				return Localization.Get("ttBlockNotPowered");
			}
			return "";
		}


		// New properties
		protected static string PropPowerSources = "PowerSources";
		protected static string PropSidedPower = "SidedPower";

		// New property values
		protected List<string> powerSources;
		protected SidedPower sidedPower;

		// Data values
		protected bool hasPower;
	}
}