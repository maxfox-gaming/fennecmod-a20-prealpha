﻿namespace FennecCore.Scripts
{
	public class BlockWorkstationImproved : BlockWorkstation
	{
		/**
		 *	Constructor
		 */
		public BlockWorkstationImproved()
		{
			this.HasTileEntity = true;
		}


		/**
		 * When the block is added to the world.
		 */
		public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			if (_blockValue.ischild)
			{
				return;
			}
			this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
			if (this.isMultiBlock)
			{
				this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
			}
			if (_blockValue.ischild)
			{
				return;
			}
			_chunk.AddTileEntity(new TileEntityWorkstationImproved(_chunk)
			{
				localChunkPos = World.toBlock(_blockPos)
			});
		}


		/**
		 *  When the block is removed from the world.
		 */
		public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			if (!_blockValue.ischild)
			{
				this.shape.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
				if (this.isMultiBlock)
				{
					this.multiBlockPos.RemoveChilds(world, _chunk.ClrIdx, _blockPos, _blockValue);
					return;
				}
			}
			else if (this.isMultiBlock)
			{
				this.multiBlockPos.RemoveParentBlock(world, _chunk.ClrIdx, _blockPos, _blockValue);
			}
			_chunk.RemoveTileEntityAt<TileEntityWorkstationImproved>((World)world, World.toBlock(_blockPos));
		}


		/**
		 * When the block is placed by a player.
		 */
		public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
		{
			Block block = Block.list[_result.blockValue.type];
			if (block.shape.IsTerrain())
			{
				_world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
			}
			else if (!block.IsTerrainDecoration)
			{
				_world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
			}
			else
			{
				_world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
			}
			TileEntityWorkstationImproved tileEntityWorkstationImproved = (TileEntityWorkstationImproved)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
			if (tileEntityWorkstationImproved != null)
			{
				tileEntityWorkstationImproved.IsPlayerPlaced = true;
			}
		}


		/**
		 * When the block is activated with E.
		 */
		public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
		{
			TileEntityWorkstationImproved tileEntityWorkstationImproved = (TileEntityWorkstationImproved)_world.GetTileEntity(_cIdx, _blockPos);
			if (tileEntityWorkstationImproved == null)
			{
				return false;
			}
			_player.AimingGun = false;
			Vector3i blockPos = tileEntityWorkstationImproved.ToWorldPos();
			_world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityWorkstationImproved.entityId, _player.entityId, null);
			return true;
		}


		/**
		 * List of block activation commands.
		 */
		public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
		{
			bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
			TileEntityWorkstationImproved tileEntityWorkstationImproved = (TileEntityWorkstationImproved)_world.GetTileEntity(_clrIdx, _blockPos);
			bool flag2 = false;
			if (tileEntityWorkstationImproved != null)
			{
				flag2 = tileEntityWorkstationImproved.IsPlayerPlaced;
			}
			this.cmds[1].enabled = (flag && flag2 && this.TakeDelay > 0f);
			return this.cmds;
		}


		/**
		 * Timer data for removal.
		 */
		private void EventData_Event(TimerEventData timerData)
		{
			World world = GameManager.Instance.World;
			object[] array = (object[])timerData.Data;
			int clrIdx = (int)array[0];
			BlockValue blockValue = (BlockValue)array[1];
			Vector3i vector3i = (Vector3i)array[2];
			BlockValue block = world.GetBlock(vector3i);
			EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
			if (block.damage > 0)
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
				return;
			}
			if (block.type != blockValue.type)
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), string.Empty, "ui_denied", null);
				return;
			}
			TileEntityWorkstationImproved tileEntityWorkstationImproved = world.GetTileEntity(clrIdx, vector3i) as TileEntityWorkstationImproved;
			if (tileEntityWorkstationImproved.IsUserAccessing())
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), string.Empty, "ui_denied", null);
				return;
			}
			LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
			this.HandleTakeInternalItems(tileEntityWorkstationImproved, uiforPlayer);
			ItemStack itemStack = new ItemStack(block.ToItemValue(), 1);
			if (!uiforPlayer.xui.PlayerInventory.AddItem(itemStack))
			{
				uiforPlayer.xui.PlayerInventory.DropItem(itemStack);
			}
			world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
		}


		/**
		 *  Handles taking of items when the workstation is picked up.
		 */
		protected virtual void HandleTakeInternalItems(TileEntityWorkstationImproved te, LocalPlayerUI playerUI)
		{
			ItemStack[] array = te.Output;
			for (int i = 0; i < array.Length; i++)
			{
				if (!array[i].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[i]))
				{
					playerUI.xui.PlayerInventory.DropItem(array[i]);
				}
			}
			array = te.Tools;
			for (int j = 0; j < array.Length; j++)
			{
				if (!array[j].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[j]))
				{
					playerUI.xui.PlayerInventory.DropItem(array[j]);
				}
			}
			array = te.Fuel;
			for (int k = 0; k < array.Length; k++)
			{
				if (!array[k].IsEmpty() && !playerUI.xui.PlayerInventory.AddItem(array[k]))
				{
					playerUI.xui.PlayerInventory.DropItem(array[k]);
				}
			}
		}



		/**
		 * Time needed to remove the station as a default.
		 */
		private float TakeDelay = 2f;


		/**
		 * Block activation commands list.
		 */
		private BlockActivationCommand[] cmds = new BlockActivationCommand[]
		{
			new BlockActivationCommand("open", "campfire", true),
			new BlockActivationCommand("take", "hand", false)
		};
	}

}