﻿using System;

namespace FennecCore.Scripts
{
    public class BlockPicture : Block
    {
        /**
         * Reads block properties after all have loaded.
         */

        public override void LateInit()
        {
            base.LateInit();
            string nextBlockName;
            if (PropertyHelpers.PropExists(this.Properties, propNext, out nextBlockName))
            {
                Block nextBlock = Block.GetBlockByName(nextBlockName);
                if (nextBlock.ToBlockValue().Equals(BlockValue.Air))
                {
                    throw new Exception("Block with name '" + nextBlock + "' is not defined for " + propNext + " in " + this.GetBlockName());
                }
                this.next = nextBlock;
            }

            string prevBlockName;
            if (PropertyHelpers.PropExists(this.Properties, propPrev, out prevBlockName))
            {
                Block prevBlock = Block.GetBlockByName(prevBlockName);
                if (prevBlock.ToBlockValue().Equals(BlockValue.Air))
                {
                    throw new Exception("Block with name '" + prevBlock + "' is not defined for " + propNext + " in " + this.GetBlockName());
                }
                this.previous = prevBlock;
            }
        }


        /**
         * When block is activated, go to previous or next state depending on what is selected.
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            BlockValue block;
            switch (_indexInBlockActivationCommands)
            {
                case 0:
                    block = this.next.ToBlockValue();
                    block.rotation = _blockValue.rotation;
                    _world.SetBlockRPC(_blockPos, block);
                    return true;
                case 1:
                    block = this.previous.ToBlockValue();
                    block.rotation = _blockValue.rotation;
                    _world.SetBlockRPC(_blockPos, block);
                    return true;
                default:
                    return false;
            }
            
        }


        /**
         * Default
         */

        public override bool OnBlockActivated(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            return true;
        }


        /**
         * Returns the block activation commands.
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            if (this.next != null)
            {
                this.cmds[0].enabled = true;
            }
            if (this.previous != null)
            {
                this.cmds[1].enabled = true;
            }
            
            return this.cmds;
        }



        /**
         * Returns the commands to activate the block looking at.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
            new BlockActivationCommand("Next", "arrow_right", false),
            new BlockActivationCommand("Prev", "arrow_left", false)
        };



        private string propNext = "Next";
        private string propPrev = "Previous";
        private Block previous;
        private Block next;
    }
}
