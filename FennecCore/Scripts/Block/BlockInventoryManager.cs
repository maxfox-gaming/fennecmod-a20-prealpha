﻿namespace FennecCore.Scripts
{
    public class BlockInventoryManager : BlockLoot
    {

        /**
         *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
         */

        public BlockInventoryManager()
        {
            this.HasTileEntity = true;
        }


        /**
         * This happens when the block is placed in the world.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            Block block = Block.list[_result.blockValue.type];
            if (block.shape.IsTerrain())
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
            }
            else if (!block.IsTerrainDecoration)
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
            }
            else
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
            }

            TileEntityInventoryManager tileEntityInventoryManager = _world.GetTileEntity(_result.clrIdx, _result.blockPos) as TileEntityInventoryManager;
            if (tileEntityInventoryManager == null)
            {
                Log.Warning("Failed to create tile entity");
                return;
            }
            if (_ea != null && _ea.entityType == EntityType.Player)
            {
                tileEntityInventoryManager.bPlayerStorage = true;
                tileEntityInventoryManager.worldTimeTouched = _world.GetWorldTime();
                tileEntityInventoryManager.SetEmpty();
            }

            Log.Out("Created successfullly.");
        }


        /**
         * This is called when block is removed. We want to destroy existing tile entity too.
         */

        public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (!_blockValue.ischild)
            {
                this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
                if (this.isMultiBlock)
                {
                    this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                    return;
                }
            }
            else if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
            }

            TileEntityInventoryManager tileEntityInventoryManager = _world.GetTileEntity(_chunk.ClrIdx, _blockPos) as TileEntityInventoryManager;
            if (tileEntityInventoryManager != null)
            {
                tileEntityInventoryManager.OnDestroy();
            }
            this.removeTileEntity(_world, _chunk, _blockPos, _blockValue);
        }


        /**
         * Helper method to add and set up a tile entity for the block.
         */

        protected override void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            TileEntityInventoryManager tileEntityInventoryManager = new TileEntityInventoryManager(_chunk);
            tileEntityInventoryManager.localChunkPos = World.toBlock(_blockPos);
            tileEntityInventoryManager.lootListName = this.lootList;
            tileEntityInventoryManager.SetContainerSize(LootContainer.GetLootContainer(this.lootList, true).size, true);

            _chunk.AddTileEntity(tileEntityInventoryManager);
        }


        /**
         * Helper method to remove a tile entity.
         */

        protected override void removeTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            _chunk.RemoveTileEntityAt<TileEntityInventoryManager>((World)world, World.toBlock(_blockPos));
        }


        /**
         * Called when a block is destroyed by an entity. It cam be useful for separating things out if needed depending on what
         * entity destroyed it.
         */

        public override Block.DestroyedResult OnBlockDestroyedBy(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, int _entityId, bool _bUseHarvestTool)
        {
            TileEntityInventoryManager tileEntityInventoryManager = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityInventoryManager;
            if (tileEntityInventoryManager != null)
            {
                tileEntityInventoryManager.OnDestroy();
            }
            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetEntity(_entityId) as EntityPlayerLocal);
            if (null != uiforPlayer && uiforPlayer.windowManager.IsWindowOpen("looting") && ((XUiC_LootWindow)uiforPlayer.xui.GetWindow("windowLooting").Controller).GetLootBlockPos() == _blockPos)
            {
                uiforPlayer.windowManager.Close("looting");
            }
            if (tileEntityInventoryManager != null)
            {
                _world.GetGameManager().DropContentOfLootContainerServer(_blockValue, _blockPos, tileEntityInventoryManager.entityId);
            }
            return Block.DestroyedResult.Downgrade;
        }




        /**
         * This is called when the player presses the activation key (usually E) on the block.
         */

        public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            TileEntityInventoryManager tileEntityInventoryManager = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityInventoryManager;
            if (tileEntityInventoryManager == null)
            {
                return false;
            }
            _player.AimingGun = false;
            Vector3i blockPos = tileEntityInventoryManager.ToWorldPos();
            tileEntityInventoryManager.bWasTouched = tileEntityInventoryManager.bTouched;
            _world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityInventoryManager.entityId, _player.entityId);
            return true;
        }


        /**
         * Returns the commands to activate the block looking at.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
            new BlockActivationCommand("Search", "search", true)
        };

    }
}