﻿using Audio;

namespace FennecCore.Scripts
{
    public class BlockWindTurbine : BlockPowerSource
    {
        /**
         * Creates a tile entity.
         */

        public override TileEntityPowerSource CreateTileEntity(Chunk chunk)
        {
            if (this.slotItem == null)
            {
                this.slotItem = ItemClass.GetItemClass(this.SlotItemName, false);
            }

            return new TileEntityPowerSource(chunk)
            {
                PowerItemType = PowerItemMapping.Types[PowerItemMapping.WindTurbine],
                SlotItem = this.slotItem
            };
        }


        /**
         * Checks whether a block can be placed.
         */

        public override bool CanPlaceBlockAt(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bOmitCollideCheck = false)
        {
            if (!base.CanPlaceBlockAt(_world, _clrIdx, _blockPos, _blockValue, _bOmitCollideCheck))
            {
                return false;
            }
            return true;
        }


        /**
         * Gets the power icon.
         */

        protected override string GetPowerSourceIcon()
        {
            return "electric_solar";
        }


        /**
         * When block is removed, do this.
         */

        public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
            Manager.BroadcastStop(_blockPos.ToVector3(), this.runningSound);
        }


        /**
         * When block is unloaded, do this.
         */

        public override void OnBlockUnloaded(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockUnloaded(_world, _clrIdx, _blockPos, _blockValue);
            Manager.Stop(_blockPos.ToVector3(), this.runningSound);
        }


        /**
         * When block is activated.
         */

        public override void OnBlockEntityTransformAfterActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
        {
            base.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);

            // Add wind turbine controller to block's transform object
            WindTurbineController controller = this.AddOrGetController(_ebcd);
            if (controller == null)
            {
                return;
            }

            TileEntityPowerSource te = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityPowerSource;
            if (te.IsOn)
            {
                controller.SetOn();
            }
            else
            {
                controller.SetOff();
            }
        }



        public WindTurbineController AddOrGetController(BlockEntityData ebcd)
        {
            if (ebcd.transform.gameObject.GetComponent<WindTurbineController>() == null)
            {
                WindTurbineController controller = ebcd.transform.gameObject.AddComponent<WindTurbineController>();
                controller.SetBaseTransform(ebcd.transform);
                return controller;
            }

            return ebcd.transform.gameObject.GetComponent<WindTurbineController>();
        }


        private string runningSound = "solarpanel_idle";

    }
}