﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FennecCore.Scripts
{
    public class BlockMultiblockMasterTransformer : BlockLoot, IBlockTransformer, IBlockMultiblockComponent
    {
        /**
         *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
         */

        public BlockMultiblockMasterTransformer()
        {
            this.HasTileEntity = true;
            base.IsNotifyOnLoadUnload = true;
        }


        /**
         * When block is initialised
         */

        public override void Init()
        {
            base.Init();
            if (!this.Properties.Values.ContainsKey("Structure"))
            {
                throw new Exception("Multiblock " + this.GetBlockName() + " does not contain a Structure property.");
            }
            this.Structure = this.Properties.Values["Structure"];
            this.WorkstationData = new WorkstationData(base.GetBlockName(), this.Properties);
            CraftingManager.AddWorkstationData(this.WorkstationData);
            MultiblockData multiblockData = new MultiblockData(this.Structure, this.GetBlockName());
            MultiblockManager.AddMultiblock(multiblockData);

            if (this.Properties.Values.ContainsKey("ParticleName"))
            {
                this.particleName = this.Properties.Values["ParticleName"];
                ParticleEffect.RegisterBundleParticleEffect(this.particleName);
            }
            if (this.Properties.Values.ContainsKey("ParticleOffset"))
            {
                this.offset = StringParsers.ParseVector3(this.Properties.Values["ParticleOffset"], 0, -1);
            }
            if (GameManager.IsDedicatedServer && this.particleName != null && this.particleName.Length > 0)
            {
                if (BlockMultiblockMasterTransformer.particleLights == null)
                {
                    BlockMultiblockMasterTransformer.particleLights = new Dictionary<int, List<Light>>();
                }
                this.particleHash = this.particleName.GetHashCode();
                if (!BlockMultiblockMasterTransformer.particleLights.ContainsKey(this.particleHash))
                {
                    BlockMultiblockMasterTransformer.particleLights.Add(this.particleHash, new List<Light>());
                    Transform dynamicParticleEffect = ParticleEffect.GetDynamicParticleEffect(this.particleHash);
                    if (dynamicParticleEffect != null)
                    {
                        Light[] componentsInChildren = dynamicParticleEffect.GetComponentsInChildren<Light>();
                        if (componentsInChildren != null)
                        {
                            for (int i = 0; i < componentsInChildren.Length; i++)
                            {
                                BlockMultiblockMasterTransformer.particleLights[this.particleHash].Add(componentsInChildren[i]);
                            }
                        }
                    }
                }
            }
        }


        /**
         * After initial block values loaded, this will load the extra properties for us.
         */

        public override void LateInit()
        {
            base.LateInit();
            this.transformationPropertyParser = new TransformationPropertyParser(this);
            this.transformationPropertyParser.ParseDynamicProperties();
            this.transformationPropertyParser.collection.AddEnabledRecipes();
            MultiblockManager.ValidateMultiblock(this.GetBlockName());
        }


        /**
         * This happens when the block is placed in the world.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            Block block = Block.list[_result.blockValue.type];
            if (block.shape.IsTerrain())
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
            }
            else if (!block.IsTerrainDecoration)
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
            }
            else
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
            }

            TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_result.clrIdx, _result.blockPos) as TileEntityMultiblockMasterTransformer;
            if (tileEntityBlockTransformer == null)
            {
                Log.Warning("Failed to create tile entity");
                return;
            }
            if (_ea != null && _ea.entityType == EntityType.Player)
            {
                tileEntityBlockTransformer.bPlayerStorage = true;
                tileEntityBlockTransformer.worldTimeTouched = _world.GetWorldTime();
                tileEntityBlockTransformer.SetEmpty();
            }
        }


        /**
         * This is the activation text that displays when the player looks at this block.
         * It will display whether the block is working, and if not, what conditions need to be fulfilled to make it work.
         */

        public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
            if (tileEntityBlockTransformer == null)
            {
                return string.Empty;
            }
            string lBlockName = Localization.Get(Block.list[_blockValue.type].GetBlockName());
            PlayerActionsLocal playerInput = ((EntityPlayerLocal)_entityFocusing).playerInput;
            string playerKey = playerInput.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain, null) + playerInput.PermanentActions.Activate.GetBindingXuiMarkupString(XUiUtils.EmptyBindingStyle.EmptyString, XUiUtils.DisplayStyle.Plain, null);

            string tooltip = "";
            if (!tileEntityBlockTransformer.MultiblockFormed())
            {
                return Localization.Get("ttMultiblockNotFormed");
            }


            // If no power, display this when looking at the block.
            if (this.transformationPropertyParser.requirePower && !tileEntityBlockTransformer.HasRequiredPower)
            {
                tooltip = Localization.Get("transformerMultiblockTooltipNoPower");
                return string.Format(tooltip, playerKey, lBlockName);
            }

            // If no heat, display this when looking at the block.
            else if (this.transformationPropertyParser.requireHeat && !tileEntityBlockTransformer.HasRequiredHeat)
            {
                tooltip = Localization.Get("transformerTooltipNoHeat");
                List<string> lHeatSources = new List<string>();
                if (this.transformationPropertyParser.heatSources.Count > 0)
                {
                    foreach (string heatSource in this.transformationPropertyParser.heatSources)
                    {
                        lHeatSources.Add(Localization.Get(heatSource));
                    }
                }
                string sources = StringHelpers.WriteListToString(lHeatSources);
                return string.Format(tooltip, playerKey, lBlockName, sources);
            }

            // Checks we have nearby blocks
            if (this.transformationPropertyParser.requireNearbyBlocks && !tileEntityBlockTransformer.HasRequiredNearbyBlocks)
            {
                tooltip = Localization.Get("transformerTooltipNoBlocksNearby");
                List<string> lNearbyBlocks = new List<string>();
                if (this.transformationPropertyParser.nearbyBlockNames.Count > 0)
                {
                foreach (string blockName in this.transformationPropertyParser.nearbyBlockNames)
                    {
                        lNearbyBlocks.Add(Localization.Get(blockName));
                    }
                }

                if (this.transformationPropertyParser.nearbyBlockTags.Count > 0)
                {
                    foreach (string blockTag in this.transformationPropertyParser.nearbyBlockTags)
                    {
                        lNearbyBlocks.Add(blockTag);
                    }
                }

                string sources = StringHelpers.WriteListToString(lNearbyBlocks);
                string needed = this.transformationPropertyParser.nearbyBlockCount.ToString();

                return string.Format(tooltip, playerKey, lBlockName, needed, sources);
            }

            // If all else fails, the block must be empty so needs items.
            if (!tileEntityBlockTransformer.IsCurrentlyWorking)
            {
                tooltip = Localization.Get("transformerTooltipReady");
                return string.Format(tooltip, playerKey, lBlockName);
            }

            // Transforming here...
            tooltip = Localization.Get(tileEntityBlockTransformer.IsCrafting() ? "transformerTooltipWorking" : "transformerTooltipReady");
            return string.Format(tooltip, playerKey, lBlockName);
        }


        /**
         * When block is added to the game.
         */
        public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (_blockValue.ischild)
            {
                return;
            }
            this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
            if (this.isMultiBlock)
            {
                this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
            }

            if (LootContainer.GetLootContainer(this.lootList, true) == null)
            {
                return;
            }
            this.addTileEntity(world, _chunk, _blockPos, _blockValue);
            this.checkParticles(world, _chunk.ClrIdx, _blockPos, _blockValue);
        }


        /**
         * If neighbor value changes, apply changes.
         */

        public override void OnNeighborBlockChange(WorldBase world, int _clrIdx, Vector3i _myBlockPos, BlockValue _myBlockValue, Vector3i _blockPosThatChanged, BlockValue _newNeighborBlockValue, BlockValue _oldNeighborBlockValue)
        {
            Transform blockParticleEffect;
            if (_myBlockPos == _blockPosThatChanged + Vector3i.up && Block.list[_newNeighborBlockValue.type].shape.IsTerrain() && Block.list[_myBlockValue.type].IsTerrainDecoration && this.particleName != null && (blockParticleEffect = world.GetGameManager().GetBlockParticleEffect(_myBlockPos)) != null)
            {
                float num = 0f;
                if (_myBlockPos.y > 0)
                {
                    sbyte density = world.GetDensity(_clrIdx, _myBlockPos.x, _myBlockPos.y, _myBlockPos.z);
                    sbyte density2 = world.GetDensity(_clrIdx, _myBlockPos.x, _myBlockPos.y - 1, _myBlockPos.z);
                    num = MarchingCubes.GetDecorationOffsetY(density, density2);
                }
                blockParticleEffect.localPosition = new Vector3((float)_myBlockPos.x, (float)_myBlockPos.y + num, (float)_myBlockPos.z) + this.getParticleOffset(_myBlockValue);
            }
        }


        /**
         * This is called when block is removed. We want to destroy existing tile entity too.
         */

        public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (!_blockValue.ischild)
            {
                this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
                if (this.isMultiBlock)
                {
                    this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                    return;
                }
            }
            else if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
            }

            TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_chunk.ClrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
            if (tileEntityBlockTransformer != null)
            {
                tileEntityBlockTransformer.OnDestroy();
            }
            this.removeTileEntity(_world, _chunk, _blockPos, _blockValue);
            this.removeParticles(_world, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);
        }


        /**
         * When block value changes.
         */

        public override void OnBlockValueChanged(WorldBase world, Chunk _chunk, int _clrIdx, Vector3i _blockPos, BlockValue _oldBlockValue, BlockValue _newBlockValue)
        {
            base.OnBlockValueChanged(world, _chunk, _clrIdx, _blockPos, _oldBlockValue, _newBlockValue);
            Transform blockParticleEffect;
            if (_oldBlockValue.rotation != _newBlockValue.rotation && this.particleName != null && (blockParticleEffect = world.GetGameManager().GetBlockParticleEffect(_blockPos)) != null)
            {
                Vector3 particleOffset = this.getParticleOffset(_oldBlockValue);
                Vector3 particleOffset2 = this.getParticleOffset(_newBlockValue);
                blockParticleEffect.localPosition -= particleOffset;
                blockParticleEffect.localPosition += particleOffset2;
                blockParticleEffect.localRotation = this.shape.GetRotation(_newBlockValue);
            }
        }


        /**
         * When block is loaded
         */

        public override void OnBlockLoaded(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockLoaded(_world, _clrIdx, _blockPos, _blockValue);
            if (this.particleName != null)
            {
                if (GameManager.IsDedicatedServer && BlockMultiblockMasterTransformer.particleLights.ContainsKey(this.particleHash) && BlockMultiblockMasterTransformer.particleLights[this.particleHash].Count > 0)
                {
                    this.dediLights = new List<Light>();
                    for (int i = 0; i < BlockMultiblockMasterTransformer.particleLights[this.particleHash].Count; i++)
                    {
                        Light light = UnityEngine.Object.Instantiate<Light>(BlockMultiblockMasterTransformer.particleLights[this.particleHash][i]);
                        Vector3 a;
                        a.x = (float)_blockPos.x;
                        a.y = (float)_blockPos.y;
                        a.z = (float)_blockPos.z;
                        light.transform.position = a + this.getParticleOffset(_blockValue) - Origin.position;
                        light.transform.parent = BlockMultiblockMasterTransformer.HierarchyParent.transform;
                        this.dediLights.Add(light);
                        LightManager.RegisterLight(light);
                    }
                }
                this.checkParticles(_world, _clrIdx, _blockPos, _blockValue);
            }
        }


        /**
         * When block is unloaded
         */

        public override void OnBlockUnloaded(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockUnloaded(_world, _clrIdx, _blockPos, _blockValue);
            this.removeParticles(_world, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);
        }


        /**
         * Helper method to add and set up a tile entity for the block.
         */

        protected override void addTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = new TileEntityMultiblockMasterTransformer(_chunk);
            tileEntityBlockTransformer.localChunkPos = World.toBlock(_blockPos);
            tileEntityBlockTransformer.lootListName = this.lootList;
            tileEntityBlockTransformer.SetContainerSize(LootContainer.GetLootContainer(this.lootList, true).size, true);
            tileEntityBlockTransformer.SetTransformationCollection(this.transformationPropertyParser.collection);
            tileEntityBlockTransformer.SetRequirePower(
                this.transformationPropertyParser.requirePower,
                this.transformationPropertyParser.powerSources
            );
            tileEntityBlockTransformer.SetRequireHeat(
                this.transformationPropertyParser.requireHeat,
                this.transformationPropertyParser.heatSources
            );
            tileEntityBlockTransformer.SetRequireNearbyBlocks(
                this.transformationPropertyParser.requireNearbyBlocks,
                this.transformationPropertyParser.nearbyBlockNames,
                this.transformationPropertyParser.nearbyBlockTags,
                this.transformationPropertyParser.requireAllTags,
                this.transformationPropertyParser.nearbyBlockRange,
                this.transformationPropertyParser.nearbyBlockCount
            );
            tileEntityBlockTransformer.SetRequireUserAccess(
                this.transformationPropertyParser.requireUserAccess
            );
            tileEntityBlockTransformer.CalculateLookupCoordinates();
            tileEntityBlockTransformer.SetMasterBlockName(this.GetBlockName());
            _chunk.AddTileEntity(tileEntityBlockTransformer);
        }


        /**
         * Helper method to remove a tile entity.
         */

        protected override void removeTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            _chunk.RemoveTileEntityAt<TileEntityMultiblockMasterTransformer>((World)world, World.toBlock(_blockPos));
        }


        /**
         * Helper method to get particle offset.
         */

        protected virtual Vector3 getParticleOffset(BlockValue _blockValue)
        {
            return this.shape.GetRotation(_blockValue) * (this.offset - new Vector3(0.5f, 0.5f, 0.5f)) + new Vector3(0.5f, 0.5f, 0.5f);
        }


        /**
         * Checks whether particles can be loaded.
         */

        public virtual void checkParticles(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
        {
            switch (_blockValue.meta)
            {
                case (byte)1:
                    if (this.particleName != null && !_world.GetGameManager().HasBlockParticleEffect(_blockPos))
                    {
                        this.addParticles(_world, _clrIdx, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);
                    }
                    break;
                default:
                    this.removeParticles(_world, _blockPos.x, _blockPos.y, _blockPos.z, _blockValue);
                    break;
            }
        }


        /**
         * Helper method to add particle effect.
         */

        protected virtual void addParticles(WorldBase _world, int _clrIdx, int _x, int _y, int _z, BlockValue _blockValue)
        {
            if (this.particleName == null || this.particleName == "")
            {
                return;
            }
            float num = 0f;
            if (_y > 0 && Block.list[_blockValue.type].IsTerrainDecoration && Block.list[_world.GetBlock(_x, _y - 1, _z).type].shape.IsTerrain())
            {
                sbyte density = _world.GetDensity(_clrIdx, _x, _y, _z);
                sbyte density2 = _world.GetDensity(_clrIdx, _x, _y - 1, _z);
                num = MarchingCubes.GetDecorationOffsetY(density, density2);
            }
            _world.GetGameManager().SpawnBlockParticleEffect(new Vector3i(_x, _y, _z), new ParticleEffect(this.particleName, new Vector3((float)_x, (float)_y + num, (float)_z) + this.getParticleOffset(_blockValue), this.shape.GetRotation(_blockValue), 1f, Color.white));
        }


        /**
         * Helper method to remove particle effects.
         */

        protected virtual void removeParticles(WorldBase _world, int _x, int _y, int _z, BlockValue _blockValue)
        {
            if (GameManager.IsDedicatedServer && this.dediLights != null)
            {
                for (int i = 0; i < this.dediLights.Count; i++)
                {
                    LightManager.UnRegisterLight(this.dediLights[i].transform.position + Origin.position, this.dediLights[i].range);
                    UnityEngine.Object.Destroy(this.dediLights[i]);
                }
                this.dediLights.Clear();
            }
            _world.GetGameManager().RemoveBlockParticleEffect(new Vector3i(_x, _y, _z));
        }



        /**
         * Called when a block is destroyed by an entity. It cam be useful for separating things out if needed depending on what
         * entity destroyed it.
         */

        public override Block.DestroyedResult OnBlockDestroyedBy(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, int _entityId, bool _bUseHarvestTool)
        {
            TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _world.GetTileEntity(_clrIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
            if (tileEntityBlockTransformer != null)
            {
                tileEntityBlockTransformer.OnDestroy();
            }
            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(GameManager.Instance.World.GetEntity(_entityId) as EntityPlayerLocal);
            if (null != uiforPlayer && uiforPlayer.windowManager.IsWindowOpen("looting") && ((XUiC_LootWindow)uiforPlayer.xui.GetWindow("windowLooting").Controller).GetLootBlockPos() == _blockPos)
            {
                uiforPlayer.windowManager.Close("looting");
            }
            if (tileEntityBlockTransformer != null)
            {
                _world.GetGameManager().DropContentOfLootContainerServer(_blockValue, _blockPos, tileEntityBlockTransformer.entityId);
            }
            return Block.DestroyedResult.Downgrade;
        }


        /**
         * If multiblock is not formed properly will display a message.
         * Otherwise will open the inventory.
         */

        public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            if (_player.inventory.IsHoldingItemActionRunning())
            {
                return false;
            }
            TileEntityMultiblockMasterTransformer tileEntityTransformer = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityMultiblockMasterTransformer;
            if (tileEntityTransformer == null)
            {
                return false;
            }

            if (!tileEntityTransformer.MultiblockFormed())
            {
                EntityPlayerLocal _playerLocal = _player as EntityPlayerLocal;
                if (_playerLocal != null)
                {
                    GameManager.ShowTooltip(_playerLocal as EntityPlayerLocal, Localization.Get("ttMultiblockNotFormed"), String.Empty, "ui_denied", null);
                }
                return false;
            }

            _player.AimingGun = false;
            Vector3i blockPos = tileEntityTransformer.ToWorldPos();
            tileEntityTransformer.bWasTouched = tileEntityTransformer.bTouched;
            _world.GetGameManager().TELockServer(_cIdx, blockPos, tileEntityTransformer.entityId, _player.entityId, null);
            return true;
        }


        /**
         * Gets activation based on command when you hold E
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            return _indexInBlockActivationCommands == 0 && this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
        }


        /**
         * Returns the commands to activate the block looking at.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("Search", "search", true)
        };

        private string particleName;
        private Vector3 offset;
        private int particleHash;
        private List<Light> dediLights;
        private static Dictionary<int, List<Light>> particleLights;
        private static GameObject HierarchyParent = new GameObject("BlockParticleLights");

        public WorkstationData WorkstationData;

        // Transformation properties to pass on to the tile entity.
        private TransformationPropertyParser transformationPropertyParser;
        private string Structure;
    }
}