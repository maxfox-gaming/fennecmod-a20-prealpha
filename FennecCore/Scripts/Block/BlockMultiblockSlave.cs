﻿using System.Globalization;

namespace FennecCore.Scripts
{
    /**
     *  <!-- Multiblock ckass you would add in XML -->
     *  <property name="Class"          value="MultiblockSlave, Mods" />
     *  <property name="MultiblockType" value="" /><!-- Optional: Leaving empty or null will make this the name of the block in XML. -->
     */

    public class BlockMultiblockSlave : Block, IBlockMultiblockComponent
    {
        /**
         * Constructor, just let you know we have a tile entity.
         */

        public BlockMultiblockSlave()
        {
            this.HasTileEntity = true;
        }


        /**
         * Initializes properties with TakeDelay
         */

        public override void Init()
        {
            base.Init();
            // Initialize the type of the multiblock to the specified value, or set it to the block name.
            if (this.Properties.Values.ContainsKey("MultiblockType") && this.Properties.Values["MultiblockType"] != "")
            {
                this.multiblockType = this.Properties.Values["MultiblockType"];
                MultiblockManager.AddBlockType(this.multiblockType);
            }
            else
            {
                this.multiblockType = this.GetBlockName();
            }

            // If a take delay is specified needs to be set to whatever value.
            if (this.Properties.Values.ContainsKey("TakeDelay"))
            {
                this.TakeDelay = StringParsers.ParseFloat(this.Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
            }
            else
            {
                this.TakeDelay = 2f;
            }
        }


        /**
         * When block is added to the world, add a MultiblockSlave TileEntity to the chunk.
         */

        public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
            if (_blockValue.ischild)
            {
                return;
            }

            TileEntityMultiblockSlave te = new TileEntityMultiblockSlave(_chunk)
            {
                localChunkPos = World.toBlock(_blockPos),
            };
            te.SetSlaveType(this.multiblockType);
            _chunk.AddTileEntity(te);
        }


        /**
         * If block is removed or destroyed, remove the tile entity (and will notify the master block since it's called in OnDestroy() event.
         */

        public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
            _chunk.RemoveTileEntityAt<TileEntityMultiblockSlave>((World)world, World.toBlock(_blockPos));
        }


        /**
         * When block is placed, set the tile entity as player placed.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            base.PlaceBlock(_world, _result, _ea);
            TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
            if (tileEntityMultiblockSlave != null)
            {
                tileEntityMultiblockSlave.IsPlayerPlaced = true;
            }
        }


        /**
         * When the block is activated with the E key, we want to notify the MASTER block. 
         * If the master block is registering that the structure is complete, we then want to activate THAT block and its interface via the slave one.
         * If no master is found, then no worries, nothing to do.
         */

        public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_cIdx, _blockPos);
            if (tileEntityMultiblockSlave == null)
            {
                return false;
            }

            ITileEntityMultiblockMaster master = tileEntityMultiblockSlave.GetMaster();
            if (master == null)
            {
                return false;
            }

            if (!(master is TileEntity))
            {
                return false;
            }

            if (!master.MultiblockFormed())
            {
                return false;
            }

            // Handles finding the actual block and chunk needed to interact with.
            Vector3i newBlockPos = ((TileEntity)master).ToWorldPos();
            BlockValue bv = _world.GetBlock(newBlockPos);
            int cIdx = ((Chunk)_world.GetChunkFromWorldPos(newBlockPos)).ClrIdx;

            // Set that block to activate instead.
            bv.Block.OnBlockActivated(_world, _cIdx, newBlockPos, bv, _player);
            _player.AimingGun = false;
            return true;
        }


        /**
         * If the block value changes, we want to notify the master block if there is one that the structure may not be valid anymore, triggering it to check once again for validity.
         */

        public override void OnBlockValueChanged(WorldBase _world, Chunk _chunk, int _clrIdx, Vector3i _blockPos, BlockValue _oldBlockValue, BlockValue _newBlockValue)
        {
            base.OnBlockValueChanged(_world, _chunk, _clrIdx, _blockPos, _oldBlockValue, _newBlockValue);
            TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockSlave == null)
            {
                return;
            }

            ITileEntityMultiblockMaster master = tileEntityMultiblockSlave.GetMaster();
            if (master == null)
            {
                return;
            }

            if (!(master is TileEntity))
            {
                return;
            }

            master.SetMultiblockNotFormed();
        }


        /**
         * Returns the light value of the block.
         */

        public override byte GetLightValue(BlockValue _blockValue)
        {
            return 0;
        }


        /**
         * Returns true if block is emitting light or turned on.
         */

        public static bool IsLit(BlockValue _blockValue)
        {
            return _blockValue.meta > 0;
        }


        /**
         * When you look at the block, this activation text will appear.
         */

        public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockSlave == null)
            {
                return "";
            }

            ITileEntityMultiblockMaster master = tileEntityMultiblockSlave.GetMaster();
            if (master == null)
            {
                if (!tileEntityMultiblockSlave.HasMaster)
                {
                    return Localization.Get("multiblockNoMaster");
                }

                master = _world.GetTileEntity(_clrIdx, tileEntityMultiblockSlave.GetMasterPos()) as ITileEntityMultiblockMaster;
                if (master == null)
                {
                    return Localization.Get("multiblockNoMaster");
                }
            }

            if (!(master is TileEntity))
            {
                return Localization.Get("multiblockNoMaster");
            }

            if (!master.MultiblockFormed())
            {
                return Localization.Get("multiblockMasterNotFormed");
            }

            // Handles finding the actual block and chunk needed to interact with.
            Vector3i masterPos = ((TileEntity)master).ToWorldPos();
            BlockValue bv = _world.GetBlock(masterPos);
            int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;

            return bv.Block.GetActivationText(_world, bv, cIdx, masterPos, _entityFocusing);
        }


        /**
         * This occurs when you press and hold the E key. We need to find the master block and use that.
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            // Checks whether we have the block activation commands from THIS block or the MASTER block
            int totalCommandsCount = this.GetBlockActivationCommands(_world, _blockValue, _cIdx, _blockPos, _player).Length;
            int slaveCommandsCount = this.cmds.Length;
            int masterCommandsCount = totalCommandsCount - slaveCommandsCount;

            bool extendedCommands = (masterCommandsCount > 0);
            bool commandingFromMaster = (extendedCommands && _indexInBlockActivationCommands < masterCommandsCount);

            // If we are commanding from the master block via this one, we want to do THAT command instead.
            if (commandingFromMaster)
            {
                TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_cIdx, _blockPos);
                if (tileEntityMultiblockSlave.HasMaster)
                {
                    Vector3i masterPos = tileEntityMultiblockSlave.GetMasterPos();
                    BlockValue bv = _world.GetBlock(masterPos);
                    int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;
                    return bv.Block.OnBlockActivated(_indexInBlockActivationCommands, _world, cIdx, masterPos, bv, _player);
                }
                return false;
            }

            // If not, we need to check our own commands
            if (_indexInBlockActivationCommands == masterCommandsCount)
            {
                this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
                return true;
            }

            return false;
        }


        /**
         * Returns the appropriate commands from the block.
         * If the master block is not found, just return a 'pickup' command.
         * If the master block IS found, return the master block commands AND the slave pickup command.
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            BlockActivationCommand[] masterCommands = null;
            BlockActivationCommand[] slaveCommands = null;

            TileEntityMultiblockSlave tileEntityMultiblockSlave = (TileEntityMultiblockSlave)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockSlave == null)
            {
                return null;
            }

            if (tileEntityMultiblockSlave.Formed() && tileEntityMultiblockSlave.HasMaster)
            {
                Vector3i masterPos = tileEntityMultiblockSlave.GetMasterPos();
                BlockValue bv = _world.GetBlock(masterPos);
                int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;
                masterCommands = bv.Block.GetBlockActivationCommands(_world, bv, cIdx, masterPos, _entityFocusing);
            }

            bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
            bool flag2 = false;
            if (tileEntityMultiblockSlave != null)
            {
                flag2 = tileEntityMultiblockSlave.IsPlayerPlaced;
            }
            this.cmds[0].enabled = (flag && flag2 && this.TakeDelay > 0f);
            slaveCommands = this.cmds;
            return ArrayHelpers.Concatenate<BlockActivationCommand>(masterCommands, slaveCommands);
        }


        /**
         * Handles the taking of the block.
         */

        public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            if (_blockValue.damage > 0)
            {
                GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI playerUI = (_player as EntityPlayerLocal).PlayerUI;
            playerUI.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUI.xui.GetChildByType<XUiC_Timer>();
            TimerEventData timerEventData = new TimerEventData();
            timerEventData.Data = new object[]
            {
            _cIdx,
            _blockValue,
            _blockPos,
            _player
            };
            timerEventData.Event += this.EventData_Event;
            childByType.SetTimer(this.TakeDelay, timerEventData, -1f, "");
        }


        /**
         * Timer take event
         */

        private void EventData_Event(TimerEventData timerData)
        {
            World world = GameManager.Instance.World;
            object[] array = (object[])timerData.Data;
            int clrIdx = (int)array[0];
            BlockValue blockValue = (BlockValue)array[1];
            Vector3i vector3i = (Vector3i)array[2];
            BlockValue block = world.GetBlock(vector3i);
            EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
            if (block.damage > 0)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
                return;
            }
            if (block.type != blockValue.type)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), string.Empty, "ui_denied", null);
                return;
            }
            TileEntityMultiblockSlave tileEntityMultiblockSlave = world.GetTileEntity(clrIdx, vector3i) as TileEntityMultiblockSlave;
            if (tileEntityMultiblockSlave.IsUserAccessing())
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), string.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
            world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
        }


        protected string multiblockType;
        private float TakeDelay = 2f;

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("take", "hand", false)
        };
    }
}
