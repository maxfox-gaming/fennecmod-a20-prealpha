﻿using System.Collections;
using UnityEngine;

namespace FennecCore.Scripts
{
	public class BlockMultiblockSlavePowered : BlockPowered, IBlockMultiblockComponent
	{
		/**
		 * Constructor
		 */

		public BlockMultiblockSlavePowered()
		{
			this.HasTileEntity = true;
		}


		/**
		 * Initializes properties with TakeDelay
		 */

		public override void Init()
		{
			base.Init();
			// Initialize the type of the multiblock to the specified value, or set it to the block name.
			if (this.Properties.Values.ContainsKey("MultiblockType") && this.Properties.Values["MultiblockType"] != "")
			{
				this.multiblockType = this.Properties.Values["MultiblockType"];
				MultiblockManager.AddBlockType(this.multiblockType);
			}
			else
			{
				this.multiblockType = this.GetBlockName();
			}
		}


		/**
		 * When block is added to the world, add a MultiblockSlave TileEntity to the chunk.
		 */

		public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
			if (_blockValue.ischild)
			{
				return;
			}

			TileEntityMultiblockSlavePowered te = new TileEntityMultiblockSlavePowered(_chunk)
			{
				localChunkPos = World.toBlock(_blockPos),
			};
			te.SetSlaveType(this.multiblockType);
			_chunk.AddTileEntity(te);
		}


		/**
		 * When block is placed, set the tile entity as player placed.
		 */

		public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
		{
			base.PlaceBlock(_world, _result, _ea);
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
			if (tileEntityMultiblockSlavePowered != null)
			{
				tileEntityMultiblockSlavePowered.IsPlayerPlaced = true;
			}
		}


		/**
		 * After activating block, do any animations to the model.
		 */

		public override void OnBlockEntityTransformAfterActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
		{
			this.shape.OnBlockEntityTransformBeforeActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
			if (_blockValue.ischild)
			{
				return;
			}
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = _world.GetTileEntity(_cIdx, _blockPos) as TileEntityMultiblockSlavePowered;
			if (tileEntityMultiblockSlavePowered != null)
			{
				tileEntityMultiblockSlavePowered.BlockTransform = _ebcd.transform;
				GameManager.Instance.StartCoroutine(this.drawWiresLater(tileEntityMultiblockSlavePowered));
				if (tileEntityMultiblockSlavePowered.GetParent().y != -9999)
				{
					IPowered powered = _world.GetTileEntity(0, tileEntityMultiblockSlavePowered.GetParent()) as IPowered;
					if (powered != null)
					{
						GameManager.Instance.StartCoroutine(this.drawWiresLater(powered));
					}
				}
			}
		}


		/**
		 * When the block is activated with the E key, we want to notify the MASTER block. 
		 * If the master block is registering that the structure is complete, we then want to activate THAT block and its interface via the slave one.
		 * If no master is found, then no worries, nothing to do.
		 */

		public override bool OnBlockActivated(WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
		{
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_cIdx, _blockPos);
			if (tileEntityMultiblockSlavePowered == null)
			{
				return false;
			}

			ITileEntityMultiblockMaster master = tileEntityMultiblockSlavePowered.GetMaster();
			if (master == null)
			{
				return false;
			}

			if (!(master is TileEntity))
			{
				return false;
			}

			if (!master.MultiblockFormed())
			{
				return false;
			}

			// Handles finding the actual block and chunk needed to interact with.
			Vector3i newBlockPos = ((TileEntity)master).ToWorldPos();
			BlockValue bv = _world.GetBlock(newBlockPos);
			int cIdx = ((Chunk)_world.GetChunkFromWorldPos(newBlockPos)).ClrIdx;

			// Set that block to activate instead.
			bv.Block.OnBlockActivated(_world, _cIdx, newBlockPos, bv, _player);
			_player.AimingGun = false;
			return true;
		}


		/**
		 * If the block value changes, we want to notify the master block if there is one that the structure may not be valid anymore, triggering it to check once again for validity.
		 */

		public override void OnBlockValueChanged(WorldBase _world, Chunk _chunk, int _clrIdx, Vector3i _blockPos, BlockValue _oldBlockValue, BlockValue _newBlockValue)
		{
			base.OnBlockValueChanged(_world, _chunk, _clrIdx, _blockPos, _oldBlockValue, _newBlockValue);
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_clrIdx, _blockPos);
			if (tileEntityMultiblockSlavePowered == null)
			{
				return;
			}

			ITileEntityMultiblockMaster master = tileEntityMultiblockSlavePowered.GetMaster();
			if (master == null)
			{
				return;
			}

			if (!(master is TileEntity))
			{
				return;
			}

			master.SetMultiblockNotFormed();
		}


		/**
		 * Marks the tile entity to draw wire to its parent.
		 */

		private IEnumerator drawWiresLater(IPowered powered)
		{
			yield return new WaitForSeconds(0.5f);
			powered.DrawWires();
			yield break;
		}


		/**
		 * Remove tile entity from the world once block is taken away or destroyed.
		 */

		public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
		{
			if (!_blockValue.ischild)
			{
				this.shape.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
				if (this.isMultiBlock)
				{
					this.multiBlockPos.RemoveChilds(world, _chunk.ClrIdx, _blockPos, _blockValue);
					return;
				}
			}
			else if (this.isMultiBlock)
			{
				this.multiBlockPos.RemoveParentBlock(world, _chunk.ClrIdx, _blockPos, _blockValue);
			}

			if (_blockValue.ischild)
			{
				return;
			}

			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = _chunk.GetTileEntity(World.toBlock(_blockPos)) as TileEntityMultiblockSlavePowered;
			if (tileEntityMultiblockSlavePowered != null)
			{
				if (!GameManager.IsDedicatedServer)
				{
					EntityPlayerLocal primaryPlayer = GameManager.Instance.World.GetPrimaryPlayer();
					if (primaryPlayer.inventory.holdingItem.Actions[1] is ItemActionConnectPowerOverride)
					{
						(primaryPlayer.inventory.holdingItem.Actions[1] as ItemActionConnectPowerOverride).CheckForWireRemoveNeeded(primaryPlayer, _blockPos);
					}
				}
				if (SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
				{
					PowerManager.Instance.RemovePowerNode(tileEntityMultiblockSlavePowered.GetPowerItem());
				}
				if (tileEntityMultiblockSlavePowered.GetParent().y != -9999)
				{
					IPowered powered = world.GetTileEntity(0, tileEntityMultiblockSlavePowered.GetParent()) as IPowered;
					if (powered != null && SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
					{
						powered.SendWireData();
					}
				}
				tileEntityMultiblockSlavePowered.RemoveWires();
			}
			_chunk.RemoveTileEntityAt<TileEntityMultiblockSlavePowered>((World)world, World.toBlock(_blockPos));
		}

		/**
		 * When you look at the block, this activation text will appear.
		 */

		public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
		{
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_clrIdx, _blockPos);
			if (tileEntityMultiblockSlavePowered == null)
			{
				return "";
			}

			ITileEntityMultiblockMaster master = tileEntityMultiblockSlavePowered.GetMaster();
			if (master == null)
			{
				return Localization.Get("multiblockNoMaster");
			}

			if (!(master is TileEntity))
			{
				return Localization.Get("multiblockNoMaster");
			}

			if (!master.MultiblockFormed())
			{
				return Localization.Get("multiblockMasterNotFormed");
			}

			// Handles finding the actual block and chunk needed to interact with.
			Vector3i masterPos = ((TileEntity)master).ToWorldPos();
			BlockValue bv = _world.GetBlock(masterPos);
			int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;

			return bv.Block.GetActivationText(_world, bv, cIdx, masterPos, _entityFocusing);
		}


		/**
		* This occurs when you press and hold the E key. We need to find the master block and use that.
		*/

		public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
		{
			// Checks whether we have the block activation commands from THIS block or the MASTER block
			int totalCommandsCount = this.GetBlockActivationCommands(_world, _blockValue, _cIdx, _blockPos, _player).Length;
			int slaveCommandsCount = this.cmds.Length;
			int masterCommandsCount = totalCommandsCount - slaveCommandsCount;

			bool extendedCommands = (masterCommandsCount > 0);
			bool commandingFromMaster = (extendedCommands && _indexInBlockActivationCommands < masterCommandsCount);

			// If we are commanding from the master block via this one, we want to do THAT command instead.
			if (commandingFromMaster)
			{
				TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_cIdx, _blockPos);
				ITileEntityMultiblockMaster master = tileEntityMultiblockSlavePowered.GetMaster();
				if (master != null && master is TileEntity && master.MultiblockFormed())
				{
					Vector3i masterPos = ((TileEntity)master).ToWorldPos();
					BlockValue bv = _world.GetBlock(masterPos);
					int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;
					return bv.Block.OnBlockActivated(_indexInBlockActivationCommands, _world, cIdx, masterPos, bv, _player);
				}
				return false;
			}

			// If not, we need to check our own commands
			if (_indexInBlockActivationCommands == masterCommandsCount)
			{
				this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
				return true;
			}

			return false;
		}


		/**
		* Returns the appropriate commands from the block.
		* If the master block is not found, just return a 'pickup' command.
		* If the master block IS found, return the master block commands AND the slave pickup command.
		*/

		public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
		{
			BlockActivationCommand[] masterCommands = null;
			BlockActivationCommand[] slaveCommands = null;

			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = (TileEntityMultiblockSlavePowered)_world.GetTileEntity(_clrIdx, _blockPos);
			ITileEntityMultiblockMaster master = tileEntityMultiblockSlavePowered.GetMaster();
			if (master != null && master is TileEntity && master.MultiblockFormed())
			{
				Vector3i masterPos = ((TileEntity)master).ToWorldPos();
				BlockValue bv = _world.GetBlock(masterPos);
				int cIdx = ((Chunk)_world.GetChunkFromWorldPos(masterPos)).ClrIdx;
				masterCommands = bv.Block.GetBlockActivationCommands(_world, bv, cIdx, masterPos, _entityFocusing);
			}

			bool flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
			bool flag2 = false;
			if (tileEntityMultiblockSlavePowered != null)
			{
				flag2 = tileEntityMultiblockSlavePowered.IsPlayerPlaced;
			}
			this.cmds[0].enabled = (flag && flag2 && this.TakeDelay > 0f);
			slaveCommands = this.cmds;
			return ArrayHelpers.Concatenate<BlockActivationCommand>(masterCommands, slaveCommands);
		}


		/**
		* Handles the taking of the block.
		*/

		public new void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
		{
			if (_blockValue.damage > 0)
			{
				GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
				return;
			}
			LocalPlayerUI playerUI = (_player as EntityPlayerLocal).PlayerUI;
			playerUI.windowManager.Open("timer", true, false, true);
			XUiC_Timer childByType = playerUI.xui.GetChildByType<XUiC_Timer>();
			TimerEventData timerEventData = new TimerEventData();
			timerEventData.Data = new object[]
			{
			_cIdx,
			_blockValue,
			_blockPos,
			_player
			};
			timerEventData.Event += this.EventData_Event;
			childByType.SetTimer(this.TakeDelay, timerEventData, -1f, "");
		}


		/**
		 * Timer take event
		 */

		private void EventData_Event(TimerEventData timerData)
		{
			World world = GameManager.Instance.World;
			object[] array = (object[])timerData.Data;
			int clrIdx = (int)array[0];
			BlockValue blockValue = (BlockValue)array[1];
			Vector3i vector3i = (Vector3i)array[2];
			BlockValue block = world.GetBlock(vector3i);
			EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
			if (block.damage > 0)
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
				return;
			}
			if (block.type != blockValue.type)
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), string.Empty, "ui_denied", null);
				return;
			}
			TileEntityMultiblockSlavePowered tileEntityMultiblockSlavePowered = world.GetTileEntity(clrIdx, vector3i) as TileEntityMultiblockSlavePowered;
			if (tileEntityMultiblockSlavePowered.IsUserAccessing())
			{
				GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttCantPickupInUse"), string.Empty, "ui_denied", null);
				return;
			}
			LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
			world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
		}


		/**
		 * Create a tile entity
		 */

		public new TileEntityMultiblockSlavePowered CreateTileEntity(Chunk chunk)
		{
			return new TileEntityMultiblockSlavePowered(chunk);
		}


		/**
		 * Block activation commands
		 */

		private BlockActivationCommand[] cmds = new BlockActivationCommand[]
		{
		new BlockActivationCommand("take", "hand", false)
		};


		private string multiblockType;
	}
}