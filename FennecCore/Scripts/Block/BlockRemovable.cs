﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace FennecCore.Scripts
{
    public class BlockRemovable : Block
    {
        /**
         * items.xml:
         * <!-- Adds the capability to have blocks removable with use of an optional tool. -->
         * <property name="Class"           value="BlockRemovable, Mods" />
         * <property name="TakeTools"       value="Item1,Item2,Item3,..." /> <!-- optional, if left blank will allow block to be taken with no requirements. -->
         * <property name="TakeDelays"      value="Delay1, Delay2, Delay3,..."/> <!-- optional, if left blank will use TakeDelay for all tools. -->
         * <property name="ClaimRequired"   value="true/false" /> <!-- Optional, defaults to false. Only allows blocks to be removed if land claimed area by player. -->
         */


        /**
         * When block is initialised
         */

        public override void Init()
        {
            base.Init();
            if (this.Properties.Values.ContainsKey("TakeDelay"))
            {
                this.TakeDelay = StringParsers.ParseFloat(this.Properties.Values["TakeDelay"], 0, -1, NumberStyles.Any);
            }
            else
            {
                this.TakeDelay = 2f;
            }
        }


        /**
         * Adds properties for other items and blocks for tool requirements.
         */

        public override void LateInit()
        {
            base.LateInit();

            this.TakeToolsDelays = new Dictionary<string, float>();
            this.ClaimRequired = false;
            this.TakeToolRequired = false;

            if (!this.Properties.Values.ContainsKey("TakeTools"))
            {
                this.TakeTools = new List<string>();
                this.TakeDelays = new List<float>();
                return;
            }


            string takeTools = this.Properties.Values["TakeTools"];
            this.TakeTools = StringHelpers.WriteStringToList(takeTools);

            foreach (string tool in this.TakeTools)
            {
                if ((ItemClass.GetItem(tool)).IsEmpty())
                {
                    throw new Exception("Item with name " + tool + " not found.");
                }
            }

            this.TakeToolRequired = true;
            if (this.Properties.Values.ContainsKey("TakeDelays"))
            {
                string takeDelays = this.Properties.Values["TakeDelays"];
                this.TakeDelays = StringHelpers.WriteStringToFloatList(takeDelays);

                if (this.TakeDelays.Count != this.TakeTools.Count)
                {
                    throw new Exception("Property 'TakeDelays' has bad formatting. Values must be floats separated by commas.");
                }

                for (int i = 0; i < this.TakeTools.Count; i += 1)
                {
                    this.TakeToolsDelays.Add(this.TakeTools[i], this.TakeDelays[i]);
                }
            }
            else
            {
                foreach (string tool in this.TakeTools)
                {
                    this.TakeToolsDelays.Add(tool, this.TakeDelay);
                }
            }
        }


        /**
         * Returns the block activation commands.
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            bool flag = false;
            bool flag2 = false;

            if (this.ClaimRequired)
            {
                flag = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
            }
            else
            {
                flag = true;
            }


            if (this.TakeToolRequired == false)
            {
                flag2 = true;
            }
            else
            {
                foreach (string tool in this.TakeTools)
                {
                    if (_entityFocusing.inventory.holdingItem.GetItemName() == tool)
                    {
                        flag2 = true;
                        break;
                    }
                }
            }

            this.cmds[0].enabled = (flag && flag2 && this.TakeDelay > 0f);
            return this.cmds;
        }


        /**
         * Timed event data used for sending forward.
         */

        private void EventData_Event(TimerEventData timerData)
        {
            World world = GameManager.Instance.World;
            object[] array = (object[])timerData.Data;
            int clrIdx = (int)array[0];
            BlockValue blockValue = (BlockValue)array[1];
            Vector3i vector3i = (Vector3i)array[2];
            BlockValue block = world.GetBlock(vector3i);
            EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
            if (block.damage > 0)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), String.Empty, "ui_denied", null);
                return;
            }
            if (block.type != blockValue.type)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), String.Empty, "ui_denied", null);
                return;
            }
            world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
        }


        /**
         * When command is given, take block with set time.
         */

        public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            if (_blockValue.damage > 0)
            {
                GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), string.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI playerUI = (_player as EntityPlayerLocal).PlayerUI;
            playerUI.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUI.xui.GetChildByType<XUiC_Timer>();
            TimerEventData timerEventData = new TimerEventData();
            timerEventData.Data = new object[]
            {
        _cIdx,
        _blockValue,
        _blockPos,
        _player
            };
            timerEventData.Event += this.EventData_Event;

            float delay = this.TakeDelay;
            if (this.TakeToolRequired)
            {
                foreach (string tool in this.TakeTools)
                {
                    if (_player.inventory.holdingItem.GetItemName() == tool)
                    {
                        delay = this.TakeToolsDelays[tool];
                    }
                }
            }
            childByType.SetTimer(delay, timerEventData, -1f, "");
        }


        /**
         * Block activation commands.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("take", "hand", false)
        };


        private List<string> TakeTools;
        private List<float> TakeDelays;
        private Dictionary<string, float> TakeToolsDelays;
        private float TakeDelay;
        private bool TakeToolRequired;
        private bool ClaimRequired;
    }
}