﻿using System;

namespace FennecCore.Scripts
{

    public class BlockMultiblockMaster : Block, IBlockMultiblockComponent
    {
        /**
         * blocks.xml:
         * <property name="Class" value="MultiblockMaster, Mods" />
         * <property name="Structure" value="(multiblocktype1:(0, -1, -1), (0, 1, 0)),(multiblocktype2:(0, 0, 1), (0, 1, 1))" />
         * <!-- multiblockType refers to either the block name directly in XML, OR the multiblock type assigned to it. The block MUST be a class MultiblockSlave, Mods -->
         */


        /**
         *	Called when the block is loaded. This block has a tile entity attached, so need to set this to true.
         */

        public BlockMultiblockMaster()
        {
            this.HasTileEntity = true;
        }



        /**
         * When block is initialised
         */

        public override void Init()
        {
            base.Init();
            if (!this.Properties.Values.ContainsKey("Structure"))
            {
                throw new Exception("Multiblock " + this.GetBlockName() + " does not contain a Structure property.");
            }
            this.Structure = this.Properties.Values["Structure"];

            MultiblockData multiblockData = new MultiblockData(this.Structure, this.GetBlockName());
            MultiblockManager.AddMultiblock(multiblockData);
        }


        /**
         * Validates multiblock structure and typings.
         */

        public override void LateInit()
        {
            base.LateInit();
            MultiblockManager.ValidateMultiblock(this.GetBlockName());
        }


        /**
         *  Called when the block is added into the world from the generator. 
         */

        public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (_blockValue.ischild)
            {
                return;
            }
            this.shape.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
            if (this.isMultiBlock)
            {
                this.multiBlockPos.AddChilds(world, _chunk, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            if (_blockValue.ischild)
            {
                return;
            }
            this.AddTileEntity(world, _chunk, _blockPos, _blockValue);
        }


        /**
         * Clean up tile entity when the block is removed from the world.
         */

        public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            if (!_blockValue.ischild)
            {
                this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
                if (this.isMultiBlock)
                {
                    this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
                    return;
                }
            }
            else if (this.isMultiBlock)
            {
                this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
            }
            _chunk.RemoveTileEntityAt<TileEntityMultiblockMaster>((World)_world, World.toBlock(_blockPos));
        }


        /**
         * Called when a player or entity places the block down.
         */

        public override void PlaceBlock(WorldBase _world, BlockPlacement.Result _result, EntityAlive _ea)
        {
            Block block = Block.list[_result.blockValue.type];
            if (block.shape.IsTerrain())
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, this.Density);
            }
            else if (!block.IsTerrainDecoration)
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue, MarchingCubes.DensityAir);
            }
            else
            {
                _world.SetBlockRPC(_result.clrIdx, _result.blockPos, _result.blockValue);
            }

            TileEntityMultiblockMaster tileEntityMultiblockMaster = (TileEntityMultiblockMaster)_world.GetTileEntity(_result.clrIdx, _result.blockPos);
            if (tileEntityMultiblockMaster != null)
            {
                tileEntityMultiblockMaster.IsPlayerPlaced = true;
            }
        }


        /**
         * Called when someone accessed the block with an activation command.
         */

        public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            TileEntityMultiblockMaster tileEntityMultiblockMaster = (TileEntityMultiblockMaster)_world.GetTileEntity(_cIdx, _blockPos);
            if (tileEntityMultiblockMaster == null)
            {
                return false;
            }

            switch (_indexInBlockActivationCommands)
            {
                case 0:
                    GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get((tileEntityMultiblockMaster.MultiblockFormed() ? "ttMultiblockFormed" : "ttMultiblockInvalid")), "ui_denied");
                    return false;
                case 1:
                    this.TakeItemWithTimer(_cIdx, _blockPos, _blockValue, _player);
                    return true;
                default:
                    return false;
            }
        }


        /**
         * Adds the tile entity
         */

        protected void AddTileEntity(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
        {
            TileEntityMultiblockMaster tileEntityMultiblockMaster = new TileEntityMultiblockMaster(_chunk);
            tileEntityMultiblockMaster.localChunkPos = World.toBlock(_blockPos);
            tileEntityMultiblockMaster.SetMasterBlockName(this.GetBlockName());
            _chunk.AddTileEntity(tileEntityMultiblockMaster);
        }


        /**
         * Returns the block activation commands.
         */

        public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            bool flag1 = _world.IsMyLandProtectedBlock(_blockPos, _world.GetGameManager().GetPersistentLocalPlayer(), false);
            bool flag2 = false;

            TileEntityMultiblockMaster tileEntityMultiblockMaster = (TileEntityMultiblockMaster)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockMaster != null)
            {
                flag2 = tileEntityMultiblockMaster.IsPlayerPlaced;
            }

            this.cmds[0].enabled = flag1 && flag2;
            this.cmds[1].enabled = flag1 && flag2;
            return this.cmds;
        }


        /**
         * Timed event data used for sending forward.
         */

        private void EventData_Event(TimerEventData timerData)
        {
            World world = GameManager.Instance.World;
            object[] array = (object[])timerData.Data;
            int clrIdx = (int)array[0];
            BlockValue blockValue = (BlockValue)array[1];
            Vector3i vector3i = (Vector3i)array[2];
            BlockValue block = world.GetBlock(vector3i);
            EntityPlayerLocal entityPlayerLocal = array[3] as EntityPlayerLocal;
            if (block.damage > 0)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttRepairBeforePickup"), String.Empty, "ui_denied", null);
                return;
            }
            if (block.type != blockValue.type)
            {
                GameManager.ShowTooltip(entityPlayerLocal, Localization.Get("ttBlockMissingPickup"), String.Empty, "ui_denied", null);
                return;
            }
            world.SetBlockRPC(clrIdx, vector3i, BlockValue.Air);
        }


        /**
         * When command is given, take block with set time.
         */

        public void TakeItemWithTimer(int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
        {
            if (_blockValue.damage > 0)
            {
                GameManager.ShowTooltip(_player as EntityPlayerLocal, Localization.Get("ttRepairBeforePickup"), String.Empty, "ui_denied", null);
                return;
            }
            LocalPlayerUI playerUI = (_player as EntityPlayerLocal).PlayerUI;
            playerUI.windowManager.Open("timer", true, false, true);
            XUiC_Timer childByType = playerUI.xui.GetChildByType<XUiC_Timer>();
            TimerEventData timerEventData = new TimerEventData();
            timerEventData.Data = new object[]
            {
        _cIdx,
        _blockValue,
        _blockPos,
        _player
            };
            timerEventData.Event += this.EventData_Event;

            float delay = this.TakeDelay;
            childByType.SetTimer(delay, timerEventData, -1f, "");
        }


        /**
         * Returns the activation text for the player looking at the block.
         */

        public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
        {
            TileEntityMultiblockMaster tileEntityMultiblockMaster = (TileEntityMultiblockMaster)_world.GetTileEntity(_clrIdx, _blockPos);
            if (tileEntityMultiblockMaster == null)
            {
                return "";
            }

            if (!tileEntityMultiblockMaster.MultiblockFormed())
            {
                return Localization.Get("multiblockMasterNotFormed");
            }

            return Localization.Get("multiblockMasterFormed");
        }




        /**
         * Block activation commands.
         */

        private BlockActivationCommand[] cmds = new BlockActivationCommand[]
        {
        new BlockActivationCommand("getStatus", "hand", false),
        new BlockActivationCommand("takeMaster", "hand", false),
        };


        private string Structure;
        private float TakeDelay = 2f;
    }
}


