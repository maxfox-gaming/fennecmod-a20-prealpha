﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FennecCore.Scripts
{
	/**
	 * Does a block exchange and reduces the item in the player's inventory by a set amount.
	 */
	public class ItemActionReduceExchange : ItemAction
	{

		/**
		 * Reads in XML properties
		 */

		public override void ReadFrom(DynamicProperties _props)
		{
			base.ReadFrom(_props);
			if (_props.Values.ContainsKey("Change_block_to"))
			{
				this.changeBlockTo = _props.Values["Change_block_to"];
			}
			if (_props.Values.ContainsKey("Do_block_action"))
			{
				this.doBlockAction = _props.Values["Do_block_action"];
			}
			int num = 1;
			while (_props.Values.ContainsKey("Focused_blockname_" + num))
			{
				string text = _props.Values["Focused_blockname_" + num];
				BlockValue item = ItemClass.GetItem(text, false).ToBlockValue();
				if (item.Equals(BlockValue.Air))
				{
					throw new Exception("Unknown block name '" + text + "' in use_action!");
				}
				this.focusedBlocks.Add(item);
				num++;
			}
			this.itemsNeeded = 1;
			if (_props.Values.ContainsKey("Reduce"))
			{
				if (!StringParsers.TryParseSInt32(_props.Values["Reduce"], out this.itemsNeeded))
				{
					throw new Exception("Can't parse 'Reduce' property as integer.");
				}

				if (this.itemsNeeded < 1)
				{
					throw new Exception("Property 'reduce' needs to be 1 or larger.");
				}
			}
		}


		/**
		 * Checks whether we have a focusing block
		 */

		private bool isFocusingBlock(WorldRayHitInfo _hitInfo)
		{
			for (int i = 0; i < this.focusedBlocks.Count; i++)
			{
				BlockValue other = this.focusedBlocks[i];
				if (_hitInfo.hit.blockValue.Equals(other))
				{
					return true;
				}
			}
			return false;
		}


		/**
		 * Checks we have enough of the needed item in the holding slot.
		 */

		private bool HaveEnough(ItemActionData _actionData)
		{
			ItemStack holdingItem = _actionData.invData.holdingEntity.inventory.GetItem(_actionData.invData.slotIdx);
			return holdingItem.count >= this.itemsNeeded;
		}



		/**
		 * Executes the action
		 */

		public override void ExecuteAction(ItemActionData _actionData, bool _bReleased)
		{
			if (!_bReleased)
			{
				return;
			}
			if (_actionData.lastUseTime > 0f)
			{
				return;
			}
			if (!this.HaveEnough(_actionData))
			{
				return;
			}
			ItemInventoryData invData = _actionData.invData;
			Ray lookRay = invData.holdingEntity.GetLookRay();
			lookRay.origin += lookRay.direction.normalized * 0.5f;
			if (!Voxel.Raycast(invData.world, lookRay, Constants.cDigAndBuildDistance, -538480645, 4095, 0f))
			{
				return;
			}
			if (Voxel.voxelRayHitInfo.bHitValid && this.isFocusingBlock(Voxel.voxelRayHitInfo))
			{
				this.hitLiquidBlock = Voxel.voxelRayHitInfo.hit.blockValue;
				this.hitLiquidPos = Voxel.voxelRayHitInfo.hit.blockPos;
				_actionData.lastUseTime = Time.time;
				invData.holdingEntity.RightArmAnimationUse = true;
				if (this.soundStart != null)
				{
					invData.holdingEntity.PlayOneShot(this.soundStart, false);
				}
			}
		}


		/**
		 * Checks action is running.
		 */

		public override bool IsActionRunning(ItemActionData _actionData)
		{
			return _actionData.lastUseTime != 0f && Time.time - _actionData.lastUseTime < this.Delay;
		}


		/**
		 * When the item is changed
		 */

		public override void StopHolding(ItemActionData _data)
		{
			base.StopHolding(_data);
			_data.lastUseTime = 0f;
		}


		/**
		 * If we have action ready to run, do the exchange here.
		 */

		public override void OnHoldingUpdate(ItemActionData _actionData)
		{
			if (_actionData.lastUseTime == 0f || this.IsActionRunning(_actionData))
			{
				return;
			}

			if (!_actionData.invData.hitInfo.hit.blockPos.Equals(this.hitLiquidPos))
			{
				this.CancelAction(_actionData);
				return;
			}

			QuestEventManager.Current.ExchangedFromItem(_actionData.invData.itemStack);
			ItemStack stack = _actionData.invData.holdingEntity.inventory.GetItem(_actionData.invData.slotIdx);
			ItemValue item = stack.itemValue;
			int count = stack.count - this.itemsNeeded;
			_actionData.invData.holdingEntity.inventory.SetItem(_actionData.invData.slotIdx, (count > 0 ? new ItemStack(item, count) : ItemStack.Empty.Clone()));
				
			if (this.doBlockAction != null && Block.list[this.hitLiquidBlock.type].blockMaterial.IsLiquid)
			{
				Block.list[this.hitLiquidBlock.type].DoExchangeAction(_actionData.invData.world, 0, this.hitLiquidPos, this.hitLiquidBlock, this.doBlockAction, _actionData.invData.holdingEntity.inventory.holdingCount);
            }
            if (this.changeBlockTo != null)
            {
                Vector3i blockPos = _actionData.invData.hitInfo.hit.blockPos;
                BlockValue block = _actionData.invData.world.GetBlock(blockPos);
                BlockValue blockValue = ItemClass.GetItem(this.changeBlockTo, false).ToBlockValue();
                blockValue.rotation = block.rotation;
                _actionData.invData.world.SetBlockRPC(blockPos, blockValue);
                if (Block.list[block.type].blockMaterial.IsLiquid)
                {
                    BlockValue block2 = _actionData.invData.world.GetBlock(blockPos + new Vector3i(-1, 0, 0));
                    if (Block.list[block2.type].blockMaterial.IsLiquid)
                    {
                        _actionData.invData.world.SetBlockRPC(blockPos + new Vector3i(-1, 0, 0), blockValue);
                    }
                    BlockValue block3 = _actionData.invData.world.GetBlock(blockPos + new Vector3i(1, 0, 0));
                    if (Block.list[block3.type].blockMaterial.IsLiquid)
                    {
                        _actionData.invData.world.SetBlockRPC(blockPos + new Vector3i(1, 0, 0), blockValue);
                    }
                    BlockValue block4 = _actionData.invData.world.GetBlock(blockPos + new Vector3i(0, 0, 1));
                    if (Block.list[block4.type].blockMaterial.IsLiquid)
                    {
                        _actionData.invData.world.SetBlockRPC(blockPos + new Vector3i(0, 0, 1), blockValue);
                    }
                    BlockValue block5 = _actionData.invData.world.GetBlock(blockPos + new Vector3i(0, 0, -1));
                    if (Block.list[block5.type].blockMaterial.IsLiquid)
                    {
                        _actionData.invData.world.SetBlockRPC(blockPos + new Vector3i(0, 0, -1), blockValue);
                    }
                }
            }
            _actionData.lastUseTime = 0f;
		}


		protected string changeBlockTo;
		protected string doBlockAction;
		protected int itemsNeeded;
		protected BlockValue hitLiquidBlock;
		protected Vector3i hitLiquidPos;
		protected List<BlockValue> focusedBlocks = new List<BlockValue>();
	}
}