﻿using System.Collections.Generic;
using System.Globalization;
using Audio;
using UnityEngine;

namespace FennecCore.Scripts
{
	public class ItemActionRepairRange : ItemActionAttack
	{
		/**
		 * Creates data that can get overridden by item_modifiers.xml entries.
		 */

		public override ItemActionData CreateModifierData(ItemInventoryData _invData, int _indexInEntityOfAction)
		{
			ItemActionRepairRange.InventoryDataRepair invData = new ItemActionRepairRange.InventoryDataRepair(_invData, _indexInEntityOfAction);
			invData.UpgradeHitOffset = this.hitCountOffset;
			invData.UpgradeRange = this.upgradeRange;
			invData.Delay = this.Delay;
			return invData;
		}


		/**
		 * Reads item data from the items.xml
		 */

		public override void ReadFrom(DynamicProperties _props)
		{
			base.ReadFrom(_props);
			if (_props.Values.ContainsKey("Repair_amount"))
			{
				this.repairAmount = StringParsers.ParseFloat(_props.Values["Repair_amount"], 0, -1, NumberStyles.Any);
			}
			else
			{
				this.repairAmount = 0f;
			}
			if (_props.Values.ContainsKey("Upgrade_hit_offset"))
			{
				this.hitCountOffset = StringParsers.ParseFloat(_props.Values["Upgrade_hit_offset"], 0, -1, NumberStyles.Any);
			}
			else
			{
				this.hitCountOffset = 0f;
			}
			if (_props.Values.ContainsKey("Repair_action_sound"))
			{
				this.repairActionSound = _props.Values["Repair_action_sound"];
			}
			else
			{
				this.repairActionSound = "";
			}
			if (_props.Values.ContainsKey("Upgrade_action_sound"))
			{
				this.upgradeActionSound = _props.Values["Upgrade_action_sound"];
			}
			else
			{
				this.upgradeActionSound = "";
			}
			if (_props.Values.ContainsKey("Allowed_upgrade_items"))
			{
				this.allowedUpgradeItems = _props.Values["Allowed_upgrade_items"];
			}
			else
			{
				this.allowedUpgradeItems = "";
			}
			if (_props.Values.ContainsKey("Restricted_upgrade_items"))
			{
				this.restrictedUpgradeItems = _props.Values["Restricted_upgrade_items"];
			}
			else
			{
				this.restrictedUpgradeItems = "";
			}
			if (_props.Values.ContainsKey("Upgrade_range"))
			{
				this.upgradeRange = StringParsers.ParseUInt16(_props.Values["Upgrade_range"]);
			}
			else
			{
				this.upgradeRange = 0;
			}
			this.soundAnimActionSyncTimer = 0.3f;
		}


		/**
		 * When the item is released or dropped.
		 */

		public override void StopHolding(ItemActionData _data)
		{
			((ItemActionRepairRange.InventoryDataRepair)_data).bUseStarted = false;
			this.bUpgradeCountChanged = false;
			LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(_data.invData.holdingEntity as EntityPlayerLocal);
			if (uiforPlayer != null)
			{
				XUiC_FocusedBlockHealth.SetData(uiforPlayer, null, 0f);
			}
		}


		/**
		 * When the item is equipped.
		 */

		public override void StartHolding(ItemActionData _data)
		{
			((ItemActionRepairRange.InventoryDataRepair)_data).bUseStarted = false;
			this.bUpgradeCountChanged = false;
		}


		/**
		 * Runs whilst action is performing.
		 */

		public override void OnHoldingUpdate(ItemActionData _actionData)
		{
			if (_actionData.invData.hitInfo.bHitValid && _actionData.invData.hitInfo.hit.distanceSq > Constants.cDigAndBuildDistance * Constants.cDigAndBuildDistance)
			{
				return;
			}
			EntityPlayerLocal entityPlayerLocal = _actionData.invData.holdingEntity as EntityPlayerLocal;
			if (entityPlayerLocal == null)
			{
				return;
			}
			LocalPlayerUI uiforPlayer = LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
			GUIWindowManager windowManager = uiforPlayer.windowManager;
			NGUIWindowManager nguiWindowManager = uiforPlayer.nguiWindowManager;
			if (windowManager.IsModalWindowOpen())
			{
				((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted = false;
				((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType = ItemActionRepairRange.EnumRepairType.None;
				return;
			}
			if (_actionData.invData.holdingEntity != _actionData.invData.world.GetPrimaryPlayer())
			{
				return;
			}
			if (!((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted)
			{
				return;
			}
			if (this.bUpgradeCountChanged)
			{
				((ItemActionRepairRange.InventoryDataRepair)_actionData).upgradePerc = (float)this.blockUpgradeCount / (float)this.currentUpgradeInfo.Hits;
				if (this.blockUpgradeCount >= this.currentUpgradeInfo.Hits)
				{
					BlockValue MainBlock = _actionData.invData.world.GetBlock(this.blockTargetPos);
					if (!this.RemoveRequiredResource(_actionData.invData))
					{
						return;
					}

					BlockValue blockValue = Block.GetBlockValue(this.currentUpgradeInfo.ToBlock, false);
					BlockValue block = _actionData.invData.world.GetBlock(this.blockTargetPos);
					blockValue.rotation = block.rotation;
					blockValue.meta = block.meta;
					QuestEventManager.Current.BlockUpgraded(this.currentUpgradeInfo.FromBlock, this.blockTargetPos);
					_actionData.invData.holdingEntity.MinEventContext.ItemActionData = _actionData;
					_actionData.invData.holdingEntity.MinEventContext.BlockValue = blockValue;
					_actionData.invData.holdingEntity.MinEventContext.Position = this.blockTargetPos.ToVector3();
					_actionData.invData.holdingEntity.FireEvent(MinEventTypes.onSelfUpgradedBlock, true);
					Block.list[block.type].DamageBlock(_actionData.invData.world, this.blockTargetClrIdx, this.blockTargetPos, block, -1, _actionData.invData.holdingEntity.entityId, false, false);
					_actionData.invData.holdingEntity.Progression.AddLevelExp((int)(blockValue.Block.blockMaterial.Experience * (float)this.currentUpgradeInfo.ItemCount), "_xpFromUpgradeBlock", Progression.XPTypes.Upgrading, true);
					if (Block.list[block.type].CustomUpgradeSound != null)
					{
						_actionData.invData.holdingEntity.PlayOneShot(Block.list[block.type].CustomUpgradeSound, false);
					}

					int range = ((ItemActionRepairRange.InventoryDataRepair)_actionData).UpgradeRange;

					if (range > 0)
					{
						List<Vector3i> positions = CoordinateHelper.GetCoordinatesAround(this.blockTargetPos, false, range, range, range);
						foreach (Vector3i position in positions)
						{
							Chunk chunk = _actionData.invData.world.GetChunkFromWorldPos(position) as Chunk;
							if (chunk == null)
							{
								continue;
							}

							BlockValue blockValue2 = Block.GetBlockValue(this.currentUpgradeInfo.ToBlock, false);
							BlockValue block2 = _actionData.invData.world.GetBlock(position);

							if (MainBlock.Block.GetBlockName() != block2.Block.GetBlockName())
							{
								continue;
							}

							if (Block.list[block2.type].CanRepair(block2))
							{
								continue;
							}

							if (!this.RemoveRequiredResource(_actionData.invData))
							{
								break;
							}

							blockValue.rotation = block2.rotation;
							blockValue.meta = block2.meta;
							QuestEventManager.Current.BlockUpgraded(this.currentUpgradeInfo.FromBlock, position);
							_actionData.invData.holdingEntity.MinEventContext.ItemActionData = _actionData;
							_actionData.invData.holdingEntity.MinEventContext.BlockValue = blockValue2;
							_actionData.invData.holdingEntity.MinEventContext.Position = position.ToVector3();
							_actionData.invData.holdingEntity.FireEvent(MinEventTypes.onSelfUpgradedBlock, true);
							Block.list[block.type].DamageBlock(_actionData.invData.world, chunk.ClrIdx, position, block2, -1, _actionData.invData.holdingEntity.entityId, false, false);
							_actionData.invData.holdingEntity.Progression.AddLevelExp((int)(blockValue2.Block.blockMaterial.Experience * (float)this.currentUpgradeInfo.ItemCount), "_xpFromUpgradeBlock", Progression.XPTypes.Upgrading, true);
						}
					}
					this.blockUpgradeCount = 0;
				}
				if (this.currentUpgradeInfo.Sound.Length > 0)
				{
					_actionData.invData.holdingEntity.PlayOneShot(this.currentUpgradeInfo.Sound, false);
				}
				this.bUpgradeCountChanged = false;
				return;
			}
			this.ExecuteAction(_actionData, false);
		}


		/**
		 * Executes the action on the click.
		 */

		public override void ExecuteAction(ItemActionData _actionData, bool _bReleased)
		{
			EntityPlayerLocal entityPlayerLocal = _actionData.invData.holdingEntity as EntityPlayerLocal;
			LocalPlayerUI.GetUIForPlayer(entityPlayerLocal);
			if (_bReleased)
			{
				((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted = false;
				((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType = ItemActionRepairRange.EnumRepairType.None;
				return;
			}
			if (Time.time - _actionData.lastUseTime < ((ItemActionRepairRange.InventoryDataRepair)_actionData).Delay)
			{
				return;
			}
			ItemInventoryData invData = _actionData.invData;
			if (invData.hitInfo.bHitValid && invData.hitInfo.hit.distanceSq > Constants.cDigAndBuildDistance * Constants.cDigAndBuildDistance)
			{
				return;
			}
			_actionData.lastUseTime = Time.time;
			if (invData.hitInfo.bHitValid && _actionData.invData.world.IsWithinTraderArea(invData.hitInfo.hit.blockPos))
			{
				return;
			}
			if (invData.hitInfo.bHitValid && GameUtils.IsBlockOrTerrain(invData.hitInfo.tag))
			{
				this.blockTargetPos = invData.hitInfo.hit.blockPos;
				this.blockTargetClrIdx = invData.hitInfo.hit.clrIdx;
				BlockValue block = invData.world.GetBlock(this.blockTargetPos);
				if (block.ischild)
				{
					this.blockTargetPos = Block.list[block.type].multiBlockPos.GetParentPos(this.blockTargetPos, block);
					block = _actionData.invData.world.GetBlock(this.blockTargetPos);
				}
				if ((invData.itemValue.MaxUseTimes > 0 && invData.itemValue.UseTimes >= (float)invData.itemValue.MaxUseTimes) || (invData.itemValue.UseTimes == 0f && invData.itemValue.MaxUseTimes == 0))
				{
					if (this.item.Properties.Values.ContainsKey(ItemClass.PropSoundJammed))
					{
						Manager.PlayInsidePlayerHead(this.item.Properties.Values[ItemClass.PropSoundJammed], -1, 0f, false);
					}
					GameManager.ShowTooltip(entityPlayerLocal, "ttItemNeedsRepair");
					return;
				}
				if (Block.list[block.type].CanRepair(block))
				{
					if (Block.list[block.type].RepairItems == null)
					{
						return;
					}
					ItemActionRepairRange.InventoryDataRepair inventoryDataRepair = (ItemActionRepairRange.InventoryDataRepair)_actionData;
					if (inventoryDataRepair.lastHitPosition != this.blockTargetPos || inventoryDataRepair.lastHitBlockValue.type != block.type)
					{
						inventoryDataRepair.lastHitPosition = this.blockTargetPos;
						inventoryDataRepair.lastHitBlockValue = block;
						inventoryDataRepair.lastHitPositionRepairItems = new float[Block.list[block.type].RepairItems.Count];
					}
					int num = Utils.FastMin((int)this.repairAmount, block.damage);
					inventoryDataRepair.repairPerc = (float)num / (float)Block.list[block.type].MaxDamage;
					inventoryDataRepair.blockDamagePerc = (float)block.damage / (float)Block.list[block.type].MaxDamage;
					EntityPlayerLocal entityPlayerLocal2 = _actionData.invData.holdingEntity as EntityPlayerLocal;
					if (entityPlayerLocal2 == null)
					{
						return;
					}
					((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType = ItemActionRepairRange.EnumRepairType.Repair;
					bool flag = false;
					for (int i = 0; i < Block.list[block.type].RepairItems.Count; i++)
					{
						string itemName = Block.list[block.type].RepairItems[i].ItemName;
						float num2 = (float)Block.list[block.type].RepairItems[i].Count * ((ItemActionRepairRange.InventoryDataRepair)_actionData).repairPerc;
						if (inventoryDataRepair.lastHitPositionRepairItems[i] <= 0f)
						{
							int count = Utils.FastMax((int)num2, 1);
							ItemStack itemStack = new ItemStack(ItemClass.GetItem(itemName, false), count);
							if (!this.canRemoveRequiredItem(_actionData.invData, itemStack))
							{
								itemStack.count = 0;
								entityPlayerLocal2.AddUIHarvestingItem(itemStack, true);
								if (!flag)
								{
									flag = true;
								}
							}
						}
					}
					if (flag)
					{
						return;
					}
					_actionData.invData.holdingEntity.RightArmAnimationUse = true;
					float num3 = 0f;
					for (int j = 0; j < Block.list[block.type].RepairItems.Count; j++)
					{
						float num4 = (float)Block.list[block.type].RepairItems[j].Count * ((ItemActionRepairRange.InventoryDataRepair)_actionData).repairPerc;
						if (inventoryDataRepair.lastHitPositionRepairItems[j] <= 0f)
						{
							string itemName2 = Block.list[block.type].RepairItems[j].ItemName;
							int num5 = Utils.FastMax((int)num4, 1);
							inventoryDataRepair.lastHitPositionRepairItems[j] += (float)num5;
							inventoryDataRepair.lastHitPositionRepairItems[j] -= num4;
							ItemStack itemStack2 = new ItemStack(ItemClass.GetItem(itemName2, false), num5);
							num3 += itemStack2.itemValue.ItemClass.MadeOfMaterial.Experience * (float)num5;
							this.removeRequiredItem(_actionData.invData, itemStack2);
							itemStack2.count *= -1;
							entityPlayerLocal2.AddUIHarvestingItem(itemStack2, false);
						}
						else
						{
							inventoryDataRepair.lastHitPositionRepairItems[j] -= num4;
						}
					}
					if (this.repairActionSound != null && this.repairActionSound.Length > 0)
					{
						invData.holdingEntity.PlayOneShot(this.repairActionSound, false);
					}
					else if (this.soundStart != null && this.soundStart.Length > 0)
					{
						invData.holdingEntity.PlayOneShot(this.soundStart, false);
					}
					if (invData.itemValue.MaxUseTimes > 0)
					{
						invData.itemValue.UseTimes += 1f;
					}
					int num6 = Block.list[block.type].DamageBlock(invData.world, invData.hitInfo.hit.clrIdx, this.blockTargetPos, block, -num, invData.holdingEntity.entityId, false, false);
					((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted = true;
					((ItemActionRepairRange.InventoryDataRepair)_actionData).blockDamagePerc = (float)num6 / (float)Block.list[block.type].MaxDamage;
					_actionData.invData.holdingEntity.MinEventContext.ItemActionData = _actionData;
					_actionData.invData.holdingEntity.MinEventContext.BlockValue = block;
					_actionData.invData.holdingEntity.MinEventContext.Position = this.blockTargetPos.ToVector3();
					_actionData.invData.holdingEntity.FireEvent(MinEventTypes.onSelfRepairBlock, true);
					entityPlayerLocal2.Progression.AddLevelExp((int)num3, "_xpFromRepairBlock", Progression.XPTypes.Repairing, true);
					return;
				}
				else if (this.isUpgradeItem)
				{
					if (!this.CanRemoveRequiredResource(_actionData.invData, _actionData, block))
					{
						if (Block.list[block.type].Properties.Values["UpgradeBlock.Item"] != null)
						{
							ItemStack @is = new ItemStack(ItemClass.GetItem(Block.list[block.type].Properties.Values["UpgradeBlock.Item"], false), 0);
							(_actionData.invData.holdingEntity as EntityPlayerLocal).AddUIHarvestingItem(@is, true);
						}
						((ItemActionRepairRange.InventoryDataRepair)_actionData).upgradePerc = 0f;
						return;
					}
					_actionData.invData.holdingEntity.RightArmAnimationUse = true;
					((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType = ItemActionRepairRange.EnumRepairType.Upgrade;
					if (this.blockTargetPos == this.lastBlockTargetPos)
					{
						this.blockUpgradeCount++;
					}
					else
					{
						this.blockUpgradeCount = 1;
					}
					this.lastBlockTargetPos = this.blockTargetPos;
					this.bUpgradeCountChanged = true;
					((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted = true;
					return;
				}
				else
				{
					((ItemActionRepairRange.InventoryDataRepair)_actionData).bUseStarted = false;
					((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType = ItemActionRepairRange.EnumRepairType.None;
				}
			}
		}


		/**
		 * Returns the repair amount.
		 */

		public float GetRepairAmount()
		{
			return this.repairAmount;
		}


		/**
		 * Checks whether there are enough resources in the backpack to remove.
		 */

		private bool CanRemoveRequiredResource(ItemInventoryData data, ItemActionData actionData, BlockValue blockValue)
		{
			Block block = Block.list[blockValue.type];
			if (!block.Properties.Values.ContainsKey("UpgradeBlock.Item"))
			{
				return false;
			}
			ItemActionRepairRange.UpgradeInfo upgradeInfo = default(ItemActionRepairRange.UpgradeInfo);
			upgradeInfo.FromBlock = Block.list[blockValue.type].GetBlockName();
			upgradeInfo.ToBlock = block.Properties.Values["UpgradeBlock.ToBlock"];
			upgradeInfo.Item = block.Properties.Values["UpgradeBlock.Item"];
			if (this.allowedUpgradeItems.Length > 0 && !this.allowedUpgradeItems.ContainsCaseInsensitive(upgradeInfo.Item))
			{
				return false;
			}
			if (this.restrictedUpgradeItems.Length > 0 && this.restrictedUpgradeItems.ContainsCaseInsensitive(upgradeInfo.Item))
			{
				return false;
			}
			if (this.upgradeActionSound.Length > 0)
			{
				upgradeInfo.Sound = this.upgradeActionSound;
			}
			else
			{
				upgradeInfo.Sound = string.Format("ImpactSurface/{0}hit{1}", data.holdingEntity.inventory.holdingItem.MadeOfMaterial.SurfaceCategory, ItemClass.GetForId(ItemClass.GetItem(upgradeInfo.Item, false).type).MadeOfMaterial.SurfaceCategory);
			}
			int num = 4;
			if (!int.TryParse(block.Properties.Values["UpgradeBlock.UpgradeHitCount"], out num))
			{
				return false;
			}

			float offset = this.hitCountOffset;
			offset = ((ItemActionRepairRange.InventoryDataRepair)actionData).UpgradeHitOffset;

			upgradeInfo.Hits = (int)(((float)num + offset < 1f) ? 1f : ((float)num + offset));
			int itemCount = 1;
			if (int.TryParse(block.Properties.Values["UpgradeBlock.ItemCount"], out itemCount))
			{
				upgradeInfo.ItemCount = itemCount;
				this.currentUpgradeInfo = upgradeInfo;
				if (this.currentUpgradeInfo.FromBlock != null)
				{
					ItemValue item = ItemClass.GetItem(this.currentUpgradeInfo.Item, false);
					if (data.holdingEntity.inventory.GetItemCount(item, false, -1, -1) >= this.currentUpgradeInfo.ItemCount)
					{
						return true;
					}
					if (data.holdingEntity.bag.GetItemCount(item, -1, -1, false) >= this.currentUpgradeInfo.ItemCount)
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}


		/**
		 * Removes a set number of resources from the bag.
		 */

		private bool RemoveRequiredResource(ItemInventoryData data)
		{
			ItemValue item = ItemClass.GetItem(this.currentUpgradeInfo.Item, false);
			if (data.holdingEntity.inventory.DecItem(item, this.currentUpgradeInfo.ItemCount, false) == this.currentUpgradeInfo.ItemCount)
			{
				EntityPlayerLocal entityPlayerLocal = data.holdingEntity as EntityPlayerLocal;
				if (entityPlayerLocal != null && this.currentUpgradeInfo.ItemCount != 0)
				{
					entityPlayerLocal.AddUIHarvestingItem(new ItemStack(item, -this.currentUpgradeInfo.ItemCount), false);
				}
				return true;
			}
			if (data.holdingEntity.bag.DecItem(item, this.currentUpgradeInfo.ItemCount, false) == this.currentUpgradeInfo.ItemCount)
			{
				EntityPlayerLocal entityPlayerLocal2 = data.holdingEntity as EntityPlayerLocal;
				if (entityPlayerLocal2 != null)
				{
					entityPlayerLocal2.AddUIHarvestingItem(new ItemStack(item, -this.currentUpgradeInfo.ItemCount), false);
				}
				return true;
			}
			return false;
		}


		/**
		 * Checks required item can be removed from the bag.
		 */

		private bool canRemoveRequiredItem(ItemInventoryData _data, ItemStack _itemStack)
		{
			return _data.holdingEntity.inventory.GetItemCount(_itemStack.itemValue, false, -1, -1) >= _itemStack.count || _data.holdingEntity.bag.GetItemCount(_itemStack.itemValue, -1, -1, false) >= _itemStack.count;
		}


		/**
		 * Removes the required item from the bag.
		 */

		private bool removeRequiredItem(ItemInventoryData _data, ItemStack _itemStack)
		{
			return _data.holdingEntity.inventory.DecItem(_itemStack.itemValue, _itemStack.count, false) == _itemStack.count || _data.holdingEntity.bag.DecItem(_itemStack.itemValue, _itemStack.count, false) == _itemStack.count;
		}


		/**
		 * Gets the crosshair type for the held item.
		 */

		public override ItemClass.EnumCrosshairType GetCrosshairType(ItemActionData _actionData)
		{
			ItemActionRepairRange.EnumRepairType repairType = ((ItemActionRepairRange.InventoryDataRepair)_actionData).repairType;
			if (repairType == ItemActionRepairRange.EnumRepairType.Repair)
			{
				return ItemClass.EnumCrosshairType.Repair;
			}
			if (repairType != ItemActionRepairRange.EnumRepairType.Upgrade)
			{
				return ItemClass.EnumCrosshairType.Plus;
			}
			return ItemClass.EnumCrosshairType.Upgrade;
		}


		/**
		 * Whether to show a tint on the item.
		 */

		protected override bool isShowOverlay(ItemActionData _actionData)
		{
			WorldRayHitInfo hitInfo = _actionData.invData.hitInfo;
			if (hitInfo.bHitValid && hitInfo.hit.distanceSq > Constants.cDigAndBuildDistance * Constants.cDigAndBuildDistance)
			{
				return false;
			}
			bool result = false;
			ItemActionRepairRange.InventoryDataRepair inventoryDataRepair = (ItemActionRepairRange.InventoryDataRepair)_actionData;
			if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.None)
			{
				if (hitInfo.bHitValid)
				{
					int damage;
					if (!hitInfo.hit.blockValue.ischild)
					{
						damage = hitInfo.hit.blockValue.damage;
					}
					else
					{
						Vector3i parentPos = Block.list[hitInfo.hit.blockValue.type].multiBlockPos.GetParentPos(hitInfo.hit.blockPos, hitInfo.hit.blockValue);
						damage = _actionData.invData.world.GetBlock(parentPos).damage;
					}
					result = (damage > 0);
				}
			}
			else if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.Repair)
			{
				EntityPlayerLocal entityPlayerLocal = _actionData.invData.holdingEntity as EntityPlayerLocal;
				result = (entityPlayerLocal != null && entityPlayerLocal.HitInfo.bHitValid && Time.time - _actionData.lastUseTime <= 1.5f);
			}
			else if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.Upgrade)
			{
				EntityPlayerLocal entityPlayerLocal2 = _actionData.invData.holdingEntity as EntityPlayerLocal;
				result = (entityPlayerLocal2 != null && entityPlayerLocal2.HitInfo.bHitValid && Time.time - _actionData.lastUseTime <= 1.5f && inventoryDataRepair.upgradePerc > 0f);
			}
			return result;
		}


		/**
		 * Gets the overlay data.
		 */

		protected override void getOverlayData(ItemActionData _actionData, out float _perc, out string _text)
		{
			ItemActionRepairRange.InventoryDataRepair inventoryDataRepair = (ItemActionRepairRange.InventoryDataRepair)_actionData;
			if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.None)
			{
				BlockValue blockValue = _actionData.invData.hitInfo.hit.blockValue;
				if (blockValue.ischild)
				{
					Vector3i parentPos = Block.list[blockValue.type].multiBlockPos.GetParentPos(_actionData.invData.hitInfo.hit.blockPos, blockValue);
					blockValue = _actionData.invData.world.GetBlock(parentPos);
				}
				int maxDamage = Block.list[blockValue.type].MaxDamage;
				_perc = ((float)maxDamage - (float)blockValue.damage) / (float)maxDamage;
				_text = string.Format("{0}/{1}", Utils.FastMax(0, maxDamage - blockValue.damage), maxDamage);
				return;
			}
			if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.Repair)
			{
				_perc = 1f - inventoryDataRepair.blockDamagePerc;
				_text = string.Format("{0}%", (_perc * 100f).ToCultureInvariantString("0"));
				return;
			}
			if (inventoryDataRepair.repairType == ItemActionRepairRange.EnumRepairType.Upgrade)
			{
				_perc = inventoryDataRepair.upgradePerc;
				_text = string.Format("{0}%", (_perc * 100f).ToCultureInvariantString("0"));
				return;
			}
			_perc = 0f;
			_text = string.Empty;
		}


		/**
		 * Checks whether the animation is currently being performed.
		 */

		public override bool IsActionRunning(ItemActionData _actionData)
		{
			ItemActionRepairRange.InventoryDataRepair inventoryDataRepair = (ItemActionRepairRange.InventoryDataRepair)_actionData;
			return Time.time - inventoryDataRepair.lastUseTime < this.Delay + 0.1f;
		}


		/**
		 * Gets the item value action info from the properties.
		 */

		public override void GetItemValueActionInfo(ref List<string> _infoList, ItemValue _itemValue, XUi _xui, int _actionIndex = 0)
		{
			base.GetItemValueActionInfo(ref _infoList, _itemValue, _xui, _actionIndex);
			_infoList.Add(ItemAction.StringFormatHandler(Localization.Get("lblBlkRpr"), this.GetRepairAmount().ToCultureInvariantString()));
		}


		/**
		 * When item mods are changed, read these values into the item action data.
		 */

		public override void OnModificationsChanged(ItemActionData _actionData)
		{
			ItemActionRepairRange.InventoryDataRepair itemActionRepairRanged = _actionData as ItemActionRepairRange.InventoryDataRepair;
			if (this.Properties.Values.ContainsKey("Delay"))
			{
				itemActionRepairRanged.Delay = StringParsers.ParseFloat(itemActionRepairRanged.invData.itemValue.GetPropertyOverride("Delay", this.Properties.Values["Delay"]), 0, -1, NumberStyles.Any);
			}

			if (this.Properties.Values.ContainsKey("Upgrade_hit_offset"))
			{
				itemActionRepairRanged.UpgradeHitOffset = StringParsers.ParseFloat(itemActionRepairRanged.invData.itemValue.GetPropertyOverride("Upgrade_hit_offset", this.Properties.Values["Upgrade_hit_offset"]), 0, -1, NumberStyles.Any);
			}

			if (this.Properties.Values.ContainsKey("Upgrade_range"))
			{
				itemActionRepairRanged.UpgradeRange = StringParsers.ParseUInt16(itemActionRepairRanged.invData.itemValue.GetPropertyOverride("Upgrade_range", this.Properties.Values["Upgrade_range"]), 0, -1, NumberStyles.Any);
			}
		}

		private const float showUpgradeDelay = 1f;
		protected BlockValue targetBlock;
		protected float repairAmount;
		protected float hitCountOffset;
		protected float soundAnimActionSyncTimer;
		protected const float SOUND_LENGTH = 0.3f;
		private ItemActionRepairRange.UpgradeInfo currentUpgradeInfo;
		private bool isUpgradeItem = true;
		private float upgradeRepeatTime;
		private Vector3i blockTargetPos;
		private int blockTargetClrIdx;
		private Vector3i lastBlockTargetPos;
		private int blockUpgradeCount;
		private bool bUpgradeCountChanged;
		private string repairActionSound;
		private string upgradeActionSound;
		private string allowedUpgradeItems;
		private string restrictedUpgradeItems;
		private int upgradeRange;

		private struct UpgradeInfo
		{
			public string FromBlock;
			public string ToBlock;
			public string Item;
			public int ItemCount;
			public string Sound;
			public int Hits;
		}

		protected enum EnumRepairType
		{
			None,
			Repair,
			Upgrade
		}

		protected class InventoryDataRepair : ItemActionAttackData
		{
			public InventoryDataRepair(ItemInventoryData _invData, int _indexInEntityOfAction) : base(_invData, _indexInEntityOfAction)
			{
			}

			public new bool uiOpenedByMe;
			public ItemActionRepairRange.EnumRepairType repairType;
			public float repairPerc;
			public float blockDamagePerc;
			public bool bUseStarted;
			public float upgradePerc;
			public BlockValue lastHitBlockValue;
			public Vector3i lastHitPosition = Vector3i.zero;
			public float[] lastHitPositionRepairItems;
			public float Delay;
			public int UpgradeRange;
			public float UpgradeHitOffset;
			public float OriginalDelay = -1f;
		}

	}
}