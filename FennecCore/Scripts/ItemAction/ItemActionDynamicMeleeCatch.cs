﻿

namespace FennecCore.Scripts
{
    public class ItemActionDynamicMeleeCatch : ItemActionDynamicMelee
    {
        protected override void hitTarget(ItemActionData _actionData, WorldRayHitInfo hitInfo, bool _isGrazingHit = false)
        {
            base.hitTarget(_actionData, hitInfo, _isGrazingHit);
            ItemActionDynamicMelee.ItemActionDynamicMeleeData itemActionDynamicMeleeData = _actionData as ItemActionDynamicMelee.ItemActionDynamicMeleeData;
            if (itemActionDynamicMeleeData == null)
            {
                return;
            }

            if (!itemActionDynamicMeleeData.HasExecuted)
            {
                return;
            }

            EntityPlayerLocal player = itemActionDynamicMeleeData.invData.holdingEntity as EntityPlayerLocal;
            if (player == null)
            {
                return;
            }

            EntityAnimalCatchable entity = itemActionDynamicMeleeData.invData.holdingEntity.MinEventContext.Other as EntityAnimalCatchable;
            if (entity == null)
            {
                return;
            }

            ItemStack itemToReturn = new ItemStack(entity.itemReturned, 1);
            if (!player.inventory.CanTakeItem(itemToReturn) & !player.bag.CanTakeItem(itemToReturn))
            {
                return;
            }

            if (!player.inventory.holdingItem.Equals(entity.holdingItem.ItemClass))
            {
                return;
            }

            if (!SingletonMonoBehaviour<ConnectionManager>.Instance.IsServer)
            {
                SingletonMonoBehaviour<ConnectionManager>.Instance.SendToServer(NetPackageManager.GetPackage<NetPackageRemoveEntityReturnItem>().Setup(entity.entityId, player.entityId, itemToReturn), true);
            }
            else
            {
                itemActionDynamicMeleeData.invData.world.RemoveEntity(entity.entityId, EnumRemoveEntityReason.Killed);
            }
            player.inventory.AddItem(itemToReturn);
        }
    }
}
