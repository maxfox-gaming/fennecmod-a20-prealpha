﻿using UnityEngine;

namespace FennecCore.Scripts
{
	/**
	 * Override class for wire tools that undo the stupid 'internal' method in the original class
	 * and extend functionality for more types of blocks and tile entities.
	 */

	public class ItemActionConnectPowerOverride : ItemActionConnectPower
	{
		/**
		 * Checks whether wires need removal.
		 */

		public void CheckForWireRemoveNeeded(EntityAlive _player, Vector3i _blockPos)
		{
			ItemActionConnectPowerOverride.ConnectPowerData connectPowerData = (ItemActionConnectPowerOverride.ConnectPowerData)_player.inventory.holdingItemData.actionData[1];
			if (connectPowerData.HasStartPoint && connectPowerData.startPoint == _blockPos)
			{
				this.DisconnectWire(connectPowerData);
			}
		}


		/**
		 * Gets the powered block.
		 */

		private TileEntityPowered GetPoweredBlock(ItemInventoryData data)
		{
			BlockValue block = data.world.GetBlock(data.hitInfo.hit.blockPos);
			Block block2 = Block.list[block.type];
			if (!(block2 is BlockPowered) && !(block2 is BlockPowerSource))
			{
				return null;
			}
			Vector3i blockPos = data.hitInfo.hit.blockPos;
			ChunkCluster chunkCluster = data.world.ChunkClusters[data.hitInfo.hit.clrIdx];
			if (chunkCluster == null)
			{
				return null;
			}
			Chunk chunk = (Chunk)chunkCluster.GetChunkSync(World.toChunkXZ(blockPos.x), blockPos.y, World.toChunkXZ(blockPos.z));
			if (chunk == null)
			{
				return null;
			}
			TileEntity tileEntity = chunk.GetTileEntity(World.toBlock(blockPos));
			if (tileEntity == null)
			{
				if (block2 is BlockPowered)
				{
					tileEntity = (block2 as BlockPowered).CreateTileEntity(chunk);
				}
				else if (block2 is BlockPowerSource)
				{
					tileEntity = (block2 as BlockPowerSource).CreateTileEntity(chunk);
				}
				else if (block2 is BlockMultiblockSlavePowered)
				{
					tileEntity = (block2 as BlockMultiblockSlavePowered).CreateTileEntity(chunk);
				}

				tileEntity.localChunkPos = World.toBlock(blockPos);
				BlockEntityData blockEntity = chunk.GetBlockEntity(blockPos);
				if (blockEntity != null)
				{
					((TileEntityPowered)tileEntity).BlockTransform = blockEntity.transform;
				}
				((TileEntityPowered)tileEntity).InitializePowerData();
				chunk.AddTileEntity(tileEntity);
			}
			return tileEntity as TileEntityPowered;
		}

		private TileEntityPowered GetPoweredBlock(Vector3i tileEntityPos)
		{
			World world = GameManager.Instance.World;
			BlockValue block = world.GetBlock(tileEntityPos);
			Block block2 = Block.list[block.type];
			if (!(block2 is BlockPowered) && !(block2 is BlockPowerSource) && !(block2 is BlockMultiblockSlavePowered))
			{
				return null;
			}
			Chunk chunk = world.GetChunkFromWorldPos(tileEntityPos.x, tileEntityPos.y, tileEntityPos.z) as Chunk;
			if (chunk == null)
			{
				return null;
			}
			TileEntity tileEntity = chunk.GetTileEntity(World.toBlock(tileEntityPos));
			if (tileEntity == null)
			{
				if (block2 is BlockPowered)
				{
					tileEntity = (block2 as BlockPowered).CreateTileEntity(chunk);
				}
				else if (block2 is BlockPowerSource)
				{
					tileEntity = (block2 as BlockPowerSource).CreateTileEntity(chunk);
				}
				else if (block2 is BlockMultiblockSlavePowered)
				{
					tileEntity = (block2 as BlockMultiblockSlavePowered).CreateTileEntity(chunk);
				}
				tileEntity.localChunkPos = World.toBlock(tileEntityPos);
				BlockEntityData blockEntity = chunk.GetBlockEntity(tileEntityPos);
				if (blockEntity != null)
				{
					((TileEntityPowered)tileEntity).BlockTransform = blockEntity.transform;
				}
				((TileEntityPowered)tileEntity).InitializePowerData();
				chunk.AddTileEntity(tileEntity);
			}
			return tileEntity as TileEntityPowered;
		}


		/**
		 * Decrease durability of tool when used.
		 */

		private void DecreaseDurability(ItemActionConnectPower.ConnectPowerData _actionData)
		{
			if (_actionData.invData.itemValue.MaxUseTimes > 0)
			{
				if (_actionData.invData.itemValue.UseTimes + 1f < (float)_actionData.invData.itemValue.MaxUseTimes)
				{
					_actionData.invData.itemValue.UseTimes += EffectManager.GetValue(PassiveEffects.DegradationPerUse, _actionData.invData.itemValue, 1f, _actionData.invData.holdingEntity, null, _actionData.invData.itemValue.ItemClass.ItemTags, true, true, true, true, 1, true);
					return;
				}
				_actionData.invData.holdingEntity.inventory.DecHoldingItem(1);
			}
		}


		/**
		 * Gets hand transform position.
		 */

		private Transform GetHandTransform(EntityAlive holdingEntity)
		{
			Transform transform = holdingEntity.RootTransform.Find("Graphics").FindInChilds(holdingEntity.GetRightHandTransformName(), true);
			Transform result = null;
			if (transform.childCount > 0)
			{
				result = transform;
			}
			else
			{
				Transform transform2 = holdingEntity.RootTransform.Find("Camera").FindInChilds(holdingEntity.GetRightHandTransformName(), true);
				if (transform2.childCount > 0)
				{
					result = transform2;
				}
			}
			return result;
		}


		private Vector3 wireOffset = Vector3.zero;

	}
}