﻿using System;
using System.Xml;

namespace FennecCore.Scripts
{

    /**
     * Allows an entity to enter fly mode.
     * <triggered_effect trigger="" action="MinEventActionFlyMode, FennecCore" active="" />
     */

    public class MinEventActionFlyMode : MinEventActionTargetedBase
    {

        /**
         * Setting fly mode on or off.
         */

        public override void Execute(MinEventParams _params)
        {
            Log.Out("Executing.");
            for (int i = 0; i < this.targets.Count; i++)
            {
                if (this.targets[i] == null)
                {
                    continue;
                }

                Log.Out("Changing");
                this.targets[i].IsFlyMode = new DataItem<bool>(active);
                Log.Out("Active: " + active.ToString());
                Log.Out("Data item: " + this.targets[i].IsFlyMode.Value.ToString());
            }
        }


        /**
         * Checks if we can activate
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            Log.Out("Checking can activate.");
            return base.CanExecute(_eventType, _params);
        }



        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "active")
                {
                    if (!StringParsers.TryParseBool(_attribute.Value, out this.active))
                    {
                        throw new Exception("Parameter: 'active' must be set to true / false.");
                    }
                }
            }
            return flag;
        }


        private bool active;
    }
}