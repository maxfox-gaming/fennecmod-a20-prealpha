﻿using System;
using System.Xml;
using System.Collections.Generic;

namespace FennecCore.Scripts
{

    /**
     * Creates an item in an inventory around the entity within a certain range. -->
     * <triggered_effect trigger="" action="AddItemToInventory, Mods" items="" min_counts="" max_counts="" probabilities="" inventory_names="" range="" directions="" />
     */

    public class MinEventActionAddItemToInventory : MinEventActionTargetedBase
    {

        /**
         * Executing the imvemtory drop.
         */

        public override void Execute(MinEventParams _params)
        {
            World world;
            if ((world = GameManager.Instance.World) == null)
            {
                return;
            }

            System.Random random = new System.Random();

            for (int i = 0; i < this.targets.Count; i++)
            {
                if (this.targets[i] == null)
                {
                    continue;
                }

                Bag bag = this.targets[i].bag;
                if (bag == null)
                {
                    continue;
                }

                List<Vector3i> coordinates = CoordinateHelper.GetCoordinatesAround(targets[i].GetBlockPosition(), this.range, directions);
                Dictionary<Vector3i, TileEntityLootContainer> lootContainers = CoordinateHelper.GetTileEntitiesInCoordinatesWithType<TileEntityLootContainer>(world, coordinates);
                if (lootContainers.Count == 0)
                {
                    continue;
                }

                foreach (KeyValuePair<Vector3i, TileEntityLootContainer> entry in lootContainers)
                {
                    if (!inventoryNames.Contains(entry.Value.blockValue.Block.GetBlockName()))
                    {
                        continue;
                    }

                    List<ItemStack> itemStacks = new List<ItemStack>();
                    bool allItemsNeeded = true;
                    for (int j = 0; j < this.items.Count; j += 1)
                    {
                        float probValue = (float)random.NextDouble();
                        if (probValue > probabilities[j])
                        {
                            continue;
                        }

                        ItemValue itemValue = ItemClass.GetItem(this.items[j]);
                        if (itemValue.Equals(ItemValue.None))
                        {
                            continue;
                        }

                        int itemCount = random.Next(minCounts[j], maxCounts[j] + 1);
                        ItemStack stack = new ItemStack(itemValue, itemCount);
                        if (useInventory)
                        {
                            allItemsNeeded &= bag.CanTakeItem(stack);
                            if (!allItemsNeeded)
                            {
                                return;
                            }
                        }

                        itemStacks.Add(stack);
                    }

                    ItemStack[] currentState = bag.GetSlots();
                    ItemStack[] previousState = currentState;

                    if (useInventory)
                    {
                        bool removed;
                        currentState = InventoryHelper.RemoveItemStacksInInventory(currentState, itemStacks, out removed);
                        if (!removed)
                        {
                            SetBagState(bag, previousState);
                            return;
                        }
                        SetBagState(bag, currentState);
                    }

                    TileEntityLootContainer container = entry.Value;
                    ItemStack[] containerState = container.items;

                    foreach (ItemStack stack in itemStacks)
                    {
                        bool added = false;
                        if (container.HasItem(stack.itemValue))
                        {
                            added &= container.TryStackItem(0, stack);
                        }

                        if (!added)
                        {
                            added &= container.AddItem(stack);
                        }

                        if (!added)
                        {
                            SetLootContainerState(container, containerState);
                            if (useInventory)
                            {
                                SetBagState(bag, previousState);
                            }
                            return;
                        }

                        container.SetModified();
                    }
                }
            }
        }


        /**
         * Checks whether the event can happen.
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            VerifyInputs();
            return base.CanExecute(_eventType, _params) && items.Count > 0
                && range.x >= 0 && range.y >= 0 && range.z >= 0 && inventoryNames.Count > 0
                && this.items.Count == this.minCounts.Count && this.items.Count == this.maxCounts.Count
                && this.items.Count == this.probabilities.Count;
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "items")
                {
                    this.items = StringHelpers.WriteStringToList(_attribute.Value);
                    foreach (string item in items)
                    {
                        if (ItemClass.GetItem(item) == ItemValue.None)
                        {
                            throw new Exception("Item with name " + item + " not found.");
                        }
                    }
                    return true;
                }

                if (name == "use_inventory")
                {
                    if (bool.TryParse(_attribute.Value, out this.useInventory))
                    {
                        return true;
                    }
                    return false;
                }


                if (name == "min_counts")
                {
                    this.minCounts = StringHelpers.WriteStringToIntList(_attribute.Value);
                    return true;
                }

                if (name == "max_counts")
                {
                    this.maxCounts = StringHelpers.WriteStringToIntList(_attribute.Value);
                    return true;
                }

                if (name == "probabilities")
                {
                    this.probabilities = StringHelpers.WriteStringToFloatList(_attribute.Value);
                    return true;
                }

                if (name == "inventory_names")
                {
                    this.inventoryNames = StringHelpers.WriteStringToList(_attribute.Value);
                    if (inventoryNames[0].ToLower() != "all")
                    {
                        this.inventoryNames = BlockHelpers.CheckBlocksDefined(this.inventoryNames);
                        return true;
                    }
                    return true;
                }

                if (name == "range")
                {
                    this.range = StringHelpers.WriteStringToVector3i(_attribute.Value);
                    if (range == Vector3i.zero)
                    {
                        return false;
                    }
                    return true;
                }

                if (name == "directions")
                {
                    this.directions = BlockFaceFCMapping.Map(_attribute.Value);
                    return true;
                }

            }
            return flag;
        }


        /**
         * Check that list values are the same, and if not, try to correct them if possible.
         */

        private void VerifyInputs()
        {
            if (minCounts.Count != items.Count)
            {
                this.minCounts = ListHelpers.MakeListLength<int>(minCounts, items.Count);
            }

            if (maxCounts.Count != items.Count)
            {
                this.maxCounts = ListHelpers.MakeListLength<int>(maxCounts, items.Count);
            }

            if (probabilities.Count != items.Count)
            {
                this.probabilities = ListHelpers.MakeListLength<float>(probabilities, items.Count);
            }
        }


        /**
         * Restores previous state of player bag or inventory.
         */

        private void SetBagState(Bag bag, ItemStack[] state)
        {
            bag.SetSlots(state);
        }


        /**
         * Sets loot container state
         */

        private void SetLootContainerState(TileEntityLootContainer te, ItemStack[] state)
        {
            te.items = state;
            te.SetModified();
        }



        private List<string> items = new List<string>();
        private List<string> inventoryNames = new List<string>();
        private List<int> minCounts = new List<int>() { 1 };
        private List<int> maxCounts = new List<int>() { 1 };
        private List<float> probabilities = new List<float>() { 1.0f };
        private Vector3i range = Vector3i.one;
        private BlockFaceFC directions = BlockFaceFC.ALL;
        private bool useInventory = false; // Hook up
    }
}