﻿using System;
using System.Xml;

namespace FennecCore.Scripts
{
    /**
     * Allows an entity to enter fly mode.
     * <triggered_effect trigger="" action="MinEventActionNoCollideMode, JetpackMod" active="" />
     */

    public class MinEventActionNoCollideMode : MinEventActionTargetedBase
    {

        /**
         * Setting fly mode on or off.
         */

        public override void Execute(MinEventParams _params)
        {
            for (int i = 0; i < this.targets.Count; i++)
            {
                if (this.targets[i] == null)
                {
                    continue;
                }

                this.targets[i].IsNoCollisionMode = new DataItem<bool>(active);
            }
        }


        /**
         * Checks if we can activate
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            return base.CanExecute(_eventType, _params);
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "active")
                {
                    if (!StringParsers.TryParseBool(_attribute.Value, out this.active))
                    {
                        throw new Exception("Parameter: 'active' must be set to true / false.");
                    }
                }
            }
            return flag;
        }

        private bool active;
    }
}