﻿using System.Xml;
using System.Collections.Generic;

namespace FennecCore.Scripts
{

    /**
     * Can move an item into the hotbar selected slot.
     * <triggered_effect trigger="onSelfBuffRemove" action="MoveItemToActiveSlot, Mods" item_tags="tags" require_all_tags="true" />
     * 
     * NOTE: Do not use this on the end of an item action, it will probably not dupe the item correctly.
     * Instead, make a buff that has a short delay between itself and the end to trigger this.
     */

    public class MinEventActionMoveItemToActiveSlot : MinEventActionTargetedBase
    {

        /**
         * Executing the upgrades
         */

        public override void Execute(MinEventParams _params)
        {
            if (this.targets == null || GameManager.Instance == null)
            {
                return;
            }
            for (int i = 0; i < this.targets.Count; i += 1)
            {
                if (this.targets[i] as EntityPlayerLocal != null && this.targets[i].bag != null)
                {
                    ItemStack[] playerBagItems = targets[i].bag.GetSlots();
                    int slotToSwap = -1;
                    for (int j = 0; j < playerBagItems.Length; j += 1)
                    {
                        if (playerBagItems[j].IsEmpty() || playerBagItems[j] == null)
                        {
                            continue;
                        }

                        if (ItemHasTags(playerBagItems[j]))
                        {
                            slotToSwap = j;
                            break;
                        }
                    }

                    if (slotToSwap == -1)
                    {
                        return;
                    }

                    int holdingIndex = targets[i].inventory.holdingItemIdx;
                    ItemStack swapItem = (targets[i].bag.GetSlots())[slotToSwap].Clone();
                    targets[i].bag.SetSlot(slotToSwap, ItemStack.Empty.Clone());
                    targets[i].inventory.SetItem(holdingIndex, swapItem);
                }
            }
        }

        /**
         * Checks whether the event can happen.
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            return base.CanExecute(_eventType, _params) && itemTags != null && itemTags.Count > 0;
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "item_tags")
                {
                    this.itemTags = StringHelpers.WriteStringToList(_attribute.Value);
                    return true;
                }

                bool requireAll;
                if (name == "require_all_tags")
                {
                    if (bool.TryParse(_attribute.Value, out requireAll)) ;
                    {
                        this.requireAllTags = requireAll;
                        return true;
                    }
                }

            }
            return flag;
        }


        private bool ItemHasTags(ItemStack item)
        {
            List<string> targetItemTags = new List<string>();
            string tags;

            if (item.itemValue == null)
            {
                return false;
            }

            if (PropertyHelpers.PropExists(item.itemValue.ItemClass.Properties, "Tags", out tags))
            {
                targetItemTags = StringHelpers.WriteStringToList(tags);
            }
            else
            {
                return false;
            }

            if (targetItemTags.Count == 0)
            {
                return false;
            }

            int found = 0;
            foreach (string tag in itemTags)
            {
                if (targetItemTags.Contains(tag))
                {
                    if (requireAllTags)
                    {
                        found += 1;
                        continue;
                    }
                    return true;
                }
            }
            return (found >= itemTags.Count);
        }


        private List<string> itemTags;
        private bool requireAllTags;
    }
}