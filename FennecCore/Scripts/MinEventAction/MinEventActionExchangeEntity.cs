﻿using System.Xml;
using System.Collections.Generic;
using UnityEngine;

namespace FennecCore.Scripts
{

    /**
     * Can move an item into the hotbar selected slot.
     * <triggered_effect trigger="onSelfBuffRemove" action="ExchangeEntity, Mods" entitygroup="name" />
     * 
     *
     */

    public class MinEventActionExchangeEntity : MinEventActionTargetedBase
    {

        /**
         * Executing the transfer of items.
         */

        public override void Execute(MinEventParams _params)
        {
            if (this.targets == null)
            {
                return;
            }
            if (GameManager.Instance == null)
            {
                return;
            }

            World world;
            if ((world = GameManager.Instance.World) == null)
            {
                return;
            }

            for (int i = 0; i < this.targets.Count; i += 1)
            {
                // Don't want players transforming into entities...
                if (targets[i] is EntityPlayer)
                {
                    continue;
                }

                Vector3 pos = targets[i].GetPosition();
                Vector3 rot = targets[i].rotation;
                EntityHelper.SpawnEntityFromGroupAt(this.entityGroupName, world, pos, rot, buffs);
                this.targets[i].timeStayAfterDeath = 0;
                this.targets[i].SetDead();
            }
        }

        /**
         * Checks whether the event can happen.
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            return base.CanExecute(_eventType, _params) && entityGroupName != null;
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "entitygroup")
                {
                    this.entityGroupName = _attribute.Value;
                    return true;
                }
                if (name == "buffs")
                {
                    this.buffs = StringHelpers.WriteStringToList(_attribute.Value);
                    return true;
                }

            }
            return flag;
        }


        private string entityGroupName;
        private List<string> buffs = new List<string>();
    }
}