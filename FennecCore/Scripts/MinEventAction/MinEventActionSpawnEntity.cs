﻿using System;
using System.Xml;
using System.Collections.Generic;
using UnityEngine;

namespace FennecCore.Scripts
{
    /**
     * Can drop an item on a triggered event.
     */

    public class MinEventActionSpawnEntity : MinEventActionTargetedBase
    {

        /**
         * Executing the drop
         */

        public override void Execute(MinEventParams _params)
        {
            if (this.minDistance > this.maxDistance)
            {
                this.minDistance = this.maxDistance;
            }

            for (int i = 0; i < this.targets.Count; i++)
            {
                if (this.targets[i] as Entity == null)
                {
                    continue;
                }

                Vector3 currentPosition = this.targets[i].GetPosition();
                EntityHelper.SpawnEntitiesAroundPosition(entitygroup, currentPosition, minDistance, maxDistance, GetCountValue());
            }
        }


        /**
         * Checks whether the event can happen.
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            return base.CanExecute(_eventType, _params) && this.entitygroup != "" && this.minDistance >= 0 && this.maxDistance >= 0 && this.countLower > 0 && this.countUpper > 0;
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "entitygroup")
                {
                    this.entitygroup = _attribute.Value;
                    return true;
                }

                if (name == "count")
                {
                    List<int> count = StringHelpers.WriteStringToIntList(_attribute.Value);
                    if (count == null)
                    {
                        return false;
                    }

                    EvaluateCount(count);
                    return true;
                }

                if (name == "min_distance")
                {
                    if (!int.TryParse(_attribute.Value, out minDistance))
                    {
                        throw new Exception("Could not parse value as an integer.");
                    }
                    if (minDistance < 0)
                    {
                        throw new Exception("Minimum distance must be non-negative.");
                    }
                    return true;
                }

                if (name == "max_distance")
                {
                    if (!int.TryParse(_attribute.Value, out maxDistance))
                    {
                        throw new Exception("Could not parse value as an integer.");
                    }
                    if (maxDistance < 0)
                    {
                        throw new Exception("Minimum distance must be non-negative.");
                    }
                    return true;
                }

                if (name == "attack_target")
                {
                    if (!bool.TryParse(_attribute.Value, out attack))
                    {
                        throw new Exception("Could not parse value as a boolean.");
                    }
                    return true;
                }

            }
            return flag;
        }


        /**
         * Evaluates the count="a,b" between a and b from the XML.
         */

        private void EvaluateCount(List<int> counts)
        {
            if (counts.Count == 0)
            {
                return;
            }

            if (counts.Count == 1)
            {
                this.countLower = counts[0];
                this.countUpper = counts[0];
                return;
            }

            if (counts.Count == 2)
            {
                this.countLower = Mathf.Min(counts[0], counts[1]);
                this.countUpper = Mathf.Max(counts[0], counts[1]);
                return;
            }

            throw new ArgumentException("Too many parameters in count.");
        }


        /**
         * Generates a random integer between countLower and countUpper.
         */

        private int GetCountValue()
        {
            System.Random random = new System.Random();
            return random.Next(countLower, countUpper + 1);
        }


        private string entitygroup;
        private int countLower = 1;
        private int countUpper = 1;
        private int minDistance = 1;
        private int maxDistance = 3;
        private bool attack = false;
    }
}