﻿using System;
using System.Xml;

namespace FennecCore.Scripts
{

    /**
     * Can move an item into the hotbar selected slot.
     * <triggered_effect trigger="" action="SwapEquipment, Mods" swap_out="" swap_in="" preserve_mods="true" />
     */

    public class MinEventActionSwapEquipment : MinEventActionTargetedBase
    {

        /**
         * Executing the upgrades
         */

        public override void Execute(MinEventParams _params)
        {
            if (this.targets == null || GameManager.Instance == null)
            {
                return;
            }

            for (int i = 0; i < this.targets.Count; i += 1)
            {
                if (targets[i] != null)
                {
                    Equipment equipment = targets[i].equipment;
                    if (equipment == null)
                    {
                        continue;
                    }

                    int slots = equipment.GetItems().Length;
                    for (int slot = 0; slot < slots; slot += 1)
                    {
                        ItemValue swapOutItem;
                        if ((swapOutItem = equipment.GetSlotItem(slot)) != ItemClass.GetItem(swapOut))
                        {
                            continue;
                        }

                        ItemValue swapInItem = ItemClass.GetItem(swapIn);
                        swapInItem.Meta = swapOutItem.Meta;
                        if (swapOutItem.HasQuality && swapInItem.HasQuality)
                        {
                            swapInItem.Quality = swapOutItem.Quality;
                        }

                        if (preserveMods)
                        {
                            if (swapOutItem.HasMods() && swapInItem.HasModSlots)
                            {
                                swapInItem.Modifications = swapOutItem.Modifications;
                                swapInItem.CosmeticMods = swapOutItem.CosmeticMods;
                            }
                        }

                        equipment.SetSlotItem(slot, swapInItem);
                        break;
                    }
                }
            }
        }

        /**
         * Checks whether the event can happen.
         */

        public override bool CanExecute(MinEventTypes _eventType, MinEventParams _params)
        {
            return base.CanExecute(_eventType, _params) && swapOut != null && swapIn != null;
        }


        /**
         * Converts the XML into the drop info needed.
         */

        public override bool ParseXmlAttribute(XmlAttribute _attribute)
        {
            bool flag = base.ParseXmlAttribute(_attribute);
            if (!flag)
            {
                string name = _attribute.Name;
                if (name == "swap_out")
                {
                    swapOut = _attribute.Value;
                    if (ItemClass.GetItem(swapOut) == ItemValue.None)
                    {
                        throw new Exception("Item for swap_out item " + swapOut + " not found.");
                    }
                    return true;
                }

                if (name == "swap_in")
                {
                    swapIn = _attribute.Value;
                    if (ItemClass.GetItem(swapIn) == ItemValue.None)
                    {
                        throw new Exception("Item for swap_in item " + swapIn + " not found.");
                    }
                    return true;
                }

                if (name == "preserve_mods")
                {
                    if (!bool.TryParse(_attribute.Value, out preserveMods))
                    {
                        throw new Exception("Could not parse preserve_mods value.");
                    }
                    return true;
                }
            }
            return flag;
        }


        private string swapOut;
        private string swapIn;
        private bool preserveMods = true;
    }
}