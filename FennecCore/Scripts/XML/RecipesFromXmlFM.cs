﻿using System;
using System.Xml;
using System.Collections;
using System.Globalization;

namespace FennecCore.Scripts
{
	public class RecipesFromXmlFM
	{
		public static bool SaveRecipes(string _filename)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.CreateXmlDeclaration();
			XmlElement node = xmlDocument.AddXmlElement("recipes");
			foreach (Recipe recipe in CraftingManager.GetAllRecipes())
			{
				XmlElement xmlElement = node.AddXmlElement("recipe").SetAttrib("name", recipe.GetName()).SetAttrib("count", recipe.count.ToString()).SetAttrib("scrapable", recipe.scrapable.ToString());
				if (recipe.tooltip != null)
				{
					xmlElement.SetAttrib("tooltip", recipe.tooltip);
				}
				if (!string.IsNullOrEmpty(recipe.craftingArea))
				{
					xmlElement.SetAttrib("craft_area", recipe.craftingArea);
				}
				if (recipe.craftingToolType != 0)
				{
					xmlElement.SetAttrib("craft_tool", ItemClass.GetForId(recipe.craftingToolType).GetItemName());
				}
				for (int i = 0; i < recipe.ingredients.Count; i++)
				{
					ItemStack itemStack = recipe.ingredients[i];
					if (((itemStack != null) ? itemStack.itemValue : null) != null && ItemClass.GetForId(itemStack.itemValue.type) != null)
					{
						xmlElement.AddXmlElement("ingredient").SetAttrib("name", ItemClass.GetForId(itemStack.itemValue.type).GetItemName()).SetAttrib("count", itemStack.count.ToString());
					}
				}
				if (recipe.wildcardForgeCategory)
				{
					xmlElement.AddXmlElement("wildcard_forge_category");
				}
			}
			xmlDocument.Save(_filename);
			return true;
		}

		// Token: 0x06005D44 RID: 23876 RVA: 0x0028F8F4 File Offset: 0x0028DAF4
		public static IEnumerator LoadRecipies(XmlFile _xmlFile)
		{
			XmlElement documentElement = _xmlFile.XmlDoc.DocumentElement;
			if (documentElement.ChildNodes.Count == 0)
			{
				throw new Exception("No element <recipes> found!");
			}
			MicroStopwatch msw = new MicroStopwatch(true);

			// Loads recipes from transformer blocks
			ExtendedRecipeList.InitializeExtendedRecipes();

			// Loads regular recipes
			foreach (object obj in documentElement.ChildNodes)
			{
				XmlNode xmlNode = (XmlNode)obj;
				if (xmlNode.NodeType == XmlNodeType.Element && xmlNode.Name.Equals("recipe"))
				{
					XmlElement xmlElement = (XmlElement)xmlNode;
					Recipe recipe = new Recipe();
					if (!xmlElement.HasAttribute("name"))
					{
						throw new Exception("Attribute 'name' missing on recipe");
					}
					string attribute = xmlElement.GetAttribute("name");
					recipe.itemValueType = ItemClass.GetItem(attribute, false).type;
					if (recipe.itemValueType == 0)
					{
						throw new Exception("No item/block with name '" + attribute + "' existing");
					}
					recipe.count = 1;
					if (xmlElement.HasAttribute("count"))
					{
						recipe.count = int.Parse(xmlElement.GetAttribute("count"));
					}
					recipe.scrapable = false;
					if (xmlElement.HasAttribute("scrapable"))
					{
						recipe.scrapable = StringParsers.ParseBool(xmlElement.GetAttribute("scrapable"), 0, -1, true);
					}
					recipe.materialBasedRecipe = false;
					if (xmlElement.HasAttribute("material_based"))
					{
						recipe.materialBasedRecipe = StringParsers.ParseBool(xmlElement.GetAttribute("material_based"), 0, -1, true);
					}
					if (xmlElement.HasAttribute("tags"))
					{
						recipe.tags = FastTags.Parse(xmlElement.GetAttribute("tags") + "," + attribute);
					}
					else if (xmlElement.HasAttribute("tag"))
					{
						recipe.tags = FastTags.Parse(xmlElement.GetAttribute("tag") + "," + attribute);
					}
					if (xmlElement.HasAttribute("tooltip"))
					{
						recipe.tooltip = xmlElement.GetAttribute("tooltip");
					}
					if (xmlElement.HasAttribute("craft_area"))
					{
						string attribute2 = xmlElement.GetAttribute("craft_area");
						recipe.craftingArea = attribute2;
					}
					else
					{
						recipe.craftingArea = "";
					}
					if (xmlElement.HasAttribute("craft_tool"))
					{
						recipe.craftingToolType = ItemClass.GetItem(xmlElement.GetAttribute("craft_tool"), false).type;
						ItemClass.list[ItemClass.GetItem(xmlElement.GetAttribute("craft_tool"), false).type].bCraftingTool = true;
					}
					else
					{
						recipe.craftingToolType = 0;
					}
					if (xmlElement.HasAttribute("craft_time"))
					{
						float craftingTime = 0f;
						StringParsers.TryParseFloat(xmlElement.GetAttribute("craft_time"), out craftingTime, 0, -1, NumberStyles.Any);
						recipe.craftingTime = craftingTime;
					}
					else
					{
						recipe.craftingTime = -1f;
					}
					if (xmlElement.HasAttribute("learn_exp_gain"))
					{
						float num = 0f;
						if (StringParsers.TryParseFloat(xmlElement.GetAttribute("learn_exp_gain"), out num, 0, -1, NumberStyles.Any))
						{
							recipe.unlockExpGain = (int)num;
						}
						else
						{
							recipe.unlockExpGain = 20;
						}
					}
					else
					{
						recipe.unlockExpGain = -1;
					}
					if (xmlElement.HasAttribute("craft_exp_gain"))
					{
						float num2 = 0f;
						if (StringParsers.TryParseFloat(xmlElement.GetAttribute("craft_exp_gain"), out num2, 0, -1, NumberStyles.Any))
						{
							recipe.craftExpGain = (int)num2;
						}
						else
						{
							recipe.craftExpGain = 1;
						}
					}
					else
					{
						recipe.craftExpGain = -1;
					}
					recipe.UseIngredientModifier = true;
					if (xmlElement.HasAttribute("use_ingredient_modifier"))
					{
						recipe.UseIngredientModifier = StringParsers.ParseBool(xmlElement.GetAttribute("use_ingredient_modifier"), 0, -1, true);
					}
					recipe.Effects = MinEffectController.ParseXml(xmlElement, null, MinEffectController.SourceParentType.None, null);
					foreach (object obj2 in xmlElement.ChildNodes)
					{
						XmlNode xmlNode2 = (XmlNode)obj2;
						if (xmlNode2.NodeType == XmlNodeType.Element)
						{
							xmlNode2.Name.Equals("effect_group");
						}
						if (xmlNode2.NodeType == XmlNodeType.Element && xmlNode2.Name.Equals("ingredient"))
						{
							XmlElement xmlElement2 = (XmlElement)xmlNode2;
							if (!xmlElement2.HasAttribute("name"))
							{
								throw new Exception("Attribute 'name' missing on ingredient in recipe '" + attribute + "'");
							}
							string attribute3 = xmlElement2.GetAttribute("name");
							ItemValue item = ItemClass.GetItem(attribute3, false);
							if (item.IsEmpty())
							{
								throw new Exception("No item/block/material with name '" + attribute3 + "' existing");
							}
							int count = 1;
							if (xmlElement2.HasAttribute("count"))
							{
								count = int.Parse(xmlElement2.GetAttribute("count"));
							}
							recipe.AddIngredient(item, count);
						}
						else if (xmlNode2.NodeType == XmlNodeType.Element && xmlNode2.Name.Equals("wildcard_forge_category"))
						{
							recipe.wildcardForgeCategory = true;
						}
					}
					recipe.Init();
					CraftingManager.AddRecipe(recipe);
				}
				if (msw.ElapsedMilliseconds > 50L)
				{
					yield return null;
					msw.ResetAndRestart();
				}
			}
			IEnumerator enumerator = null;
			yield break;
			yield break;
		}
	}
}

