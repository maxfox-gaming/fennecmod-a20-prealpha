using System;
using System.Collections.Generic;

namespace FennecCore.Scripts
{

    /**
     * Class to contain methods for string manipulations that are used across modlets.
     */

    public static class StringHelpers
    {
        /**
         * Reads a comma separated string into a list.
         */

        public static List<string> WriteStringToList(string _s, char separator = ',')
        {
            if (_s == null)
            {
                return new List<string>() { "" };
            }

            if (!_s.Contains(separator.ToString()))
            {
                return new List<string>() { _s.Trim() };
            }

            string[] strings = _s.Split(separator);
            List<string> list = new List<string>();
            foreach (string str in strings)
            {
                list.Add(str.Trim());
            }
            return list;
        }


        /**
         * Reads a comma separated string into a list of integers if possible.
         * Any strings that cannot be parsed as integers will be skipped.
         */

        public static List<int> WriteStringToIntList(string _s)
        {
            if (_s == null)
            {
                return new List<int>();
            }

            if (!_s.Contains(","))
            {
                int _i;
                if (!int.TryParse(_s, out _i))
                {
                    return new List<int>();
                }


                return new List<int>() { _i };
            }

            string[] strings = _s.Split(',');
            List<int> list = new List<int>();
            foreach (string str in strings)
            {
                int _i;
                if (int.TryParse(str, out _i))
                {
                    list.Add(_i);
                }
            }
            return list;
        }


        /**
         * Reads a string into a float list.
         * Any strings that cannot be parsed as floats will be skipped.
         */

        public static List<float> WriteStringToFloatList(string _s)
        {
            if (_s == null)
            {
                return new List<float>();
            }

            if (!_s.Contains(","))
            {
                float _i;
                if (!float.TryParse(_s, out _i))
                {
                    return new List<float>();
                }


                return new List<float>() { _i };
            }

            string[] strings = _s.Split(',');
            List<float> list = new List<float>();
            foreach (string str in strings)
            {
                float _i;
                if (float.TryParse(str, out _i))
                {
                    list.Add(_i);
                }
            }
            return list;
        }


        /**
         * Reads a comma separated string into a list of floats if possible.
         * Any strings that cannot be parsed as integers will be skipped.
         */

        public static List<double> WriteStringToDoubleList(string _s)
        {
            if (_s == null)
            {
                return new List<double>();
            }

            if (!_s.Contains(","))
            {
                double _f;
                if (!double.TryParse(_s, out _f))
                {
                    return new List<double>();
                }


                return new List<double>() { _f };
            }

            string[] strings = _s.Split(',');
            List<double> list = new List<double>();
            foreach (string str in strings)
            {
                double _f;
                if (double.TryParse(str, out _f))
                {
                    list.Add(_f);
                }
            }
            return list;
        }



        /**
         * Writes a list to a comma separated string.
         */

        public static string WriteListToString(List<string> _list, string join = ",")
        {
            if (_list == null)
            {
                return "";
            }

            switch (_list.Count)
            {
                case 0:
                    return "";
                case 1:
                    return _list[0];
                default:
                    return String.Join(join, _list);
            }
        }


        /**
         * Writes a vector3i to a string.
         */

        public static string WriteVector3iToString(Vector3i _vector)
        {
            return _vector.ToStringNoBlanks();
        }


        /**
         * Writes a string to a vector3i.
         */

        public static Vector3i WriteStringToVector3i(string _s)
        {
            // Removes enclosing brackets if string is provided as such
            _s = _s.Trim('(', ')');

            List<string> values = WriteStringToList(_s);
            switch (values.Count)
            {
                case 1:
                    int vector3iValue;
                    if (!int.TryParse(_s, out vector3iValue))
                    {
                        return Vector3i.zero;
                    }
                    return new Vector3i(vector3iValue, vector3iValue, vector3iValue);
                case 3:
                    List<int> valueInts = new List<int>();
                    foreach (string value in values)
                    {
                        int valueInt;
                        if (!int.TryParse(value, out valueInt))
                        {
                            return Vector3i.zero;
                        }
                        valueInts.Add(valueInt);
                    }
                    return new Vector3i(valueInts[0], valueInts[1], valueInts[2]);
                default:
                    return Vector3i.zero;
            }
        }



        /**
         * Writes a string to a non-negative integer
         */

        public static int WriteStringToNonNegativeInt(string _s)
        {
            int output;
            if (!StringParsers.TryParseSInt32(_s, out output))
            {
                throw new Exception("Could not write " + _s + " as a number.");
            }
            if (output < 0)
            {
                throw new Exception("Integer " + _s + " is less than zero.");
            }
            return output;
        }


        /**
        * Writes a string to a non-negative integer
        */

        public static int WriteStringToPositiveInt(string _s)
        {
            int output;
            if (!StringParsers.TryParseSInt32(_s, out output))
            {
                throw new Exception("Could not write " + _s + " as a number.");
            }
            if (output < 1)
            {
                throw new Exception("Integer " + _s + " is less than zero.");
            }
            return output;
        }


        /**
         * Converts number or list of numbers (as strings) to a Vector3i.
         */

        public static Vector3i WriteStringToNonNegativeVector3Range(string _s)
        {
            List<string> _list = StringHelpers.WriteStringToList(_s);
            switch (_list.Count)
            {
                case 1:
                    _list[1] = _list[0];
                    _list[2] = _list[0];
                    break;
                case 3:
                    break;
                default:
                    throw new Exception("Entry must either be one number, or 3 numbers with commas between (like 1,2,5)");
            }

            int rangeX = StringHelpers.WriteStringToNonNegativeInt(_list[0]);
            int rangeY = StringHelpers.WriteStringToNonNegativeInt(_list[1]);
            int rangeZ = StringHelpers.WriteStringToNonNegativeInt(_list[2]);
            return new Vector3i(rangeX, rangeY, rangeZ);
        }
    }
}