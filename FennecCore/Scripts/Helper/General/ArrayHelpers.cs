﻿namespace FennecCore.Scripts
{
    public static class ArrayHelpers
    {
        public static T[] Concatenate<T>(this T[] array1, T[] array2)
        {
            if (array1 == null && array2 == null)
            {
                return null;
            }

            if (array2 == null)
            {
                return array1;
            }

            if (array1 == null)
            {
                return array2;
            }

            T[] concatenated = new T[array1.Length + array2.Length];
            for (int i = 0; i < array1.Length; i += 1)
            {
                concatenated[i] = array1[i];
            }
            for (int i = 0; i < array2.Length; i += 1)
            {
                concatenated[array1.Length + i] = array2[i];
            }

            return concatenated;
        }
    }
}