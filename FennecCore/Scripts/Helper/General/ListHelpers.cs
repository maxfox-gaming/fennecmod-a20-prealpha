﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    static class ListHelpers
    {

        /**
         * This makes any list a certain length.
         * If the length is shorter, the input will be trimmed down.
         * If the length is longer, the {copyPos} entry will be copied until the input length is desired.
         */

        public static List<T> MakeListLength<T>(List<T> input, int length, int copyPos = 0)
        {
            if (copyPos >= input.Count)
            {
                copyPos -= 1;
                MakeListLength<T>(input, length, copyPos);
            }

            if (copyPos < 0)
            {
                copyPos = 0;
                MakeListLength<T>(input, length, copyPos);
            }

            if (input.Count > length)
            {
                List<T> output = new List<T>();
                for (int i = 0; i < length; i += 1)
                {
                    output[i] = input[i];
                }
                return output;
            }

            T copyItem = input[copyPos];
            int inputLength = input.Count;
            int toAdd = length - inputLength;

            for (int i = 0; i < toAdd; i += 1)
            {
                input.Add(copyItem);
            }

            return input;
        }
    }
}