﻿namespace FennecCore.Scripts
{
    public static class PropertyHelpers
    {
        /**
         * Checks whether a property is defined, and returns it as a string if so.
         */

        public static bool PropExists(DynamicProperties properties, string key, out string value)
        {
            value = "";
            if (properties.Values.ContainsKey(key))
            {
                value = properties.Values[key].ToString();
                return true;
            }
            return false;
        }
    }
}