﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FennecCore.Scripts
{
    /**
     * Class to contain helper methods for navigating through coordinates and chunks in the world.
     */

    public static class CoordinateHelper
    {
        /**
         * Returns a list of all coordinates around a position.
         * Range in X, Y and Z can be specified.
         * Example: Adjacent blocks NSEW - set rangeX = rangeZ = 1, rangeY = 0, cardinal to true.
         */

        public static List<Vector3i> GetCoordinatesAround(Vector3i _pos, bool onlyCardinal = false, int rangeX = 1, int rangeY = 1, int rangeZ = 1, bool includeCurrent = false)
        {
            if (rangeX < 0 | rangeY < 0 | rangeZ < 0)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            List<Vector3i> positions = new List<Vector3i>();
            for (int x = -rangeX; x <= rangeX; x += 1)
            {
                for (int y = -rangeY; y <= rangeY; y += 1)
                {
                    for (int z = -rangeZ; z <= rangeZ; z += 1)
                    {
                        Vector3i step = new Vector3i(x, y, z);
                        if (!includeCurrent && step == Vector3i.zero)
                        {
                            continue;
                        }

                        if (onlyCardinal && !IsCardinal(step))
                        {
                            continue;
                        }

                        Vector3i coordinate = _pos + step;
                        if (coordinate.y < 0 | coordinate.y > 255)
                        {
                            continue;
                        }

                        positions.Add(coordinate);
                    }
                }
            }
            return positions;
        }


        /**
         * Shorter version to pass in vector3i of ranges instead.
         */

        public static List<Vector3i> GetCoordinatesAround(Vector3i _pos, Vector3i range, bool onlyCardinal = false, bool includeCurrent = false)
        {
            return GetCoordinatesAround(_pos, onlyCardinal, range.x, range.y, range.z, includeCurrent);
        }


        /**
         * Gets a set of coordinates around a LIST of blocks.
         */

        public static List<Vector3i> GetCoordinatesAround(List<Vector3i> _positions, bool onlyCardinal = false, int rangeX = 1, int rangeY = 1, int rangeZ = 1, bool includeCurrent = false)
        {
            List<Vector3i> positions = new List<Vector3i>();
            foreach (Vector3i _pos in positions)
            {
                positions.AddRange(CoordinateHelper.GetCoordinatesAround(_pos, onlyCardinal, rangeX, rangeY, rangeZ, includeCurrent));
            }
            positions = ((IEnumerable<Vector3i>)positions.Distinct()) as List<Vector3i>;
            foreach (Vector3i _pos in positions)
            {
                positions.Remove(_pos);
            }
            return positions;
        }


        /**
         * Shorter version to pass in vector3i of ranges instead.
         */

        public static List<Vector3i> GetCoordinatesAround(List<Vector3i> _positions, Vector3i range, bool onlyCardinal = false, bool includeCurrent = false)
        {
            return GetCoordinatesAround(_positions, onlyCardinal, range.x, range.y, range.z, includeCurrent);
        }


        /**
         * Gets coordinates around a block based on block face state.
         */

        public static List<Vector3i> GetCoordinatesAround(Vector3i _position, Vector3i range, BlockFaceFC direction, bool onlyCardinal = false)
        {
            List<Vector3i> coordinates = new List<Vector3i>();
            List<Vector3i> subCoords = new List<Vector3i>();
            coordinates.Add(_position);

            switch (direction)
            {
                case BlockFaceFC.TOP:
                    subCoords = CoordinateHelper.GetCoordinatesAbove(_position, range.y);
                    break;
                case BlockFaceFC.TOPRANGE:
                    subCoords = CoordinateHelper.GetCoordinatesAboveRange(_position, range.y);
                    break;
                case BlockFaceFC.BOTTOM:
                    subCoords = CoordinateHelper.GetCoordinatesBelow(_position, range.y);
                    break;
                case BlockFaceFC.BOTTOMRANGE:
                    subCoords = CoordinateHelper.GetCoordinatesBelowRange(_position, range.y);
                    break;
                case BlockFaceFC.SIDES:
                    range.y = 0;
                    subCoords = CoordinateHelper.GetCoordinatesAround(_position, range, true);
                    break;
                default:
                    subCoords = CoordinateHelper.GetCoordinatesAround(_position, range, onlyCardinal);
                    break;
                    // TODO: Cover cases NESW
            }

            foreach (Vector3i coordinate in subCoords)
            {
                coordinates.Add(coordinate);
            }

            return coordinates;
        }



        /**
         * Gets a list of coordinates directly above the passed in vector.
         */

        public static List<Vector3i> GetCoordinatesAbove(Vector3i _pos, int range = 1)
        {
            if (range < 1)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            List<Vector3i> positions = new List<Vector3i>();
            for (int y = 1; y <= range; y += 1)
            {
                Vector3i step = new Vector3i(0, y, 0);
                positions.Add(_pos + step);
            }

            return positions;
        }


        /**
         * Gets a list of coorinates directly above the passed in vector list.
         */

        public static List<Vector3i> GetCoordinatesAbove(List<Vector3i> _positions, int range = 1)
        {
            List<Vector3i> positions = new List<Vector3i>();
            foreach (Vector3i _pos in _positions)
            {
                positions.AddRange(CoordinateHelper.GetCoordinatesAbove(_pos, range));
            }
            return positions;
        }


        /**
         * Gets coordinates above a block in a range around the upper blocks.
         */

        public static List<Vector3i> GetCoordinatesAboveRange(Vector3i _pos, int range = 1)
        {
            if (range < 1)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            List<Vector3i> positions = new List<Vector3i>();
            for (int x = -range; x <= range; x += 1)
            {
                for (int y = 1; y <= range; y += 1)
                {
                    for (int z = -range; z <= range; z += 1)
                    {
                        Vector3i step = new Vector3i(x, y, z);
                        if (step == Vector3i.zero)
                        {
                            continue;
                        }

                        Vector3i coordinate = _pos + step;
                        if (coordinate.y < 0 | coordinate.y > 255)
                        {
                            continue;
                        }

                        positions.Add(coordinate);
                    }
                }
            }

            return positions;
        }


        /**
         * Gets a list of coorinates directly below the passed in vector.
         */

        public static List<Vector3i> GetCoordinatesBelow(Vector3i _pos, int range = 1)
        {
            if (range < 1)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            List<Vector3i> positions = new List<Vector3i>();
            for (int y = 1; y <= range; y += 1)
            {
                Vector3i step = new Vector3i(0, -y, 0);
                positions.Add(_pos + step);
            }

            return positions;
        }


        /**
         * Gets a list of coorinates directly below the passed in vector list.
         */

        public static List<Vector3i> GetCoordinatesBelow(List<Vector3i> _positions, int range = 1)
        {
            List<Vector3i> positions = new List<Vector3i>();
            foreach (Vector3i _pos in _positions)
            {
                positions.AddRange(CoordinateHelper.GetCoordinatesBelow(_pos, range));
            }
            return positions;
        }


        /**
         * Gets coordinates below a block in a range around the lower blocks.
         */

        public static List<Vector3i> GetCoordinatesBelowRange(Vector3i _pos, int range = 1)
        {
            if (range < 1)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            List<Vector3i> positions = new List<Vector3i>();
            for (int x = -range; x <= range; x += 1)
            {
                for (int y = 1; y <= -range; y += 1)
                {
                    for (int z = -range; z <= range; z += 1)
                    {
                        Vector3i step = new Vector3i(x, -y, z);
                        if (step == Vector3i.zero)
                        {
                            continue;
                        }

                        Vector3i coordinate = _pos + step;
                        if (coordinate.y < 0 | coordinate.y > 255)
                        {
                            continue;
                        }

                        positions.Add(coordinate);
                    }
                }
            }

            return positions;
        }


        /**
         * Sane as above function but uses yield instead for better performance in other aspects.
         */

        private static IEnumerable<Vector3i> YieldCoordinatesAround(Vector3i _pos, bool onlyCardinal = false, int rangeX = 1, int rangeY = 1, int rangeZ = 1)
        {
            if (rangeX < 0 | rangeY < 0 | rangeZ < 0)
            {
                throw new ArgumentException("Ranges must be non-negative.");
            }

            for (int x = -rangeX; x <= rangeX; x += 1)
            {
                for (int y = -rangeY; y <= rangeY; y += 1)
                {
                    for (int z = -rangeZ; z <= rangeZ; z += 1)
                    {
                        Vector3i step = new Vector3i(x, y, z);
                        if (step == Vector3i.zero)
                        {
                            continue;
                        }

                        if (onlyCardinal && !IsCardinal(step))
                        {
                            continue;
                        }

                        Vector3i coordinate = _pos + step;
                        if (coordinate.y < 0 | coordinate.y > 255)
                        {
                            continue;
                        }

                        yield return coordinate;
                    }
                }
            }
        }


        /**
         * Shorter version to pass in vector3i of ranges instead.
         */

        private static IEnumerable<Vector3i> YieldCoordinatesAround(Vector3i _pos, Vector3i range, bool onlyCardinal = false)
        {
            return YieldCoordinatesAround(_pos, onlyCardinal, range.x, range.y, range.z);
        }


        /**
         * Returns the block directly below this one.
         */

        public static Vector3i GetCoordinateBelow(Vector3i coordinate)
        {
            if (coordinate.y > 0)
            {
                coordinate.y -= 1;
                return coordinate;
            }
            return coordinate;
        }


        /**
         * Whether a vector is cardinal from the origin.
         */

        public static bool IsCardinal(Vector3i vector)
        {
            if (vector.x != 0 & vector.y == 0 & vector.z == 0)
            {
                return true;
            }
            else if (vector.x == 0 & vector.y != 0 & vector.z == 0)
            {
                return true;
            }
            else if (vector.x == 0 & vector.y == 0 & vector.z != 0)
            {
                return true;
            }
            return false;
        }


        /**
         * Gets a chunk from a given coordinate.
         */

        public static Chunk GetChunkFromCoordinate(World _world, Vector3i coordinate)
        {
            return _world.GetChunkFromWorldPos(coordinate) as Chunk;
        }



        /**
         * Gets a set of chunks from world positions.
         */

        public static List<Chunk> GetChunksFromCoordinates(World _world, List<Vector3i> coordinates)
        {
            List<Chunk> chunks = new List<Chunk>();
            foreach (Vector3i coordinate in coordinates)
            {
                Chunk chunk = GetChunkFromCoordinate(_world, coordinate);
                if (!chunks.Contains(chunk))
                {
                    chunks.Add(chunk);
                }
            }
            return chunks;
        }


        /**
         * Returns a list of all tile entities within a set of world coordinates. 
         * You could use this in order to find out whether nearby tile entities are of a certain type, for example.
         */

        public static Dictionary<Vector3i, TileEntity> GetTileEntitiesInCoordinatesWithType(World _world, List<Vector3i> coordinates, TileEntityType type = TileEntityType.None)
        {
            Dictionary<Vector3i, TileEntity> tileEntities = new Dictionary<Vector3i, TileEntity>();

            List<Chunk> chunks = GetChunksFromCoordinates(_world, coordinates);
            if (chunks == null || chunks.Count == 0)
            {
                return tileEntities;
            }

            foreach (Chunk chunk in chunks)
            {
                if (chunk == null)
                {
                    continue;
                }

                DictionaryList<Vector3i, TileEntity> tileEntitiesInChunk = chunk.GetTileEntities();

                if (tileEntitiesInChunk == null || tileEntitiesInChunk.Count == 0)
                {
                    continue;
                }

                foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesInChunk.dict)
                {
                    // NOTE - the KEY in the dict is actually the chunk local coord so cannot be used.
                    if (entry.Value == null || !coordinates.Contains(entry.Value.ToWorldPos()))
                    {
                        continue;
                    }

                    if (type == TileEntityType.None)
                    {
                        tileEntities.Add(entry.Value.ToWorldPos(), entry.Value);
                        continue;
                    }

                    if (entry.Value.GetTileEntityType() == type)
                    {
                        tileEntities.Add(entry.Value.ToWorldPos(), entry.Value);
                    }
                }
            }
            return tileEntities;
        }


        /**
         * Gets tile entities with derived type.
         */

        public static Dictionary<Vector3i, T> GetTileEntitiesInCoordinatesWithType<T>(World _world, List<Vector3i> coordinates) where T : TileEntity
        {
            Dictionary<Vector3i, T> tileEntities = new Dictionary<Vector3i, T>();

            List<Chunk> chunks = GetChunksFromCoordinates(_world, coordinates);
            if (chunks == null || chunks.Count == 0)
            {
                return tileEntities;
            }

            foreach (Chunk chunk in chunks)
            {
                if (chunk == null)
                {
                    continue;
                }

                DictionaryList<Vector3i, TileEntity> tileEntitiesInChunk = chunk.GetTileEntities();

                if (tileEntitiesInChunk == null || tileEntitiesInChunk.Count == 0)
                {
                    continue;
                }

                foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesInChunk.dict)
                {
                    // NOTE - the KEY in the dict is actually the chunk local coord so cannot be used.
                    if (entry.Value == null || !coordinates.Contains(entry.Value.ToWorldPos()))
                    {
                        continue;
                    }

                    if (entry.Value is T)
                    {
                        tileEntities.Add(entry.Value.ToWorldPos(), (T)entry.Value);
                    }
                }
            }
            return tileEntities;
        }




        /**
         * Returns a tile entity from a given coordinate.
         */

        public static TileEntity GetTileEntityWithTypeAt(World _world, Vector3i coordinate, TileEntityType type = TileEntityType.None)
        {
            Chunk chunk = GetChunkFromCoordinate(_world, coordinate);
            Dictionary<Vector3i, TileEntity> tileEntitiesInChunk = chunk.GetTileEntities().dict;
            if (tileEntitiesInChunk.Count == 0)
            {
                return null;
            }

            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesInChunk)
            {
                if (coordinate != entry.Value.ToWorldPos())
                {
                    continue;
                }

                if (type == TileEntityType.None)
                {
                    return entry.Value;
                }

                if (entry.Value.GetTileEntityType() == type)
                {
                    return entry.Value;
                }
            }

            return null;
        }



        /**
         * Returns a list of all tile entities within a set of world coordinates. 
         * You could use this in order to find out whether nearby tile entities are of a certain type, for example.
         * This one accepts a list of types.
         */

        public static Dictionary<Vector3i, TileEntity> GetTileEntitiesInCoordinatesWithTypes(World _world, List<Vector3i> coordinates, List<TileEntityType> types = null)
        {
            List<Chunk> chunks = GetChunksFromCoordinates(_world, coordinates);
            Dictionary<Vector3i, TileEntity> tileEntities = new Dictionary<Vector3i, TileEntity>();
            foreach (Chunk chunk in chunks)
            {
                Dictionary<Vector3i, TileEntity> tileEntitiesInChunk = chunk.GetTileEntities().dict;

                if (tileEntitiesInChunk.Count == 0)
                {
                    return tileEntitiesInChunk;
                }

                foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesInChunk)
                {
                    if (!coordinates.Contains(entry.Value.ToWorldPos()))
                    {
                        continue;
                    }

                    if (types == null)
                    {
                        tileEntities.Add(entry.Value.ToWorldPos(), entry.Value);
                        continue;
                    }

                    if (types.Contains(TileEntityType.None))
                    {
                        tileEntities.Add(entry.Value.ToWorldPos(), entry.Value);
                        continue;
                    }

                    foreach (TileEntityType type in types)
                    {
                        if (entry.Value.GetTileEntityType() == type)
                        {
                            tileEntities.Add(entry.Value.ToWorldPos(), entry.Value);
                            continue;
                        }
                    }
                }
            }
            return tileEntities;
        }


        /**
         * Gets the block at a coordinate.
         */

        public static Block GetBlockAtCoordinate(World _world, Vector3i _pos)
        {
            return _world.GetBlock(_pos).Block;
        }


        /**
         * Gets a dictionary of blocks with their coordinates.
         */

        public static Dictionary<Vector3i, Block> GetBlocksFromCoordinates(World _world, List<Vector3i> coordinates)
        {
            Dictionary<Vector3i, Block> blockPositions = new Dictionary<Vector3i, Block>();
            foreach (Vector3i coordinate in coordinates)
            {
                blockPositions.Add(coordinate, GetBlockAtCoordinate(_world, coordinate));
            }
            return blockPositions;
        }


        /**
         * Gets the name of a block at a certain coordinate.
         */

        public static string GetBlockNameAtCoordinate(World _world, Vector3i _pos)
        {
            return GetBlockAtCoordinate(_world, _pos).GetBlockName();
        }


        /**
         * Gets tags for a block in coordinates.
         */

        public static string[] GetBlockTagsAtCoordinate(World _world, Vector3i _pos)
        {
            BlockValue block = _world.GetBlock(_pos);
            if (block.Block.FilterTags == null)
            {
                return null;
            }

            return block.Block.FilterTags;
        }


        /**
         * Returns whether the block at a coordinate is the block needed.
         */

        public static bool BlockAtCoordinateIs(World _world, Vector3i _pos, string name)
        {
            return GetBlockNameAtCoordinate(_world, _pos).ToLower().Trim().Equals(name.ToLower().Trim());
        }


        /**
         * Returns whether the block at a coordinate has a tag or all tags.
         */

        public static bool BlockAtCoordinateHasTags(World _world, Vector3i _pos, List<string> tagNames, bool matchAll = false)
        {
            string[] blockTags = GetBlockTagsAtCoordinate(_world, _pos);
            if (blockTags == null)
            {
                return false;
            }
            if (blockTags[0] == null)
            {
                return false;
            }

            if (matchAll)
            {
                int numTagsNeeded = tagNames.Count;
                int numTagsFound = 0;
                foreach (string tag in blockTags)
                {
                    if (tagNames.Contains(tag))
                    {
                        numTagsFound += 1;
                    }
                }

                return (numTagsFound >= numTagsNeeded);
            }

            foreach (string tag in blockTags)
            {
                if (tagNames.Contains(tag))
                {
                    return true;
                }
            }

            return false;
        }


        /**
         * Returns whether the block at a coodinate is one of the specified blocks needed.
         */

        public static bool BlockAtCoordinateIsOneOf(World _world, Vector3i _pos, List<string> names)
        {
            foreach (string name in names)
            {
                if (BlockAtCoordinateIs(_world, _pos, name))
                {
                    return true;
                }
            }
            return false;
        }


        /**
         * Returns how many of the specified blocks are in the specified coordinates.
         */

        public static int CountBlocksInCoordinatesThatAre(World _world, List<Vector3i> coordinates, List<string> names)
        {
            int result = 0;
            foreach (Vector3i coordinate in coordinates)
            {
                if (BlockAtCoordinateIsOneOf(_world, coordinate, names))
                {
                    result += 1;
                }
            }
            return result;
        }


        /**
         * Counts up how many blocks in specified coordinates have tags.
         */

        public static int CountBlocksInCoordinatesThatHaveTags(World _world, List<Vector3i> coordinates, List<string> tags, bool matchAll = false)
        {
            int result = 0;
            foreach (Vector3i coordinate in coordinates)
            {
                if (BlockAtCoordinateHasTags(_world, coordinate, tags, matchAll))
                {
                    result += 1;
                }
            }

            return result;
        }


        /**
         * Returns whether there are enough blocks in a range of coordinates of certain types.
         */

        public static bool EnoughBlocksInCoordinatesThatAre(World _world, List<Vector3i> coordinates, List<string> names, int needed)
        {
            return (needed <= CountBlocksInCoordinatesThatAre(_world, coordinates, names));
        }


        /**
         * Returns whether enough blocks in a range of coordinates have the required tags/
         */

        public static bool EnoughBlocksInCoordinatesThatHaveTags(World _world, List<Vector3i> coordinates, List<string> tagNames, int needed, bool matchAll = false)
        {
            return (needed <= CountBlocksInCoordinatesThatHaveTags(_world, coordinates, tagNames, matchAll));
        }


        /**
         * Returns the block placements rotated by 90 degrees counterclockwise along the Y axis.
         */

        public static Dictionary<Vector3i, string> RotateBlockPlacementsY90CC(Dictionary<Vector3i, string> blockPositions)
        {
            Dictionary<Vector3i, string> transformedPositions = new Dictionary<Vector3i, string>();
            foreach (KeyValuePair<Vector3i, string> entry in blockPositions)
            {
                transformedPositions.Add(new Vector3i(entry.Key.z, entry.Key.y, -entry.Key.x), entry.Value);
            }
            return transformedPositions;
        }


        /**
         * Returns whether we have enough entities in the boundaries for the position.
         */

        public static bool EnoughEntitiesInBounds(World world, Vector3i center, Vector3i range, List<string> names, int needed, out int count)
        {
            count = GetEntitiesInBounds(world, center, range, names).Count;
            return count >= needed;
        }


        /**
         * Returns a list of entities in the coordinates.
         */

        public static List<Entity> GetEntitiesInBounds(World world, Vector3i center, Vector3i range, List<string> names)
        {
            Bounds bounds = new Bounds(center.ToVector3(), 2 * range.ToVector3());
            List<Entity> entities = new List<Entity>();
            int boundsXMin = Utils.Fastfloor((bounds.min.x - (float)World.cCollisionBlocks) / 16f);
            int boundsXMax = Utils.Fastfloor((bounds.max.x + (float)World.cCollisionBlocks) / 16f);
            int boundsYMin = Utils.Fastfloor((bounds.min.y - (float)World.cCollisionBlocks));
            int boundsYMax = Utils.Fastfloor((bounds.max.y + (float)World.cCollisionBlocks));
            int boundsZMin = Utils.Fastfloor((bounds.min.z - (float)World.cCollisionBlocks) / 16f);
            int boundsZMax = Utils.Fastfloor((bounds.max.z + (float)World.cCollisionBlocks) / 16f);

            for (int i = boundsXMin; i <= boundsXMax; i++)
            {
                for (int j = boundsZMin; j <= boundsZMax; j++)
                {
                    Chunk chunkSync = world.ChunkCache.GetChunkSync(i, j);
                    if (chunkSync == null)
                    {
                        continue;
                    }

                    List<Entity> entitiesInChunk = chunkSync.GetAllEntities();
                    foreach (Entity entity in entitiesInChunk)
                    {
                        if (
                            entity.boundingBox.Intersects(bounds)
                            && entity.GetBlockPosition().y >= boundsYMin
                            && entity.GetBlockPosition().y <= boundsYMax
                            && names.Contains(entity.EntityClass.entityClassName)
                            && !entity.IsDead()
                        )
                        {
                            entities.Add(entity);
                        }
                    }
                }
            }
            return entities;
        }
    }
}