﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class TileEntityHelper
    {
        /**
         * Returns a list of all nearby inventory managers around a secure signed container.
         */

        public static List<TileEntityInventoryManager> GetNearbyInventoryManagersAroundSecureSignedContainer(TileEntitySecureLootContainerSigned __instance)
        {
            List<TileEntityInventoryManager> inventoryManagers = new List<TileEntityInventoryManager>();
            List<Vector3i> nearbyCoordinates = CoordinateHelper.GetCoordinatesAround(__instance.ToWorldPos(), new Vector3i(10, 10, 10));
            Dictionary<Vector3i, TileEntity> tileEntities = CoordinateHelper.GetTileEntitiesInCoordinatesWithType(GameManager.Instance.World, nearbyCoordinates, TileEntityType.None);
            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntities)
            {
                if (entry.Value is TileEntityInventoryManager)
                {
                    inventoryManagers.Add(entry.Value as TileEntityInventoryManager);
                }
            }
            return inventoryManagers;
        }


        /**
         * Retrieves all nearby signed containers.
         */

        public static List<TileEntitySecureLootContainerSigned> GetNearbySignedContainers(TileEntityInventoryManager __instance, int rangeX = 5, int rangeY = 5, int rangeZ = 5)
        {
            List<TileEntitySecureLootContainerSigned> signedContainers = new List<TileEntitySecureLootContainerSigned>();
            List<Vector3i> nearbyCoordinates = CoordinateHelper.GetCoordinatesAround(__instance.ToWorldPos(), false, rangeX, rangeY, rangeZ);

            Dictionary<Vector3i, TileEntity> tileEntities = CoordinateHelper.GetTileEntitiesInCoordinatesWithType(GameManager.Instance.World, nearbyCoordinates, TileEntityType.None);
            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntities)
            {
                if (entry.Value is TileEntitySecureLootContainerSigned)
                {
                    signedContainers.Add(entry.Value as TileEntitySecureLootContainerSigned);
                }
            }
            return signedContainers;
        }


        private static Vector3i rangeDefault = new Vector3i(10, 10, 10);
    }
}