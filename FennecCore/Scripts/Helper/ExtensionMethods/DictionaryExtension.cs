﻿using System;
using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class DictionaryExtension
    {
        /**
         *	Inserts a key into the dictionary only if it's a new key.
         */

        public static void AddIfNew<S, T>(this Dictionary<S, T> _dict, S _key, T _value)
        {
            if (_dict.ContainsKey(_key))
            {
                return;
            }
            _dict.Add(_key, _value);
        }


        /**
         *	Update the transformation dictionary with max value.
         */

        public static void UpdateWithHighest<S>(this Dictionary<S, int> _dict, S _key, int _value)
        {
            if (_dict.ContainsKey(_key))
            {
                if (_dict[_key] >= _value)
                {
                    return;
                }
                _dict.Remove(_key);
            }
            _dict.Add(_key, _value);
        }
    }
}