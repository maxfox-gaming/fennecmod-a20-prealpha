﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

	public static class ChunkExtension
	{
		/**
		 * Returns all entities in a chunk.
		 */

		public static List<Entity> GetAllEntities(this Chunk chunk)
		{
			List<Entity> entities = new List<Entity>();

			foreach (List<Entity> entityList in chunk.entityLists)
			{
				foreach (Entity entity in entityList)
				{
					entities.Add(entity);
				}
			}
			return entities;
		}
	}
}