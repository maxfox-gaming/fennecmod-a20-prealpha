﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class ListExtension
    {
        /**
         * Adds an entry to a list, but only if it doesn't match an existing entry.
         */

        public static void AddIfNew<T>(this List<T> _list, T _value)
        {
            if (!_list.Contains(_value))
            {
                _list.Add(_value);
            }
        }
    }
}