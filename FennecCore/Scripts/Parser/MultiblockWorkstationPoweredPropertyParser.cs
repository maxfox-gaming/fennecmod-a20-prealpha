﻿using System;

namespace FennecCore.Scripts
{
    public class MultiblockWorkstationPoweredPropertyParser
    {

        /**
         * Constructor
         */

        public MultiblockWorkstationPoweredPropertyParser(BlockMultiblockMasterWorkstationPowered blockMultiblockMasterWorkstationPowered)
        {
            this.blockWorkstationPowered = blockMultiblockMasterWorkstationPowered;
            this.blockName = blockWorkstationPowered.GetBlockName();
            this.blockWorkstationPoweredBlock = Block.GetBlockValue(this.blockName, true);
        }


        /**
         *	Parses the transformations properties for the block.
         */

        public void ParseDynamicProperties()
        {
            this.GetPropertiesForBlock();
            this.CheckRequiresPower();
        }


        /**
         *  Returns a list of Transformations properties.
         */

        protected void GetPropertiesForBlock()
        {
            if (this.blockWorkstationPoweredBlock.type == 0)
            {
                Log.Warning("Powered workstation block " + this.blockName + " is not identified.");
                return;
            }

            this.workstationPoweredBlockProps = this.blockWorkstationPoweredBlock.Block.Properties;
        }


        /**
         * Checks whether the block requires power and parses the power sources.
         */

        protected void CheckRequiresPower()
        {
            this.requirePower = false;

            string requirePowerValue;
            if (!PropertyHelpers.PropExists(workstationPoweredBlockProps, propRequirePower, out requirePowerValue))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requirePowerValue, out this.requirePower))
            {
                throw new Exception("Require power could noot be parsed as a boolean value.");
            }
        }


        // Block input data
        protected BlockMultiblockMasterWorkstationPowered blockWorkstationPowered;
        protected string blockName;
        protected BlockValue blockWorkstationPoweredBlock;
        protected DynamicProperties workstationPoweredPropClass;
        protected DynamicProperties workstationPoweredBlockProps;

        // The name of the workstation powered class and properties to parse.
        protected string propRequirePower = "RequirePower";

        // The parsed property data.
        public bool requirePower;
    }
}