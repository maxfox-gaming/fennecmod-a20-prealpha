﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FennecCore.Scripts
{
    public class TransformationPropertyParser
    {
        public TransformationPropertyParser(IBlockTransformer blockTransformer)
        {
            this.blockTransformer = blockTransformer;
            this.blockName = blockTransformer.GetBlockName();
            this.blockTransformerBlock = Block.GetBlockValue(this.blockName, true);
        }


        /**
         *	Parses the transformations properties for the block.
         */

        public void ParseDynamicProperties()
        {
            this.GetTransformationPropertiesForBlock();
            this.PopulateTransformationData();
            this.CheckAllItemsExist();
            this.ParseTransformationData();
            this.CheckRequiresPower();
            this.CheckRequiresHeat();
            this.CheckRequireBlocksNearby();
            this.CheckRequireEntitiesNearby();
            this.CheckRequireUserAccess();
        }


        /**
         * Checks whether there is a property class of Transformations in the list.
         */

        protected bool HasTransformationPropertyClass()
        {
            return this.blockTransformerBlock.Block.Properties.Classes.ContainsKey(propClassTransformations);
        }


        /**
         *  Returns a list of Transformations properties.
         */

        protected void GetTransformationPropertiesForBlock()
        {
            if (this.blockTransformerBlock.type == 0)
            {
                Log.Warning("Transformer block " + this.blockName + " is not identified.");
                return;
            }

            if (!this.HasTransformationPropertyClass())
            {
                throw new ArgumentException("Block " + this.blockName + " has no property class of Transformations.");
            }

            this.transformationPropClass = this.blockTransformerBlock.Block.Properties.Classes[propClassTransformations];
            this.transformationBlockProps = this.blockTransformerBlock.Block.Properties;

            foreach (KeyValuePair<string, object> entry in this.transformationPropClass.Values.Dict.Dict)
            {
                if (!this.PropStringFormattedCorrectly(entry.Key))
                {
                    throw new Exception("The property " + entry.Key + " is not a valid property name.");
                }
                this.transformationProperties.Add(entry.Key);
            }
        }


        /**
         *	Checks that the Transformation properties are defined correctly.
         */

        protected bool PropStringFormattedCorrectly(string _string)
        {
            if (!_string.StartsWith(this.propValueTransformation))
            {
                return false;
            }
            return (this.GetType(_string) != TransformationStringType.INVALID);
        }


        /**
         *	Populates the protected dictionaries with max number of transformations, inputs and outputs.
         */

        protected void PopulateTransformationData()
        {
            Regex rgx = new Regex(this.pattern);
            foreach (string transformationProperty in this.transformationProperties)
            {
                int transformationIndex = 0;
                int inputOutputIndex = 0;
                switch (this.GetType(transformationProperty))
                {
                    case TransformationStringType.INVALID:
                        throw new Exception("String " + transformationProperty + " is not a valid string.");
                    case TransformationStringType.INPUT:
                    case TransformationStringType.OUTPUT:
                        MatchCollection ioMatches = rgx.Matches(transformationProperty);
                        if (ioMatches.Count != 2)
                        {
                            throw new ArgumentException("Transformation property " + transformationProperty + " is not correctly formatted.");
                        }

                        transformationIndex = int.Parse(ioMatches[0].Value);
                        inputOutputIndex = int.Parse(ioMatches[1].Value);

                        this.GetInputOutputDictionary(transformationProperty).UpdateWithHighest(transformationIndex, inputOutputIndex);
                        this.transformationIndexes.AddIfNew(transformationIndex);
                        break;
                    case TransformationStringType.HIDEOUTPUTS:
                        MatchCollection hMatches = rgx.Matches(transformationProperty);
                        if (hMatches.Count != 1)
                        {
                            throw new ArgumentException("Transformation property " + transformationProperty + " is not correctly formatted.");
                        }
                        transformationIndex = int.Parse(hMatches[0].Value);

                        string propValue;
                        if (!this.PropExists(transformationProperty, out propValue))
                        {
                            throw new ArgumentException("Property " + transformationProperty + " does not exist.");
                        }

                        List<int> outputsToHide = StringHelpers.WriteStringToIntList(propValue);
                        this.hiddenOutputs.AddIfNew(transformationIndex, outputsToHide);
                        break;
                    default:
                        break;
                }
            }
        }


        /**
         *	Returns the input or output dictionary to add to, depending on the transformation string.
         */

        protected Dictionary<int, int> GetInputOutputDictionary(string transformationString)
        {
            switch (this.GetType(transformationString))
            {
                case TransformationStringType.INPUT:
                    return this.transformMaxInputs;
                case TransformationStringType.OUTPUT:
                    return this.transformMaxOutputs;
            }

            throw new ArgumentException("The string " + transformationString + " does not contain input or output data.");
        }


        /**
         * Gets the transformation type based on the transformation string given.
         */

        protected TransformationStringType GetType(string transformationString)
        {
            if (transformationString.Contains(this.inputString))
            {
                return TransformationStringType.INPUT;
            }
            else if (transformationString.Contains(this.outputString))
            {
                return TransformationStringType.OUTPUT;
            }
            else if (transformationString.Contains(this.timeString))
            {
                return TransformationStringType.TIME;
            }
            else if (transformationString.Contains(this.recipeString))
            {
                return TransformationStringType.RECIPE;
            }
            else if (transformationString.Contains(this.probabilityString))
            {
                return TransformationStringType.PROBABILITY;
            }
            else if (transformationString.Contains(this.craftAreaString))
            {
                return TransformationStringType.CRAFTAREA;
            }
            else if (transformationString.Contains(this.hideOutputString))
            {
                return TransformationStringType.HIDEOUTPUTS;
            }
            else if (transformationString.Contains(this.entitiesString))
            {
                return TransformationStringType.ENTITIES;
            }
            else if (transformationString.Contains(this.entRangeString))
            {
                return TransformationStringType.ENTRANGE;
            }
            return TransformationStringType.INVALID;
        }


        /**
         * Checks all items exist.
         */

        protected void CheckAllItemsExist()
        {
            if (this.transformationIndexes.Count == 0)
            {
                throw new Exception("No indexes were found.");
            }

            foreach (int transformationIndex in this.transformationIndexes)
            {
                int maxInputs = this.transformMaxInputs[transformationIndex];
                int maxOutputs = this.transformMaxOutputs[transformationIndex];

                for (int i = 1; i <= maxInputs; i += 1)
                {
                    this.CheckItemsExistAtIndex(TransformationStringType.INPUT, transformationIndex, i);
                }

                for (int i = 1; i <= maxOutputs; i += 1)
                {
                    this.CheckItemsExistAtIndex(TransformationStringType.OUTPUT, transformationIndex, i);
                }
            }
        }


        /**
         * Checks if item exists at Transformation{index}_(Input/Output){subIndex) and throws an exception if not.
         */

        protected void CheckItemsExistAtIndex(TransformationStringType type, int index, int subIndex)
        {
            string ioString = this.GetIOString(type);
            string transformProperty = this.propValueTransformation + index.ToString() + ioString + subIndex.ToString();
            if (!this.transformationPropClass.Values.ContainsKey(transformProperty))
            {
                throw new Exception("Missing the property '" + transformProperty + "' for block '" + this.blockName + "'.");
            }

            string propValue = this.transformationPropClass.Values[transformProperty];
            string itemName = (propValue.Contains(",") ? propValue.Split(',')[0] : propValue).Trim();
            int id = ItemClass.GetItem(itemName, false).GetItemOrBlockId();

            if (id == 0)
            {
                throw new Exception("Item '" + itemName + "' not found in '" + transformProperty + "' for block '" + this.blockName + "'.");
            }
        }


        /**
         *	Takes all transformation data and creates a transformation collection from it.
         */

        protected void ParseTransformationData()
        {
            Dictionary<int, TransformationData> parsedTransformationData = new Dictionary<int, TransformationData>();
            this.AddTransformationDataEntries(parsedTransformationData);
            this.AddTimeDataToTransformationData(parsedTransformationData);
            this.AddRecipeDataToTransformationData(parsedTransformationData);
            this.AddCraftAreaDataToTransformationData(parsedTransformationData);
            this.AddProbabilityDataToTransformationData(parsedTransformationData);
            this.AddEntitiesDataToTransformationData(parsedTransformationData);
            this.AddIOToTransformationData(parsedTransformationData);
            this.AddToHashmap(parsedTransformationData);
            this.ConvertToTransformationCollection(parsedTransformationData);
        }


        /**
         * Adds the keys into the transformation data dictionary for each transformation found.
         */

        protected void AddTransformationDataEntries(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                tData.Add(transformationIndex, new TransformationData());
            }
        }


        /**
         * Adds the set time from XML to the transformation data.
         */

        protected void AddTimeDataToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                string timeProperty = this.propValueTransformation + transformationIndex.ToString() + this.timeString;
                tData[transformationIndex].transformationTime = this.GetTransformationTime(timeProperty);
            }
        }


        /**
         * Gets the transformation time as a double for the transformation property. If none found or parse error, return as a defult double.
         */

        protected double GetTransformationTime(string timeProperty)
        {
            double time;
            if (!this.transformationPropClass.Values.ContainsKey(timeProperty))
            {
                return defaultTransformationTime;
            }

            if (!StringParsers.TryParseDouble(this.transformationPropClass.Values[timeProperty].Trim(), out time))
            {
                Log.Warning("Could not parse double for " + timeProperty + ". Using default time of " + defaultTransformationTime.ToString() + ".");
                return defaultTransformationTime;
            }
            return time;
        }


        /**
         * Adds whether the data should show a recipe in the crafting menu.
         */

        protected void AddRecipeDataToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                string useRecipeProperty = this.propValueTransformation + transformationIndex.ToString() + this.recipeString;
                tData[transformationIndex].useRecipe = this.GetTransformationUseRecipe(useRecipeProperty);
            }
        }


        /**
         * Gets the transformation recipe as a bool for the transformation property. If none found or parse error, return as a defult bool.
         */

        protected bool GetTransformationUseRecipe(string useRecipeProperty)
        {
            bool useRecipe;
            if (!this.transformationPropClass.Values.ContainsKey(useRecipeProperty))
            {
                return false;
            }

            if (!StringParsers.TryParseBool(this.transformationPropClass.Values[useRecipeProperty].Trim(), out useRecipe))
            {
                Log.Warning("Could not parse bool for " + useRecipeProperty + ". Using default false.");
                return false;
            }
            return useRecipe;
        }


        /**
         * Adds whether the data should show a recipe in the crafting menu.
         */

        protected void AddProbabilityDataToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                string showProbabilityProperty = this.propValueTransformation + transformationIndex.ToString() + this.probabilityString;
                tData[transformationIndex].showProbability = this.GetTransformationShowProbability(showProbabilityProperty);
            }
        }


        /**
         * Gets the transformation probability display as a bool for the transformation property. If none found or parse error, return as a defult bool.
         */

        protected bool GetTransformationShowProbability(string useProbabilityProperty)
        {
            bool showProbability;
            if (!this.transformationPropClass.Values.ContainsKey(useProbabilityProperty))
            {
                return true;
            }

            if (!StringParsers.TryParseBool(this.transformationPropClass.Values[useProbabilityProperty].Trim(), out showProbability))
            {
                Log.Warning("Could not parse bool for " + useProbabilityProperty + ". Using default true.");
                return true;
            }
            return showProbability;
        }


        /**
         * Adds the craft area.
         */

        protected void AddCraftAreaDataToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                string craftAreaProperty = this.propValueTransformation + transformationIndex.ToString() + this.craftAreaString;
                tData[transformationIndex].craftArea = this.GetTransformationCraftArea(craftAreaProperty);
            }
        }


        /**
         * Gets the craft area or uses the default block name.
         */

        protected string GetTransformationCraftArea(string craftAreaProperty)
        {
            if (!this.transformationPropClass.Values.ContainsKey(craftAreaProperty))
            {
                return this.blockName;
            }
            return this.transformationPropClass.Values[craftAreaProperty].Trim();
        }


        /**
         * Adds nearby entity data to transformation data.
         */

        protected void AddEntitiesDataToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                string entitiesProperty = this.propValueTransformation + transformationIndex.ToString() + this.entitiesString;
                string entRangeProperty = this.propValueTransformation + transformationIndex.ToString() + this.entRangeString;
                string entities;
                if (this.PropExists(entitiesProperty, out entities))
                {
                    tData[transformationIndex].entities = StringHelpers.WriteStringToList(entities);
                }
                string range;
                if (this.PropExists(entRangeProperty, out range))
                {
                    tData[transformationIndex].range = StringHelpers.WriteStringToNonNegativeVector3Range(range);
                }
            }
        }


        /**
         * Adds the Transformation{X}_(In|Out)put{Y} fields to the tData structure.
         */

        protected void AddIOToTransformationData(Dictionary<int, TransformationData> tData)
        {
            foreach (int transformationIndex in this.transformationIndexes)
            {
                this.AddToTData(tData, transformationIndex, this.transformMaxInputs[transformationIndex], TransformationStringType.INPUT);
                this.AddToTData(tData, transformationIndex, this.transformMaxOutputs[transformationIndex], TransformationStringType.OUTPUT);
            }
        }


        /**
         * Adds an element to transformation data.
         */

        protected void AddToTData(Dictionary<int, TransformationData> tData, int transformationIndex, int maxIOIndex, TransformationStringType type)
        {
            string ioString = this.GetIOString(type);
            for (int i = 1; i <= maxIOIndex; i += 1)
            {
                string ioProp = this.propValueTransformation + transformationIndex.ToString() + ioString + i.ToString();
                if (!this.transformationPropClass.Values.ContainsKey(ioProp))
                {
                    Log.Warning("The following property is not defined: " + ioProp + ". Skipping.");
                    continue;
                }

                tData[transformationIndex].Add(this.GetITransformerItemFor(ioProp, transformationIndex, i));
            }
        }


        /**
         * Returns the IO string.
         */

        protected string GetIOString(TransformationStringType type)
        {
            switch (type)
            {
                case (TransformationStringType.INPUT):
                    return this.inputString;
                case (TransformationStringType.OUTPUT):
                    return this.outputString;
                default:
                    Log.Warning("Property is not an input or output string. Skipping.");
                    return String.Empty; ;
            }
        }


        /**
         *  Converts the property value string into a TransformerItemInput or TransformerItemOutput.
         */

        protected ITransformerItem GetITransformerItemFor(string itemPropName, int mainIndex, int ioIndex)
        {
            string itemPropData = this.transformationPropClass.Values[itemPropName];

            TransformationStringType type = this.GetType(itemPropName);
            if (type != TransformationStringType.INPUT && type != TransformationStringType.OUTPUT)
            {
                throw new Exception("Item property name " + itemPropName + " is not an input or output string.");
            }

            string itemName = "";
            int itemCount = defaultItemCount;
            double itemProb = defaultItemProb;

            if (!itemPropData.Contains(","))
            {
                itemName = itemPropData.Trim();
            }
            else
            {
                string[] propData = itemPropData.Split(',');
                itemName = propData[0].Trim();
                if (!int.TryParse(propData[1].Trim(), out itemCount))
                {
                    Log.Warning("Could not parse " + propData[1] + " as int value for '" + itemPropName + "' in '" + this.blockName + "'. Using default " + defaultItemCount.ToString() + ".");
                    itemCount = defaultItemCount;
                }

                itemProb = defaultItemProb;
                if (propData.Length > 2)
                {
                    if (!double.TryParse(propData[2].Trim(), out itemProb))
                    {
                        Log.Warning("Could not parse " + propData[2] + " as a probability double for '" + itemPropName + "' in '" + this.blockName + "'.");
                        itemProb = defaultItemProb;
                    }
                }

            }

            ITransformerItem transformerItem = this.GetITransformerItemForType(type, itemName, itemCount, itemProb);
            if (transformerItem is TransformerItemOutput)
            {
                if (this.hiddenOutputs.ContainsKey(mainIndex) && this.hiddenOutputs[mainIndex].Contains(ioIndex))
                {
                    ((TransformerItemOutput)transformerItem).display = false;
                }
            }

            return transformerItem;
        }


        /**
         * Returns a TransformerItemInput or TransformerItemOutput for the ITransformerItem passed in.
         */

        protected ITransformerItem GetITransformerItemForType(TransformationStringType type, string itemName, int itemCount, double itemProb)
        {
            switch (type)
            {
                case (TransformationStringType.INPUT):
                    return new TransformerItemInput(ItemClass.GetItem(itemName), itemCount, itemProb);
                case (TransformationStringType.OUTPUT):
                    return new TransformerItemOutput(ItemClass.GetItem(itemName), itemCount, itemProb);
                default:
                    throw new Exception("Item property name " + itemName + " is not an input or output string.");
            }
        }


        /**
         * Adds the transformation data to hashmap.
         */

        protected void AddToHashmap(Dictionary<int, TransformationData> parsedTransformationData)
        {
            foreach (KeyValuePair<int, TransformationData> entry in parsedTransformationData)
            {
                TransformationData.AddToHashmap(entry.Value);
            }
        }


        /**
         * Converts the list of transformation data to a collection.
         */

        protected void ConvertToTransformationCollection(Dictionary<int, TransformationData> parsedTransformationData)
        {
            List<TransformationData> transformationList = new List<TransformationData>();
            foreach (KeyValuePair<int, TransformationData> entry in parsedTransformationData)
            {
                transformationList.Add(entry.Value);
            }
            this.collection = new TransformationCollection(transformationList);
        }


        /**
         * Checks whether the block requires power and parses the power sources.
         */

        protected void CheckRequiresPower()
        {
            this.requirePower = false;
            this.powerSources = new List<string>() { "electricwirerelay" };

            string requirePowerValue;
            if (!this.PropExists(propRequirePower, out requirePowerValue))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requirePowerValue, out this.requirePower))
            {
                throw new Exception("Require power could noot be parsed as a boolean value.");
            }

            this.powerSources = new List<string>();
            string powerSources;
            if (this.PropExists(propPowerSources, out powerSources))
            {
                this.powerSources = BlockHelpers.CheckBlocksDefined(StringHelpers.WriteStringToList(powerSources));
                return;
            }
            this.powerSources.Add("electricwirerelay");
        }


        /**
         * Checks whether block requires heat and parses the heat sources.
         */

        protected void CheckRequiresHeat()
        {
            this.requireHeat = false;
            this.heatSources = new List<string>() { "campfire" };

            string requireHeatValue;
            if (!this.PropExists(propRequireHeat, out requireHeatValue))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requireHeatValue, out this.requireHeat))
            {
                throw new Exception("Could not parse require heat parameter as a boolean.");
            }

            this.heatSources = new List<string>();
            string heatSources;
            if (this.PropExists(propHeatSources, out heatSources))
            {
                this.heatSources = BlockHelpers.CheckBlocksDefined(StringHelpers.WriteStringToList(heatSources));
            }
            this.heatSources.Add("campfire");
        }


        /**
         * Checks whether nearby blocks are required and parses the nearby block sources.
         */

        protected void CheckRequireBlocksNearby()
        {
            this.requireNearbyBlocks = false;
            this.nearbyBlockRange = Vector3i.one;
            this.nearbyBlockCount = 0;
            this.nearbyBlockNames = new List<string>();
            this.nearbyBlockTags = new List<string>();
            this.requireAllTags = false;

            // Checks whether we need nearby blocks or not. If not, just exit out as we no longer need to specify additional data.
            string requireNearbyBlocks;
            if (!this.PropExists(propRequireNearbyBlocks, out requireNearbyBlocks))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requireNearbyBlocks, out this.requireNearbyBlocks))
            {
                throw new Exception("Require nearby blocks property needs to be true or false.");
            }


            // Check whether we have a range specified. If so, we need to parse it.
            string nearbyBlockRanges;
            if (this.PropExists(propNearbyBlockRange, out nearbyBlockRanges))
            {
                this.nearbyBlockRange = StringHelpers.WriteStringToNonNegativeVector3Range(nearbyBlockRanges);
            }

            // Check whether we have a count of nearby blocks.
            string nearbyBlocks;
            if (this.PropExists(propNearbyBlockCount, out nearbyBlocks))
            {
                this.nearbyBlockCount = StringHelpers.WriteStringToPositiveInt(nearbyBlocks);
            }

            // Checks whether the blocks specified exist for block names.
            string nearbyBlockNames;
            if (this.PropExists(propNearbyBlockNames, out nearbyBlockNames))
            {
                this.nearbyBlockNames = BlockHelpers.CheckBlocksDefined(StringHelpers.WriteStringToList(nearbyBlockNames));
            }

            // Checks block tags.
            string nearbyBlockTags;
            if (this.PropExists(propNearbyBlockTags, out nearbyBlockTags))
            {
                this.nearbyBlockTags = StringHelpers.WriteStringToList(nearbyBlockTags);
            }

            // Checks require all tags.
            string requireAllTags;
            if (this.PropExists(propRequireAllTags, out requireAllTags))
            {
                if (!StringParsers.TryParseBool(requireAllTags, out this.requireAllTags))
                {
                    throw new Exception("Could not parse require all tags parameter as a boolean value.");
                }
            }
        }


        /**
         * Check whether the block requires entities around it and if so will output the data.
         */

        protected void CheckRequireEntitiesNearby()
        {
            this.requireNearbyEntities = false;
            this.nearbyEntityNames = new List<string>();
            this.nearbyEntityCount = 0;
            this.nearbyEntityRange = Vector3i.one;
            this.nearbyEntitySpeedUp = false;

            // Check if nearby entities specified, and set to true if so.
            string nearbyEntitiesNeeded;
            if (this.PropExists(propRequireNearbyEntities, out nearbyEntitiesNeeded))
            {
                if (!StringParsers.TryParseBool(nearbyEntitiesNeeded, out requireNearbyEntities))
                {
                    throw new Exception("Property " + propRequireNearbyEntities + " could not be parsed as a true or false value.");
                }
            }

            if (!requireNearbyEntities)
            {
                return;
            }

            string entityCount;
            if (this.PropExists(propNearbyEntityCount, out entityCount))
            {
                this.nearbyEntityCount = StringHelpers.WriteStringToPositiveInt(entityCount);
            }

            string entityRange;
            if (this.PropExists(propNearbyEntityRange, out entityRange))
            {
                this.nearbyEntityRange = StringHelpers.WriteStringToNonNegativeVector3Range(entityRange);
            }

            string entityNames;
            if (this.PropExists(propNearbyEntityNames, out entityNames))
            {
                this.nearbyEntityNames = StringHelpers.WriteStringToList(entityNames);
            }

            string speedUp;
            if (this.PropExists(propNearbyEntitySpeedUp, out speedUp))
            {
                if (!StringParsers.TryParseBool(speedUp, out this.nearbyEntitySpeedUp))
                {
                    throw new Exception("Could not parse property " + propNearbyEntitySpeedUp + " as a true or false.");
                }
            }


            // If ranges are not set for individual transformation data ranges in the collection, set them.
            foreach (TransformationData tData in this.collection.collection)
            {
                if (tData.entities.Count > 0 && tData.range == Vector3i.zero)
                {
                    tData.range = this.nearbyEntityRange;
                }
            }
        }


        /**
         * Checks whether the user is needed to be accessing the block in order for processing to occur.
         */

        protected void CheckRequireUserAccess()
        {
            this.requireUserAccess = false;
            string requireUserAccess;
            if (!this.PropExists(this.propRequireUserAccess, out requireUserAccess))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requireUserAccess, out this.requireUserAccess))
            {
                throw new Exception("Could not parse value as boolean.");
            }
        }


        /**
         * Checks whether a property is defined, and returns it as a string if so.
         */

        protected bool PropExists(string key, out string value)
        {
            value = "";
            if (this.transformationBlockProps.Values.ContainsKey(key))
            {
                value = this.transformationBlockProps.Values[key].ToString();
                return true;
            }
            if (this.transformationPropClass.Values.ContainsKey(key))
            {
                value = this.transformationPropClass.Values[key].ToString();
                return true;
            }

            return false;
        }



        // Block input data
        protected IBlockTransformer blockTransformer;
        protected string blockName;
        protected BlockValue blockTransformerBlock;
        protected DynamicProperties transformationPropClass;
        protected DynamicProperties transformationBlockProps;

        // The name of the transformations class and properties to parse.
        protected string propClassTransformations = "Transformations";
        protected string propValueTransformation = "Transformation";
        protected string propRequirePower = "RequirePower";
        protected string propPowerSources = "PowerSources";
        protected string propRequireHeat = "RequireHeat";
        protected string propHeatSources = "HeatSources";
        protected string propRequireNearbyBlocks = "RequireNearbyBlocks";
        protected string propNearbyBlockRange = "NearbyBlockRange";
        protected string propNearbyBlockCount = "NearbyBlockCount";
        protected string propNearbyBlockNames = "NearbyBlockNames";
        protected string propNearbyBlockTags = "NearbyBlockTags";
        protected string propRequireAllTags = "RequireAllTags";
        protected string propRequireUserAccess = "RequireUserAccess";
        protected string propRequireNearbyEntities = "RequireNearbyEntities";
        protected string propNearbyEntityNames = "NearbyEntityNames";
        protected string propNearbyEntityCount = "NearbyEntityCount";
        protected string propNearbyEntityRange = "NearbyEntityRange";
        protected string propNearbyEntitySpeedUp = "NearbyEntitySpeedUp";
        protected string propNearbyEntityBuffs = "NearbyEntityBuffs";

        // Used to detect inputs, outputs, and time strings
        protected string inputString = "_Input";
        protected string outputString = "_Output";
        protected string timeString = "_Time";
        protected string recipeString = "_ShowRecipe";
        protected string probabilityString = "_ShowProb";
        protected string craftAreaString = "_CraftArea";
        protected string hideOutputString = "_HideOutput";
        protected string entitiesString = "_Entities";
        protected string entRangeString = "_EntRange";

        // Used for extracting transformation numbers from the string.
        protected string pattern = @"([0-9]+)";

        // A list holding all transformation properties and how many.
        protected List<string> transformationProperties = new List<string>();
        protected List<int> transformationIndexes = new List<int>();

        // These dictionaries hold how many inputs and outputs each transformation has.
        protected Dictionary<int, int> transformMaxInputs = new Dictionary<int, int>();
        protected Dictionary<int, int> transformMaxOutputs = new Dictionary<int, int>();
        protected Dictionary<int, List<int>> hiddenOutputs = new Dictionary<int, List<int>>();

        // Default values
        public static int defaultItemCount = 1;
        public static double defaultTransformationTime = 2.0;
        public static double defaultItemProb = 1.0;

        // The parsed transformation data.
        public TransformationCollection collection;
        public bool requirePower;
        public List<string> powerSources;
        public bool requireHeat;
        public List<string> heatSources;
        public bool requireNearbyBlocks;
        public Vector3i nearbyBlockRange;
        public int nearbyBlockCount;
        public List<string> nearbyBlockNames;
        public List<string> nearbyBlockTags;
        public bool requireAllTags;
        public bool requireNearbyEntities;
        public List<string> nearbyEntityNames;
        public int nearbyEntityCount;
        public Vector3i nearbyEntityRange;
        public bool nearbyEntitySpeedUp;
        public bool requireUserAccess;
    }
}