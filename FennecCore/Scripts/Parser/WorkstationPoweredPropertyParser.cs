﻿using System;
using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public class WorkstationPoweredPropertyParser
    {
        /**
         * Constructor
         */

        public WorkstationPoweredPropertyParser(BlockWorkstationPowered blockWorkstationPowered)
        {
            this.blockWorkstationPowered = blockWorkstationPowered;
            this.blockName = blockWorkstationPowered.GetBlockName();
            this.blockWorkstationPoweredBlock = Block.GetBlockValue(this.blockName, true);
        }


        /**
         *	Parses the transformations properties for the block.
         */

        public void ParseDynamicProperties()
        {
            this.GetPropertiesForBlock();
            this.CheckRequiresPower();
            this.CheckRequiresSidedPower();
        }


        /**
         *  Returns a list of Transformations properties.
         */

        protected void GetPropertiesForBlock()
        {
            if (this.blockWorkstationPoweredBlock.type == 0)
            {
                Log.Warning("Powered workstation block " + this.blockName + " is not identified.");
                return;
            }

            this.workstationPoweredBlockProps = this.blockWorkstationPoweredBlock.Block.Properties;
        }


        /**
         * Checks whether the block requires power and parses the power sources.
         */

        protected void CheckRequiresPower()
        {
            this.requirePower = false;
            this.powerSources = new List<string>() { "electricwirerelay" };

            string requirePowerValue;
            if (!PropertyHelpers.PropExists(workstationPoweredBlockProps, propRequirePower, out requirePowerValue))
            {
                return;
            }

            if (!StringParsers.TryParseBool(requirePowerValue, out this.requirePower))
            {
                throw new Exception("Require power could noot be parsed as a boolean value.");
            }

            this.powerSources = new List<string>();
            string powerSources;
            if (PropertyHelpers.PropExists(workstationPoweredBlockProps, propPowerSources, out powerSources))
            {
                this.powerSources = BlockHelpers.CheckBlocksDefined(StringHelpers.WriteStringToList(powerSources));
                return;
            }
            this.powerSources.Add("electricwirerelay");
        }


        /**
         * Check whether block can only receive power from a certain side, and default to all if not.
         */

        protected void CheckRequiresSidedPower()
        {
            this.sidedPower = SidedPower.ALL;

            string sidedPowerValue;
            if (!PropertyHelpers.PropExists(workstationPoweredBlockProps, propPowerSided, out sidedPowerValue))
            {
                return;
            }

            this.sidedPower = SidedPowerMapping.Map(sidedPowerValue);
        }



        // Block input data
        protected BlockWorkstationPowered blockWorkstationPowered;
        protected string blockName;
        protected BlockValue blockWorkstationPoweredBlock;
        protected DynamicProperties workstationPoweredPropClass;
        protected DynamicProperties workstationPoweredBlockProps;

        // The name of the workstation powered class and properties to parse.
        protected string propRequirePower = "RequirePower";
        protected string propPowerSources = "PowerSources";
        protected string propPowerSided = "PowerSided";

        // The parsed property data.
        public bool requirePower;
        public List<string> powerSources;
        public SidedPower sidedPower;
    }
}