﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace FennecCore.Scripts
{
    public class InventoryManagerLink
    {
        private Vector3i managerCoordinate;
        private Vector3i containerCoordinate;
        private string hash;

        private static Regex parser = new Regex(@"{M:(.+);C:(.+);}");


        /**
         * Constructor.
         */

        public InventoryManagerLink(Vector3i managerCoordinate, Vector3i containerCoordinate)
        {
            this.managerCoordinate = managerCoordinate;
            this.containerCoordinate = containerCoordinate;
            this.hash = MD5Hash.Calculate(managerCoordinate.ToString() + containerCoordinate.ToString());
        }


        /**
         * Returns the inventory manager tile entity.
         */

        public TileEntityInventoryManager GetManager()
        {
            return CoordinateHelper.GetTileEntityWithTypeAt(GameManager.Instance.World, managerCoordinate, TileEntityMapping.Types[TileEntityMapping.InventoryManager]) as TileEntityInventoryManager;
        }


        /**
         * Returns the secure signed container tile entity.
         */

        public TileEntitySecureLootContainerSigned GetSignedContainer()
        {
            return CoordinateHelper.GetTileEntityWithTypeAt(GameManager.Instance.World, managerCoordinate, TileEntityMapping.Types[TileEntityMapping.SecureLootSigned]) as TileEntitySecureLootContainerSigned;
        }


        /**
         * Returns the coordinates of the InventoryManager block.
         */

        public Vector3i GetManagerCoordinate()
        {
            return this.managerCoordinate;
        }


        /**
         * Returns the coordinates of the secure signed container block.
         */

        public Vector3i GetContainerCoordinate()
        {
            return this.containerCoordinate;
        }


        /**
         * Gets the hash for the manager container pair.
         */

        public string GetHash()
        {
            return this.hash;
        }


        /**
         * Returns the text from the signed container if it is there.
         */

        public string GetContainerText()
        {
            TileEntitySecureLootContainerSigned container = this.GetSignedContainer();
            if (container == null)
            {
                return "";
            }
            return container.GetText();
        }


        /**
         * Reads the data into an object.
         */

        public static InventoryManagerLink Read(string _s)
        {
            Match match = parser.Match(_s);
            if (!match.Success)
            {
                throw new Exception("Trying to read string failed: '" + _s + "'");
            }

            Vector3i managerCoordinate = StringHelpers.WriteStringToVector3i(match.Groups[1].ToString());
            Vector3i containerCoordinate = StringHelpers.WriteStringToVector3i(match.Groups[2].ToString());

            return new InventoryManagerLink(managerCoordinate, containerCoordinate);
        }


        /**
         * Writes the data into a string.
         */

        public string Write()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("{M:");
            sb.Append(StringHelpers.WriteVector3iToString(this.managerCoordinate));
            sb.Append(";C:");
            sb.Append(StringHelpers.WriteVector3iToString(this.containerCoordinate));
            sb.Append(";}");
            return sb.ToString();
        }
    }
}