﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
	public static class PowerItemMapping
	{
		// VANILLA
		public static string Consumer = "Consumer";
		public static string ConsumerToggle = "ConsumerToggle";
		public static string Trigger = "Trigger";
		public static string Timer = "Timer";
		public static string Generator = "Generator";
		public static string SolarPanel = "SolarPanel";
		public static string BatteryBank = "BatteryBank";
		public static string RangedTrap = "RangedTrap";
		public static string ElectricWireRelay = "ElectricWireRelay";
		public static string TripWireRelay = "TripWireRelay";
		public static string PressurePlate = "PressurePlate";

		// FENNEC-CORE
		public static string WindTurbine = "WindTurbine";
		public static string WaterTurbine = "WaterTurbine";


		public static Dictionary<string, PowerItem.PowerItemTypes> Types = new Dictionary<string, PowerItem.PowerItemTypes>()
		{
			// VANILLA
			{ Consumer, PowerItem.PowerItemTypes.Consumer },
			{ ConsumerToggle, PowerItem.PowerItemTypes.ConsumerToggle },
			{ Trigger, PowerItem.PowerItemTypes.Trigger},
			{ Timer, PowerItem.PowerItemTypes.Timer },
			{ Generator, PowerItem.PowerItemTypes.Generator },
			{ SolarPanel, PowerItem.PowerItemTypes.SolarPanel },
			{ BatteryBank, PowerItem.PowerItemTypes.BatteryBank },
			{ RangedTrap, PowerItem.PowerItemTypes.RangedTrap },
			{ ElectricWireRelay, PowerItem.PowerItemTypes.ElectricWireRelay },
			{ TripWireRelay, PowerItem.PowerItemTypes.TripWireRelay },
			{ PressurePlate, PowerItem.PowerItemTypes.PressurePlate },

			// FENNEC-CORE
			{ WindTurbine, (PowerItem.PowerItemTypes)100 },
			{ WaterTurbine, (PowerItem.PowerItemTypes)101 }
		};
	}
}