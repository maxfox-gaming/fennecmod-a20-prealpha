﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

    public static class TileEntityMapping
    {
        // VANILLA
        public static string Campfire = "Campfire";
        public static string Forge = "Forge";
        public static string GoreBlock = "GoreBlock";
        public static string LandClaim = "LandClaim";
        public static string Light = "Light";
        public static string Loot = "Loot";
        public static string None = "None";
        public static string Powered = "Powered";
        public static string PowerMeleeTrap = "PowerMeleeTrap";
        public static string PowerRangedTrap = "PowerRangedTrap";
        public static string PowerSource = "PowerSource";
        public static string SecureDoor = "SecureDoor";
        public static string SecureLoot = "SecureLoot";
        public static string SecureLootSigned = "SecureLootSigned";
        public static string Sign = "Sign";
        public static string Sleeper = "Sleeper";
        public static string Trader = "Trader";
        public static string Trigger = "Trigger";
        public static string VendingMachine = "VendingMachine";
        public static string Workstation = "Workstation";

        // FENNEC-CORE
        public static string BlockTransformer = "BlockTransformer";
        public static string WorkstationImproved = "WorkstationImproved";
        public static string WorkstationPowered = "WorkstationPowered";
        public static string InventoryManager = "InventoryManager";
        public static string MBSlave = "MBSlave";
        public static string MBSlaveLoot = "MBSlaveLoot";
        public static string MBSlaveLootInput = "MBSlaveLootInput";
        public static string MBSlaveLootOutput = "MBSlaveLootOutput";
        public static string MBSlavePowered = "MBSlavePowered";
        public static string MBMstr = "MBMstr";
        public static string MBMstrTransformer = "MBMstrTransformer";
        public static string MBMstrWorkstation = "MBMstrWorkstation";
        public static string MBMstrWorkstationPowered = "MBMstrWorkstationPowered";

        public static Dictionary<string, TileEntityType> Types = new Dictionary<string, TileEntityType>()
    {
        // VANILLA
        { Campfire, TileEntityType.Campfire },
        { Forge, TileEntityType.Forge},
        { GoreBlock, TileEntityType.GoreBlock },
        { LandClaim, TileEntityType.LandClaim },
        { Light, TileEntityType.Light },
        { Loot, TileEntityType.Loot },
        { None, TileEntityType.None },
        { Powered, TileEntityType.Powered },
        { PowerMeleeTrap, TileEntityType.PowerMeleeTrap },
        { PowerRangedTrap, TileEntityType.PowerRangeTrap },
        { PowerSource, TileEntityType.PowerSource },
        { SecureDoor, TileEntityType.SecureDoor },
        { SecureLoot, TileEntityType.SecureLoot },
        { SecureLootSigned, TileEntityType.SecureLootSigned },
        { Sign, TileEntityType.Sign },
        { Sleeper, TileEntityType.Sleeper },
        { Trader, TileEntityType.Trader },
        { Trigger, TileEntityType.Trigger },
        { VendingMachine, TileEntityType.VendingMachine },
        { Workstation, TileEntityType.Workstation },

        // FENNEC-CORE
        { BlockTransformer, (TileEntityType)70 },
        { WorkstationImproved, (TileEntityType)71 },
        { WorkstationPowered, (TileEntityType)72 },
        { InventoryManager, (TileEntityType)73 },
        { MBSlave, (TileEntityType)74 },
        { MBSlaveLoot, (TileEntityType)75 },
        { MBSlaveLootInput, (TileEntityType)76 },
        { MBSlaveLootOutput, (TileEntityType)77 },
        { MBSlavePowered, (TileEntityType)78 },
        { MBMstr, (TileEntityType)79 },
        { MBMstrTransformer, (TileEntityType)80 },
        { MBMstrWorkstation, (TileEntityType)81 },
        { MBMstrWorkstationPowered, (TileEntityType)82 }
    };
    }
}