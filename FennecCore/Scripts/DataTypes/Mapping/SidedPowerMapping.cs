﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    static class SidedPowerMapping
    {
        private static Dictionary<string, SidedPower> acceptedValues = new Dictionary<string, SidedPower>()
    {
        { "top", SidedPower.TOP },
        { "bottom", SidedPower.BOTTOM },
        { "sides", SidedPower.SIDES },
        { "all", SidedPower.ALL }
    };


        /**
         * Returns the sided power variant for the passed in string value.
         */

        public static SidedPower Map(string _s)
        {
            if (acceptedValues.ContainsKey(_s.ToLower().Trim()))
            {
                return acceptedValues[_s];
            }
            return SidedPower.ALL;
        }
    }
}