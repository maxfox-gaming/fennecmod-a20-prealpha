﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    static class BlockFaceFCMapping
    {
        private static Dictionary<string, BlockFaceFC> acceptedValues = new Dictionary<string, BlockFaceFC>()
    {
        { "top", BlockFaceFC.TOP },
        { "toprange", BlockFaceFC.TOPRANGE },
        { "bottom", BlockFaceFC.BOTTOM },
        { "bottomrange", BlockFaceFC.BOTTOMRANGE },
        { "north", BlockFaceFC.NORTH },
        { "east", BlockFaceFC.EAST },
        { "south", BlockFaceFC.SOUTH },
        { "west", BlockFaceFC.WEST },
        { "sides", BlockFaceFC.SIDES },
        { "all", BlockFaceFC.ALL },
        { "none", BlockFaceFC.NONE }
    };


        /**
         * Returns the block face variant for the passed in string value.
         */

        public static BlockFaceFC Map(string _s)
        {
            if (acceptedValues.ContainsKey(_s.ToLower().Trim()))
            {
                return acceptedValues[_s];
            }
            return BlockFaceFC.ALL;
        }
    }
}