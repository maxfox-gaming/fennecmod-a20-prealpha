﻿namespace FennecCore.Scripts
{
    /**
     * Decided what sides that a block accepts power from.
     */

    public enum SidedPower
    {
        TOP,
        BOTTOM,
        SIDES,
        ALL
    };
}