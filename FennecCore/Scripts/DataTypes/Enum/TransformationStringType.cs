﻿namespace FennecCore.Scripts
{
    /**
     * Decides what type of string is used for transformation data.
     */

    public enum TransformationStringType
    {
        INVALID,
        INPUT,
        OUTPUT,
        TIME,
        RECIPE,
        PROBABILITY,
        CRAFTAREA,
        HIDEOUTPUTS,
        ENTITIES,
        ENTRANGE
    }
}