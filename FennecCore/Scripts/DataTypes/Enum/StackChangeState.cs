﻿namespace FennecCore.Scripts
{
    public enum StackChangedState
    {
        ERROR,
        CHANGED,
        UNCHANGED
    }
}