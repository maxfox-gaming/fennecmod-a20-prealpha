﻿namespace FennecCore.Scripts
{

    /**
     * This is to determine what face or side of the block should do something.
     * SIDES will do all four of NSEW
     * ALL will do every block face and accept all directions.
     * TOPRANGE / BOTTOMRANGE will do directly above / below, with specific radius around.
     * NONE selects no block faces
     */

    public enum BlockFaceFC
    {
        TOP,
        TOPRANGE,
        BOTTOM,
        BOTTOMRANGE,
        NORTH,
        EAST,
        SOUTH,
        WEST,
        SIDES,
        ALL,
        NONE
    }
}