﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

    public class ReceivePowerLink
    {
        private Vector3i masterBlockPos;
        private List<Vector3i> childBlockCoords;
        private List<Vector3i> lookupCoords;
        private bool hasPower;

        public ReceivePowerLink(Vector3i master, List<Vector3i> childBlocks, List<Vector3i> lookups)
        {
            this.masterBlockPos = master;
            this.childBlockCoords = childBlocks;
            this.lookupCoords = lookups;
            this.hasPower = false;
        }

        public bool IsParent(Vector3i pos)
        {
            return this.masterBlockPos == pos;
        }


        public bool HasParent(Vector3i pos)
        {
            return childBlockCoords.Contains(pos);
        }


        public List<Vector3i> GetLookupCoords()
        {
            return this.lookupCoords;
        }


        public void SetPowered()
        {
            this.hasPower = true;
        }


        public void SetUnpowered()
        {
            this.hasPower = false;
        }


        public bool HasPower()
        {
            return this.hasPower;
        }
    }
}