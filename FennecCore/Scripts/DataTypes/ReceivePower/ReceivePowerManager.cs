﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class ReceivePowerManager
    {
        private static List<ReceivePowerLink> links = new List<ReceivePowerLink>();


        public static void AddLink(ReceivePowerLink link)
        {
            ReceivePowerManager.links.Add(link);
        }


        public static ReceivePowerLink GetLinkFor(Vector3i pos)
        {
            foreach (ReceivePowerLink link in ReceivePowerManager.links)
            {
                if (link.IsParent(pos) || link.HasParent(pos))
                {
                    return link;
                }
            }
            return null;
        }


        public static void RemoveLink(Vector3i pos)
        {
            foreach (ReceivePowerLink link in ReceivePowerManager.links)
            {
                if (link.IsParent(pos) || link.HasParent(pos))
                {
                    ReceivePowerManager.links.Remove(link);
                    return;
                }
            }
        }
    }
}