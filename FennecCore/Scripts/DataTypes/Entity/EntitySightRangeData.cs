﻿using System;

namespace FennecCore.Scripts
{
    public class EntitySightRangeData
    {
        public int id;
        public float sightRange;

        public EntitySightRangeData(int id, float sightRange)
        {
            this.id = id;
            this.sightRange = sightRange;
        }
    }
}