﻿using System;

namespace FennecCore.Scripts
{

    public class InputByproductData
    {
        public ItemValue InputItem
        {
            get { return inputItem; }
        }

        public ItemValue Byproduct
        {
            get { return byproductItem; }
        }

        public double Probability
        {
            get { return probability; }
        }

        public double ProbAsPercent
        {
            get { return Math.Round(probability * 100, 1); }
        }

        public static InputByproductData Empty
        {
            get { return new InputByproductData(ItemValue.None, ItemValue.None, 0); }
        }


        /**
         * Constructor
         */

        public InputByproductData(ItemValue inputItem, ItemValue byproductItem, double probability)
        {
            this.inputItem = inputItem;
            this.byproductItem = byproductItem;
            this.probability = NumHelpers.CheckProb(probability);
        }


        /**
         * Returns whether the byproduct should be given or not.
         */

        public bool ShouldGiveByproduct()
        {
            return (RandomStatic.Next() <= probability);
        }


        /**
         * Adds a recipe for the transformation.
         */

        public void AddRecipe()
        {
            if (this.Equals(InputByproductData.Empty))
            {
                Log.Out("Recipe cannot be created.");
                return;
            }

            RecipeInputByproduct recipe = new RecipeInputByproduct();
            recipe.itemValueType = byproductItem.type;
            recipe.probabilityValue = probability;
            recipe.count = 1;
            recipe.scrapable = false;
            recipe.materialBasedRecipe = false;
            recipe.craftingArea = "byproduct_input";
            recipe.craftingToolType = 0;
            recipe.craftingTime = byproductItem.ItemClass.MeltTimePerUnit;
            recipe.unlockExpGain = -1;
            recipe.craftExpGain = -1;
            recipe.UseIngredientModifier = true;
            recipe.wildcardForgeCategory = false;
            recipe.AddInputIngredient(inputItem, 1);
            
            ExtendedRecipeList.Add(recipe);
        }




        /**
         * Returns true if one byproduct data item is equal to the other one.
         */

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
            {
                return false;
            }

            InputByproductData other = obj as InputByproductData;

            return
            (
                this.inputItem.Equals(other.inputItem) &&
                this.byproductItem.Equals(other.byproductItem) &&
                this.probability.Equals(other.probability)
            );
        }


        /**
         * Return the hash code for this data type.
         */

        public override int GetHashCode()
        {
            return (inputItem.GetHashCode() + byproductItem.GetHashCode() + probability.GetHashCode());
        }


        private ItemValue inputItem;
        private ItemValue byproductItem;
        private double probability;
    }
}