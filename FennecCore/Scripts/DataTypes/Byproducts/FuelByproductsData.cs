﻿using System;

namespace FennecCore.Scripts
{
    public class FuelByproductData
    {

        public static FuelByproductData Empty
        {
            get { return new FuelByproductData(ItemValue.None, ItemValue.None, ItemValue.None, 0); }
        }

        public ItemValue FuelItem
        {
            get { return this.fuelItem; }
        }

        public ItemValue Byproduct
        {
            get { return this.byproduct; }
        }

        public ItemValue Requirement
        {
            get { return this.requirement; }
        }

        public float Probability
        {
            get { return (float)probability; }
        }

        public double ProbabilityAsPercent
        {
            get { return Math.Round(probability, 1); }
        }


        /**
         * Constructor - also adds data to manager.
         */

        public FuelByproductData(ItemValue fuelItem, ItemValue byproduct, ItemValue requirement, double probability = 1.0)
        {
            this.fuelItem = fuelItem;
            this.byproduct = byproduct;
            this.requirement = requirement;
            this.probability = NumHelpers.CheckProb(probability);
        }


        /**
         * Returns whether a byproduct should be given based on the probability.
         */

        public bool ShouldGiveByproduct()
        {
            return (RandomStatic.Next() <= this.probability);
        }


        /**
         * Adds a recipe for the transformation.
         */

        public void AddRecipe()
        {
            if (this.Equals(FuelByproductData.Empty))
            {
                Log.Out("Recipe cannot be created.");
                return;
            }

            RecipeFuelByproduct recipe = new RecipeFuelByproduct();
            recipe.itemValueType = byproduct.type;
            recipe.probabilityValue = probability;
            recipe.count = 1;
            recipe.scrapable = false;
            recipe.materialBasedRecipe = false;
            recipe.craftingArea = "byproduct_fuel";
            recipe.craftingToolType = 0;
            recipe.craftingTime = ItemClass.GetFuelValue(fuelItem);
            recipe.unlockExpGain = -1;
            recipe.craftExpGain = -1;
            recipe.UseIngredientModifier = true;
            recipe.wildcardForgeCategory = false;
            recipe.AddFuelIngredient(fuelItem, 1);
            if (!requirement.Equals(ItemValue.None))
            {
                recipe.AddRequirementIngredient(requirement, 1);
            }
            ExtendedRecipeList.Add(recipe);
        }


        /**
         * Checks if one is equal to another.
         */

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
            {
                return false;
            }

            FuelByproductData data = obj as FuelByproductData;
            return (
                this.fuelItem.Equals(data.fuelItem) &&
                this.byproduct.Equals(data.byproduct) &&
                this.requirement.Equals(data.requirement) &&
                this.probability.Equals(data.probability)
            );
        }


        /**
         * Gets hash code.
         */

        public override int GetHashCode()
        {
            return fuelItem.GetHashCode() + byproduct.GetHashCode() + requirement.GetHashCode();
        }


        private ItemValue fuelItem;
        private ItemValue byproduct;
        private ItemValue requirement;
        private double probability;

        public static string propFuelByproducts = "FuelByproduct";
        public static string propFuelRequirements = "FuelRequirement";
    }
}