﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class ByproductManager
    {
        /**
         * Handles fuel byproduct allocation
         */

        public static ItemStack[] HandleFuelByproductFor(ItemValue fuelItem, ItemStack[] stackArray)
        {
            if (fuelItem == null || fuelItem == ItemValue.None)
            {
                return stackArray;
            }

            FuelByproductData fuelByproductData = fuelItem.GetFuelByproductData();
            if (fuelByproductData.Equals(FuelByproductData.Empty))
            {
                return stackArray;
            }

            if (!fuelByproductData.ShouldGiveByproduct())
            {
                return stackArray;
            }


            Dictionary<int, int> slots;
            ItemStack[] previousState = (ItemStack[])stackArray.Clone();
            if (!fuelByproductData.Requirement.Equals(ItemValue.None))
            {
                ItemStack required = new ItemStack(fuelByproductData.Requirement, 1);
                if (!InventoryHelper.InventoryHasRequiredItem(stackArray, required, out slots))
                {
                    return stackArray;
                }

                foreach (KeyValuePair<int, int> entry in slots)
                {
                    if (!InventoryHelper.RemoveItemsInSlot(stackArray, entry.Key, entry.Value))
                    {
                        stackArray = previousState;
                        return stackArray;
                    }
                }
            }

            ItemStack byproduct = new ItemStack(fuelByproductData.Byproduct, 1);
            List<int> sameItems = new List<int>();
            List<int> emptyItems = new List<int>();
            for (int i = 0; i < stackArray.Length; i += 1)
            {
                if (stackArray[i].itemValue.Equals(fuelByproductData.Byproduct))
                {
                    sameItems.Add(i);
                    continue;
                }

                if (stackArray[i].Equals(ItemStack.Empty))
                {
                    emptyItems.Add(i);
                }
            }

            if (sameItems.Count > 0)
            {
                foreach (int i in sameItems)
                {
                    if (InventoryHelper.TryAddItemToSlot(stackArray, i, byproduct))
                    {
                        return stackArray;
                    }
                }
            }

            if (emptyItems.Count > 0)
            {
                foreach (int i in emptyItems)
                {
                    if (InventoryHelper.TryAddItemToSlot(stackArray, i, byproduct))
                    {
                        return stackArray;
                    }
                }
            }

            stackArray = previousState;
            return stackArray;
        }


        /**
         * Checks the stack array, and adds input items to the stack array.
         */

        public static ItemStack[] HandleInputByproductFor(ItemValue inputItem, ItemStack[] stackArray, out StackChangedState change)
        {
            change = StackChangedState.UNCHANGED;
            if (inputItem == null || inputItem.Equals(ItemValue.None))
            {
                return stackArray;
            }

            InputByproductData inputByproductData = inputItem.GetInputByproductData();
            if (inputByproductData.Equals(InputByproductData.Empty))
            {
                return stackArray;
            }

            if (!inputByproductData.ShouldGiveByproduct())
            {
                return stackArray;
            }


            ItemStack[] previousState = stackArray.Clone() as ItemStack[];
            ItemStack byproduct = new ItemStack(inputByproductData.Byproduct, 1);
            List<int> sameItems = new List<int>();
            List<int> emptyItems = new List<int>();
            for (int i = 0; i < stackArray.Length; i += 1)
            {
                if (stackArray[i].itemValue.Equals(inputByproductData.Byproduct))
                {
                    sameItems.Add(i);
                    continue;
                }

                if (stackArray[i].Equals(ItemStack.Empty))
                {
                    emptyItems.Add(i);
                }
            }




            if (sameItems.Count > 0)
            {
                foreach (int i in sameItems)
                {
                    if (InventoryHelper.TryAddItemToSlot(stackArray, i, byproduct))
                    {
                        change = StackChangedState.CHANGED;
                        return stackArray;
                    }
                }
            }

            if (emptyItems.Count > 0)
            {
                foreach (int i in emptyItems)
                {
                    if (InventoryHelper.TryAddItemToSlot(stackArray, i, byproduct))
                    {
                        change = StackChangedState.CHANGED;
                        return stackArray;
                    }
                }
            }

            change = StackChangedState.ERROR;
            stackArray = previousState;
            return stackArray;
        }




        /**
         * Extension method for ItemValue to get the fuel byproduct data.
         */

        public static FuelByproductData GetFuelByproductData(this ItemValue fuelItem)
        {
            ItemClass itemClass = ItemClass.list[fuelItem.type];
            FuelByproductData data;

            if (itemClass == null)
            {
                return FuelByproductData.Empty;
            }

            if (itemClass.IsBlock())
            {
                Block block = Block.list[fuelItem.type];
                if (block is BlockFuelByproduct)
                {

                    return (data = (block as BlockFuelByproduct).FuelByproductData) != null ? data : FuelByproductData.Empty;
                }
            }

            if (itemClass is ItemClassFuelByproduct)
            {
                return (data = (itemClass as ItemClassFuelByproduct).FuelByproductData) != null ? data : FuelByproductData.Empty;
            }

            return FuelByproductData.Empty;
        }


        /**
         * Returns the input byproduct data for an input byproduct item.
         */

        public static InputByproductData GetInputByproductData(this ItemValue inputItem)
        {
            ItemClass itemClass = inputItem.ItemClass;
            if (itemClass == null)
            {
                return InputByproductData.Empty;
            }

            if (!(itemClass is ItemClassInputByproduct))
            {
                return InputByproductData.Empty;
            }

            return (itemClass as ItemClassInputByproduct).ByproductData;
        }
    }
}