﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace FennecCore.Scripts
{
    public class TransformationCollection
    {
        /**
         * Creates an empty transformation collection.
         */

        public TransformationCollection()
        {
            this.collection = new List<TransformationData>();
        }


        /**
         *  Imports a transformation collection from a list of transformation data objects/
         */

        public TransformationCollection(List<TransformationData> collection)
        {
            this.collection = (collection == null ? new List<TransformationData>() : collection);
        }


        /**
         *  Imports a transformation collection from a list of hashes.
         */

        public TransformationCollection(List<string> hashes)
        {
            List<TransformationData> collection = new List<TransformationData>();
            foreach (string hash in hashes)
            {
                if (TransformationData.hashmap.ContainsKey(hash))
                {
                    collection.Add(TransformationData.hashmap[hash]);
                }
            }
            this.collection = collection;
        }


        /**
         *	Adds a TransformationData object to the collection.
         */

        public void Add(TransformationData transformationData)
        {
            this.collection.Add(transformationData);
        }


        /**
         * Adds recipe data for the transformation collection.
         */

        public void AddEnabledRecipes()
        {
            if (this.collection == null)
            {
                return;
            }

            foreach (TransformationData tData in this.collection)
            {
                if (!tData.useRecipe)
                {
                    continue;
                }

                List<Tuple<ItemStack, double>> inputStacks = tData.GetAllInputs();
                List<Tuple<ItemStack, double>> outputStacks = tData.GetAllOutputs(true);

                foreach (Tuple<ItemStack, double> entry in outputStacks)
                {
                    RecipeTransformer recipe = new RecipeTransformer();
                    recipe.itemValueType = entry.Item1.itemValue.type;
                    recipe.probabilityValue = entry.Item2;
                    recipe.showProbability = tData.showProbability;
                    recipe.count = entry.Item1.count;
                    recipe.scrapable = false;
                    recipe.materialBasedRecipe = false;
                    recipe.craftingArea = tData.craftArea;
                    recipe.craftingToolType = 0;
                    recipe.craftingTime = (float)tData.transformationTime;
                    recipe.unlockExpGain = -1;
                    recipe.craftExpGain = -1;
                    recipe.UseIngredientModifier = true;
                    recipe.wildcardForgeCategory = false;
                    foreach (Tuple<ItemStack, double> ingredientData in inputStacks)
                    {
                        recipe.AddIngredient(ingredientData.Item1);
                    }

                    ExtendedRecipeList.Add(recipe);
                }
            }
        }


        /**
         * Reads a string into a TransformationCollection object.
         */

        public static TransformationCollection Read(string _s, bool fromHash = false)
        {
            MatchCollection collectionCheck = TransformationCollection.readParser["tColParse"].Matches(_s);
            if (collectionCheck.Count == 0)
            {
                throw new Exception("Could not read data string " + _s + " into TransformationCollection.");
            }

            MatchCollection tDataGroups = TransformationCollection.readParser["tDataParse"].Matches(_s);
            if (tDataGroups.Count == 0)
            {
                throw new Exception("Could not find groups for data string " + _s + ".");
            }

            Dictionary<int, string> tDataStrings = new Dictionary<int, string>();

            int i = 0;
            foreach (Match tDataGroup in tDataGroups)
            {
                i += 1;
                tDataStrings.Add(i, tDataGroup.Groups[(fromHash ? 0 : 1)].ToString());
            }

            TransformationCollection tCollection = new TransformationCollection();

            foreach (KeyValuePair<int, string> entry in tDataStrings)
            {
                tCollection.Add(TransformationData.Read(entry.Value, fromHash));
            }

            return tCollection;
        }


        /**
         * Writes out transformation data to a string.
         */

        public string Write(bool toHash = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("#tC#");
           
            foreach (TransformationData tData in this.collection)
            {
                sb.Append(tData.Write(toHash));
            }
            sb.Append("#_tC#");
            return sb.ToString();
        }


        /**
         * Writes out the collection for debugging
         */

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Transformation Collection:");
            sb.Append(Environment.NewLine);
            foreach (TransformationData tData in this.collection)
            {
                sb.Append(tData.ToString());
            }
            return sb.ToString();
        }



        public List<TransformationData> collection;

        /**
         * Regexes for parsing input strings into the system and reading collections and data.
         */

        public static Dictionary<string, Regex> readParser = new Dictionary<string, Regex>()
        {
            { "tColParse",          new Regex(@"#tC#(.+?)#_tC#") },
            { "tDataParse",         new Regex(@"#tD#(.+?)#_tD#") },
            { "tTimeParse",         new Regex(@"#tT#(.+?)#_tT#") },
            { "tRecipeParse",       new Regex(@"#tR#(.+?)#_tR#") },
            { "tProbabilityParse",  new Regex(@"#tP#(.+)?#_tP#") },
            { "tCraftAreaParse",    new Regex(@"#tc#(.+?)#_tc#") },
            { "tInputParse",        new Regex(@"#tI#(.+?)#_tI#") },
            { "tOutputParse",       new Regex(@"#tO#(.+?)#_tO#") },
            { "tEntitiesParse",     new Regex(@"#tE#(.+?)#_tE#") },
            { "tEntRangeParse",     new Regex(@"#tr#(.+?)#_tr#") }
        };
    }
}