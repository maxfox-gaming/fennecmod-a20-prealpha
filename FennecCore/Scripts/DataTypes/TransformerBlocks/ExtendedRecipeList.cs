﻿using System;
using System.Collections.Generic;


namespace FennecCore.Scripts
{
    public static class ExtendedRecipeList
    {
        public static void Add(Recipe recipe)
        {
            if (extendedRecipeList.Contains(recipe))
            {
                return;
            }

            extendedRecipeList.Add(recipe);
        }


        public static void InitializeExtendedRecipes()
        {
            Log.Out("Initialize Recipe");
            foreach (Recipe recipe in extendedRecipeList)
            {
                recipe.Init();
                CraftingManager.AddRecipe(recipe);
            }
        }


        private static List<Recipe> extendedRecipeList = new List<Recipe>();
    }
}
