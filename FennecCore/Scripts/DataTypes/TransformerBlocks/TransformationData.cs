﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace FennecCore.Scripts
{
    public class TransformationData
    {
        /**
         *  Initialize transformation data object.
         */

        public TransformationData()
        {
            this.inputsOutputs = new Dictionary<string, List<ITransformerItem>>();
        }


        /**
         * Clones the transformation object so we can write to it and not affect the original.
         */

        public TransformationData Clone()
        {
            TransformationData tData2 = new TransformationData();
            tData2.inputsOutputs = this.inputsOutputs;
            tData2.transformationTime = this.transformationTime;
            tData2.showProbability = false;
            tData2.useRecipe = false;
            tData2.craftArea = this.craftArea;
            tData2.entities = this.entities;
            tData2.range = this.range;
            return tData2;
        }


        /**
         *	Adds a new item to the input or output area of the dictionary, depending on the type.
         */

        public void Add(ITransformerItem transformerItem)
        {
            List<ITransformerItem> transformerItems;
            string key = this.GetKey(transformerItem);
            if (key == "")
            {
                return;
            }

            if (this.inputsOutputs.ContainsKey(key))
            {
                transformerItems = this.inputsOutputs[key];
                transformerItems.Add(transformerItem);
                this.inputsOutputs.Remove(key);
                this.inputsOutputs.Add(key, transformerItems);
                return;
            }

            transformerItems = new List<ITransformerItem>();
            transformerItems.Add(transformerItem);
            this.inputsOutputs.Add(key, transformerItems);
        }


        /**
         *	Checks which key to return from the dictionary based on types.
         */

        public string GetKey(ITransformerItem transformerItem)
        {
            if (transformerItem is TransformerItemInput)
            {
                return this.inputs;
            }
            if (transformerItem is TransformerItemOutput)
            {
                return this.outputs;
            }
            return "";
        }


        /**
         *	Checks whether the data has inputs specified.
         */
        public bool HasInputs()
        {
            return (this.inputsOutputs.ContainsKey(inputs));
        }


        /**
         *	Checks whether the data has outputs specified.
         */
        public bool HasOutputs()
        {
            return (this.inputsOutputs.ContainsKey(outputs));
        }


        /**
         *	Retrieves all inputs from the item collection.
         */

        public List<Tuple<ItemStack, double>> GetAllInputs()
        {
            List<Tuple<ItemStack, double>> inputTable = new List<Tuple<ItemStack, double>>();
            if (!this.HasInputs())
            {
                return inputTable;
            }

            foreach (TransformerItemInput transformerItemInput in this.inputsOutputs[this.inputs])
            {
                inputTable.Add(new Tuple<ItemStack, double>(transformerItemInput.itemStack, transformerItemInput.prob));
            }

            return inputTable;
        }


        /**
         * Returns a list of all inputs after probability calculations.
         */

        public List<ItemStack> GetAllInputsAfterProbabilityCalculation()
        {
            List<ItemStack> list = new List<ItemStack>();
            if (!this.HasInputs())
            {
                return list;
            }

            foreach (TransformerItemInput transformerItemInput in this.inputsOutputs[this.inputs])
            {
                double randomDouble = RandomStatic.Next();
                if (randomDouble <= transformerItemInput.prob)
                {
                    list.Add(transformerItemInput.itemStack);
                }
            }

            return list;
        }



        /**
         *  Returns a dictionary of output itemstacks with their probabilities.
         */

        public List<Tuple<ItemStack, double>> GetAllOutputs(bool ignoreHidden = false)
        {
            List<Tuple<ItemStack, double>> outputTable = new List<Tuple<ItemStack, double>>();
            if (!this.HasOutputs())
            {
                return outputTable;
            }

            foreach (TransformerItemOutput transformerItemOutput in this.inputsOutputs[this.outputs])
            {
                if (ignoreHidden && !transformerItemOutput.display)
                {
                    continue;
                }
                outputTable.Add(new Tuple<ItemStack, double>(transformerItemOutput.itemStack, transformerItemOutput.prob));
            }

            return outputTable;
        }


        /**
         *	Retrieves all outputs from the item collection in a list, based on probabilities.
         */

        public List<ItemStack> GetAllOutputsAfterProbabilityCalculation()
        {
            List<ItemStack> list = new List<ItemStack>();
            if (!this.HasOutputs())
            {
                return list;
            }

            foreach (TransformerItemOutput transformerItemOutput in this.inputsOutputs[this.outputs])
            {
                double randomDouble = RandomStatic.Next();
                if (randomDouble <= transformerItemOutput.prob)
                {
                    list.Add(transformerItemOutput.itemStack);
                }
            }

            return list;
        }


        /**
         *	Checks whether all inputs are fulfilled, given a list of item stacks.
         */

        public bool HasAllInputs(ItemStack[] itemStacks, out List<Tuple<int, ItemStack>> locations)
        {
            locations = new List<Tuple<int, ItemStack>>();
            List<Tuple<int, ItemStack>> foundSoFar = new List<Tuple<int, ItemStack>>();

            // This is required to stop nullref, since sometimes null is passed in from an inventory.
            if (itemStacks == null)
            {
                return false;
            }

            if (!this.HasInputs())
            {
                return false;
            }

            // First check we have all inputs, before probability calculation.
            List<Tuple<ItemStack, double>> inputs = this.GetAllInputs();
            List<ItemStack> inputsToTake = this.GetAllInputsAfterProbabilityCalculation();
            int stacksToMatch = inputs.Count;
            int matches = 0;

            if (inputs.Count == 0)
            {
                return true;
            }

            foreach (Tuple<ItemStack, double> entry in inputs)
            {
                for (int i = 0; i < itemStacks.Length; i += 1)
                {
                    if (matches >= stacksToMatch)
                    {
                        break;
                    }

                    if (itemStacks[i] == null)
                    {
                        continue;
                    }

                    if (itemStacks[i].IsEmpty())
                    {
                        continue;
                    }

                    if (itemStacks[i].itemValue.type != entry.Item1.itemValue.type)
                    {
                        continue;
                    }

                    if (itemStacks[i].count >= entry.Item1.count)
                    {
                        matches += 1;
                        // If, after probability calc, we have this input, add the location.
                        if (inputsToTake.Contains(entry.Item1))
                        {
                            foundSoFar.Add(new Tuple<int, ItemStack>(i, entry.Item1));
                        }
                    }
                }
            }

            if (matches < stacksToMatch)
            {
                return false;
            }

            locations = foundSoFar;
            return true;
        }


        /**
         * Checks whether we have needed entities in an area.
         */

        public bool HasNeededEntitiesNearby(World world, Vector3i pos)
        {
            if (entities.Count == 0 || range == Vector3i.zero)
            {
                return true;
            }

            List<Entity> entitiesNearby = CoordinateHelper.GetEntitiesInBounds(world, pos, range, entities);
            foreach (Entity entity in entitiesNearby)
            {
                if (entities.Contains(entity.EntityClass.entityClassName))
                {
                    return true;
                }
            }

            return false;
        }


        /**
         * Reads transformation data from a string and outputs a new TransformationData object.
         */

        public static TransformationData Read(string _s, bool fromHash = false, bool fromQueue = false)
        {
            TransformationData tData;

            if (fromHash)
            {
                Match match = TransformationCollection.readParser["tDataParse"].Match(_s);
                string hash = fromQueue ? _s : match.Groups[1].ToString();
                if (TransformationData.GetTransformationDataWithHash(hash) == null)
                {
                    Log.Warning("Hash not found when trying to read data..");
                }

                return TransformationData.GetTransformationDataWithHash(hash);
            }

            tData = new TransformationData();
            TransformationData.ReadTime(_s, ref tData);
            TransformationData.ReadUseRecipe(_s, ref tData);
            TransformationData.ReadShowProbability(_s, ref tData);
            TransformationData.ReadCraftArea(_s, ref tData);
            TransformationData.ReadEntityParams(_s, ref tData);
            TransformationData.ReadInputs(_s, ref tData);
            TransformationData.ReadOutputs(_s, ref tData);
            TransformationData.AddToHashmap(tData);
            return tData;
        }


        /**
         * Reads the time string from the TransformationData string.
         */

        private static void ReadTime(string _s, ref TransformationData tData)
        {
            MatchCollection timeCheck = TransformationCollection.readParser["tTimeParse"].Matches(_s);
            if (timeCheck.Count == 0)
            {
                tData.transformationTime = TransformationPropertyParser.defaultTransformationTime;
                return;
            }

            double time;
            if (!StringParsers.TryParseDouble(timeCheck[0].Groups[1].ToString(), out time))
            {
                throw new Exception("Could not parse " + timeCheck[0].Value.ToString() + " as a double.");
            }
            tData.transformationTime = time;
        }


        /**
         * Reads whether we should have a recipe.
         */

        private static void ReadUseRecipe(string _s, ref TransformationData tData)
        {
            MatchCollection recipeCheck = TransformationCollection.readParser["tRecipeParse"].Matches(_s);
            if (recipeCheck.Count == 0)
            {
                tData.useRecipe = false;
                return;
            }

            bool useRecipe;
            if (!StringParsers.TryParseBool(recipeCheck[0].Groups[1].ToString(), out useRecipe))
            {
                throw new Exception("Could not parse " + recipeCheck[0].Value.ToString() + " as a bool.");
            }
            tData.useRecipe = useRecipe;
        }


        /**
         * Reads whether we should display recipe probability.
         */

        private static void ReadShowProbability(string _s, ref TransformationData tData)
        {
            MatchCollection probabilityCheck = TransformationCollection.readParser["tProbabilityParse"].Matches(_s);
            if (probabilityCheck.Count == 0)
            {
                tData.showProbability = true;
                return;
            }

            bool showProbability;
            if (!StringParsers.TryParseBool(probabilityCheck[0].Groups[1].ToString(), out showProbability))
            {
                throw new Exception("Could not parse " + probabilityCheck[0].Value.ToString() + " as a bool.");
            }
            tData.showProbability = showProbability;
        }



        /**
         * Reads whether we should have a craft area.
         */

        private static void ReadCraftArea(string _s, ref TransformationData tData)
        {
            MatchCollection craftAreaCheck = TransformationCollection.readParser["tCraftAreaParse"].Matches(_s);
            if (craftAreaCheck.Count == 0)
            {
                tData.craftArea = "";
                return;
            }

            tData.craftArea = craftAreaCheck[0].Groups[1].ToString();
        }


        /**
         * Reads all inputs from the TransformationData input strings
         */

        private static void ReadInputs(string _s, ref TransformationData tData)
        {
            MatchCollection inputCheck = TransformationCollection.readParser["tInputParse"].Matches(_s);
            if (inputCheck.Count == 0)
            {
                throw new Exception("No inputs found in string " + _s + ".");
            }

            foreach (Match inputMatch in inputCheck)
            {
                tData.Add(TransformerItemInput.Read(inputMatch.ToString()));
            }
        }


        /**
         * Reads all outputs from the TransformationData output strings.
         */

        private static void ReadOutputs(string _s, ref TransformationData tData)
        {
            MatchCollection outputCheck = TransformationCollection.readParser["tOutputParse"].Matches(_s);
            if (outputCheck.Count == 0)
            {
                throw new Exception("No outputs found in string " + _s + ".");
            }

            foreach (Match outputMatch in outputCheck)
            {
                tData.Add(TransformerItemOutput.Read(outputMatch.ToString()));
            }
        }


        /**
         * Reads entity and range params.
         */

        private static void ReadEntityParams(string _s, ref TransformationData tData)
        {
            MatchCollection entityCheck = TransformationCollection.readParser["tEntitiesParse"].Matches(_s);
            MatchCollection rangesCheck = TransformationCollection.readParser["tEntRangeParse"].Matches(_s);
            if (entityCheck.Count == 0)
            {
                tData.entities = new List<string>();
            }
            else
            {
                tData.entities = StringHelpers.WriteStringToList(entityCheck[0].Groups[1].ToString());
            }
            if (rangesCheck.Count == 0)
            {
                tData.range = Vector3i.zero;
            }
            else
            {
                tData.range = StringHelpers.WriteStringToVector3i(rangesCheck[0].Groups[1].ToString());
            }
        }


        /**
         * Writes the transformation data to a string for saving to a stream.
         */

        public string Write(bool toHash = false)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("#tD#");
            if (toHash)
            {
                sb.Append(this.GetComparisonHash());
                sb.Append("#_tD#");
                return sb.ToString();
            }

            sb.Append("#tT#");
            sb.Append(this.transformationTime.ToString());
            sb.Append("#_tT#");
            sb.Append("#tR#");
            sb.Append(this.useRecipe.ToString());
            sb.Append("#_tR#");
            sb.Append("#tP#");
            sb.Append(this.showProbability.ToString());
            sb.Append("#_tP#");
            sb.Append("#tc#");
            sb.Append(this.craftArea);
            sb.Append("#_tc#");
            sb.Append("#tE#");
            sb.Append(StringHelpers.WriteListToString(this.entities));
            sb.Append("#_tE#");
            sb.Append("#_tr#");
            sb.Append(StringHelpers.WriteVector3iToString(this.range));
            sb.Append("#_tr#");
            foreach (TransformerItemInput tInputs in this.inputsOutputs[this.inputs])
            {
                sb.Append(tInputs.Write());
            }
            foreach (TransformerItemOutput tOutputs in this.inputsOutputs[this.outputs])
            {
                sb.Append(tOutputs.Write());
            }

            sb.Append("#_tD#");
            return sb.ToString();
        }


        /**
         * Writes for a hash comparison (things like time etc do not need to be compared).
         */

        public string WriteForHash()
        {
            StringBuilder sb = new StringBuilder();
            foreach (TransformerItemInput tInputs in this.inputsOutputs[this.inputs])
            {
                sb.Append(tInputs.Write());
            }
            foreach (TransformerItemOutput tOutputs in this.inputsOutputs[this.outputs])
            {
                sb.Append(tOutputs.Write());
            }
            return sb.ToString();
        }


        /**
         * Returns string for debugging.
         */

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Transformation Data:");
            sb.Append(Environment.NewLine);
            sb.Append("Time: " + this.transformationTime.ToString() + " sec");
            sb.Append(Environment.NewLine);
            sb.Append("Show Recipe: " + this.useRecipe.ToString());
            sb.Append(Environment.NewLine);
            sb.Append("Show Probabilities: " + this.showProbability.ToString());
            sb.Append(Environment.NewLine);
            foreach (TransformerItemInput input in this.inputsOutputs[this.inputs])
            {
                sb.Append("   ");
                sb.Append(input.ToString());
                sb.Append(Environment.NewLine);
            }
            foreach (TransformerItemOutput output in this.inputsOutputs[this.outputs])
            {
                sb.Append("   ");
                sb.Append(output.ToString());
                sb.Append(Environment.NewLine);
            }
            return sb.ToString();
        }


        /**
         * Returns whether this is the same as another.
         */

        public bool IsSameAs(TransformationData _other)
        {
            return (this.GetComparisonHash() == _other.GetComparisonHash());
        }


        /**
         * Returns the md5 hash for this object.
         */

        public string GetMD5Hash()
        {
            return MD5Hash.Calculate(this.Write());
        }


        /**
         * Gets the comparison hash.
         */

        public string GetComparisonHash()
        {
            return MD5Hash.Calculate(this.WriteForHash());
        }



        /**
         * Returns a transformation data object from hash map.
         */

        public static TransformationData GetTransformationDataWithHash(string hash)
        {
            if (TransformationData.hashmap.ContainsKey(hash))
            {
                return TransformationData.hashmap[hash];
            }
            return null;
        }


        /**
         * Allows a transformation object to be added to hash map externally.
         */

        public static bool AddToHashmap(TransformationData tData)
        {
            if (TransformationData.hashmap == null)
            {
                TransformationData.hashmap = new Dictionary<string, TransformationData>();
            }

            string hash = tData.GetComparisonHash();
            if (!TransformationData.hashmap.ContainsKey(hash))
            {
                TransformationData.hashmap.Add(hash, tData);
                return true;
            }
            return false;
        }


        /**
         * If this class wants to add a hash map, this is done privately.
         */

        private bool AddToHashmap()
        {
            return TransformationData.AddToHashmap(this);
        }


        public static Dictionary<string, TransformationData> hashmap;
        public Dictionary<string, List<ITransformerItem>> inputsOutputs;
        public double transformationTime = TransformationPropertyParser.defaultTransformationTime;
        public string inputs = "Inputs";
        public string outputs = "Outputs";
        public bool useRecipe = false;
        public bool showProbability = true;
        public List<string> entities = new List<string>();
        public Vector3i range = Vector3i.zero;
        public string craftArea = String.Empty;
    }
}