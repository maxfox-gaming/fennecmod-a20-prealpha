﻿using System;
using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public static class MultiblockManager
    {
        private static Dictionary<string, MultiblockData> multiblocks = new Dictionary<string, MultiblockData>();
        private static List<string> blockTypes = new List<string>();


        /**
         * Adds a new multiblock structure definition to the manager. This should be called on the master block's PostInit() method so that modded blocks can also be taken into account.
         */

        public static void AddMultiblock(MultiblockData multiblockData)
        {
            string master = multiblockData.GetMaster();
            if (!multiblocks.ContainsKey(master))
            {
                multiblocks.Add(master, multiblockData);
            }
        }


        /**
         * Gets multiblock data
         */

        public static List<string> GetBlockTypesFor(string name)
        {
            List<string> blockTypes = new List<string>();
            if (!multiblocks.TryGetValue(name, out MultiblockData multiblockData))
            {
                Log.Warning("No type found for block " + name);
                return blockTypes;
            }
            return multiblockData.GetBlockNamesTypesList();
        }


        /**
         * Returns the block types and coordinates in world space according to whatever rotation is set.
         */

        public static Dictionary<Vector3i, string> GetCoordinatesInWorldSpace(string masterBlock, Vector3i coordinate, int rotation = 0)
        {
            return MultiblockManager.multiblocks[masterBlock].GetMultiblockPositionsInWorldSpace(coordinate, rotation);
        }


        /**
         * Adds a block type to the list if specified in the XML. This should be called on the slave's Init() method but only if a type is specified separately.
         */

        public static void AddBlockType(string blockType)
        {
            if (!MultiblockManager.blockTypes.Contains(blockType))
            {
                MultiblockManager.blockTypes.Add(blockType);
            }
        }


        /**
         * Verifies whether all blocks are valid as the world loads. Should be called by each multiblockMaster extension class' LateInit() method after the sturtcure has been registered in 
         * the manager.
         */

        public static bool ValidateMultiblock(string masterBlock)
        {
            if (!MultiblockManager.multiblocks.ContainsKey(masterBlock))
            {
                throw new Exception("Multiblock structure data for block " + masterBlock + " not found.");
            }

            foreach (string blockName in MultiblockManager.multiblocks[masterBlock].GetBlockNamesTypesList())
            {
                BlockValue blockvalue = Block.GetBlockValue(blockName, false);
                if (blockvalue.type != 0)
                {
                    continue;
                }

                if (MultiblockManager.blockTypes.Contains(blockName))
                {
                    continue;
                }

                throw new Exception("Block name " + blockName + " is not a valid name or type.");
            }

            return true;
        }
    }
}