﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FennecCore.Scripts
{
    public class MultiblockData
    {
        private Dictionary<Vector3i, string> multiblockPositions;
        private Dictionary<Vector3i, string>[] transformations;
        private string masterBlock;
        private List<string> blockNamesTypes;
        private string patternVectors = @"(\(\s*-?\s*[0-9]+\s*,\s*-?\s*[0-9]+\s*,\s*-?\s*[0-9]+\s*\))";

        /**
         * Constructor (takes in a string)
         */

        public MultiblockData(string multiblockStructure, string masterBlock)
        {
            this.masterBlock = masterBlock;
            this.ConvertStructureStringToCoords(multiblockStructure);
            this.CreateBlockNameTypeLlist();
            this.CreateRotationalData();
        }


        /**
         * Constructor (takes in a dictionary directly)
         */

        public MultiblockData(Dictionary<Vector3i, string> blockPlaces, string masterBlock)
        {
            this.masterBlock = masterBlock;
            this.multiblockPositions = blockPlaces;
            this.CreateBlockNameTypeLlist();
            this.CreateRotationalData();
        }


        /**
         * Returns the master block.
         */

        public string GetMaster()
        {
            return this.masterBlock;
        }


        /**
         * Returns the raw multiblock dictionary data.
         */

        public Dictionary<Vector3i, string> GetMultiblockPositionsRaw()
        {
            return this.multiblockPositions;
        }


        /**
         * Returns the block name and type list for the structure.
         */

        public List<string> GetBlockNamesTypesList()
        {
            return this.blockNamesTypes;
        }


        /**
         * Returns a dictionary of coordinates and block names in world space for comparison.
         */

        public Dictionary<Vector3i, string> GetMultiblockPositionsInWorldSpace(Vector3i masterCoordinate, int orientation = 0)
        {
            Dictionary<Vector3i, string> worldSpaceCoordinates = new Dictionary<Vector3i, string>();
            foreach (KeyValuePair<Vector3i, string> entry in this.transformations[orientation % 4])
            {
                worldSpaceCoordinates.Add(entry.Key + masterCoordinate, entry.Value);
            }

            return worldSpaceCoordinates;
        }


        /**
         * When the block name and type dictionary is provided, create a unique list to check for validity (call this on a PostInit() method in MultiblockMaster to validate)
         */

        private void CreateBlockNameTypeLlist()
        {
            this.blockNamesTypes = new List<string>();
            foreach (KeyValuePair<Vector3i, string> entry in this.multiblockPositions)
            {
                if (!this.blockNamesTypes.Contains(entry.Value))
                {
                    this.blockNamesTypes.Add(entry.Value);
                }
            }
        }



        /**
         * If a string is given, it needs to be converted from a multiblock string to a dictionary of coordinates and the multiblock slaves.
         */

        private void ConvertStructureStringToCoords(string multiblockStructure)
        {
            this.multiblockPositions = new Dictionary<Vector3i, string>();
            List<string> structureList = StringHelpers.WriteStringToList(multiblockStructure, ';');

            Regex vectors = new Regex(this.patternVectors);
            foreach (string entry in structureList)
            {
                List<string> blockAndCoords = StringHelpers.WriteStringToList(entry.Trim('{', '}'), ':');
                string blockName = blockAndCoords[0];
                string coords = blockAndCoords[1];

                MatchCollection matches = vectors.Matches(coords);
                foreach (Match match in matches)
                {
                    Vector3i coordinate = StringHelpers.WriteStringToVector3i(match.Value);
                    this.multiblockPositions.Add(coordinate, blockName);
                }
            }
        }


        /**
         * Returns the transformations.
         */

        private void CreateRotationalData()
        {
            this.transformations = new Dictionary<Vector3i, string>[4];
            for (int i = 0; i < transformations.Length; i += 1)
            {
                if (i == 0)
                {
                    transformations[i] = this.multiblockPositions;
                    continue;
                }

                this.transformations[i] = CoordinateHelper.RotateBlockPlacementsY90CC(transformations[i - 1]);
            }
        }
    }
}