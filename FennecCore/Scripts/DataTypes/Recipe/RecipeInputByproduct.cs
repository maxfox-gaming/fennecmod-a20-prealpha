﻿using System;

namespace FennecCore.Scripts
{
    public class RecipeInputByproduct : Recipe, IRecipeProbability, IRecipeByproduct
    {
        /**
         * This casts the first ingredient to ItemStackFuel and the subsequent ones to ItemStackRequirement.
         */

        public new void Init()
        {
            base.Init();
            for (int i = 0; i < this.ingredients.Count; i += 1)
            {
                ItemStack stack = this.ingredients[i];
                if (i == 0)
                {
                    this.ingredients[i] = new ItemStackInput(stack.itemValue, stack.count);
                }
            }
        }


        /**
        * Adds an item stack to the ingredients.
        */

        public void AddIngredient(ItemStack stack)
        {
            this.ingredients.Add(stack);
        }


        /**
         * Adds an ingredient as ItemStackInput
         */

        public void AddInputIngredient(ItemValue inputItem, int count)
        {
            this.AddIngredient(new ItemStackInput(inputItem, count));
        }


        /**
         * Returns a probability value stored as a percent
         */

        public double ConvertToProbPerc()
        {
            return Math.Round(probabilityValue * 100, 1);
        }


        public double probabilityValue;
    }
}