﻿using System;

namespace FennecCore.Scripts
{
	public class RecipeFuelByproduct : Recipe, IRecipeProbability, IRecipeByproduct
	{
		/**
		 * This casts the first ingredient to ItemStackFuel and the subsequent ones to ItemStackRequirement.
		 */

		public new void Init()
		{
			base.Init();
			for (int i = 0; i < this.ingredients.Count; i += 1)
			{
				ItemStack stack = this.ingredients[i];
				if (i == 0)
				{
					this.ingredients[i] = new ItemStackFuel(stack.itemValue, stack.count);
				}
				else
				{
					this.ingredients[i] = new ItemStackRequirement(stack.itemValue, stack.count);
				}
			}
		}




		/**
		 * Adds an item stack to the ingredients.
		 */

		public void AddIngredient(ItemStack stack)
		{
			this.ingredients.Add(stack);
		}


		/**
		 * Add a fuel ingredient
		 */

		public void AddFuelIngredient(ItemValue fuelItem, int count)
		{
			this.ingredients.Add(new ItemStackFuel(fuelItem, count));
		}

		/**
		 * Add a requirement ingredient
		 */

		public void AddRequirementIngredient(ItemValue requirementItem, int count)
		{
			this.ingredients.Add(new ItemStackRequirement(requirementItem, count));
		}


		/**
		 * Converts a probability value to a percentage adjusted value.
		 */

		public double ConvertToProbPerc()
		{
			return Math.Round(probabilityValue * 100, 1);
		}


		public double probabilityValue;
		public bool showProbability;
	}
}