﻿using System;

namespace FennecCore.Scripts
{
	public class RecipeTransformer : Recipe, IRecipeProbability
	{
		/**
		 * Adds an item stack to the ingredients.
		 */

		public void AddIngredient(ItemStack stack)
		{
			this.ingredients.Add(stack);
		}


		/**
		 * Converts a probability value to a percentage adjusted value.
		 */

		public double ConvertToProbPerc()
		{
			return Math.Round(probabilityValue * 100, 1);
		}


		public double probabilityValue;
		public bool showProbability;
	}
}