﻿namespace FennecCore.Scripts
{

    public interface IRecipeProbability
    {
        /**
         * Initialize recipe data.
         */

        void Init();


        /**
         * Adds an item stack to the ingredients.
         */

        void AddIngredient(ItemStack stack);


        /**
         * Returns a probability value stored as a percent
         */

        double ConvertToProbPerc();
    }
}