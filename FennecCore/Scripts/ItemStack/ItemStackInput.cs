﻿namespace FennecCore.Scripts
{
    public class ItemStackInput : ItemStack
    {
        public ItemStackInput(ItemValue item, int count) : base(item, count)
        {

        }


        /**
         * Clones the stack to give out the same type instead of a regular ItemStack.
         */

        public new ItemStackInput Clone()
        {
            if (this.itemValue != null)
            {
                return new ItemStackInput(this.itemValue.Clone(), this.count);
            }
            return new ItemStackInput(ItemValue.None.Clone(), this.count);
        }
    }
}