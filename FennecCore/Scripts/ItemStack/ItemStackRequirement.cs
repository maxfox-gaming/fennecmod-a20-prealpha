﻿namespace FennecCore.Scripts
{
    public class ItemStackRequirement : ItemStack
    {
        public ItemStackRequirement(ItemValue itemValue, int count) : base(itemValue, count)
        {

        }


        /**
         * Clones the stack to give out the same type instead of a regular ItemStack.
         */

        public new ItemStackRequirement Clone()
        {
            if (this.itemValue != null)
            {
                return new ItemStackRequirement(this.itemValue.Clone(), this.count);
            }
            return new ItemStackRequirement(ItemValue.None.Clone(), this.count);
        }
    }
}