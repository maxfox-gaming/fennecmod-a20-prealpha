﻿namespace FennecCore.Scripts
{
    public class ItemStackFuel : ItemStack
    {
        public ItemStackFuel(ItemValue itemValue, int count) : base(itemValue, count)
        {

        }


        /**
         * Clones the stack to give out the same type instead of a regular ItemStack.
         */

        public new ItemStackFuel Clone()
        {
            if (this.itemValue != null)
            {
                return new ItemStackFuel(this.itemValue.Clone(), this.count);
            }
            return new ItemStackFuel(ItemValue.None.Clone(), this.count);
        }
    }
}