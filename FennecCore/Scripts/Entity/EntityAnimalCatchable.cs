﻿using System;

namespace FennecCore.Scripts
{
    public class EntityAnimalCatchable : EntityAnimalRabbit
    {
        public override void CopyPropertiesFromEntityClass()
        {
            base.CopyPropertiesFromEntityClass();

            EntityClass entityClass = EntityClass.list[this.entityClass];
            if (!entityClass.Properties.Values.ContainsKey(this.propItemToReturn))
            {
                throw new Exception("Entity " + entityClass.entityClassName + " does not have a " + this.propItemToReturn + " property.");
            }

            this.itemReturned = ItemClass.GetItem(entityClass.Properties.Values[this.propItemToReturn], false);
            if (this.itemReturned.IsEmpty())
            {
                throw new Exception("Item with name '" + entityClass.Properties.Values[this.propItemToReturn] + "' not found!");
            }

            this.holdingItem = ItemClass.GetItem("meleeHandPlayer", false);
            if (entityClass.Properties.Values.ContainsKey(this.propHoldingItem))
            {
                this.holdingItem = ItemClass.GetItem(entityClass.Properties.Values[this.propHoldingItem], false);
                if (this.holdingItem.IsEmpty())
                {
                    throw new Exception("Item with name '" + EntityClass.Properties.Values[this.propHoldingItem] + "' not found!");
                }
            }
        }


        private string propHoldingItem = "HoldingItem";
        private string propItemToReturn = "ItemToReturn";
        public ItemValue itemReturned;
        public ItemValue holdingItem;
    }
}