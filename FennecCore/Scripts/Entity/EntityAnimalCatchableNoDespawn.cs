﻿namespace FennecCore.Scripts
{
    public class EntityAnimalCatchableNoDespawn : EntityAnimalCatchable
    {
        /**
         * Sets false for despawn.
         */

        protected override bool canDespawn()
        {
            return false;
        }
    }
}