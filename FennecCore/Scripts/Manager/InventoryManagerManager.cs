﻿using System.Collections.Generic;
using System.Text;

namespace FennecCore.Scripts
{
    public static class InventoryManagerManager
    {
        public static Dictionary<string, InventoryManagerLink> links = new Dictionary<string, InventoryManagerLink>();


        /**
         * Adds containers to the manager.
         */

        public static void Add(InventoryManagerLink link)
        {
            string hash = link.GetHash();
            if (!links.ContainsKey(hash))
            {
                InventoryManagerManager.links.Add(hash, link);
            }
        }


        /**
         * Adds containers to the manager from a string.
         */

        public static void Add(string _s)
        {
            InventoryManagerLink link = InventoryManagerLink.Read(_s);
            InventoryManagerManager.Add(link);
        }


        /**
         * Removes all master links to a set of signed containers.
         */

        public static void RemoveManager(Vector3i managerCoordinate)
        {
            foreach (KeyValuePair<string, InventoryManagerLink> entry in InventoryManagerManager.links)
            {
                if (entry.Value.GetManagerCoordinate() == managerCoordinate)
                {
                    InventoryManagerManager.links.Remove(entry.Key);
                }
            }
        }


        /**
         * Removes a container.
         */

        public static void RemoveContainer(Vector3i containerCoordinate)
        {
            foreach (KeyValuePair<string, InventoryManagerLink> entry in InventoryManagerManager.links)
            {
                if (entry.Value.GetContainerCoordinate() == containerCoordinate)
                {
                    InventoryManagerManager.links.Remove(entry.Key);
                }
            }
        }


        /**
         * Get all containers associated to a manager.
         */

        public static List<InventoryManagerLink> GetLinksForManager(Vector3i managerCoordinate)
        {
            List<InventoryManagerLink> managerLinks = new List<InventoryManagerLink>();
            foreach (KeyValuePair<string, InventoryManagerLink> entry in InventoryManagerManager.links)
            {
                if (entry.Value.GetManagerCoordinate() == managerCoordinate)
                {
                    managerLinks.Add(entry.Value);
                }
            }
            return managerLinks;
        }


        /**
         * Get all managers associated to a container.
         */

        public static List<InventoryManagerLink> GetLinksForContainer(Vector3i containerCoordinate)
        {
            List<InventoryManagerLink> containerLinks = new List<InventoryManagerLink>();
            foreach (KeyValuePair<string, InventoryManagerLink> entry in InventoryManagerManager.links)
            {
                if (entry.Value.GetContainerCoordinate() == containerCoordinate)
                {
                    containerLinks.Add(entry.Value);
                }
            }
            return containerLinks;
        }



        public static string Write()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, InventoryManagerLink> entry in InventoryManagerManager.links)
            {
                sb.Append(entry.Value.Write());
                sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}