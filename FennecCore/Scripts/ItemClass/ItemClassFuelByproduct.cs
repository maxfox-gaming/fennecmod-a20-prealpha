﻿using System;

namespace FennecCore.Scripts
{

    public class ItemClassFuelByproduct : ItemClass
    {

        public FuelByproductData FuelByproductData
        {
            get { return byproductData; }
        }


        /**
         * Initializes values after all items have loaded in.
         */

        public new void LateInit()
        {
            string byproductItemName;
            if (!PropertyHelpers.PropExists(Properties, propFuelByproduct, out byproductItemName))
            {
                throw new Exception("Item with name " + Name + " does not define a " + propFuelByproduct + " property.");
            }
            if ((byproduct = ItemClass.GetItem(byproductItemName)).Equals(ItemValue.None))
            {
                throw new Exception("Item with name " + byproductItemName + " not found for " + Name + ".");
            }


            string requirementItemName;
            requirement = ItemValue.None;
            if (PropertyHelpers.PropExists(Properties, propFuelRequirement, out requirementItemName))
            {
                if ((requirement = ItemClass.GetItem(requirementItemName)).Equals(ItemValue.None))
                {
                    throw new Exception("Item with name " + requirementItemName + " not found for " + Name + ".");
                }
            }


            string prob;
            byproductChangeProb = 1.0;
            if (PropertyHelpers.PropExists(Properties, propByproductProb, out prob))
            {
                if (!StringParsers.TryParseDouble(prob, out byproductChangeProb))
                {
                    throw new Exception("Could not parse value " + prob + " as double in " + Name + ".");
                }
            }


            string recipe;
            showRecipe = true;
            if (PropertyHelpers.PropExists(Properties, propShowRecipe, out recipe))
            {
                if (!StringParsers.TryParseBool(recipe, out showRecipe))
                {
                    throw new Exception("Could not parse value " + recipe + " as true/false  in " + Name + ".");
                }
            }



            byproductData = new FuelByproductData(ItemClass.GetItem(Name), byproduct, requirement, byproductChangeProb);
            if (showRecipe)
            {
                byproductData.AddRecipe();
            }
        }


        private FuelByproductData byproductData;
        private ItemValue byproduct;
        private ItemValue requirement;
        private double byproductChangeProb;
        private bool showRecipe;
        public static string propFuelByproduct = "FuelByproduct";
        public static string propFuelRequirement = "FuelRequirement";
        public static string propByproductProb = "ByproductChangeProb";
        public static string propShowRecipe = "ShowRecipe";
    }

}