﻿using System;

namespace FennecCore.Scripts
{
    public class ItemClassInputByproduct : ItemClass
    {
        public InputByproductData ByproductData
        {
            get { return this.inputByproductData; }
        }



        /**
         * Initialize the item values for byproduct info after all items and blocks are loaded.
         */

        public new void LateInit()
        {
            string byproductItemName;
            byproductItem = ItemValue.None;
            if (!PropertyHelpers.PropExists(Properties, propInputByproduct, out byproductItemName))
            {
                throw new Exception("Property " + propInputByproduct + " is not defined for " + Name + ".");
            }
            if ((byproductItem = ItemClass.GetItem(byproductItemName)).Equals(ItemValue.None))
            {
                throw new Exception("Item name " + byproductItemName + " is not defined in " + Name + ".");
            }


            string probabilityValue;
            probability = 1;
            if (PropertyHelpers.PropExists(Properties, propChangeProb, out probabilityValue))
            {
                if (!StringParsers.TryParseDouble(probabilityValue, out probability))
                {
                    throw new Exception("Probability value " + probabilityValue + " for item " + Name + " is not a number.");
                }
            }


            string recipe;
            showRecipe = true;
            if (PropertyHelpers.PropExists(Properties, propShowRecipe, out recipe))
            {
                if (!StringParsers.TryParseBool(recipe, out showRecipe))
                {
                    throw new Exception("Could not parse value " + recipe + " as true/false  in " + Name + ".");
                }
            }


            inputByproductData = new InputByproductData(ItemClass.GetItem(Name), byproductItem, probability);
            if (showRecipe)
            {
                inputByproductData.AddRecipe();
            }
        }


        private ItemValue byproductItem;
        private double probability;
        private bool showRecipe;
        private InputByproductData inputByproductData;

        public static string propInputByproduct = "InputByproduct";
        public static string propChangeProb = "ByproductChangeProb";
        public static string propShowRecipe = "ShowRecipe";
    }
}