﻿using System;
using UnityEngine;

namespace FennecCore.Scripts
{
    public class WindTurbineController : MonoBehaviour
    {
        private bool isOn = false;
        private Transform baseObj;

        
        public void SetBaseTransform(Transform transform)
        {
            this.baseObj = transform;
        }


        public void SetOn()
        {
            this.isOn = true;
        }


        public void SetOff()
        {
            this.isOn = false;
        }


        public void Update()
        {
            try
            {
                Vector3 rotateDir = (this.isOn ? Vector3.right * WeatherManager.GetWindSpeed() / 10 : Vector3.zero);
                Transform rotator = this.baseObj.Find("Assembly/Rotator");
                rotator.Rotate(rotateDir);
            }
            catch (NullReferenceException e)
            {
                Log.Warning(e.Message);
            }
        }
    }
}