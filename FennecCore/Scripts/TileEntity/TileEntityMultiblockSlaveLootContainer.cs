﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public class TileEntityMultiblockSlaveLootContainer : TileEntityLootContainer, IInventory, ITileEntityMultiblockSlaveInventory
    {
        /**
         * Constructor
         */

        public TileEntityMultiblockSlaveLootContainer(Chunk _chunk) : base(_chunk)
        {
            this.containerSize = new Vector2i(3, 3);
            this.lootListName = null;
        }


        /**
         * Clones the loot container from another one.
         */

        private TileEntityMultiblockSlaveLootContainer(TileEntityMultiblockSlaveLootContainer _other) : base(null)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.type = _other.type;
        }


        /**
         * Clones the container
         */

        public new TileEntityMultiblockSlaveLootContainer Clone()
        {
            return new TileEntityMultiblockSlaveLootContainer(this);
        }


        /**
         * Gets the master block if there is one. Useful for when you are interacting with multiblock via its slave.
         */

        public ITileEntityMultiblockMaster GetMaster()
        {
            return this.master;
        }


        /**
         * Gets the master block position
         */

        public Vector3i GetMasterPos()
        {
            return this.masterPos;
        }


        /**
         * Sets the master block for this slave block. Slave blocks can only ever have one master block at a time.
         */

        public void SetMaster(ITileEntityMultiblockMaster master)
        {
            if (this.HasMaster)
            {
                return;
            }
            this.master = master;
            this.master.TriggerUpdate();
            this.masterPos = master.ToWorldPos();
            this.formed = true;
        }


        /**
         * Unsets the master for this tile entity.
         */

        public void UnsetMaster()
        {
            if (this.master != null)
            {
                this.master.TriggerUpdate();
            }
            this.master = null;
            this.masterPos = Vector3i.zero;
            this.formed = false;
        }


        /**
         * Sets the type of this slave (useful for defining types rather than using block names where substitutes are possible
         */

        public void SetSlaveType(string type)
        {
            this.type = type;
        }


        /**
         * Gets the type of the slave, or the block name if it was not defined before.
         */

        public string GetSlaveType()
        {
            if (this.type != null)
            {
                return this.type;
            }

            World world = GameManager.Instance.World;
            return world != null ? world.GetBlock(this.ToWorldPos()).Block.GetBlockName() : null;
        }


        /**
         * Removes the master block binding
         */

        public void UnbindMaster()
        {
            if (this.HasMaster)
            {
                this.master = null;
            }
            this.masterPos = Vector3i.zero;
            this.formed = false;
        }


        /**
         * If this slave block hsa been destroyed and it had a master block, the multiblock will no longer be formed correctly so notify the master block.
         */

        public override void OnRemove(World world)
        {
            base.OnRemove(world);
            bool foundMaster = false;
            ITileEntityMultiblockMaster masterTE = null;

            if (this.HasMaster)
            {
                if (this.master == null)
                {
                    Chunk chunk = world.GetChunkFromWorldPos(this.masterPos) as Chunk;
                    if (chunk != null)
                    {
                        masterTE = world.GetTileEntity(chunk.ClrIdx, this.masterPos) as ITileEntityMultiblockMaster;
                        if (masterTE != null)
                        {
                            foundMaster = true;
                        }
                    }
                }
                else
                {
                    masterTE = this.master;
                    foundMaster = true;
                }

                if (foundMaster && masterTE != null)
                {
                    masterTE.SetMultiblockNotFormed();
                }
            }
            this.OnDestroy();
        }


        /**
         * Checks whether the multiblock is formed
         */

        public bool Formed()
        {
            return this.formed;
        }


        /**
         * Reads the type of the multiblock
         */

        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.type = _br.ReadString();
            this.masterPos = StringHelpers.WriteStringToVector3i(_br.ReadString());
            this.formed = _br.ReadBoolean();
        }


        /**
         * Writes out tile entity data to save file.
         */

        public override void write(PooledBinaryWriter stream, TileEntity.StreamModeWrite _eStreamMode)
        {
            base.write(stream, _eStreamMode);
            stream.Write(this.type);
            stream.Write(StringHelpers.WriteVector3iToString(this.masterPos));
            stream.Write(this.formed);
        }


        /**
         * Gets the tile entity type
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.MBSlaveLoot];
        }


        /**
         * When something happens to the tile entity.
         */

        private void tileEntityChanged()
        {
            for (int i = 0; i < this.listeners.Count; i++)
            {
                this.listeners[i].OnTileEntityChanged(this, 0);
            }
        }


        private Vector2i containerSize;
        private ItemStack[] itemsArr;
        private List<Entity> entList;

        public bool IsPlayerPlaced = false;
        private ITileEntityMultiblockMaster master;
        private string type;
        private bool formed;
        private Vector3i masterPos;

        public bool HasMaster
        {
            get { return (this.master != null || !this.masterPos.Equals(Vector3i.zero)); }
        }
    }
}