﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public class TileEntityMultiblockMasterWorkstationPowered : TileEntityMultiblockMasterWorkstation, ITileEntityMultiblockMaster, ITileEntityReceivePower
    {
        public new event XUiEvent_InputStackChanged InputChanged;
        public new event XUiEvent_FuelStackChanged FuelChanged;


        /**
        * Constructor
        */

        public TileEntityMultiblockMasterWorkstationPowered(Chunk _chunk) : base(_chunk)
        {
            /*
            this.fuel = ItemStack.CreateArray(3);
            this.tools = ItemStack.CreateArray(3);
            this.output = ItemStack.CreateArray(6);
            this.input = ItemStack.CreateArray(3);
            this.lastInput = ItemStack.CreateArray(3);
            this.queue = new RecipeQueueItem[4];
            this.materialNames = new string[0];
            this.isModuleUsed = new bool[5];
            this.currentMeltTimesLeft = new float[this.input.Length];
            */
            this.dataObject = 0;
            this.hasPower = false;
        }


        /**
         * Checks if block is powered. It is required to be next to a TileEntityPowered type of block in any cardinal direction.
         */

        public bool IsPowered(World world)
        {
            if (!this.MultiblockFormed())
            {
                this.hasPower = false;
                return this.hasPower;
            }

            if (this.poweredSlaveCoords == null || this.poweredSlaveCoords.Count == 0)
            {
                this.hasPower = true;
                return this.hasPower;
            }

            if (!this.requiresPower)
            {
                this.hasPower = true;
                return this.hasPower;
            }

            int flag = 0;
            foreach (Vector3i coord in poweredSlaveCoords)
            {
                Chunk chunk = world.GetChunkFromWorldPos(coord) as Chunk;
                Vector3i pos = Chunk.ToLocalPosition(coord);
                if (chunk == null)
                {
                    continue;
                }

                BlockPowered powered = world.GetBlock(coord).Block as BlockPowered;
                if (powered == null)
                {
                    this.SetMultiblockNotFormed();
                    this.hasPower = false;
                    return this.hasPower;
                }

                if (!powered.HasTileEntity)
                {
                    this.SetMultiblockNotFormed();
                    this.hasPower = false;
                    return this.hasPower;
                }

                TileEntityPowered te = chunk.GetTileEntity(pos) as TileEntityPowered;
                if (te == null)
                {
                    continue;
                }

                if (te.IsPowered)
                {
                    flag += 1;
                }
            }

            this.hasPower = (flag >= poweredSlaveCoords.Count);
            this.setModified();
            return this.hasPower;
        }


        /**
         * Updates the master block
         */

        public override bool UpdateMaster(World world, bool checkRotationIndex = false)
        {
            // If master block not found already, attempt to get it.
            if (this.masterBlockName == null)
            {
                Vector3i worldPos = this.ToWorldPos();
                Block block = world.GetBlock(worldPos).Block;
                if (block == null)
                {
                    return false;
                }

                this.masterBlockName = block.GetBlockName();
            }

            // Need to check each rotation in turn to make sure that the structure is formed.
            for (int i = 0; i < 4; i += 1)
            {
                if (checkRotationIndex)
                {
                    if (this.rotationIndex == -1)
                    {
                        UpdateMaster(world);
                        return false;
                    }

                    if (this.rotationIndex != i)
                    {
                        continue;
                    }
                }

                Dictionary<Vector3i, string> multiblockStructure = MultiblockManager.GetCoordinatesInWorldSpace(this.masterBlockName, this.ToWorldPos(), i);
                List<ITileEntityMultiblockSlave> slaveBlocks = new List<ITileEntityMultiblockSlave>();
                List<Vector3i> poweredSlaveCoords = new List<Vector3i>();
                foreach (KeyValuePair<Vector3i, string> blockData in multiblockStructure)
                {
                    Vector3i blockPosition = blockData.Key;
                    string blockNameType = blockData.Value;
                    Block block = world.GetBlock(blockPosition).Block;

                    if (!block.HasTileEntity)
                    {
                        break;
                    }

                    Chunk chunk = world.GetChunkFromWorldPos(blockPosition) as Chunk;
                    if (chunk == null)
                    {
                        break;
                    }

                    ITileEntityMultiblockSlave te = chunk.GetTileEntity(Chunk.ToLocalPosition(blockPosition)) as ITileEntityMultiblockSlave;
                    if (te == null) //Must be a powered one that's inactive.
                    {
                        BlockPowered powered = chunk.GetBlock(Chunk.ToLocalPosition(blockPosition)).Block as BlockPowered;
                        if (powered == null)
                        {
                            break;
                        }

                        string currentBlockType;
                        if (!PropertyHelpers.PropExists(powered.Properties, "MultiblockType", out currentBlockType))
                        {
                            break;
                        }

                        if (!blockData.Value.Equals(currentBlockType))
                        {
                            break;
                        }

                        poweredSlaveCoords.AddIfNew(blockPosition);
                        continue;
                    }

                    string slaveType = te.GetSlaveType();
                    if (slaveType == null || slaveType != blockNameType)
                    {
                        break;
                    }
                    slaveBlocks.Add(te);
                }

                if ((slaveBlocks.Count + poweredSlaveCoords.Count) < multiblockStructure.Count)
                {
                    continue;
                }

                this.activeSlaveBlocks = slaveBlocks;
                this.poweredSlaveCoords = poweredSlaveCoords;
                foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
                {
                    slaveBlock.SetMaster(this);
                }

                this.SetMultiblockFormed();
                this.rotationIndex = i;
                this.forceUpdate = false;
                return true;
            }
            this.rotationIndex = -1;
            this.SetMultiblockNotFormed();
            return false;
        }



        /**
         * Clears slave data from the block
         */

        protected override void ClearSlaveData()
        {
            base.ClearSlaveData();
            this.poweredSlaveCoords = new List<Vector3i>();
        }


        /**
         * Loads data from save file and reads into the tile entity.
         */

        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.requiresPower = _br.ReadBoolean();
            this.hasPower = _br.ReadBoolean();
            int poweredSlavesCount = _br.ReadInt32();
            for (int i = 0; i < poweredSlavesCount; i += 1)
            {
                this.poweredSlaveCoords.AddIfNew(StringHelpers.WriteStringToVector3i(_br.ReadString()));
            }

        }


        /**
         * Writes out the required data to the tile entity.
         */

        public override void write(PooledBinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
        {
            base.write(_bw, _eStreamMode);
            _bw.Write(this.requiresPower);
            _bw.Write(this.hasPower);
            _bw.Write(this.poweredSlaveCoords.Count);
            foreach (Vector3i coord in this.poweredSlaveCoords)
            {
                _bw.Write(StringHelpers.WriteVector3iToString(coord));
            }
        }


        /**
         * Sets required power.
         */

        public void SetRequirePower(bool requirePower)
        {
            this.requiresPower = requirePower;
        }

        /**
         * Returns WorkstationPowered as a tile entity type.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstationPowered];
        }


        /*
        private const float cFuelBurnPerTick = 1f;
        private ItemStack[] fuel;
        private ItemStack[] input;
        private ItemStack[] tools;
        private ItemStack[] output;
        private RecipeQueueItem[] queue;
        private ulong lastTickTime;
        private int fuelInStorageInTicks;
        private float currentBurnTimeLeft;
        private float[] currentMeltTimesLeft;
        private ItemStack[] lastInput;
        private bool isBurning;
        private bool isBesideWater;
        private bool isPlayerPlaced;
        private string[] materialNames;
        private bool[] isModuleUsed;
        */
        private int Debug = 1;

        private enum Module
        {
            Tools,
            Input,
            Output,
            Fuel,
            Material_Input,
            Count
        }

        private int debug
        {
            get
            {
                int current = this.Debug;
                this.Debug -= 1;
                return (current > 0 ? current : 0);
            }
        }

        private int rotationIndex;
        private string masterBlockName;

        // Saved variables that are called on Write() and Read() methods.
        private bool requiresPower;

        private List<ITileEntityMultiblockSlave> activeSlaveBlocks = new List<ITileEntityMultiblockSlave>();
        private List<Vector3i> poweredSlaveCoords = new List<Vector3i>();

        // These store whether the block is powered, heated and has nearby blocks, to reduce amount of calls to check.
        private bool hasPower;

        private int TickCounter;

        public bool UpdateReady
        {
            get { return ((this.TickCounter += 1) % 4) == 0; }
        }

        public bool HasRequiredPower { get { return this.hasPower; } }
    }
}