﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

    public class TileEntityMultiblockMasterWorkstation : TileEntityWorkstation, ITileEntityMultiblockMaster
    {
        public new event XUiEvent_InputStackChanged InputChanged;
        public new event XUiEvent_FuelStackChanged FuelChanged;


        /**
         * Constructor
         */

        public TileEntityMultiblockMasterWorkstation(Chunk _chunk) : base(_chunk)
        {
            /*
            this.fuel = ItemStack.CreateArray(3);
            this.tools = ItemStack.CreateArray(3);
            this.output = ItemStack.CreateArray(6);
            this.input = ItemStack.CreateArray(3);
            this.lastInput = ItemStack.CreateArray(3);
            this.queue = new RecipeQueueItem[4];
            this.materialNames = new string[0];
            this.isModuleUsed = new bool[5];
            this.currentMeltTimesLeft = new float[this.input.Length];
            */
            this.dataObject = 0;
        }



        /**
         * Loads data from save file and reads into the tile entity.
         * In this case, we only want to read the name, rotational index, and whether it's player placed.
         * Once loaded, the Update method will handle the rest by calling the master block to recheck the slaves, with the rotational index if needed.
         */

        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.masterBlockName = _br.ReadString();
            this.rotationIndex = _br.ReadInt32();
            this.formed = _br.ReadBoolean();
        }


        /**
         * Writes out the required data to the tile entity.
         * We only need save the name, whether it's player placed and its rotation index, since loading the data again will trigger the update causing the slave blocks
         * and other data to repopulate without having to store it in a file.
         */

        
        public override void write(PooledBinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
        {
            base.write(_bw, _eStreamMode);
            _bw.Write(this.masterBlockName);
            _bw.Write(this.rotationIndex);
            _bw.Write(this.formed);
        }
        

        /**
         * Returns MultiblockMasterWorkstation as a tile entity type.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.MBMstrWorkstation];
        }


        /**
         * If the block is removed we want to clear out all slave data present.
         */

        public override void OnRemove(World world)
        {
            this.ClearSlaveData();
            base.OnRemove(world);
        }


        /**
         * Triggers an update (so slave blocks can call it)
         */

        public void TriggerUpdate()
        {
            this.setModified();
        }


        /**
         * Updates the master block and finds whether the structure is valid or not.
         */

        public virtual bool UpdateMaster(World world, bool checkRotationIndex = false)
        {
            // If master block not found already, attempt to get it.
            if (this.masterBlockName == null)
            {
                Vector3i worldPos = this.ToWorldPos();
                Block block = world.GetBlock(worldPos).Block;
                if (block == null)
                {
                    return false;
                }

                this.masterBlockName = block.GetBlockName();
            }

            // Need to check each rotation in turn to make sure that the structure is formed.
            for (int i = 0; i < 4; i += 1)
            {
                if (checkRotationIndex)
                {
                    if (this.rotationIndex == -1)
                    {
                        UpdateMaster(world);
                        return false;
                    }

                    if (this.rotationIndex != i)
                    {
                        continue;
                    }
                }

                Dictionary<Vector3i, string> multiblockStructure = MultiblockManager.GetCoordinatesInWorldSpace(this.masterBlockName, this.ToWorldPos(), i);
                List<ITileEntityMultiblockSlave> slaveBlocks = new List<ITileEntityMultiblockSlave>();
                foreach (KeyValuePair<Vector3i, string> blockData in multiblockStructure)
                {
                    Vector3i blockPosition = blockData.Key;
                    string blockNameType = blockData.Value;
                    Block block = world.GetBlock(blockPosition).Block;
                    if (!block.HasTileEntity)
                    {
                        break;
                    }
                    Chunk chunk = world.GetChunkFromWorldPos(blockPosition) as Chunk;
                    if (chunk == null)
                    {
                        return false;
                    }

                    TileEntity tileEntity = chunk.GetTileEntity(Chunk.ToLocalPosition(blockPosition));

                    ITileEntityMultiblockSlave te = chunk.GetTileEntity(Chunk.ToLocalPosition(blockPosition)) as ITileEntityMultiblockSlave;
                    if (tileEntity == null)
                    {
                        break;
                    }

                    string slaveType = te.GetSlaveType();
                    if (slaveType != null && slaveType == blockNameType)
                    {
                        slaveBlocks.Add(tileEntity as ITileEntityMultiblockSlave);
                        continue;
                    }
                }

                if (slaveBlocks.Count < multiblockStructure.Count)
                {
                    continue;
                }

                this.activeSlaveBlocks = slaveBlocks;
                foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
                {
                    slaveBlock.SetMaster(this);
                }

                this.SetMultiblockFormed();
                this.rotationIndex = i;
                this.forceUpdate = false;
                return true;
            }
            this.rotationIndex = -1;
            this.SetMultiblockNotFormed();
            return false;
        }


        /**
         * Returns whether the multiblock structure is valid or not.
         */

        public bool MultiblockFormed()
        {
            return this.formed;
        }


        /**
         * Tells this tile entity that the multiblock structure is valid.
         */

        public bool SetMultiblockFormed()
        {
            this.formed = true;
            this.setModified();
            return this.formed;
        }


        /**
         * Tells the tile entity that the multiblock structure is not valid.
         */

        public bool SetMultiblockNotFormed()
        {
            this.formed = false;
            this.setModified();
            return this.formed;
        }


        /**
         * Sets the name of the master multiblock.
         */

        public void SetMasterBlockName(string name)
        {
            this.masterBlockName = name;
            //this.setModified();
        }


        /**
         * Gets the active slave blkocks.
         */

        public List<ITileEntityMultiblockSlave> GetSlaveBlocks()
        {
            if (this.activeSlaveBlocks == null)
            {
                return null;
            }
            return this.activeSlaveBlocks;
        }


        /**
         * Removes the connection to slave blocks.
         */

        protected virtual void ClearSlaveData()
        {
            if (this.activeSlaveBlocks == null)
            {
                return;
            }

            foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
            {
                slaveBlock.UnbindMaster();
            }
            this.activeSlaveBlocks = null;
            this.setModified();
        }


        /*
        private const float cFuelBurnPerTick = 1f;
        private ItemStack[] fuel;
        private ItemStack[] input;
        private ItemStack[] tools;
        private ItemStack[] output;
        private RecipeQueueItem[] queue;
        private ulong lastTickTime;
        private int fuelInStorageInTicks;
        private float currentBurnTimeLeft;
        private float[] currentMeltTimesLeft;
        private ItemStack[] lastInput;
        private bool isBurning;
        private bool isBesideWater;
        private bool isPlayerPlaced;
        private string[] materialNames;
        private bool[] isModuleUsed;
        */
        protected bool forceUpdate;
        protected bool formed;
        private bool valid;
        private int rotationIndex;
        private string masterBlockName;
        private int TickCounter;
        private List<ITileEntityMultiblockSlave> activeSlaveBlocks = new List<ITileEntityMultiblockSlave>();
        private int Debug = 1;
        
        public bool UpdateReady
        {
            get { return ((this.TickCounter += 1) % 4) == 0; }
        }

        public bool ForceUpdate { get { return this.ForceUpdate; } }

        private int debug
        {
            get
            {
                int current = this.Debug;
                this.Debug -= 1;
                return (current > 0 ? current : 0);
            }
        }

        private enum Module
        {
            Tools,
            Input,
            Output,
            Fuel,
            Material_Input,
            Count
        }
    }
}