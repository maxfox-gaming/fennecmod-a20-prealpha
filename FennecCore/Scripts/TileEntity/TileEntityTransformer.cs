﻿using System;
using System.Collections.Generic;

namespace FennecCore.Scripts
{

    /**
     *  Tile entity class for block transformer. Takes input items and changes them into output items. 
     */

    public class TileEntityTransformer : TileEntityLootContainer, ITileEntityReceivePower
    {
        /**
         * Loads the tile entity data when instantiated from placing a block.
         */

        public TileEntityTransformer(Chunk _chunk) : base(_chunk)
        {
            this.tQueue = new TransformationQueue();
            this.useHash = true;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.hasNearbyEntities = false;
            this.userAccessing = false;
            this.entitiesNearby = 0;
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Loads the tile entity by copying another of its kind.
         */

        private TileEntityTransformer(TileEntityTransformer _other) : base(null)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.collection = _other.collection;
            this.tQueue = _other.tQueue;
            this.useHash = _other.useHash;
            this.requiresPower = _other.requiresPower;
            this.requiresHeat = _other.requiresHeat;
            this.powerSources = _other.powerSources;
            this.heatSources = _other.heatSources;
            this.requiresNearbyBlocks = _other.requiresNearbyBlocks;
            this.nearbyBlockNames = _other.nearbyBlockNames;
            this.nearbyBlockTags = _other.nearbyBlockTags;
            this.nearbyBlockRequireAllTags = _other.nearbyBlockRequireAllTags;
            this.nearbyBlockRange = _other.nearbyBlockRange;
            this.nearbyBlocksNeeded = _other.nearbyBlocksNeeded;
            this.requiresNearbyEntities = _other.requiresNearbyEntities;
            this.nearbyEntityNames = _other.nearbyEntityNames;
            this.nearbyEntityRange = _other.nearbyEntityRange;
            this.nearbyEntitiesNeeded = _other.nearbyEntitiesNeeded;
            this.useSpeedup = _other.useSpeedup;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.hasNearbyEntities = false;
            this.userAccessing = false;
            this.entitiesNearby = 0;
            this.CalculateLookupCoordinates();
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Sets the above method public
         */

        public new TileEntityTransformer Clone()
        {
            return new TileEntityTransformer(this);
        }


        /**
         * Copies properties from another tile entity of this type into this one.
         */

        public void CopyLootContainerDataFromOther(TileEntityTransformer _other)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.collection = _other.collection;
            this.tQueue = _other.tQueue;
            this.useHash = _other.useHash;
            this.requiresPower = _other.requiresPower;
            this.requiresHeat = _other.requiresHeat;
            this.powerSources = _other.powerSources;
            this.heatSources = _other.heatSources;
            this.requiresNearbyBlocks = _other.requiresNearbyBlocks;
            this.nearbyBlockNames = _other.nearbyBlockNames;
            this.nearbyBlockTags = _other.nearbyBlockTags;
            this.nearbyBlockRequireAllTags = _other.nearbyBlockRequireAllTags;
            this.nearbyBlockRange = _other.nearbyBlockRange;
            this.nearbyBlocksNeeded = _other.nearbyBlocksNeeded;
            this.requiresNearbyEntities = _other.requiresNearbyEntities;
            this.nearbyEntityNames = _other.nearbyEntityNames;
            this.nearbyEntityRange = _other.nearbyEntityRange;
            this.nearbyEntitiesNeeded = _other.nearbyEntitiesNeeded;
            this.useSpeedup = _other.useSpeedup;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.hasNearbyEntities = false;
            this.userAccessing = false;
            this.entitiesNearby = 0;
            this.CalculateLookupCoordinates();
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Each game tick, this method is called.
         */

        public override void UpdateTick(World world)
        {
            if (!this.UpdateCanHappen(world))
            {
                return;
            }

            this.HandleAddingToQueue(world);
            this.HandleProcessingQueue();
        }


        /**
         * Checks that an update can happen.
         */

        public virtual bool UpdateCanHappen(World world)
        {
            return (this.IsPowered(world) && this.IsHeated(world) && this.HasNearbyBlocks(world) && this.HasNearbyEntities(world) && (this.tQueue.QueueHasJobs() | !this.IsEmpty()));
        }


        /**
         * Checks whether block is crafting or not.
         */

        public virtual bool IsCrafting()
        {
            return this.tQueue.QueueHasJobs();
        }


        /**
         * Checks if block is powered. It is required to be next to a TileEntityPowered type of block in any cardinal direction.
         */

        public virtual bool IsPowered(World world)
        {
            if (!this.CanTick())
            {
                return this.hasPower;
            }

            if (!this.requiresPower)
            {
                return this.hasPower = true;
            }

            if (this.poweredBlockCoords.Count == 0)
            {
                return this.hasPower = false;
            }

            Dictionary<Vector3i, TileEntity> tileEntitiesNearby = CoordinateHelper.GetTileEntitiesInCoordinatesWithType(world, this.poweredBlockCoords, TileEntityType.Powered);

            if (tileEntitiesNearby.Count == 0)
            {
                return this.hasPower = false;
            }

            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesNearby)
            {
                TileEntityPowered otherTileEntity = entry.Value as TileEntityPowered;
                Vector3i otherTileEntityPos = entry.Key;

                foreach (string powerSource in this.powerSources)
                {
                    string name = CoordinateHelper.GetBlockNameAtCoordinate(world, otherTileEntityPos);
                    if (!CoordinateHelper.BlockAtCoordinateIs(world, otherTileEntityPos, powerSource))
                    {
                        continue;
                    }

                    if (otherTileEntity.IsPowered)
                    {
                        return this.hasPower = true;
                    }
                }
            }
            return this.hasPower = false;
        }


        /**
         * Returns true if a heat source is under the block
         */

        public virtual bool IsHeated(World world)
        {
            if (!this.CanTick())
            {
                return this.hasHeat;
            }

            if (!this.requiresHeat)
            {
                return this.hasHeat = true;
            }

            // If TE is on very bottom of world this will prevent out of boundary shenanigans.
            if (this.ToWorldPos() == this.heatedBlockCoords[0])
            {
                return this.hasHeat = false;
            }

            Dictionary<Vector3i, TileEntity> tileEntitiesUnderneath = CoordinateHelper.GetTileEntitiesInCoordinatesWithType(world, this.heatedBlockCoords, TileEntityType.Workstation);

            if (tileEntitiesUnderneath.Count == 0)
            {
                return this.hasHeat = false;
            }

            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesUnderneath)
            {
                TileEntityWorkstation otherTileEntity = entry.Value as TileEntityWorkstation;
                Vector3i otherTileEntityPos = entry.Key;

                foreach (string heatSource in this.heatSources)
                {
                    if (!CoordinateHelper.BlockAtCoordinateIs(world, otherTileEntityPos, heatSource))
                    {
                        continue;
                    }

                    // Checks that there is fuel being used for the workstation as well as if it's burning.
                    foreach (ItemStack itemStack in otherTileEntity.Fuel)
                    {
                        if (itemStack != null & !itemStack.Equals(ItemStack.Empty.Clone()))
                        {
                            if (otherTileEntity.IsActive(world))
                            {
                                return this.hasHeat = true;
                            }
                        }
                    }
                }
            }
            return this.hasHeat = false;
        }


        /**
         * Checks whether nearby blocks are needed.
         */

        public virtual bool HasNearbyBlocks(World world)
        {
            if (!this.CanTick())
            {
                return this.hasNearbyBlocks;
            }

            if (!this.requiresNearbyBlocks)
            {
                return this.hasNearbyBlocks = true;
            }

            bool namesFound = false;
            bool tagsFound = false;

            if (this.nearbyBlockNames.Count > 0)
            {
                if (nearbyBlockNames.Count == 1)
                {
                    if (this.nearbyBlockNames[0] == "")
                    {
                        namesFound = true;
                    }
                }
                if (!namesFound)
                {
                    namesFound = CoordinateHelper.EnoughBlocksInCoordinatesThatAre(world, this.nearbyBlockCoords, this.nearbyBlockNames, this.nearbyBlocksNeeded);
                }
            }
            else
            {
                namesFound = true;
            }

            if (this.nearbyBlockTags.Count > 0)
            {
                if (nearbyBlockTags.Count == 1)
                {
                    if (this.nearbyBlockTags[0] == "")
                    {
                        tagsFound = true;
                    }
                }
                if (!tagsFound)
                {
                    tagsFound = CoordinateHelper.EnoughBlocksInCoordinatesThatHaveTags(world, this.nearbyBlockCoords, this.nearbyBlockTags, this.nearbyBlocksNeeded, this.nearbyBlockRequireAllTags);
                }
            }
            else
            {
                tagsFound = true;
            }

            return this.hasNearbyBlocks = (tagsFound & namesFound);
        }


        /**
         * Checks whether we have entities nearby.
         */

        public virtual bool HasNearbyEntities(World world)
        {
            if (!this.CanTick())
            {
                return this.hasNearbyEntities;
            }

            if (!this.requiresNearbyEntities)
            {
                return this.hasNearbyEntities = true;
            }

            return this.hasNearbyEntities = CoordinateHelper.EnoughEntitiesInBounds(world, this.ToWorldPos(), this.nearbyEntityRange, this.nearbyEntityNames, this.nearbyEntitiesNeeded, out this.entitiesNearby);
        }



        /**
         * Checks whether the user is accessing the block
         */

        public bool HasUserAccessing()
        {
            if (!this.CanTick())
            {
                return this.userAccessing;
            }

            if (!this.requiresUserAccess)
            {
                return this.userAccessing = true;
            }

            return this.userAccessing = bUserAccessing;
        }


        /**
         * Checks whether the game object can tick or not. Random value is also applied so that multiple tile entities won't all try and tick at once causing tons of lag.
         */

        public bool CanTick()
        {
            return ((GameManager.Instance.World.GetWorldTime() + (ulong)this.random) % 4 == 0);
        }


        /**
         * Deals with inserting items into the queue and removing them from the block inventory.
         */

        public void HandleAddingToQueue(World world)
        {
            foreach (TransformationData tData in this.collection.collection)
            {
                TransformationData modifiedData = tData.Clone();
                if (!modifiedData.HasNeededEntitiesNearby(world, this.ToWorldPos()))
                {
                    continue;
                }

                modifiedData.transformationTime = GetModifiedTime(tData.transformationTime);

                ulong transformationTime;
                List<Tuple<int, ItemStack>> locations;
                TransformationJob job;
                if (!this.QueueReadyItems(modifiedData, out transformationTime, out locations, out job))
                {
                    continue;
                }

                if (job == null)
                {
                    continue;
                }

                ItemStack[] previousState;
                if (!this.StoreReadyItems(locations, out previousState))
                {
                    this.RestorePreviousState(previousState);
                    this.tQueue.RemoveEntry(job);
                }
                this.tileEntityChanged();
            }
        }


        /**
         * Retrieves jobs from the queue and processes them.
         */

        protected void HandleProcessingQueue()
        {
            if (!this.tQueue.QueueHasJobs())
            {
                return;
            }

            int jobsProcessed = 0;
            List<TransformationJob> jobs = this.tQueue.GetNextTransformations();
            foreach (TransformationJob job in jobs)
            {
                ItemStack[] previousState;
                bool processed = this.ProcessQueueJob(job, out previousState);
                if (processed)
                {
                    this.tQueue.RemoveEntry(job);
                    jobsProcessed += 1;
                    continue;
                }
                job.MarkJobNotInProgress();
                this.RestorePreviousState(previousState);
            }

            if (jobsProcessed > 0)
            {
                this.tileEntityChanged();
            }
        }


        /**
         * Queues ready transformations.
         */

        protected bool QueueReadyItems(TransformationData tData, out ulong transformationTime, out List<Tuple<int, ItemStack>> stackLocations, out TransformationJob job)
        {
            transformationTime = 0;
            job = null;
            if (!tData.HasAllInputs(InventoryHelper.GetItemStacksFromFilledSlots(this.items), out stackLocations))
            {
                return false;
            }
            return this.AddToQueue(tData, out transformationTime, out job);
        }


        /**
         * Removes each item from the inventory and then returns true if all were successfully removed.
         * Outputs the original state of the container in case of failure for safe reverse.
         */

        protected bool StoreReadyItems(List<Tuple<int, ItemStack>> locations, out ItemStack[] previousState)
        {
            previousState = (ItemStack[])this.items.Clone();

            // If there are no inputs after probability calculation, then congrats, nothing to do, success.
            if (locations.Count == 0)
            {
                return true;
            }

            foreach (Tuple<int, ItemStack> entry in locations)
            {
                if (!InventoryHelper.RemoveItemsInSlot(this.items, entry.Item1, entry.Item2.count))
                {
                    return false;
                }
            }
            return true;
        }


        /**
         * Adds to the queue, giving out the transformation time and the job added.
         */

        protected bool AddToQueue(TransformationData tData, out ulong time, out TransformationJob job)
        {
            time = 0;
            job = null;
            bool added = this.tQueue.Add(tData, out time, out job);
            return added;
        }


        /**
         * Processes a queue job.
         */

        protected bool ProcessQueueJob(TransformationJob job, out ItemStack[] previousState)
        {
            previousState = (ItemStack[])this.items.Clone();
            List<ItemStack> outputs = job.GetTransformationData().GetAllOutputsAfterProbabilityCalculation();
            List<int> assignedSlots = new List<int>();
            List<Tuple<int, ItemStack>> stacksToAdd = new List<Tuple<int, ItemStack>>();
            bool canAddAll = true;

            // If there are no ouputs after probability calculation, then congrats, nothing to do, success.
            if (outputs.Count == 0)
            {
                return true;
            }

            foreach (ItemStack output in outputs)
            {
                List<Tuple<int, ItemStack>> whereToAdd;
                if (!InventoryHelper.RoomInSlotsFor(this.items, output, ref assignedSlots, out whereToAdd))
                {
                    canAddAll = false;
                    return false;
                }

                stacksToAdd.AddRange(whereToAdd);
            }

            if (canAddAll)
            {
                foreach (Tuple<int, ItemStack> positionAndStack in stacksToAdd)
                {
                    if (!InventoryHelper.TryAddItemToSlot(this.items, positionAndStack.Item1, positionAndStack.Item2))
                    {
                        return false;
                    }
                }
            }

            return canAddAll;
        }


        /**
         * Restores the previous state of the items before removal in case of failure.
         */

        protected void RestorePreviousState(ItemStack[] previousState)
        {
            this.items = ItemStack.Clone(previousState, 0, this.items.Length);
            this.tileEntityChanged();
        }


        /**
         * If speed modifier is enabled, calculate how much faster the speed should be.
         */

        protected double GetModifiedTime(double time)
        {
            if (!useSpeedup)
            {
                return time;
            }

            if (entitiesNearby <= nearbyEntitiesNeeded)
            {
                return time;
            }

            return Math.Pow(nearbyEntitiesNeeded / entitiesNearby, (1 / 3)) * time;
        }



        /**
         * Reads data into the tile entity when loaded.
         */

        public override void read(PooledBinaryReader _br, StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            string collectionString = _br.ReadString();
            this.collection = TransformationCollection.Read(collectionString, useHash);
            string tQueueString = _br.ReadString();
            this.tQueue = tQueue.Read(tQueueString, useHash);
            this.requiresPower = _br.ReadBoolean();
            this.powerSources = StringHelpers.WriteStringToList(_br.ReadString());
            this.requiresHeat = _br.ReadBoolean();
            this.heatSources = StringHelpers.WriteStringToList(_br.ReadString());
            this.requiresNearbyBlocks = _br.ReadBoolean();
            this.nearbyBlockTags = StringHelpers.WriteStringToList(_br.ReadString());
            this.nearbyBlockRequireAllTags = _br.ReadBoolean();
            this.nearbyBlockNames = StringHelpers.WriteStringToList(_br.ReadString());
            this.nearbyBlocksNeeded = _br.ReadInt32();
            this.nearbyBlockRange = StringHelpers.WriteStringToVector3i(_br.ReadString());
            this.requiresNearbyEntities = _br.ReadBoolean();
            this.nearbyEntityNames = StringHelpers.WriteStringToList(_br.ReadString());
            this.nearbyEntitiesNeeded = _br.ReadInt32();
            this.nearbyEntityRange = StringHelpers.WriteStringToVector3i(_br.ReadString());
            this.useSpeedup = _br.ReadBoolean();
            this.requiresUserAccess = _br.ReadBoolean();

            // After read:
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.hasNearbyEntities = false;
            this.userAccessing = false;
            this.entitiesNearby = 0;
            this.CalculateLookupCoordinates();
            this.random = RandomStatic.Range(0, 20);
        }

        /**
         * Saves tile entity data to the strem.
         */

        public override void write(PooledBinaryWriter _bw, StreamModeWrite _eStreamMode)
        {
            base.write(_bw, _eStreamMode);
            try
            {
                _bw.Write(this.collection.Write(useHash));
                string tQ = this.tQueue.Write(useHash);
                _bw.Write(tQ);
                _bw.Write(this.requiresPower);
                _bw.Write(StringHelpers.WriteListToString(this.powerSources));
                _bw.Write(this.requiresHeat);
                _bw.Write(StringHelpers.WriteListToString(this.heatSources));
                _bw.Write(this.requiresNearbyBlocks);
                _bw.Write(StringHelpers.WriteListToString(this.nearbyBlockTags));
                _bw.Write(this.nearbyBlockRequireAllTags);
                _bw.Write(StringHelpers.WriteListToString(this.nearbyBlockNames));
                _bw.Write(this.nearbyBlocksNeeded);
                _bw.Write(StringHelpers.WriteVector3iToString(this.nearbyBlockRange));
                _bw.Write(this.requiresNearbyEntities);
                _bw.Write(StringHelpers.WriteListToString(this.nearbyEntityNames));
                _bw.Write(this.nearbyEntitiesNeeded);
                _bw.Write(StringHelpers.WriteVector3iToString(this.nearbyEntityRange));
                _bw.Write(this.useSpeedup);
                _bw.Write(this.requiresUserAccess);
            }
            catch (Exception ex)
            {
                Log.Warning("Error when writing transformation collection.");
                BlockTransformer block = this.blockValue.Block as BlockTransformer;
                if (block == null)
                {
                    block = GameManager.Instance.World.GetBlock(this.ToWorldPos()).Block as BlockTransformer;
                }
                if (block == null)
                {
                    throw new Exception("Cannot retrieve block data for tile entity at pos " + this.ToWorldPos().ToString());
                }
                Log.Out("Retrieved block: " + block.GetBlockName());
                TransformationPropertyParser parser = block.transformationPropertyParser;
                _bw.Write(parser.collection.Write(useHash));
                _bw.Write(this.tQueue.Write(useHash));
                _bw.Write(parser.requirePower);
                _bw.Write(StringHelpers.WriteListToString(parser.powerSources));
                _bw.Write(parser.requireHeat);
                _bw.Write(StringHelpers.WriteListToString(parser.heatSources));
                _bw.Write(parser.requireNearbyBlocks);
                _bw.Write(StringHelpers.WriteListToString(parser.nearbyBlockTags));
                _bw.Write(parser.requireAllTags);
                _bw.Write(StringHelpers.WriteListToString(parser.nearbyBlockNames));
                _bw.Write(parser.nearbyBlockCount);
                _bw.Write(StringHelpers.WriteVector3iToString(parser.nearbyBlockRange));
                _bw.Write(parser.requireNearbyEntities);
                _bw.Write(StringHelpers.WriteListToString(parser.nearbyEntityNames));
                _bw.Write(parser.nearbyEntityCount);
                _bw.Write(StringHelpers.WriteVector3iToString(parser.nearbyEntityRange));
                _bw.Write(parser.nearbyEntitySpeedUp);
                _bw.Write(parser.requireUserAccess);
            }
        }


        /**
         * What happens when the block is upgraded or downgraded.
         */

        public void UpgradeDowngradeFrom(TileEntityTransformer _other)
        {
            base.UpgradeDowngradeFrom(_other);
            this.OnDestroy();
            if (_other is TileEntityTransformer)
            {
                TileEntityTransformer tileEntityBlockTransformer = _other as TileEntityTransformer;
                this.bTouched = tileEntityBlockTransformer.bTouched;
                this.worldTimeTouched = tileEntityBlockTransformer.worldTimeTouched;
                this.bPlayerBackpack = tileEntityBlockTransformer.bPlayerBackpack;
                this.bPlayerStorage = tileEntityBlockTransformer.bPlayerStorage;
                this.items = ItemStack.Clone(tileEntityBlockTransformer.itemsArr, 0, this.containerSize.x * this.containerSize.y);
                if (this.items.Length != this.containerSize.x * this.containerSize.y)
                {
                    Log.Error("Error upgrading.");
                }
            }
        }


        /**
         * What happens when the tile entities change state.
         */

        private void tileEntityChanged()
        {
            for (int i = 0; i < this.listeners.Count; i++)
            {
                this.listeners[i].OnTileEntityChanged(this, 0);
            }
        }


        /**
         * Returns the tile entity enum type for comparison.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.BlockTransformer];
        }


        /**
         * Sets the collection variable to hold details of what items transform into what other ones.
         */

        public void SetTransformationCollection(TransformationCollection collection)
        {
            this.collection = collection;
        }


        /**
         * Sets required power.
         */

        public void SetRequirePower(bool requirePower, List<string> powerSourceBlocks)
        {
            this.requiresPower = requirePower;
            this.powerSources = powerSourceBlocks;
        }


        /**
         * Sets required heat.
         */

        public void SetRequireHeat(bool requireHeat, List<string> heatSourceBlocks)
        {
            this.requiresHeat = requireHeat;
            this.heatSources = heatSourceBlocks;
        }


        /**
         * Sets the nearby block properties.
         */

        public void SetRequireNearbyBlocks(bool requireNearbyBlocks, List<string> nearbyBlockNames, List<string> nearbyBlockTags, bool nearbyBlockRequireAllTags, Vector3i nearbyBlockRange, int nearbyBlocksNeeded)
        {
            this.requiresNearbyBlocks = requireNearbyBlocks;
            this.nearbyBlockNames = nearbyBlockNames;
            this.nearbyBlockTags = nearbyBlockTags;
            this.nearbyBlockRequireAllTags = nearbyBlockRequireAllTags;
            this.nearbyBlockRange = nearbyBlockRange;
            this.nearbyBlocksNeeded = nearbyBlocksNeeded;
        }


        /**
         * Sets the nearby entity properties.
         */

        public void SetRequireNearbyEntities(bool requireNearbyEntities, List<string> nearbyEntityNames, Vector3i nearbyEntityRange, int nearbyEntitiesNeeded, bool nearbyEntitySpeedUp)
        {
            this.requiresNearbyEntities = requireNearbyEntities;
            this.nearbyEntityNames = nearbyEntityNames;
            this.nearbyEntityRange = nearbyEntityRange;
            this.nearbyEntitiesNeeded = nearbyEntitiesNeeded;
            this.useSpeedup = nearbyEntitySpeedUp;
        }


        /**
         * Sets whether the block requires the user to be accessing it.
         */

        public void SetRequireUserAccess(bool requireUserAccess)
        {
            this.requiresUserAccess = requireUserAccess;
        }


        /**
         * Calculates the lookup coordinates.
         */

        public void CalculateLookupCoordinates()
        {
            this.poweredBlockCoords = new List<Vector3i>();
            this.heatedBlockCoords = new List<Vector3i>();
            this.nearbyBlockCoords = new List<Vector3i>();
            this.nearbyEntityCoords = new List<Vector3i>();

            World world = GameManager.Instance.World;
            Vector3i tileEntityPos = this.ToWorldPos();

            this.poweredBlockCoords = CoordinateHelper.GetCoordinatesAround(tileEntityPos, true, 1, 1, 1);
            this.heatedBlockCoords.Add(CoordinateHelper.GetCoordinateBelow(tileEntityPos));
            this.nearbyBlockCoords = CoordinateHelper.GetCoordinatesAround(tileEntityPos, this.nearbyBlockRange);
            this.nearbyEntityCoords = CoordinateHelper.GetCoordinatesAround(tileEntityPos, this.nearbyEntityRange);
        }



        // Saved variables that are called on Write() and Read() methods.
        private bool useHash;
        private bool bDisableModifiedCheck;
        private bool requiresPower;
        private bool requiresHeat;
        private bool requiresNearbyBlocks;
        private bool requiresNearbyEntities;
        private bool requiresUserAccess;
        private bool useSpeedup;
        private List<string> powerSources;
        private List<string> heatSources;
        private List<string> nearbyBlockNames;
        private List<string> nearbyBlockTags;
        private List<string> nearbyEntityNames;
        private bool nearbyBlockRequireAllTags;
        private Vector3i nearbyBlockRange;
        private Vector3i nearbyEntityRange;
        private int nearbyBlocksNeeded;
        private int nearbyEntitiesNeeded;
        private Vector2i containerSize;
        private ItemStack[] itemsArr;
        private List<Entity> entList;
        private TransformationCollection collection;
        protected TransformationQueue tQueue;
        private int entitiesNearby;

        // These are calculated on reading and writing to save extra work.
        private List<Vector3i> poweredBlockCoords;
        private List<Vector3i> heatedBlockCoords;
        private List<Vector3i> nearbyBlockCoords;
        private List<Vector3i> nearbyEntityCoords;

        // These store whether the block is powered, heated and has nearby blocks, to reduce amount of calls to check.
        private bool hasPower;
        private bool hasHeat;
        private bool hasNearbyBlocks;
        private bool hasNearbyEntities;
        private bool userAccessing;
        private int random;

        // Fields to recall without further calculation
        public bool HasRequiredPower { get { return this.hasPower; } }
        public bool HasRequiredHeat { get { return this.hasHeat; } }
        public bool HasRequiredNearbyBlocks { get { return this.hasNearbyBlocks; } }
        public bool HasRequiredNearbyEntities { get { return this.hasNearbyEntities; } }
        public bool HasPlayerAccessing { get { return this.userAccessing; } }
        public bool IsCurrentlyWorking { get { return this.tQueue.QueueHasJobs(); } }
    }
}