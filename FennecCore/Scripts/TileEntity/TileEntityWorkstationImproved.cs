﻿using UnityEngine;

namespace FennecCore.Scripts
{
	// DEPRECATED - Will most likely be obsolete in future patch.
	public class TileEntityWorkstationImproved : TileEntityWorkstation
	{
		public new event XUiEvent_InputStackChanged InputChanged;
		public new event XUiEvent_FuelStackChanged FuelChanged;


		public new string[] MaterialNames
		{
			get
			{
				return this.materialNames;
			}
		}

		public new ItemStack[] Tools
		{
			get
			{
				return this.tools;
			}
			set
			{
				this.tools = ItemStack.Clone(value);
				this.setModified();
			}
		}

		public new ItemStack[] Fuel
		{
			get
			{
				return this.fuel;
			}
			set
			{
				this.fuel = ItemStack.Clone(value);
				this.setModified();
			}
		}

		public new ItemStack[] Input
		{
			get
			{
				return this.input;
			}
			set
			{
				this.input = ItemStack.Clone(value);
				this.setModified();
			}
		}

		public new ItemStack[] Output
		{
			get
			{
				return this.output;
			}
			set
			{
				this.output = ItemStack.Clone(value);
				this.setModified();
			}
		}

		public new RecipeQueueItem[] Queue
		{
			get
			{
				return this.queue;
			}
			set
			{
				this.queue = value;
				this.setModified();
			}
		}

		public new bool IsBurning
		{
			get
			{
				return this.isBurning;
			}
			set
			{
				this.isBurning = value;
				this.setModified();
			}
		}

		public new bool IsPlayerPlaced
		{
			get
			{
				return this.isPlayerPlaced;
			}
			set
			{
				this.isPlayerPlaced = value;
				this.setModified();
			}
		}

		public new bool IsBesideWater
		{
			get
			{
				return this.isBesideWater;
			}
		}

		public new float BurnTimeLeft
		{
			get
			{
				return this.currentBurnTimeLeft;
			}
		}

		public new float BurnTotalTimeLeft
		{
			get
			{
				return this.getTotalFuelSeconds() + this.currentBurnTimeLeft;
			}
		}

		public new int InputSlotCount
		{
			get
			{
				return 3;
			}
		}



		/**
		 * Constructor - does the same as parent so nothing new to add
		 */

		public TileEntityWorkstationImproved(Chunk _chunk) : base(_chunk)
		{
			this.fuel = ItemStack.CreateArray(3);
			this.tools = ItemStack.CreateArray(3);
			this.output = ItemStack.CreateArray(6);
			this.input = ItemStack.CreateArray(3);
			this.lastInput = ItemStack.CreateArray(3);
			this.queue = new RecipeQueueItem[4];
			this.materialNames = new string[0];
			this.isModuleUsed = new bool[5];
			this.currentMeltTimesLeft = new float[this.input.Length];
			this.dataObject = 0;
			this.processByproduct = false;
			this.fuelItem = ItemValue.None;
		}


		/**
		 * Resets the tick time;
		 */

		public new void ResetTickTime()
		{
			this.lastTickTime = GameTimer.Instance.ticks;
		}


		/**
		 * Can the workstation run.
		 */

		public new bool CanOperate(ulong _worldTimeInTicks)
		{
			return this.isBurning;
		}


		/**
		 * Is the workstation active.
		 */

		public override bool IsActive(World world)
		{
			return this.IsBurning;
		}


		/**
		 * When setting chunk, add UI elements.
		 */

		public override void OnSetLocalChunkPosition()
		{
			if (base.localChunkPos == Vector3i.zero)
			{
				return;
			}
			BlockValue block = this.chunk.GetBlock(World.toBlockXZ(base.localChunkPos.x), base.localChunkPos.y, World.toBlockXZ(base.localChunkPos.z));
			Block block2 = Block.list[block.type];
			if (block2.Properties.Values.ContainsKey("Workstation.InputMaterials"))
			{
				string text = block2.Properties.Values["Workstation.InputMaterials"];
				if (text.Contains(","))
				{
					this.materialNames = text.Replace(" ", "").Split(new char[]
					{
					','
					});
				}
				else
				{
					this.materialNames = new string[]
					{
					text
					};
				}

				if (this.input.Length != 3 + this.materialNames.Length)
				{
					ItemStack[] array = new ItemStack[3 + this.materialNames.Length];
					for (int i = 0; i < this.input.Length; i++)
					{
						array[i] = this.input[i].Clone();
					}
					this.input = array;
					for (int j = 0; j < this.materialNames.Length; j++)
					{
						ItemClass itemClass = ItemClass.GetItemClass("unit_" + this.materialNames[j], false);
						if (itemClass != null)
						{
							int num = j + 3;
							this.input[num] = new ItemStack(new ItemValue(itemClass.Id, false), 0);
						}
					}
				}
			}

			if (block2.Properties.Values.ContainsKey("Workstation.Modules"))
			{
				string[] array2 = new string[0];
				string text2 = block2.Properties.Values["Workstation.Modules"];
				if (text2.Contains(","))
				{
					array2 = text2.Replace(" ", "").Split(new char[]
					{
					','
					});
				}
				else
				{
					array2 = new string[]
					{
					text2
					};
				}
				for (int k = 0; k < array2.Length; k++)
				{
					Log.Out("Checking " + array2[k]);
					TileEntityWorkstationImproved.Module module = EnumUtils.Parse<TileEntityWorkstationImproved.Module>(array2[k], true);
					Log.Out("Module int: " + ((int)module).ToString());
					this.isModuleUsed[(int)module] = true;
					Log.Out("Module is set to true.");
				}
				if (this.isModuleUsed[4])
				{
					this.isModuleUsed[1] = true;
				}
			}
		}


		/**
		 * Gets timer for the item slot.
		 */

		public new float GetTimerForSlot(int inputSlot)
		{
			if (inputSlot >= this.currentMeltTimesLeft.Length)
			{
				return 0f;
			}
			return this.currentMeltTimesLeft[inputSlot];
		}


		/**
		 * Updates light state when workstation operating.
		 */

		private void updateLightState(World world)
		{
			BlockValue block = world.GetBlock(base.ToWorldPos());
			if (!this.CanOperate(GameTimer.Instance.ticks) && block.meta != 0)
			{
				block.meta = 0;
				world.SetBlockRPC(base.ToWorldPos(), block);
				return;
			}
			if (this.CanOperate(GameTimer.Instance.ticks) && block.meta != 15)
			{
				block.meta = 15;
				world.SetBlockRPC(base.ToWorldPos(), block);
			}
		}


		/**
		 * Returns the tile entity enum type for comparison.
		 */

		public override TileEntityType GetTileEntityType()
		{
			return TileEntityMapping.Types[TileEntityMapping.WorkstationImproved];
		}


		/**
		 * Updates the workstation data.
		 */

		public override void UpdateTick(World world)
		{
			float num = (GameTimer.Instance.ticks - this.lastTickTime) / 20f;
			float num2 = Mathf.Min(num, this.BurnTotalTimeLeft);
			float timePassed = this.isModuleUsed[3] ? num2 : num;
			this.checkIfByWater(world, base.ToWorldPos());
			this.updateLightState(world);
			this.HandleFuel(world, timePassed);
			this.HandleRecipeQueue(timePassed);
			this.HandleMaterialInput(timePassed);
			if (this.isModuleUsed[3])
			{
				this.isBurning &= (this.BurnTotalTimeLeft > 0f);
			}
			this.lastTickTime = GameTimer.Instance.ticks;
			this.setModified();
		}


		/**
		 * Handles materials going into the workstation.
		 */

		private void HandleMaterialInput(float timePassed)
		{
			if (this.isModuleUsed[4] && (this.isBurning || !this.isModuleUsed[3]))
			{
				for (int i = 0; i < this.input.Length - this.materialNames.Length; i++)
				{
					if (this.input[i].IsEmpty())
					{
						this.input[i].Clear();
						this.currentMeltTimesLeft[i] = -2.14748365E+09f;
						if (this.InputChanged != null)
						{
							this.InputChanged();
						}
					}
					else
					{
						ItemClass forId = ItemClass.GetForId(this.input[i].itemValue.type);
						if (forId != null)
						{
							if (this.currentMeltTimesLeft[i] >= 0f && this.input[i].count > 0)
							{
								if (this.lastInput[i].itemValue.type != this.input[i].itemValue.type)
								{
									this.currentMeltTimesLeft[i] = -2.14748365E+09f;
								}
								else
								{
									this.currentMeltTimesLeft[i] -= timePassed;
								}
							}
							if (this.currentMeltTimesLeft[i] == -2.14748365E+09f && this.input[i].count > 0)
							{
								for (int j = 0; j < this.materialNames.Length; j++)
								{
									if (forId.MadeOfMaterial.ForgeCategory != null && forId.MadeOfMaterial.ForgeCategory.EqualsCaseInsensitive(this.materialNames[j]))
									{
										ItemClass itemClass = ItemClass.GetItemClass("unit_" + this.materialNames[j], false);
										if (itemClass != null && itemClass.MadeOfMaterial.ForgeCategory != null)
										{
											float num = (float)forId.GetWeight() * ((forId.MeltTimePerUnit > 0f) ? forId.MeltTimePerUnit : 1f);
											if (this.isModuleUsed[0])
											{
												for (int k = 0; k < this.tools.Length; k++)
												{
													float num2 = 1f;
													this.tools[k].itemValue.ModifyValue(null, null, PassiveEffects.CraftingSmeltTime, ref num, ref num2, FastTags.Parse(forId.Name), true);
													num *= num2;
												}
											}
											if (num > 0f && this.currentMeltTimesLeft[i] == -2.14748365E+09f)
											{
												this.currentMeltTimesLeft[i] = num;
											}
											else
											{
												this.currentMeltTimesLeft[i] += num;
											}
										}
									}
								}
								this.lastInput[i] = this.input[i].Clone();
							}
							if (this.currentMeltTimesLeft[i] != -2.14748365E+09f)
							{
								int num3 = 0;
								int num4 = 3;
								while (num4 < this.input.Length & num3 < this.materialNames.Length)
								{
									if (forId.MadeOfMaterial.ForgeCategory != null && forId.MadeOfMaterial.ForgeCategory.EqualsCaseInsensitive(this.materialNames[num3]))
									{
										ItemClass itemClass2 = ItemClass.GetItemClass("unit_" + this.materialNames[num3], false);
										if (itemClass2 != null && itemClass2.MadeOfMaterial.ForgeCategory != null)
										{
											if (this.input[num4].itemValue.type == 0)
											{
												this.input[num4] = new ItemStack(new ItemValue(itemClass2.Id, false), this.input[num4].count);
											}
											bool flag = false;
											while (this.currentMeltTimesLeft[i] < 0f && this.currentMeltTimesLeft[i] != -2.14748365E+09f)
											{
												if (this.input[i].count <= 0)
												{
													this.input[i].Clear();
													this.currentMeltTimesLeft[i] = 0f;
													flag = true;
													if (this.InputChanged != null)
													{
														this.InputChanged();
														break;
													}
													break;
												}
												else
												{
													if (this.input[num4].count + forId.GetWeight() > itemClass2.Stacknumber.Value)
													{
														this.currentMeltTimesLeft[i] = -2.14748365E+09f;
														break;
													}
													this.input[num4].count += forId.GetWeight();
													this.input[i].count--;
													float num5 = (float)forId.GetWeight() * ((forId.MeltTimePerUnit > 0f) ? forId.MeltTimePerUnit : 1f);
													if (this.isModuleUsed[0])
													{
														for (int l = 0; l < this.tools.Length; l++)
														{
															if (!this.tools[l].IsEmpty())
															{
																float num6 = 1f;
																this.tools[l].itemValue.ModifyValue(null, null, PassiveEffects.CraftingSmeltTime, ref num5, ref num6, FastTags.Parse(itemClass2.Name), true);
																num5 *= num6;
															}
														}
													}
													this.currentMeltTimesLeft[i] += num5;
													if (this.input[i].count <= 0)
													{
														this.input[i].Clear();
														this.currentMeltTimesLeft[i] = 0f;
														flag = true;
														if (this.InputChanged != null)
														{
															this.InputChanged();
															break;
														}
														break;
													}
													else
													{
														if (this.InputChanged != null)
														{
															this.InputChanged();
														}
														flag = true;
													}
												}
											}
											if (flag && this.currentMeltTimesLeft[i] < 0f && this.currentMeltTimesLeft[i] != -2.14748365E+09f)
											{
												this.currentMeltTimesLeft[i] = -2.14748365E+09f;
												break;
											}
											break;
										}
									}
									num3++;
									num4++;
								}
							}
						}
					}
				}
			}
		}


		/**
		 * Cycles recipe queue as things get crafted.
		 */

		private void cycleRecipeQueue()
		{
			RecipeQueueItem recipeQueueItem = null;
			if (this.queue[this.queue.Length - 1] != null && this.queue[this.queue.Length - 1].Multiplier > 0)
			{
				return;
			}
			for (int i = 0; i < this.queue.Length; i++)
			{
				recipeQueueItem = this.queue[this.queue.Length - 1];
				if (recipeQueueItem != null && recipeQueueItem.Multiplier != 0)
				{
					break;
				}
				for (int j = this.queue.Length - 1; j >= 0; j--)
				{
					RecipeQueueItem recipeQueueItem2 = this.queue[j];
					if (j != 0)
					{
						RecipeQueueItem recipeQueueItem3 = this.queue[j - 1];
						if (recipeQueueItem3.Multiplier < 0)
						{
							recipeQueueItem3.Multiplier = 0;
						}
						recipeQueueItem2.Recipe = recipeQueueItem3.Recipe;
						recipeQueueItem2.Multiplier = recipeQueueItem3.Multiplier;
						recipeQueueItem2.CraftingTimeLeft = recipeQueueItem3.CraftingTimeLeft;
						recipeQueueItem2.IsCrafting = recipeQueueItem3.IsCrafting;
						recipeQueueItem2.Quality = recipeQueueItem3.Quality;
						recipeQueueItem2.OneItemCraftTime = recipeQueueItem3.OneItemCraftTime;
						recipeQueueItem2.StartingEntityId = recipeQueueItem3.StartingEntityId;
						this.queue[j] = recipeQueueItem2;
						recipeQueueItem3 = new RecipeQueueItem();
						recipeQueueItem3.Recipe = null;
						recipeQueueItem3.Multiplier = 0;
						recipeQueueItem3.CraftingTimeLeft = 0f;
						recipeQueueItem3.OneItemCraftTime = 0f;
						recipeQueueItem3.IsCrafting = false;
						recipeQueueItem3.Quality = 0;
						recipeQueueItem3.StartingEntityId = -1;
						this.queue[j - 1] = recipeQueueItem3;
					}
				}
			}
			if (recipeQueueItem != null && recipeQueueItem.Recipe != null && !recipeQueueItem.IsCrafting && recipeQueueItem.Multiplier != 0)
			{
				recipeQueueItem.IsCrafting = true;
			}
		}


		/**
		 * Recipe queue timings get handled here.
		 */

		private void HandleRecipeQueue(float _timePassed)
		{
			if (this.bUserAccessing)
			{
				return;
			}
			if (this.queue.Length == 0)
			{
				return;
			}
			if (this.isModuleUsed[3] && !this.isBurning)
			{
				return;
			}
			RecipeQueueItem recipeQueueItem = this.queue[this.queue.Length - 1];
			if (recipeQueueItem == null)
			{
				return;
			}
			if (recipeQueueItem.CraftingTimeLeft >= 0f)
			{
				recipeQueueItem.CraftingTimeLeft -= _timePassed;
			}
			while (recipeQueueItem.CraftingTimeLeft < 0f && this.hasRecipeInQueue())
			{
				if (recipeQueueItem.Multiplier > 0)
				{
					ItemValue itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, false);
					if (ItemClass.list[recipeQueueItem.Recipe.itemValueType] != null && ItemClass.list[recipeQueueItem.Recipe.itemValueType].HasQuality)
					{
						itemValue = new ItemValue(recipeQueueItem.Recipe.itemValueType, (int)recipeQueueItem.Quality, (int)recipeQueueItem.Quality, false, null, 1f);
					}
					if (ItemStack.AddToItemStackArray(this.output, new ItemStack(itemValue, recipeQueueItem.Recipe.count), -1) == -1)
					{
						return;
					}
					GameSparksCollector.IncrementCounter(GameSparksCollector.GSDataKey.CraftedItems, itemValue.ItemClass.Name, 1, true, GameSparksCollector.GSDataCollection.SessionUpdates);
					RecipeQueueItem recipeQueueItem2 = recipeQueueItem;
					recipeQueueItem2.Multiplier -= 1;
					recipeQueueItem.CraftingTimeLeft += recipeQueueItem.OneItemCraftTime;
				}
				if (recipeQueueItem.Multiplier <= 0)
				{
					float craftingTimeLeft = recipeQueueItem.CraftingTimeLeft;
					this.cycleRecipeQueue();
					recipeQueueItem = this.queue[this.queue.Length - 1];
					recipeQueueItem.CraftingTimeLeft += ((craftingTimeLeft < 0f) ? craftingTimeLeft : 0f);
				}
			}
		}


		/**
		 * Fuel countdown.
		 */

		private bool HandleFuel(World _world, float _timePassed)
		{
			bool result = false;
			if (!this.isModuleUsed[3])
			{
				Log.Out("Not using fuel!");
				return false;
			}
			if (!this.isBurning)
			{
				Log.Out("Not Burning!");
				return false;
			}
			base.emitHeatMapEvent(_world, EnumAIDirectorChunkEvent.Campfire);
			if (this.currentBurnTimeLeft > 0f || (this.currentBurnTimeLeft == 0f && this.getTotalFuelSeconds() > 0f))
			{
				this.currentBurnTimeLeft -= _timePassed;
				this.currentBurnTimeLeft = (float)Mathf.FloorToInt(this.currentBurnTimeLeft * 100f) / 100f;
				result = true;
				if (this.currentBurnTimeLeft == 0)
				{
					ItemStack[] newOutput = ByproductManager.HandleFuelByproductFor(this.fuel[0].itemValue, this.output);
					if (newOutput != this.output)
					{
						this.output = newOutput;
						// DEPRECATED
						// if (this.OutputChanged != null)
						// {
						//     this.OutputChanged();
						// }
					}
				}

			}

			while (this.currentBurnTimeLeft < 0f && this.getTotalFuelSeconds() > 0f)
			{
				if (this.fuel[0].count > 0)
				{
					this.fuel[0].count--;

					this.currentBurnTimeLeft += this.GetFuelTime(this.fuel[0]);
					result = true;
					if (this.FuelChanged != null)
					{
						this.FuelChanged();
					}
				}
				else
				{
					this.cycleFuelStacks();
					result = true;
				}
			}
			if (this.getTotalFuelSeconds() == 0f && this.currentBurnTimeLeft < 0f)
			{
				this.currentBurnTimeLeft = 0f;
				result = true;
			}
			return result;
		}






		/**
		 * Checks if the workstation is next to water.
		 */

		private void checkIfByWater(World _world, Vector3i _blockPos)
		{
			this.isBesideWater = false;
			this.isBesideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y + 1, _blockPos.z).type].blockMaterial.IsLiquid;
			this.isBesideWater |= Block.list[_world.GetBlock(_blockPos.x + 1, _blockPos.y, _blockPos.z).type].blockMaterial.IsLiquid;
			this.isBesideWater |= Block.list[_world.GetBlock(_blockPos.x - 1, _blockPos.y, _blockPos.z).type].blockMaterial.IsLiquid;
			this.isBesideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y, _blockPos.z + 1).type].blockMaterial.IsLiquid;
			this.isBesideWater |= Block.list[_world.GetBlock(_blockPos.x, _blockPos.y, _blockPos.z - 1).type].blockMaterial.IsLiquid;
			this.isBurning &= !this.isBesideWater;
		}


		/**
		 * Gets fuel time from an item stack.
		 */

		private float getFuelTime(ItemStack _itemStack)
		{
			if (_itemStack == null)
			{
				return 0f;
			}
			ItemClass forId = ItemClass.GetForId(_itemStack.itemValue.type);
			float result = 0f;
			if (forId == null)
			{
				return result;
			}
			if (!forId.IsBlock())
			{
				if (forId.FuelValue != null)
				{
					result = (float)forId.FuelValue.Value;
				}
			}
			else if (forId.Id < Block.list.Length)
			{
				Block block = Block.list[forId.Id];
				if (block != null)
				{
					result = (float)block.FuelValue;
				}
			}
			return result;
		}


		/**
		 * If one fuel stack runs out, cycles to the next.
		 */

		private void cycleFuelStacks()
		{
			if (this.fuel.Length < 2)
			{
				return;
			}
			for (int i = 0; i < this.fuel.Length - 1; i++)
			{
				for (int j = 0; j < this.fuel.Length; j++)
				{
					ItemStack itemStack = this.fuel[j];
					if (itemStack.count <= 0 && j + 1 < this.fuel.Length)
					{
						ItemStack itemStack2 = this.fuel[j + 1];
						itemStack = itemStack2.Clone();
						this.fuel[j] = itemStack;
						itemStack2 = ItemStack.Empty.Clone();
						this.fuel[j + 1] = itemStack2;
					}
				}
				if (this.fuel[0].count > 0)
				{
					break;
				}
			}
		}


		/**
		 * Returns the total number of seconds burn time left.
		 */

		private float getTotalFuelSeconds()
		{
			float num = 0f;
			for (int i = 0; i < this.fuel.Length; i++)
			{
				if (!this.fuel[i].IsEmpty())
				{
					num += (float)ItemClass.GetFuelValue(this.fuel[i].itemValue) * (float)this.fuel[i].count;
				}
			}
			return num;
		}


		/**
		 * Gets fuel time for an item.
		 */

		private float GetFuelTime(ItemStack _fuel)
		{
			if (_fuel.itemValue.type == 0)
			{
				return 0f;
			}
			return (float)ItemClass.GetFuelValue(_fuel.itemValue);
		}


		/**
		 * Checks if a recipe is in the queue.
		 */

		private bool hasRecipeInQueue()
		{
			for (int i = 0; i < this.queue.Length; i++)
			{
				if (this.queue[i].Multiplier > 0 && this.queue[i].Recipe != null)
				{
					return true;
				}
			}
			return false;
		}



		public override void read(PooledBinaryReader _br, StreamModeRead _eStreamMode)
		{
			base.read(_br, _eStreamMode);
			this.processByproduct = _br.ReadBoolean();
			this.fuelItem = ItemValue.ReadAndCreate(_br);
		}


		public override void write(PooledBinaryWriter _bw, StreamModeWrite _eStreamMode)
		{
			base.write(_bw, _eStreamMode);
			_bw.Write(this.processByproduct);
			this.fuelItem.Write(_bw);
		}



		private const float cFuelBurnPerTick = 1f;
		private ItemStack[] fuel;
		private ItemStack[] input;
		private ItemStack[] tools;
		private ItemStack[] output;
		private RecipeQueueItem[] queue;
		private ulong lastTickTime;
		private int fuelInStorageInTicks;
		private float currentBurnTimeLeft;
		private float[] currentMeltTimesLeft;
		private ItemStack[] lastInput;
		private bool isBurning;
		private bool isBesideWater;
		private bool isPlayerPlaced;
		private string[] materialNames;
		private bool[] isModuleUsed;

		private bool processByproduct;
		private ItemValue fuelItem;



		private enum Module
		{
			Tools,
			Input,
			Output,
			Fuel,
			Material_Input,
			Count
		}
	}
}