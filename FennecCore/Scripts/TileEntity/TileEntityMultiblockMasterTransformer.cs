﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

    /**
     *  Tile entity class for block transformer. Takes input items and changes them into output items. 
     */
    public class TileEntityMultiblockMasterTransformer : TileEntityTransformer, ITileEntityMultiblockMaster
    {
        /**
         * Loads the tile entity data when instantiated from placing a block.
         */

        public TileEntityMultiblockMasterTransformer(Chunk _chunk) : base(_chunk)
        {
            this.tQueue = new TransformationQueue();
            this.useHash = true;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.userAccessing = false;
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Loads the tile entity by copying another of its kind.
         */

        private TileEntityMultiblockMasterTransformer(TileEntityMultiblockMasterTransformer _other) : base(null)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.collection = _other.collection;
            this.tQueue = _other.tQueue;
            this.useHash = _other.useHash;
            this.requiresPower = _other.requiresPower;
            this.requiresHeat = _other.requiresHeat;
            this.powerSources = _other.powerSources;
            this.heatSources = _other.heatSources;
            this.requiresNearbyBlocks = _other.requiresNearbyBlocks;
            this.nearbyBlockNames = _other.nearbyBlockNames;
            this.nearbyBlockTags = _other.nearbyBlockTags;
            this.nearbyBlockRequireAllTags = _other.nearbyBlockRequireAllTags;
            this.nearbyBlockRange = _other.nearbyBlockRange;
            this.nearbyBlocksNeeded = _other.nearbyBlocksNeeded;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.userAccessing = false;
            this.CalculateLookupCoordinates();
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Sets the above method public
         */

        public new TileEntityMultiblockMasterTransformer Clone()
        {
            return new TileEntityMultiblockMasterTransformer(this);
        }


        /**
         * Copies properties from another tile entity of this type into this one.
         */

        public void CopyLootContainerDataFromOther(TileEntityMultiblockMasterTransformer _other)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.collection = _other.collection;
            this.tQueue = _other.tQueue;
            this.useHash = _other.useHash;
            this.requiresPower = _other.requiresPower;
            this.requiresHeat = _other.requiresHeat;
            this.powerSources = _other.powerSources;
            this.heatSources = _other.heatSources;
            this.requiresNearbyBlocks = _other.requiresNearbyBlocks;
            this.nearbyBlockNames = _other.nearbyBlockNames;
            this.nearbyBlockTags = _other.nearbyBlockTags;
            this.nearbyBlockRequireAllTags = _other.nearbyBlockRequireAllTags;
            this.nearbyBlockRange = _other.nearbyBlockRange;
            this.nearbyBlocksNeeded = _other.nearbyBlocksNeeded;
            this.hasPower = false;
            this.hasHeat = false;
            this.hasNearbyBlocks = false;
            this.userAccessing = false;
            this.CalculateLookupCoordinates();
            this.random = RandomStatic.Range(0, 5);
        }


        /**
         * Checks if block is powered. It is required to be next to a TileEntityPowered type of block in any cardinal direction.
         */

        public override bool IsPowered(World world)
        {
            if (!this.MultiblockFormed())
            {
                this.hasPower = false;
                return this.hasPower;
            }

            if (this.poweredSlaveCoords == null || this.poweredSlaveCoords.Count == 0)
            {
                this.hasPower = true;
                return this.hasPower;
            }

            int flag = 0;
            foreach (Vector3i coord in this.poweredSlaveCoords)
            {
                Chunk chunk = world.GetChunkFromWorldPos(coord) as Chunk;
                Vector3i pos = Chunk.ToLocalPosition(coord);
                if (chunk == null)
                {
                    continue;
                }

                BlockPowered powered = world.GetBlock(coord).Block as BlockPowered;
                if (powered == null)
                {
                    this.SetMultiblockNotFormed();
                    this.hasPower = false;
                    return this.hasPower;
                }

                if (!powered.HasTileEntity)
                {
                    this.SetMultiblockNotFormed();
                    this.hasPower = false;
                    return this.hasPower;
                }

                TileEntityPowered te = chunk.GetTileEntity(pos) as TileEntityPowered;
                if (te == null)
                {
                    continue;
                }

                if (te.IsPowered)
                {
                    flag += 1;
                }
            }

            this.hasPower = (flag >= poweredSlaveCoords.Count);
            this.setModified();
            return this.hasPower;
        }


        /**
         * Each game tick, this method is called.
         */

        public override void UpdateTick(World world)
        {

            if (MultiblockFormed())
            {
                if (!this.UpdateCanHappen(world))
                {
                    return;
                }

                HandleAddingToQueue(world);
                HandleProcessingQueue();
                CheckShouldShowParticles(world);
                return;
            }
            UpdateMaster(world);
        }


        /**
         * Checks that an update can happen.
         */

        public override bool UpdateCanHappen(World world)
        {
            return (this.IsPowered(world) && (this.tQueue.QueueHasJobs() | !this.IsEmpty()));
        }


        /**
         * Checks whether we should show particles.
         */

        protected virtual void CheckShouldShowParticles(World world)
        {
            bool flag = this.MultiblockFormed() && this.IsPowered(world) && this.IsCrafting();
            BlockValue blockValue = world.GetBlock(this.ToWorldPos());
            blockValue.meta = (flag ? (byte)1 : (byte)0);

            Chunk chunk = world.GetChunkFromWorldPos(this.ToWorldPos()) as Chunk;
            if (chunk == null)
            {
                return;
            }

            BlockMultiblockMasterTransformer transformerBlock = blockValue.Block as BlockMultiblockMasterTransformer;
            if (transformerBlock != null)
            {
                transformerBlock.checkParticles(world, chunk.ClrIdx, this.ToWorldPos(), blockValue);
            }
        }


        /**
         * Reads data into the tile entity when loaded.
         */

        public override void read(PooledBinaryReader _br, StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.masterBlockName = _br.ReadString();
            this.IsPlayerPlaced = _br.ReadBoolean();
            this.rotationIndex = _br.ReadInt32();
            this.formed = _br.ReadBoolean();
            this.requiresPower = _br.ReadBoolean();
            this.requiresHeat = _br.ReadBoolean();
            this.hasPower = _br.ReadBoolean();
            this.hasHeat = _br.ReadBoolean();
            int poweredSlaveCount = _br.ReadInt32();
            for (int i = 0; i < poweredSlaveCount; i += 1)
            {
                this.poweredSlaveCoords.AddIfNew(StringHelpers.WriteStringToVector3i(_br.ReadString()));
            }
        }


        /**
         * Saves tile entity data to the strem.
         */

        public override void write(PooledBinaryWriter _bw, StreamModeWrite _eStreamMode)
        {
            base.write(_bw, _eStreamMode);
            _bw.Write(this.masterBlockName);
            _bw.Write(this.IsPlayerPlaced);
            _bw.Write(this.rotationIndex);
            _bw.Write(this.formed);
            _bw.Write(this.requiresPower);
            _bw.Write(this.requiresHeat);
            _bw.Write(this.hasPower);
            _bw.Write(this.hasHeat);
            _bw.Write(this.poweredSlaveCoords.Count);
            foreach (Vector3i coord in this.poweredSlaveCoords)
            {
                _bw.Write(StringHelpers.WriteVector3iToString(coord));
            }

        }


        /**
         * What happens when the block is upgraded or downgraded.
         */

        public void UpgradeDowngradeFrom(TileEntityMultiblockMasterTransformer _other)
        {
            base.UpgradeDowngradeFrom(_other);
            this.OnDestroy();
            if (_other is TileEntityMultiblockMasterTransformer)
            {
                TileEntityMultiblockMasterTransformer tileEntityBlockTransformer = _other as TileEntityMultiblockMasterTransformer;
                this.bTouched = tileEntityBlockTransformer.bTouched;
                this.worldTimeTouched = tileEntityBlockTransformer.worldTimeTouched;
                this.bPlayerBackpack = tileEntityBlockTransformer.bPlayerBackpack;
                this.bPlayerStorage = tileEntityBlockTransformer.bPlayerStorage;
                this.items = ItemStack.Clone(tileEntityBlockTransformer.itemsArr, 0, this.containerSize.x * this.containerSize.y);
                if (this.items.Length != this.containerSize.x * this.containerSize.y)
                {
                    Log.Error("Error upgrading.");
                }
            }
        }


        /**
         * What happens when the tile entities change state.
         */

        private void tileEntityChanged()
        {
            for (int i = 0; i < this.listeners.Count; i++)
            {
                this.listeners[i].OnTileEntityChanged(this, 0);
            }
        }


        /**
         * Returns the tile entity enum type for comparison.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.MBMstrTransformer];
        }


        /**
         * If the block is removed we want to clear out all slave data present.
         */

        public override void OnRemove(World world)
        {
            base.OnRemove(world);
            this.ClearSlaveData();
            this.OnDestroy();
        }


        /**
         * Triggers an update (so slave blocks can call it)
         */

        public void TriggerUpdate()
        {
            this.setModified();
        }


        /**
         * Updates the master block
         */

        public bool UpdateMaster(World world, bool checkRotationIndex = false)
        {
            // If master block not found already, attempt to get it.
            if (this.masterBlockName == null)
            {
                Vector3i worldPos = this.ToWorldPos();
                Block block = world.GetBlock(worldPos).Block;
                if (block == null)
                {
                    return false;
                }

                this.masterBlockName = block.GetBlockName();
            }

            // Need to check each rotation in turn to make sure that the structure is formed.
            for (int i = 0; i < 4; i += 1)
            {
                if (checkRotationIndex)
                {
                    if (this.rotationIndex == -1)
                    {
                        UpdateMaster(world);
                        return false;
                    }

                    if (this.rotationIndex != i)
                    {
                        continue;
                    }
                }

                Dictionary<Vector3i, string> multiblockStructure = MultiblockManager.GetCoordinatesInWorldSpace(this.masterBlockName, this.ToWorldPos(), i);
                List<ITileEntityMultiblockSlave> slaveBlocks = new List<ITileEntityMultiblockSlave>();
                List<Vector3i> poweredSlaveCoords = new List<Vector3i>();
                foreach (KeyValuePair<Vector3i, string> blockData in multiblockStructure)
                {
                    Vector3i blockPosition = blockData.Key;
                    string blockNameType = blockData.Value;
                    Block block = world.GetBlock(blockPosition).Block;

                    if (block.ToBlockValue().Equals(BlockValue.Air))
                    {
                        break;
                    }

                    if (!block.HasTileEntity)
                    {
                        break;
                    }

                    Chunk chunk = world.GetChunkFromWorldPos(blockPosition) as Chunk;
                    if (chunk == null)
                    {
                        break;
                    }

                    ITileEntityMultiblockSlave te = chunk.GetTileEntity(Chunk.ToLocalPosition(blockPosition)) as ITileEntityMultiblockSlave;
                    if (te == null) //Must be a powered one that's inactive.
                    {
                        BlockPowered powered = chunk.GetBlock(Chunk.ToLocalPosition(blockPosition)).Block as BlockPowered;
                        if (powered == null)
                        {
                            break;
                        }

                        string currentBlockType;
                        if (!PropertyHelpers.PropExists(powered.Properties, "MultiblockType", out currentBlockType))
                        {
                            break;
                        }

                        if (!blockData.Value.Equals(currentBlockType))
                        {
                            break;
                        }

                        poweredSlaveCoords.AddIfNew(blockPosition);
                        continue;
                    }

                    string slaveType = te.GetSlaveType();
                    if (slaveType == null || slaveType != blockNameType)
                    {
                        break;
                    }
                    slaveBlocks.Add(te);
                }

                if ((slaveBlocks.Count + poweredSlaveCoords.Count) < multiblockStructure.Count)
                {
                    continue;
                }

                this.activeSlaveBlocks = slaveBlocks;
                this.poweredSlaveCoords = poweredSlaveCoords;
                foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
                {
                    slaveBlock.SetMaster(this);
                }

                this.SetMultiblockFormed();
                this.rotationIndex = i;
                this.forceUpdate = false;
                return true;
            }
            this.rotationIndex = -1;
            this.SetMultiblockNotFormed();
            return false;
        }


        /**
         * Returns whether the multiblock structure is valid or not.
         */

        public bool MultiblockFormed()
        {
            return this.formed;
        }


        /**
         * Tells this tile entity that the multiblock structure is valid.
         */

        public bool SetMultiblockFormed()
        {
            this.formed = true;
            this.setModified();
            return this.formed;
        }


        /**
         * Tells the tile entity that the multiblock structure is not valid.
         */

        public bool SetMultiblockNotFormed()
        {
            this.formed = false;
            this.setModified();
            return this.formed;
        }


        /**
         * Sets the name of the master multiblock.
         */

        public void SetMasterBlockName(string name)
        {
            this.masterBlockName = name;
            this.setModified();
        }


        /**
         * Gets the active slave blkocks.
         */

        public List<ITileEntityMultiblockSlave> GetSlaveBlocks()
        {
            if (this.activeSlaveBlocks == null)
            {
                return null;
            }
            return this.activeSlaveBlocks;
        }


        /**
         * Removes the connection to slave blocks.
         */

        protected virtual void ClearSlaveData()
        {
            if (this.activeSlaveBlocks == null)
            {
                return;
            }

            foreach (ITileEntityMultiblockSlave slaveBlock in this.activeSlaveBlocks)
            {
                slaveBlock.UnbindMaster();
            }
            this.activeSlaveBlocks = null;
            this.SetMultiblockNotFormed();
        }


        // Saved variables that are called on Write() and Read() methods.
        private bool useHash;
        private bool bDisableModifiedCheck;
        private bool requiresPower;
        private bool requiresHeat;
        private bool requiresNearbyBlocks;
        private List<string> powerSources;
        private List<string> heatSources;
        private List<string> nearbyBlockNames;
        private List<string> nearbyBlockTags;
        private bool nearbyBlockRequireAllTags;
        private Vector3i nearbyBlockRange;
        private int nearbyBlocksNeeded;
        private Vector2i containerSize;
        private ItemStack[] itemsArr;
        private List<Entity> entList;
        private TransformationCollection collection;

        // These are calculated on reading and writing to save extra work.
        private List<Vector3i> poweredBlockCoords;
        private List<Vector3i> heatedBlockCoords;
        private List<Vector3i> nearbyBlockCoords;

        // These store whether the block is powered, heated and has nearby blocks, to reduce amount of calls to check.
        private bool hasPower;
        private bool hasHeat;
        private bool hasNearbyBlocks;
        private bool userAccessing;
        private int random;

        private bool forceUpdate;
        private int Debug = 1;

        private int debug
        {
            get
            {
                int current = this.Debug;
                this.Debug -= 1;
                return (current > 0 ? current : 0);
            }
        }

        private int rotationIndex;
        private string masterBlockName;
        public bool IsPlayerPlaced;
        private bool formed;

        public bool ForceUpdate { get { return this.ForceUpdate; } }
        private int TickCounter;

        public bool UpdateReady
        {
            get { return ((this.TickCounter += 1) % 4) == 0; }
        }

        private List<ITileEntityMultiblockSlave> activeSlaveBlocks = new List<ITileEntityMultiblockSlave>();
        private List<Vector3i> poweredSlaveCoords = new List<Vector3i>();

        // Fields to recall without further calculation
        public bool HasRequiredPower { get { return this.hasPower; } }
        public bool HasRequiredHeat { get { return this.hasHeat; } }
        public bool HasRequiredNearbyBlocks { get { return this.hasNearbyBlocks; } }
        public bool IsCurrentlyWorking { get { return this.tQueue.QueueHasJobs(); } }
    }
}