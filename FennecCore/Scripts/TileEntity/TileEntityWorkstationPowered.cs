﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{
    public class TileEntityWorkstationPowered : TileEntityWorkstation, ITileEntityReceivePower
    {
        public new event XUiEvent_InputStackChanged InputChanged;
        public new event XUiEvent_FuelStackChanged FuelChanged;


        /**
         * Constructor
         */

        public TileEntityWorkstationPowered(Chunk _chunk) : base(_chunk)
        {
            /*
            this.fuel = ItemStack.CreateArray(3);
            this.tools = ItemStack.CreateArray(3);
            this.output = ItemStack.CreateArray(6);
            this.input = ItemStack.CreateArray(3);
            this.lastInput = ItemStack.CreateArray(3);
            this.queue = new RecipeQueueItem[4];
            this.materialNames = new string[0];
            this.isModuleUsed = new bool[5];
            this.currentMeltTimesLeft = new float[this.input.Length];
            */
            this.dataObject = 0;
            this.hasPower = false;
            this.poweredBlockCoords = new List<Vector3i>();
            this.sidedPower = SidedPower.ALL;
        }


        /**
         * Updates the light values for the workstation if it gives off light.
         */

        private void updateLightState(World world)
        {
            BlockValue block = world.GetBlock(base.ToWorldPos());
            if (!this.CanOperate(GameTimer.Instance.ticks) && block.meta != 0)
            {
                block.meta = 0;
                world.SetBlockRPC(base.ToWorldPos(), block);
                return;
            }
            if (this.CanOperate(GameTimer.Instance.ticks) && block.meta != 15)
            {
                block.meta = 15;
                world.SetBlockRPC(base.ToWorldPos(), block);
            }
        }


        /**
         * Checks if block is powered. It is required to be next to a TileEntityPowered type of block in any cardinal direction.
         */

        public bool IsPowered(World world)
        {
            if (!this.requiresPower)
            {
                return this.hasPower = true;
            }

            if (this.poweredBlockCoords == null)
            {
                this.CalculateLookupCoordinates();
            }

            if (this.poweredBlockCoords.Count == 0)
            {
                return this.hasPower = false;
            }

            if (world == null)
            {
                return this.hasPower = false;
            }

            Dictionary<Vector3i, TileEntity> tileEntitiesNearby = CoordinateHelper.GetTileEntitiesInCoordinatesWithType(world, this.poweredBlockCoords, TileEntityType.Powered);
            if (tileEntitiesNearby.Count == 0)
            {
                return this.hasPower = false;
            }

            foreach (KeyValuePair<Vector3i, TileEntity> entry in tileEntitiesNearby)
            {
                TileEntityPowered otherTileEntity = entry.Value as TileEntityPowered;
                Vector3i otherTileEntityPos = entry.Key;

                foreach (string powerSource in this.powerSources)
                {
                    string name = CoordinateHelper.GetBlockNameAtCoordinate(world, otherTileEntityPos);
                    if (!CoordinateHelper.BlockAtCoordinateIs(world, otherTileEntityPos, powerSource))
                    {
                        continue;
                    }

                    if (otherTileEntity.IsPowered)
                    {
                        return this.hasPower = true;
                    }
                }
            }
            return this.hasPower = false;
        }


        /**
         * Calculates the lookup coordinates.
         */

        public void CalculateLookupCoordinates()
        {
            this.poweredBlockCoords = new List<Vector3i>();

            World world = GameManager.Instance.World;
            Vector3i tileEntityPos = this.ToWorldPos();

            switch (this.sidedPower)
            {
                case SidedPower.TOP:
                    this.poweredBlockCoords = CoordinateHelper.GetCoordinatesAbove(tileEntityPos, 1);
                    break;
                case SidedPower.BOTTOM:
                    this.poweredBlockCoords = CoordinateHelper.GetCoordinatesBelow(tileEntityPos, 1);
                    break;
                case SidedPower.SIDES:
                    this.poweredBlockCoords = CoordinateHelper.GetCoordinatesAround(tileEntityPos, true, 1, 0, 1);
                    break;
                default:
                    this.poweredBlockCoords = CoordinateHelper.GetCoordinatesAround(tileEntityPos, true, 1, 1, 1);
                    break;
            }
        }


        /**
         * Loads data from save file and reads into the tile entity.
         */

        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.requiresPower = _br.ReadBoolean();
            this.powerSources = StringHelpers.WriteStringToList(_br.ReadString());
            this.hasPower = false;
            this.sidedPower = (SidedPower)_br.ReadInt32();
            this.CalculateLookupCoordinates();
        }


        /**
         * Writes out the required data to the tile entity.
         */

        public override void write(PooledBinaryWriter _bw, TileEntity.StreamModeWrite _eStreamMode)
        {
            base.write(_bw, _eStreamMode);
            _bw.Write(this.requiresPower);
            _bw.Write(StringHelpers.WriteListToString(this.powerSources));
            _bw.Write((int)this.sidedPower);
        }


        /**
         * Sets required power.
         */

        public void SetRequirePower(bool requirePower, List<string> powerSourceBlocks, SidedPower sidedPower)
        {
            this.requiresPower = requirePower;
            this.powerSources = powerSourceBlocks;
            this.sidedPower = sidedPower;
        }

        /**
         * Returns WorkstationPowered as a tile entity type.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.WorkstationPowered];
        }


        /*
        private const float cFuelBurnPerTick = 1f;
        private ItemStack[] fuel;
        private ItemStack[] input;
        private ItemStack[] tools;
        private ItemStack[] output;
        private RecipeQueueItem[] queue;
        private ulong lastTickTime;
        private int fuelInStorageInTicks;
        private float currentBurnTimeLeft;
        private float[] currentMeltTimesLeft;
        private ItemStack[] lastInput;
        private bool isBurning;
        private bool isBesideWater;
        private bool isPlayerPlaced;
        private string[] materialNames;
        private bool[] isModuleUsed;

        private enum Module
        {
            Tools,
            Input,
            Output,
            Fuel,
            Material_Input,
            Count
        }
        */

        // Saved variables that are called on Write() and Read() methods.
        private bool requiresPower;
        private List<string> powerSources;
        private SidedPower sidedPower;

        // These are calculated on reading and writing to save extra work.
        private List<Vector3i> poweredBlockCoords;

        public bool HasRequiredPower { get { return this.hasPower; } }

        // These store whether the block is powered, heated and has nearby blocks, to reduce amount of calls to check.
        private bool hasPower;
    }
}