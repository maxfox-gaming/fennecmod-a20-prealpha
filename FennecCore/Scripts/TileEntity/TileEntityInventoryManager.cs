﻿using System.Collections.Generic;

namespace FennecCore.Scripts
{

    public class TileEntityInventoryManager : TileEntityLootContainer
    {

        public TileEntityInventoryManager(Chunk _chunk) : base(_chunk)
        {
            this.SetNeedsUpdate();
        }


        private TileEntityInventoryManager(TileEntityInventoryManager _other) : base(null)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.AddNearbySignedContainersToManager();
        }


        public new TileEntityInventoryManager Clone()
        {
            return new TileEntityInventoryManager(this);
        }


        /**
         * Copies properties from another tile entity of this type into this one.
         */

        public void CopyLootContainerDataFromOther(TileEntityInventoryManager _other)
        {
            this.lootListName = _other.lootListName;
            this.containerSize = _other.containerSize;
            this.items = ItemStack.Clone(_other.items);
            this.bTouched = _other.bTouched;
            this.worldTimeTouched = _other.worldTimeTouched;
            this.bPlayerBackpack = _other.bPlayerBackpack;
            this.bPlayerStorage = _other.bPlayerStorage;
            this.bUserAccessing = _other.bUserAccessing;
            this.AddNearbySignedContainersToManager();

        }


        /**
         * Updates the tile entity.
         */

        public override void UpdateTick(World world)
        {
            base.UpdateTick(world);
            if (this.needsUpdate)
            {
                this.AddNearbySignedContainersToManager();
                this.needsUpdate = false;
            }
        }


        /**
         * Adds nearby secure signed containers to link.
         */

        private void AddNearbySignedContainersToManager()
        {
            List<TileEntitySecureLootContainerSigned> list = TileEntityHelper.GetNearbySignedContainers(this);
            if (list == null)
            {
                return;
            }
            if (list.Count == 0)
            {
                return;
            }

            foreach (TileEntitySecureLootContainerSigned tileEntity in list)
            {
                this.UpdateManager(tileEntity.ToWorldPos(), TileEntityInventoryManager.UpdateState.Add);
            }
        }


        /**
         * Returns the nearby coordinates around the tile entity.
         */

        private List<Vector3i> GetNearbyCoordinates()
        {
            if (this.nearbyCoordinates != null)
            {
                return this.nearbyCoordinates;
            }
            return (this.nearbyCoordinates = CoordinateHelper.GetCoordinatesAround(this.ToWorldPos(), new Vector3i(10, 10, 10)));
        }


        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
        }


        public override void write(PooledBinaryWriter stream, StreamModeWrite _eStreamMode)
        {
            base.write(stream, _eStreamMode);
        }


        /**
        * Returns the tile entity enum type for comparison.
        */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.InventoryManager];
        }


        /**
         * When the tile entity is destroyed, remove it from the world.
         */

        public override void OnDestroy()
        {
            base.OnDestroy();
            InventoryManagerManager.RemoveManager(this.ToWorldPos());
        }


        public void UpdateManager(Vector3i containerCoordinate, UpdateState state)
        {
            switch (state)
            {
                case UpdateState.Add:
                    InventoryManagerManager.Add(new InventoryManagerLink(this.ToWorldPos(), containerCoordinate));
                    break;
                case UpdateState.Remove:
                    InventoryManagerManager.RemoveContainer(containerCoordinate);
                    break;
                default:
                    break;
            }
        }


        public void SetNeedsUpdate()
        {
            this.needsUpdate = true;
        }


        // Saved variables that are called on Write() and Read() methods.

        private bool bDisableModifiedCheck;
        private Vector2i containerSize;
        private ItemStack[] itemsArr;
        private List<Entity> entList;

        private bool needsUpdate;

        private List<Vector3i> nearbyCoordinates;


        public enum UpdateState
        {
            Add,
            Remove
        };
    }
}