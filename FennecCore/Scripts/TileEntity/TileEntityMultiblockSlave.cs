﻿namespace FennecCore.Scripts
{

    // Token: 0x02000614 RID: 1556
    public class TileEntityMultiblockSlave : TileEntity, ITileEntityMultiblockSlave
    {

        /**
         * Constructor
         */

        public TileEntityMultiblockSlave(Chunk _chunk) : base(_chunk)
        {
            this.masterPos = Vector3i.zero;
        }


        /**
         * Copies properties from one slave into another.
         */

        private TileEntityMultiblockSlave(TileEntityMultiblockSlave _other) : base(null)
        {
            if (this.GetMaster() != null)
            {
                _other.SetMaster(this.master);
            }
            this.masterPos = _other.masterPos;
            this.type = _other.type;
        }


        /**
         * Clones a multiblock slave.
         */

        public new TileEntityMultiblockSlave Clone()
        {
            return new TileEntityMultiblockSlave(this);
        }


        /**
         * Each tick, an action is performed.
         */

        public override void UpdateTick(World world)
        {
            base.UpdateTick(world);

        }


        /**
         * Gets the master block if there is one. Useful for when you are interacting with multiblock via its slave.
         */

        public ITileEntityMultiblockMaster GetMaster()
        {
            return this.master;
        }


        /**
         * Gets the master block position
         */

        public Vector3i GetMasterPos()
        {
            return this.masterPos;
        }


        /**
         * Sets the master block for this slave block. Slave blocks can only ever have one master block at a time.
         */

        public void SetMaster(ITileEntityMultiblockMaster master)
        {
            if (this.master != null)
            {
                return;
            }
            master.TriggerUpdate();
            this.master = master;
            this.masterPos = master.ToWorldPos();
            this.formed = true;
            this.setModified();
        }


        /**
         * Unsets the master for this tile entity.
         */

        public void UnsetMaster()
        {
            if (this.master != null)
            {
                this.master.TriggerUpdate();
            }
            this.master = null;
            this.masterPos = Vector3i.zero;
            this.formed = false;
            this.setModified();
        }


        /**
         * Sets the type of this slave (useful for defining types rather than using block names where substitutes are possible
         */

        public void SetSlaveType(string type)
        {
            this.type = type;
            this.setModified();
        }


        /**
         * Gets the type of the slave, or the block name if it was not defined before.
         */

        public string GetSlaveType()
        {
            if (this.type != null)
            {
                return this.type;
            }

            World world = GameManager.Instance.World;
            return world != null ? world.GetBlock(this.ToWorldPos()).Block.GetBlockName() : null;
        }


        /**
         * Removes the master block binding
         */

        public void UnbindMaster()
        {
            if (this.HasMaster)
            {
                this.master = null;
            }
            this.masterPos = Vector3i.zero;
            this.formed = false;
            this.setModified();
        }


        /**
         * If this slave block hsa been destroyed and it had a master block, the multiblock will no longer be formed correctly so notify the master block.
         */

        public override void OnRemove(World world)
        {
            base.OnRemove(world);
            bool foundMaster = false;
            ITileEntityMultiblockMaster masterTE = null;

            if (this.HasMaster)
            { 
                if (this.master == null)
                {
                    Chunk chunk = world.GetChunkFromWorldPos(this.masterPos) as Chunk;
                    if (chunk != null)
                    {
                        masterTE = world.GetTileEntity(chunk.ClrIdx, this.masterPos) as ITileEntityMultiblockMaster;
                        if (masterTE != null)
                        {
                            foundMaster = true;
                        }
                    }
                }
                else
                {
                    masterTE = this.master;
                    foundMaster = true;
                }

                if (foundMaster && masterTE != null)
                {
                    masterTE.SetMultiblockNotFormed();
                }
            }

            this.OnDestroy();
        }


        /**
         * Checks whether the multiblock is formed.
         */

        public bool Formed()
        {
            return this.formed;
        }


        /**
         * Reads the type of the multiblock
         */

        public override void read(PooledBinaryReader _br, TileEntity.StreamModeRead _eStreamMode)
        {
            base.read(_br, _eStreamMode);
            this.type = _br.ReadString();
            this.masterPos = StringHelpers.WriteStringToVector3i(_br.ReadString());
            this.formed = _br.ReadBoolean();
        }


        /**
         * Writes out tile entity data to save file.
         */

        public override void write(PooledBinaryWriter stream, TileEntity.StreamModeWrite _eStreamMode)
        {
            base.write(stream, _eStreamMode);
            stream.Write(this.type);
            stream.Write(StringHelpers.WriteVector3iToString(this.masterPos));
            stream.Write(this.formed);
        }


        /**
         * Returns the tile entity type, used for instantiating the object.
         */

        public override TileEntityType GetTileEntityType()
        {
            return TileEntityMapping.Types[TileEntityMapping.MBSlave];
        }


        /**
         * What happens when the block is upgraded/downgraded
         */

        public override void UpgradeDowngradeFrom(TileEntity _other)
        {
            base.UpgradeDowngradeFrom(_other);
            this.OnDestroy();
        }


        /**
         * What happens when the tile entity is changed in any way.
         */

        private void tileEntityChanged()
        {
            for (int i = 0; i < this.listeners.Count; i++)
            {
                this.listeners[i].OnTileEntityChanged(this, 0);
            }
        }


        /**
         * Resets all tile entity data.
         */

        public override void Reset()
        {
            base.Reset();
            this.setModified();
        }


        public bool IsPlayerPlaced = false;
        private Vector3i masterPos;
        private ITileEntityMultiblockMaster master;
        private string type;
        private bool formed;

        public bool HasMaster
        {
            get { return (this.master != null || !this.masterPos.Equals(Vector3i.zero)); }
        }
    }
}