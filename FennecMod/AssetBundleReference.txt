Complete list of Unity asset bundles, sounds, etc for reference.

#@modfolder:Resources/Crops.unity3d?ChrysanthemumHDStage1
#@modfolder:Resources/Crops.unity3d?ChrysanthemumHDStage2
#@modfolder:Resources/Crops.unity3d?ChrysanthemumHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?CottonHDStage1
#@modfolder:Resources/Crops.unity3d?CottonHDStage2
#@modfolder:Resources/Crops.unity3d?CottonHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?DeadCrop
#@modfolder:Resources/Crops.unity3d?GoldenrodHDStage1
#@modfolder:Resources/Crops.unity3d?GoldenrodHDStage2
#@modfolder:Resources/Crops.unity3d?GoldenrodHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?HopsHDStage1
#@modfolder:Resources/Crops.unity3d?HopsHDStage2
#@modfolder:Resources/Crops.unity3d?HopsHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?MaizeHDStage1
#@modfolder:Resources/Crops.unity3d?MaizeHDStage2
#@modfolder:Resources/Crops.unity3d?MaizeHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?SnowberryHDStage1
#@modfolder:Resources/Crops.unity3d?SnowberryHDStage2
#@modfolder:Resources/Crops.unity3d?SnowberryHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?SoybeanHDStage1
#@modfolder:Resources/Crops.unity3d?SoybeanHDStage2
#@modfolder:Resources/Crops.unity3d?SoybeanHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?TomatoHDStage1
#@modfolder:Resources/Crops.unity3d?TomatoHDStage2
#@modfolder:Resources/Crops.unity3d?TomatoHDStage3Harvest
#@modfolder:Resources/Crops.unity3d?WheatHDStage1
#@modfolder:Resources/Crops.unity3d?WheatHDStage2
#@modfolder:Resources/Crops.unity3d?WheatHDStage3Harvest

#@modfolder:Resources/Instructions.unity3d?OilDistillationColumnInstructions
#@modfolder:Resources/Instructions.unity3d?OilMolderInstructions
#@modfolder:Resources/Instructions.unity3d?OilRecyclerInstructions
#@modfolder:Resources/Instructions.unity3d?OilRetortInstructions

#@modfolder:Resources/Materials.unity3d?BlackBearMaterial
#@modfolder:Resources/Materials.unity3d?BossZombieBlueFlareMaterial
#@modfolder:Resources/Materials.unity3d?BossZombieHawaiianBomberMaterial
#@modfolder:Resources/Materials.unity3d?BossZombieWolfieEyeMaterial
#@modfolder:Resources/Materials.unity3d?BossZombieWolfieHairMaterial
#@modfolder:Resources/Materials.unity3d?BossZombieWolfieMaterial
#@modfolder:Resources/Materials.unity3d?ChickMaterial
#@modfolder:Resources/Materials.unity3d?FoxHairMaterial
#@modfolder:Resources/Materials.unity3d?FoxMaterial
#@modfolder:Resources/Materials.unity3d?PinkWolfMaterial
#@modfolder:Resources/Materials.unity3d?RoosterMaterial
#@modfolder:Resources/Materials.unity3d?ZombieArleneBurntMaterial
#@modfolder:Resources/Materials.unity3d?ZombieArleneFrozenMaterial
#@modfolder:Resources/Materials.unity3d?ZombieBikerFrozenMaterial
#@modfolder:Resources/Materials.unity3d?ZombieBoeFrozenMaterial
#@modfolder:Resources/Materials.unity3d?ZombieDarleneFrozenMaterial
#@modfolder:Resources/Materials.unity3d?ZombieSpiderFrozenMaterial

#@modfolder:Resources/Particles.unity3d?Corrosive
#@modfolder:Resources/Particles.unity3d?Meteor
#@modfolder:Resources/Particles.unity3d?SmallFiresParticleSystem

#@modfolder:Resources/Props.unity3d?Beehive
#@modfolder:Resources/Props.unity3d?Coop
#@modfolder:Resources/Props.unity3d?FishTank
#@modfolder:Resources/Props.unity3d?FishTankGrowing
#@modfolder:Resources/Props.unity3d?FishTankReady
#@modfolder:Resources/Props.unity3d?GroceryBag
#@modfolder:Resources/Props.unity3d?IngotMoldEmpty
#@modfolder:Resources/Props.unity3d?IngotMoldProcessing
#@modfolder:Resources/Props.unity3d?IngotMoldReady
#@modfolder:Resources/Props.unity3d?PlankPile
#@modfolder:Resources/Props.unity3d?ScrapPlankPile
#@modfolder:Resources/Props.unity3d?Stick01
#@modfolder:Resources/Props.unity3d?Stick02
#@modfolder:Resources/Props.unity3d?Stick03
#@modfolder:Resources/Props.unity3d?Stick04
#@modfolder:Resources/Props.unity3d?Stick05
#@modfolder:Resources/Props.unity3d?Stick06
#@modfolder:Resources/Props.unity3d?Stick07
#@modfolder:Resources/Props.unity3d?Stick08
#@modfolder:Resources/Props.unity3d?WoodLog

#@modfolder:Resources/Sounds.unity3d?FoxHowl1
#@modfolder:Resources/Sounds.unity3d?FoxHowl2
#@modfolder:Resources/Sounds.unity3d?FoxHowlDistant
#@modfolder:Resources/Sounds.unity3d?FoxPain1
#@modfolder:Resources/Sounds.unity3d?FoxPain2
#@modfolder:Resources/Sounds.unity3d?FoxPain3
#@modfolder:Resources/Sounds.unity3d?FoxSniff1
#@modfolder:Resources/Sounds.unity3d?PantsFart1
#@modfolder:Resources/Sounds.unity3d?PantsFart2
#@modfolder:Resources/Sounds.unity3d?PantsFart3
#@modfolder:Resources/Sounds.unity3d?PantsFart4
#@modfolder:Resources/Sounds.unity3d?PantsFart5
#@modfolder:Resources/Sounds.unity3d?PantsFart6
#@modfolder:Resources/Sounds.unity3d?Rummage1
#@modfolder:Resources/Sounds.unity3d?SoiledFart1
#@modfolder:Resources/Sounds.unity3d?SoiledFart2
#@modfolder:Resources/Sounds.unity3d?Swoosh1
#@modfolder:Resources/Sounds.unity3d?ToiletFart1
#@modfolder:Resources/Sounds.unity3d?ToiletFart2
#@modfolder:Resources/Sounds.unity3d?ToiletFart3
#@modfolder:Resources/Sounds.unity3d?ToiletFart4
#@modfolder:Resources/Sounds.unity3d?ToiletFart5
#@modfolder:Resources/Sounds.unity3d?ToiletFart6

#@modfolder:Resources/Traps.unity3d?CoalPit1
#@modfolder:Resources/Traps.unity3d?CoalPit2
#@modfolder:Resources/Traps.unity3d?CoalPit3
#@modfolder:Resources/Traps.unity3d?SteelBarbedWire

#@modfolder:Resources/Trees.unity3d?AppleTree0
#@modfolder:Resources/Trees.unity3d?AppleTree1
#@modfolder:Resources/Trees.unity3d?AppleTree2
#@modfolder:Resources/Trees.unity3d?AppleTree3
#@modfolder:Resources/Trees.unity3d?AppleTree4
#@modfolder:Resources/Trees.unity3d?PearTree0
#@modfolder:Resources/Trees.unity3d?PearTree1
#@modfolder:Resources/Trees.unity3d?PearTree2
#@modfolder:Resources/Trees.unity3d?PearTree3
#@modfolder:Resources/Trees.unity3d?PearTree4

#@modfolder:Resources/Workstations.unity3d?BeltSander
#@modfolder:Resources/Workstations.unity3d?CoffeeTeaSet
#@modfolder:Resources/Workstations.unity3d?Crusher
#@modfolder:Resources/Workstations.unity3d?Firepit
#@modfolder:Resources/Workstations.unity3d?Forge
#@modfolder:Resources/Workstations.unity3d?GlassFurnace
#@modfolder:Resources/Workstations.unity3d?Kiln
#@modfolder:Resources/Workstations.unity3d?KitchenWorktable
#@modfolder:Resources/Workstations.unity3d?Lathe
#@modfolder:Resources/Workstations.unity3d?Loom
#@modfolder:Resources/Workstations.unity3d?MoldTable
#@modfolder:Resources/Workstations.unity3d?PrimitiveCementMixer
#@modfolder:Resources/Workstations.unity3d?SewingMachine
#@modfolder:Resources/Workstations.unity3d?SmithingTable
#@modfolder:Resources/Workstations.unity3d?SoakingBasin
#@modfolder:Resources/Workstations.unity3d?SolderBench
#@modfolder:Resources/Workstations.unity3d?TanningRack

#@modfolder:Resources/WorkstationsPower.unity3d?ArcFurnacePowerRelay
#@modfolder:Resources/WorkstationsPower.unity3d?BeltSanderPowerBank
#@modfolder:Resources/WorkstationsPower.unity3d?BlastFurnacePowerBank
#@modfolder:Resources/WorkstationsPower.unity3d?CementMixerPowerSupply
#@modfolder:Resources/WorkstationsPower.unity3d?CrusherPowerBank
#@modfolder:Resources/WorkstationsPower.unity3d?FumeHood
#@modfolder:Resources/WorkstationsPower.unity3d?LathePowerBank
#@modfolder:Resources/WorkstationsPower.unity3d?OilPowerSupply
#@modfolder:Resources/WorkstationsPower.unity3d?PurificationPlantPowerRelay
#@modfolder:Resources/WorkstationsPower.unity3d?SewingTable
#@modfolder:Resources/WorkstationsPower.unity3d?SolderBenchPowerBank
#@modfolder:Resources/WorkstationsPower.unity3d?TableSawPowerBank


Block upgrade paths
Flagstone (500) -> Cobblestone (1500) -> Concrete (5000) -> High Strength Concrete (10000) -> Steel-Plated Concrete (20000)
Primitive Brick (800) -> Brick (2000) -> Concrete (5000) -> High Strength Concrete (10000) -> Steel-Plated Concrete (20000)
Frame (50) -> Wood (500) -> Reinforced Wood (1500) -> Braced Timber (3500) -> NFPC Composite (10000) -> Lead Reinforce NFPC (20000)
Scrap Frame (250) -> Crude Iron (1000) -> Forged Iron (3000) -> Crude Steel (6000) -> Steel (10000) -> Reinforced Steel