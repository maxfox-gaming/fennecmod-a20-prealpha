﻿using System.Reflection;

namespace FennecBase.Harmony
{
    /**
     * Harmony initialiser to patch all methods when the game loads.
     */

    public class Init : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            Log.Out("Loading Patch: " + GetType());

            HarmonyLib.Harmony harmony = new HarmonyLib.Harmony(GetType().ToString());
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }
}