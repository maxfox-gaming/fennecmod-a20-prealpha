﻿using System;
using System.Xml;
using System.Reflection;
using HarmonyLib;

namespace FennecBase.Harmony
{
    /**
     * Harmony patch that adds new XML patching operations to the currently existing ones for the default modding framework.
     */

    [HarmonyPatch]
    public class XMLPatchExtension
    {
        private static MethodBase TargetMethod()
        {
            return typeof(XmlPatcher).GetMethod("singlePatch", BindingFlags.Static | BindingFlags.NonPublic);
        }
        public static bool Prefix(XmlFile _targetFile, XmlElement _patchElement, string _patchName, ref bool __result)
        {
            __result = false;
            string name = _patchElement.Name.ToLower();
            if (!_patchElement.HasAttribute("xpath"))
            {
                throw new Exception("XML.Patch (" + _patchElement.GetXPath() + "): Patch element does not have an 'xpath' attribute");
            }
            string attribute = _patchElement.GetAttribute("xpath");

            switch (name)
            {
                case "removeattributepartial":
                    __result = _targetFile.RemoveAttributePartialByXPath(attribute, _patchElement, _patchName) > 0;
                    return false;
                case "removetagsfromattribute":
                    __result = _targetFile.RemoveTagsFromAttrubuteByXPath(attribute, _patchElement, _patchName) > 0;
                    return false;
                case "attributemath":
                    __result = _targetFile.AttrubuteMathByXPath(attribute, _patchElement, _patchName) > 0;
                    return false;
                case "insertafteriterate":
                    __result = _targetFile.InsertAfterIterateByXPath(attribute, _patchElement, _patchName) > 0;
                    return false;
                default:
                    return true;
            }
        }
    }
}