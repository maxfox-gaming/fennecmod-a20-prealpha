using System;
using System.Collections.Generic;
using UnityEngine;

/**
 * Class to contain methods for manipulations needed in this modlet.
 */

public static class Helper
{
    /**
     * Reads a comma separated string into a list.
     */

    public static List<string> WriteStringToList(this string _s, char separator = ',')
    {
        if (_s == null)
        {
            return new List<string>() { "" };
        }

        if (!_s.Contains(separator.ToString()))
        {
            return new List<string>() { _s.Trim() };
        }

        string[] strings = _s.Split(separator);
        List<string> list = new List<string>();
        foreach (string str in strings)
        {
            list.Add(str.Trim());
        }
        return list;
    }


    /**
     * Writes a list to a comma separated string.
     */

    public static string WriteListToString(this List<string> _list, string join = ",")
    {
        if (_list == null)
        {
            return "";
        }

        switch (_list.Count)
        {
            case 0:
                return "";
            case 1:
                return _list[0];
            default:
                return String.Join(join, _list);
        }
    }


    /**
     * String extension method
     */

    public static Iterator GetIterator(this string str)
    {
        return new Iterator(str);
    }


    /**
     * This makes any list a certain length.
     * If the length is shorter, the input will be trimmed down.
     * If the length is longer, the {copyPos} entry will be copied until the input length is desired.
     */

    public static List<T> MakeListLength<T>(this List<T> input, int length, int copyPos = 0)
    {
        if (input.Count == length)
        {
            return input;
        }

        if (copyPos >= input.Count)
        {
            copyPos -= 1;
            MakeListLength<T>(input, length, copyPos);
        }

        if (copyPos < 0)
        {
            copyPos = 0;
            MakeListLength<T>(input, length, copyPos);
        }

        if (input.Count > length)
        {
            List<T> output = new List<T>();
            for (int i = 0; i < length; i += 1)
            {
                output[i] = input[i];
            }
            return output;
        }

        T copyItem = input[copyPos];
        int inputLength = input.Count;
        int toAdd = Mathf.Abs(length - inputLength);

        for (int i = 0; i < toAdd; i += 1)
        {
            input.Add(copyItem);
        }

        return input;
    }


    /**
     * Condenses a list down if it has same values.
     */

    public static List<T> Condense<T>(this List<T> list)
    {
        List<T> output = new List<T>();
        foreach (T value in list)
        {
            if (!output.Contains(value))
            {
                output.Add(value);
            }
        }
        return output;
    }

}