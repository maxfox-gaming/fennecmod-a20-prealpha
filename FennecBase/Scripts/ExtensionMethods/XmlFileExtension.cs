﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;

/**
 * Adds new parsing methods to be used when reading XML files.
 */

public static class XmlFileExtension
{
	/**
	 * Removes partial text from an attribute.
	 * <removeattributepartial xpath="" name="" text"" />
	 * xpath:	The xpath to find the element containing the attribute
	 * name:	The name of the attribute to modify.
	 * text:	The text that should be removed.
	 */

	public static int RemoveAttributePartialByXPath(this XmlFile xmlFile, string _xpath, XmlElement _xml, string _patchName)
	{
		if (!_xml.HasAttribute("name"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'name' attribute");
		}
		if (!_xml.HasAttribute("text"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'text' attribute");
		}

		string text			= _xml.GetAttribute("text");
		string attribute	= _xml.GetAttribute("name");

		XmlNodeList xmlNodeList = xmlFile.XmlDoc.SelectNodes(_xpath);
		if (xmlNodeList == null)
		{
			return 0;
		}
		int count = xmlNodeList.Count;
		foreach (object obj in xmlNodeList)
		{
			XmlNode xmlNode = (XmlNode)obj;
			XmlNodeType nodeType = xmlNode.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				throw new Exception("XML.SetAttributeByXPath (" + _xml.GetXPath() + "): Matched node is not an XML element but " + nodeType.ToStringCached<XmlNodeType>());
			}
			XmlElement xmlElement = (XmlElement)xmlNode;
			string attributeText = xmlElement.GetAttribute(attribute);
			if (attributeText == String.Empty)
            {
				throw new Exception("Attribute " + attribute + " is either undefined or has no value.");
            }

			string modifiedText = attributeText.Replace(text, "");

			xmlElement.SetAttribute(attribute, modifiedText);
			if (_patchName != null)
			{
				XmlComment newChild = xmlFile.XmlDoc.CreateComment(string.Concat(new string[]
				{
					"Attribute \"",
					xmlNode.Name,
					"\" partial removal of string \"",
					text,
					"\" by: \"",
					_patchName,
					"\""
				}));
				xmlElement.PrependChild(newChild);
			}
		}
		return count;
	}




	/**
	 * Gets a comma separated list of items to remove from another comma separated list in an attribute.
	 * <removetagsfromattribute xpath="" name="" tags="" />
	 * xpath:	The xpath to find the node
	 * name:	The name of the attribute to affect
	 * tags:	A comma separated list of items to remove from the attribute value
	 */

	public static int RemoveTagsFromAttrubuteByXPath(this XmlFile xmlFile, string _xpath, XmlElement _xml, string _patchName)
	{
		if (!_xml.HasAttribute("name"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'name' attribute");
		}
		if (!_xml.HasAttribute("tags"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'tags' attribute");
		}

		string tags			= _xml.GetAttribute("tags");
		string attribute	= _xml.GetAttribute("name");

		XmlNodeList xmlNodeList = xmlFile.XmlDoc.SelectNodes(_xpath);
		if (xmlNodeList == null)
		{
			return 0;
		}
		int count = xmlNodeList.Count;
		foreach (object obj in xmlNodeList)
		{
			XmlNode xmlNode = (XmlNode)obj;
			XmlNodeType nodeType = xmlNode.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				throw new Exception("XML.SetAttributeByXPath (" + _xml.GetXPath() + "): Matched node is not an XML element but " + nodeType.ToStringCached<XmlNodeType>());
			}
			XmlElement xmlElement = (XmlElement)xmlNode;
			string attributeText = xmlElement.GetAttribute(attribute);
			if (attributeText == String.Empty)
			{
				throw new Exception("Attribute " + attribute + " is either undefined or has no value.");
			}

			List<string> attributeList	= attributeText.WriteStringToList();
			List<string> tagsList		= tags.WriteStringToList(); 
			
			foreach (string tag in tagsList)
            {
				if (attributeList.Contains(tag))
				{
					attributeList.Remove(tag);
				}
            }			

			string modifiedText = attributeList.WriteListToString();

			xmlElement.SetAttribute(attribute, modifiedText);
			if (_patchName != null)
			{
				XmlComment newChild = xmlFile.XmlDoc.CreateComment(string.Concat(new string[]
				{
					"Attribute \"",
					xmlNode.Name,
					"\" removal of tags \"",
					tags,
					"\" by: \"",
					_patchName,
					"\""
				}));
				xmlElement.PrependChild(newChild);
			}
		}
		return count;
	}



	/**
	 * Does a math operation on an attribute and returns the result.
	 * <attributemath xpath="" name="" operation="" value="" int="" />
	 * xpath:		The xpath to the element
	 * name:		The name of the attribute to alter. This value can either be a single number.
	 *				or a comma-separated list of numbers.
	 * operation:	The operation to perform
	 *				add:		Adds the 'value' parameter to the attribute value.
	 *				subtract:	Subtracts the 'value' parameter from the attribvute value.
	 *				multiply:	Multiplies the 'value' parameter by the attribute value.
	 *				divide:		Divides the attribute value by the 'value' parameter.
	 * value:		The value to do the math on. Can either be a single number, or a comma separated
	 *				list of numbers.
	 * min:			If specified, the output will be capped at this minimum value.
	 * max:			If specified, the output will be capped at this maximum value.
	 * int:			Whether to return an integer value or a decimal.
	 * condense:	Whether to condense down any same values into one, i.e. if the resulting output 
	 *				would be count="2,2" whether to turn it to count="2" or leave it.
	 */

	public static int AttrubuteMathByXPath(this XmlFile xmlFile, string _xpath, XmlElement _xml, string _patchName)
	{
		if (!_xml.HasAttribute("name"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'name' attribute");
		}
		if (!_xml.HasAttribute("operation"))
        {
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'operation' attribute");
        }
		if (!_xml.HasAttribute("value"))
        {
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have a 'value' attribute");
        }

		string attribute	= _xml.GetAttribute("name");
		string operation	= _xml.GetAttribute("operation");
		string value		= _xml.GetAttribute("value");
		List<string> values	= value.WriteStringToList();
		bool usingMin		= _xml.HasAttribute("min");
		bool usingMax		= _xml.HasAttribute("max");
		string condense		= (_xml.HasAttribute("condense") ? _xml.GetAttribute("condense") : "false");
		string useInt		= (_xml.HasAttribute("int") ? _xml.GetAttribute("int") : "true");


		if (!operations.Contains(operation.ToLower()))
        {
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'operation' attribute must be of 'add', 'subtract', 'multiply', 'divide'.");
        }

		List<float> floatVals = new List<float>();
		foreach (string val in values)
		{
			float floatVal;
			if (!float.TryParse(val, out floatVal))
			{
				throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'value' attribute could not be parsed as a number or number collection.");
			}
			floatVals.Add(floatVal);
		}

		float minVal = 0f;
		if (usingMin && !float.TryParse(_xml.GetAttribute("min"), out minVal))
        {
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'min' attribute must be a number.");
		}

		float maxVal = 0f;
		if (usingMax && !float.TryParse(_xml.GetAttribute("max"), out maxVal))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'max' attribute must be a number.");
		}

		bool usingInt;
		if (!bool.TryParse(useInt, out usingInt))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'int' attribute must be of 'true', 'false'.");
		}

		bool condenseOutput;
		if (!bool.TryParse(condense, out condenseOutput))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): The 'condense' attribute must be of 'true', 'false'.");
		}


		XmlNodeList xmlNodeList = xmlFile.XmlDoc.SelectNodes(_xpath);
		if (xmlNodeList == null)
		{
			return 0;
		}
		int count = xmlNodeList.Count;
		foreach (object obj in xmlNodeList)
		{
			XmlNode xmlNode = (XmlNode)obj;
			XmlNodeType nodeType = xmlNode.NodeType;
			if (nodeType != XmlNodeType.Element)
			{
				throw new Exception("XML.SetAttributeByXPath (" + _xml.GetXPath() + "): Matched node is not an XML element but " + nodeType.ToStringCached<XmlNodeType>());
			}
			XmlElement xmlElement = (XmlElement)xmlNode;
			string attributeText = xmlElement.GetAttribute(attribute);
			if (attributeText == String.Empty)
			{
				throw new Exception("Attribute " + attribute + " is either undefined or has no value.");
			}

			List<float> floatValsCopy = new List<float>(floatVals);
			List<string> attributeValues = attributeText.WriteStringToList();
			List<float> attributeFloats = new List<float>();

			foreach (string attributeValue in attributeValues)
			{
		        float subject;
				if (!float.TryParse(attributeValue, out subject))
				{
					throw new Exception("Attribute " + attribute + ":\"" + attributeText + "\" could not be parsed as a number.");
				}
				attributeFloats.Add(subject);
			}

			floatValsCopy.MakeListLength(attributeValues.Count);

			List<float> floatCalculated = new List<float>();
			for (int i = 0; i < attributeFloats.Count; i += 1)
            {
				float calculated = attributeFloats[i];
				switch (operation.ToLower())
				{
					case "add":
						calculated += floatValsCopy[i] ;
						break;
					case "subtract":
						calculated -= floatValsCopy[i];
						break;
					case "multiply":
						calculated *= floatValsCopy[i];
						break;
					case "divide":
						if (floatValsCopy[i] == 0)
						{
							throw new Exception("The 'value' attribute " + attribute + " cannot be zero when dividing.");
						}
						calculated /= floatValsCopy[i];
						break;
					default:
						Log.Warning("Operation " + operation + " was not defined.");
						break;
				}

				if (usingMin && calculated < minVal)
				{
					calculated = minVal;
				}
				if (usingMax && calculated > maxVal)
				{
					calculated = maxVal;
				}

				floatCalculated.Add(calculated);
			}

			
			List<string> modifiedValues = new List<string>();
			foreach (float finalValue in floatCalculated)
			{
				string modifiedValue;
				if (usingInt)
				{
					modifiedValue = ((int)finalValue).ToString(CultureInfo.InvariantCulture);
				}
				else
				{
					modifiedValue = finalValue.ToString(CultureInfo.InvariantCulture);
				}
				modifiedValues.Add(modifiedValue);
			}
			
			if (condenseOutput)
            {
				modifiedValues.Condense();
            }

			xmlElement.SetAttribute(attribute, modifiedValues.WriteListToString());
			if (_patchName != null)
			{
				XmlComment newChild = xmlFile.XmlDoc.CreateComment(string.Concat(new string[]
				{
					"Attribute \"",
					xmlNode.Name,
					"\" operation \"",
					operation,
					"\" value \"",
					value,
					"\" by: \"",
					_patchName,
					"\""
				}));
				xmlElement.PrependChild(newChild);
			}
		}
		return count;
	}


	/**
	 * <insertAfterIterate xpath="/path/to/element[@name='this_{X}']" iterator="{X}">
	 *	 <element name="this_{X + 1}" value="that1" />
	 *	 <element name="this_{X + 2}" value="that2" />
	 * </insertAfterIterate>
	 * 
	 * xpath:		The xpath to get to the node you want to append stuff to.
	 *				Note that the XPath must contain the iterator specified in the
	 *				next attribute.
	 * iterator:	Needs to be of the form {X} where X can be replaced by whatever.
	 */

	public static int InsertAfterIterateByXPath(this XmlFile xmlFile, string _xpath, XmlElement _xml, string _patchName = null)
	{
		if (!_xml.HasAttribute("iterator"))
		{
			throw new Exception("XML.Patch (" + _xml.GetXPath() + "): Patch element does not have an 'iterator' attribute");
		}

		string iteratorString = _xml.GetAttribute("iterator");
		Iterator baseIterator = _xpath.GetIterator();
		if (!baseIterator.IteratorStringIs(iteratorString))
        {
			Log.Out("Base iterator string not matching.");
			return 0;
        }

		Dictionary<XmlNode, int> indexes				= new Dictionary<XmlNode, int>(); 
		Dictionary<XmlNode, XmlNode> parentChildLink	= new Dictionary<XmlNode, XmlNode>();
		int count = 1;
		for (int i = 0; i <= count; i += 1)
		{
			string modifiedXPath = baseIterator.FormatIteratorStringToLiteral(count);
			XmlNodeList modifiedNodeList = xmlFile.XmlDoc.SelectNodes(modifiedXPath);
			if (modifiedNodeList == null | modifiedNodeList.Count == 0)
            {
				continue;
            }

			foreach (object obj in modifiedNodeList)
            {
				XmlNode xmlNode = (XmlNode)obj;
				indexes[xmlNode.ParentNode] = count;
				parentChildLink[xmlNode.ParentNode] = xmlNode;
            }
			count += 1;
		}

		List<Tuple<XmlNode, int>> finalNodes = new List<Tuple<XmlNode, int>>();
		foreach (KeyValuePair<XmlNode, int> entry in indexes)
		{
			finalNodes.Add(new Tuple<XmlNode, int> ( parentChildLink[entry.Key], entry.Value));
		}

		int foundNodes = finalNodes.Count;
		for (int i = 0; i < finalNodes.Count; i += 1)
        {
			if (_xml.ChildNodes.Count == 0)
			{
				continue;
			}

			Tuple<XmlNode, int> entry = finalNodes[i];
			for (int j = _xml.ChildNodes.Count - 1; j >= 0; j -= 1)
			{
				XmlNode xmlNode2 = _xml.ChildNodes[j];
				XmlNodeList selected = xmlNode2.SelectNodes("//*");
				foreach (object obj2 in selected)
				{
					XmlNode select = (XmlNode)obj2;
					XmlAttributeCollection attributes = select.Attributes;
					if (attributes.Count == 0)
					{
						continue;
					}
					foreach (XmlAttribute attribute in attributes)
					{
						try
						{
							Iterator subIterator = attribute.Value.GetIterator();
							if (!subIterator.IsChildIteratorOf(baseIterator))
							{
								continue;
							}

							attribute.Value = subIterator.FormatIteratorStringToLiteral(entry.Item2);
						}
						catch (Exception)
						{
							continue;
						}
					}
				}

				XmlNode xmlNode3 = xmlFile.XmlDoc.ImportNode(xmlNode2, true);
				if (_patchName != null && xmlNode2.NodeType == XmlNodeType.Element)
				{
					XmlComment newChild = xmlFile.XmlDoc.CreateComment("Element inserted by: \"" + _patchName + "\"");
					xmlNode3.PrependChild(newChild);
				}

				entry.Item1.ParentNode.InsertAfter(xmlNode3, entry.Item1);
			}
		}

		return foundNodes;
	}



	private static readonly List<string> operations = new List<string>()
	{
		"add",
		"subtract",
		"multiply",
		"divide"
	};

	
}
