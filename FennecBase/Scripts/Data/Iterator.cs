﻿using System;
using System.Text;
using System.Text.RegularExpressions;

public class Iterator
{

    /**
     * Constructor.
     */

    public Iterator(string iteratorString)
    {
        originalString = iteratorString;
        if (!HasCorrectFormat())
        {
            throw new Exception("Iterator is malformed.");
        }
    }


    /**
     * Returns whether this iterator is a child iterator of another iterator.
     */

    public bool IsChildIteratorOf(Iterator iter)
    {
       return (iter.iteratorText == this.iteratorText && iter.IsBaseIterator() && !this.IsBaseIterator());
    }


    /**
     * Returns whether this iterator is a base iterator.
     */

    public bool IsBaseIterator()
    {
        return (!this.hasIncrement);
    }


    /**
     * Returns whether the iterator matches a literal iterator passed in.
     */
    
    public bool IteratorStringIs(string iteratorString)
    {
        return (iteratorString.Replace(" ", "") == iteratorFull);
    }


    /**
     * Returns a literal string representing the position of the iterator.
     */

    public string FormatIteratorStringToLiteral(int index)
    {
        if (!originalString.Contains(iteratorFull))
        {
            return String.Empty;
        }

        return originalString.Replace(iteratorFull, GetPositionAfterIncrement(index).ToString());
    }


    /**
     * When passing in a numeric value, gets the incremented value forward or backward by position.
     */

    public int GetPositionAfterIncrement(int index)
    {
        if (!hasIncrement)
        {
            return index;
        }

        switch (increment)
        {
            case Increment.NONE:
                return index;
            case Increment.FORWARD:
                return index + position;
            case Increment.BACKWARD:
                return index - position;
            default:
                throw new Exception("Increment is of an undefined type.");
        }
    }


    /**
     * Checks that the iterator is of the form {x} or {x + a} or {x - a}
     * x is any string of characters not starting with a number
     * a is an integer value.
     * + and - are representing the literal operators.
     */

    protected bool HasCorrectFormat()
    {
        Regex regex = new Regex(iteratorPattern);
        Match match = regex.Match(originalString);

        if (!match.Success | match.Captures.Count == 0)
        {
            return false;
        }

        iteratorText = match.Groups[1].Captures[0].Value;
        pieces       = originalString.Split(new string[] { iteratorFull }, StringSplitOptions.None);

        hasIncrement = (match.Groups.Count > 3 && match.Groups[2].Captures.Count > 0 && match.Groups[3].Captures.Count > 0);
        if (!hasIncrement)
        {
            increment = Increment.NONE;
            iteratorFull = "{" + iteratorText + "}";
            return true;
        }

        string sign = match.Groups[2].Captures[0].Value;
        switch (sign)
        {
            case "+":
                increment = Increment.FORWARD;
                break;
            case "-":
                increment = Increment.BACKWARD;
                break;
            default:
                throw new Exception("Iterator " + match.Captures[1].Value + " needs to be '+' or '-'.");
        }

        string pos = match.Groups[3].Captures[0].Value;
        if (!int.TryParse(pos, out position))
        {
            throw new Exception("The position " + pos + " must be an integer value.");
        }
        iteratorFull = "{" + iteratorText + sign + pos + "}";
        return true;
    }


    public string originalString;
    public string[] pieces;
    public string iteratorFull;
    public string iteratorText;
    public bool hasIncrement;
    public Increment increment;
    public int position;
    protected string iteratorPattern = @"^.*?\{([^0-9\}\+-]+?[^\}\+-]*?)(?:([\+-])\s*([0-9]+))?\}.*?$";
    
    public enum Increment {
        FORWARD,
        BACKWARD,
        NONE
    }
}
